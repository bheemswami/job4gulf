<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WalkinInterviewDetail extends Model
{
    public $with=['city'];
    function city(){
        return $this->hasOne('App\City','id','city_id');
    }
}
