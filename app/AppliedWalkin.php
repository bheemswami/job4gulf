<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppliedWalkin extends Model
{
    //
    public function walkin(){
        return $this->belongsTo('App\Walkin');
    }
    public function user(){
        return $this->belongsTo('App\User','apply_by');
    }
}
