<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostJob extends Model
{
    
    function employer_info(){
        return $this->hasOne('App\Employer','id','user_id');
    }
    function country(){
        return $this->hasOne('App\Country','id','job_in_country');
    }
    function skills(){
        return $this->hasMany('App\PostJobsKeyword','post_job_id','id');
    }
    function jobs_in_city(){
        return $this->hasMany('App\PostJobsCity','post_job_id','id');
    }
    function industry(){
        return $this->hasOne('App\Industry','id','industry_id');
    }
    function functional_area(){
        return $this->hasOne('App\FunctionalArea','id','functional_area_id');
    }
    function job_plan(){
        return $this->hasOne('App\JobPlan','id','job_type');
    }
    function job_applicants(){
        return $this->hasMany('App\AppliedJob','job_id','id');
    }
    function job_qualifications(){
        return $this->hasMany('App\PostJobsQualification','post_job_id','id');
    }
    function job_specializations(){
        return $this->hasMany('App\PostJobsSpecialization','post_job_id','id');
    }
    function job_nationality(){
        return $this->hasMany('App\PostJobsNationality','post_job_id','id');
    }
    function candidate_country(){
        return $this->hasMany('App\PostJobsCandidateCountry','post_job_id','id');
    }
    function candidate_city(){
        return $this->hasMany('App\PostJobsCandidateCity','post_job_id','id');
    }
}
