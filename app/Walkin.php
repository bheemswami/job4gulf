<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Walkin extends Model
{
    function country(){
        return $this->hasOne('App\Country','id','country_id');
    }
    function industry(){
        return $this->hasOne('App\Industry','id','comp_industry');
    }
    function walkin_interview(){
        return $this->hasMany('App\WalkinInterviewDetail','walkin_id','id');
    }
    function walkin_position(){
        return $this->hasMany('App\WalkinPosition','walkin_id','id');
    }

}
