<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TmpPaypalDetail extends Model
{
    protected $guarded = ['id'];
}
