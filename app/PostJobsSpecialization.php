<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostJobsSpecialization extends Model
{
  protected $guarded = ['id'];
}
