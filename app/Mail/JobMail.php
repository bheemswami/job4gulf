<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\PostJob;
class JobMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $post_job;
    public function __construct(PostJob $post_job)
    {
        $this->post_job=$post_job;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
       return $this->from(config('constants.email_config')['from_email'], config('constants.email_config')['from_name'])
                   ->subject('Job | '.$this->post_job->job_title)
                   ->view('email_templates.current_posted_job')->with(['job_detail'=>$this->post_job]);
    }
}
