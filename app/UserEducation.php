<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class UserEducation extends Model
{
	protected $fillable=[
		'user_id', 'basic_course_id', 'basic_specialization_id', 'basic_comp_year', 'master_course_id', 'master_specialization_id', 'master_comp_year', 'institute', 'status'
	];
    function basic_course(){
        return $this->hasOne('App\Course','id','basic_course_id');
    }
	function basic_specialization(){
        return $this->hasOne('App\Specialization','id','basic_specialization_id');
    }
    function master_course(){
        return $this->hasOne('App\Course','id','master_course_id');
    }
    function master_specialization(){
        return $this->hasOne('App\Specialization','id','master_specialization_id');
    }
}
