<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Industry extends Model
{
	protected $guarded=['id'];
	function job_post(){
		return $this->hasMany('App\PostJob','industry_id','id')->whereStatus(1)->where('expiry_date','>=',date('Y-m-d'));
	}
	function consultant(){
		return $this->hasMany('App\Employer','comp_industry','id')->where('comp_type',1)->where('is_deleted',0)->where('status',1);
	}
	function employer(){
		return $this->hasMany('App\Employer','comp_industry','id')->where('comp_type',2)->where('status',1);
	}
	function walkins(){
		return $this->hasMany('App\Walkin','comp_industry','id')->where('status',1);
	}
}
