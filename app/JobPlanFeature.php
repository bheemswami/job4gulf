<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobPlanFeature extends Model
{
    protected $guarded=['id'];
}