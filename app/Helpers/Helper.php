<?php 
use App\User;
use App\Setting;
use App\PageBanner;
use App\Country;
use App\City;
use App\Industry;
use App\Degree;
use App\Course;
use App\PostJob;
use App\PurchasePlan;
use App\LatestNews;
use App\AppliedJob;
function getRoleId($type){
    $id=0;
    switch($type){
        case 'admin':
            $id= 1;
            break;
        case 'candidate':
            $id= 2;
            break;
        case 'employer':
            $id= 3;
            break;
    }
    return $id;
}

function loggedUserProfile(){
        return User::with(['additional_info','education','country','city'])->where('id',Auth::user()->id)->first();
}
function loggedEmployerProfile(){
    Cache::remember('logged_user', config('constants.CACHING_TIME'), function() {
        return User::with(['employer_info'])->where('id',Auth::user()->id)->first();
    });
    return Cache::get('logged_user');
}
function getSiteSetting(){
    return Setting::first();
}
function getDigitRange($s,$e,$label=null){
    $range=[];
    for($i=$s;$i<=$e;$i++){
        $range[$i]=$i;
    }
    if($label=='year'){
        $range[$s].=' year';
        $range[end($range)].=' + year';
    }
    return $range;
}
function getTimeRange($start, $end, $interval = '30 mins', $format = '12') {
    $startTime = strtotime($start); 
    $endTime   = strtotime($end);
    $returnTimeFormat = ($format == '12') ? 'h:i' : 'G:i:s';

    $current   = time(); 
    $addTime   = strtotime('+'.$interval, $current); 
    $diff      = $addTime - $current;

    $times = array(); 
    while ($startTime < $endTime) { 
        $times[date($returnTimeFormat, $startTime)] = date($returnTimeFormat, $startTime); 
        $startTime += $diff; 
    } 
    //$times[] = date($returnTimeFormat, $startTime); 
    return $times; 
}
function genRandomNum($length){
    $string = '1928374656574839232764126534';
    $string_shuffled = str_shuffle($string);
    $otp = substr($string_shuffled, 1, $length);
    return $otp;
}
function generateRandomString($length = 10){
        $characters = '123456789abcdefg99hijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ9BHEEM2SWAMI';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
}
function genTransactionId($length = 10,$prefix=null){
        $characters = '1234567890TRANSACTIONID0987654321ABCDEFGHIJKLMNOPQRSTUVWXYZ111BHEEMSWAMI9001456808BIKANER1RAJASTHAN334001';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $prefix.$randomString;
}
function encryptVal($val){
    return str_rot13($val);
}
function decryptVal($val){
    return str_rot13($val);
}

function dateDiff($date1=null,$date2=null){
    $date1=($date1==null) ? date_create(date('Y-m-d')) : date_create($date1);
    $date2=($date2==null) ? date_create(date('Y-m-d')) : date_create($date2);
   $diff=date_diff($date1,$date2);
    return $diff;
}
function dateConvert($date=null,$format=null){
    if($date==null)
        return date($format);
    if($format==null)
        return date('Y-m-d',strtotime($date));
    else 
        return date($format,strtotime($date));
}

function timeConvert($time,$format=null){
	if($format==null)
		return date('H:i:s',strtotime($time));
	else 
		return date($format,strtotime($time));
}
 function time_elapsed_string($datetime, $full = false) {
        $now = new DateTime;
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }

function limit_text($text, $limit) {
  if (strlen($text) > $limit) {
        $text = substr($text, 0, $limit) . '...';
  }
  return $text;
}
function limit_words($string, $word_limit)
{
    if (str_word_count($string, 0) > $word_limit) {
        $words = explode(" ",$string);
        return implode(" ",array_splice($words,0,$word_limit)).'...';
    }
    return $string;
}

function checkFile($filename,$path,$default=null) {
    $src=url('public/images/default/'.$default);
    $path='public/'.$path;
    if($filename != NULL && $filename !='' && $filename != '0')
    {
        $file_path = app()->basePath($path.$filename);
        if(File::exists($file_path)){
            $src=url($path.$filename);
        } 
    }
    return $src;      
}


function getLatLong($address){
        $prepAddr = str_replace(' ','+',$address);
        $geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false');
        $output= json_decode($geocode);
        if(!empty($output->results))
            return ['lat'=>$output->results[0]->geometry->location->lat,'long'=>$output->results[0]->geometry->location->lng];

        return ['lat'=>'','long'=>''];
}

function timeFormatAmPm($time=null){
    if($time==null || $time==''){
        return '';
    }
    $exp = explode(' ', $time);
    $temp = date_parse($exp[0]);
    $temp['minute'] = str_pad($temp['minute'], 2, '0', STR_PAD_LEFT);
    return date('h:i a', strtotime($temp['hour'] . ':' . $temp['minute']));
}

function unlinkImg($img,$path) {
    if($img !=null || $img !='')
    {
        $path='public/'.$path.'/';
        $image_path = app()->basePath($path.$img);
        $image_path2 = app()->basePath($path.'thumbnail/'.$img);
        if(File::exists($image_path)){
            unlink($image_path);
            unlink($image_path2);
        }
    }       
}


function getPlanFeatureData($job_plan){
        $plan_feature_ids=[];
        $plan_features=['days_of_live_job'=>0,'plan_validity_days'=>0,'straight_to_inbox'=>0,'job_alert_emails'=>0,'job_email_upto'=>0,'unlimited_space'=>0,'use_email_ids'=>0,'job_refreshed'=>0,'greater_visibility'=>0,'share_jobs'=>0,'bulk_downloads'=>0,'manage_response'=>0,'redirect_to_website'=>0,'job_promote'=>0];
        if($job_plan->features!=null){
            foreach($job_plan->features as $val){
                $plan_feature_ids[]=$val->feature_id;
                if($val->feature_id==1){
                    $plan_features['basic_job_live']=$val->units;
                } else if($val->feature_id==2){
                    $plan_features['premium_job_live']=$val->units;
                } else if($val->feature_id==3){
                    $plan_features['plan_validity_days']=$val->units;
                } else if($val->feature_id==4){
                    $plan_features['straight_to_inbox']=1;
                } else if($val->feature_id==5){
                    $plan_features['job_alert_emails']=1;
                } else if($val->feature_id==6){
                    $plan_features['job_email_upto']=$val->units;
                } else if($val->feature_id==7){
                    $plan_features['unlimited_space']=1;
                } else if($val->feature_id==8){
                    $plan_features['use_email_ids']=$val->units;
                } else if($val->feature_id==9){
                    $plan_features['job_refreshed']=1;
                } else if($val->feature_id==10){
                    $plan_features['greater_visibility']=1;
                } else if($val->feature_id==11){
                    $plan_features['job_promote']=1;
                } else if($val->feature_id==12){
                    $plan_features['share_jobs']=1;
                } else if($val->feature_id==13){
                    $plan_features['bulk_downloads']=1;
                } else if($val->feature_id==14){
                    $plan_features['manage_response']=1;
                } else if($val->feature_id==15){
                    $plan_features['redirect_to_website']=1;
                }
            }
        }
        $plan_features['plan_feature_ids']=join($plan_feature_ids,',');
        return $plan_features;
    }
function planPurchasedOrNot($plan_id,$user_id=0){
    if($user_id==0)
        $user_id=Auth::user()->id;
    return PurchasePlan::where('user_id',$user_id)->where('plan_id',$plan_id)->first();
}
function userPurchasePlans($type=null){
    if($type=='active_plan'){
        return PurchasePlan::with('job_plan')->whereStatus(1)->where('user_id',Auth::user()->id)->where('plan_start_date','<=',date('Y-m-d'))->where('plan_end_date','>=',date('Y-m-d'))->first();
    }
    return PurchasePlan::with('job_plan')->where('user_id',Auth::user()->id)->orderBy('plan_id','DESC')->get();
}
function getPlanStatusLabel($start_date,$end_date,$status,$type='is_admin'){
    if($type=='is_admin'){
        if(strtotime(date('Y-m-d')) < strtotime($start_date)) {
            return '<span class="text-warning">Upcoming</span>';
        } else if(strtotime(date('Y-m-d')) > strtotime($end_date)) {
            return '<span class="text-danger">Expired</span>';
        } else if($status==0) {
            return '<span class="text-danger">De-Activate</span>';
        } else { 
            return '<span class="text-success">Active</span>';
        }
    } else {
        return '';
    }
}
function getCountryCodes(){
    return Country::select('phonecode',DB::raw('CONCAT(sortname, " (+", phonecode,")") AS codecountry'))->where('status',1)->where('phonecode','!=','')->orderBy('sortname','ASC')->pluck('codecountry','phonecode');
}
function getCountryPluck($is_gulf=0){
    return Country::where('status',1)->where('is_gulf',$is_gulf)->pluck('name','id');
}

function getCityPluck(){
    return City::where('status',1)->pluck('name','id');
}
function getDegreePluck(){
    return Degree::where('status',1)->pluck('name','id');
}
function getCoursePluck(){
    return Course::where('status',1)->pluck('name','id');
}

function getCityNames($data_list){
    $names='';
    if($data_list!=null) {                    
         foreach($data_list as $value){
           $names.= $value->city->name.', ';
         }
    }
    return rtrim($names,', ');
}
function getAllCountries(){
    Cache::remember('allCountryList', config('constants.CACHING_TIME'), function() {
        return Country::where('status',1)->get();
    });    
    return Cache::get('allCountryList');
}
function getGulfCountries(){
    Cache::remember('allGulfCountry', config('constants.CACHING_TIME'), function() {
        return Country::where('status',1)->where('is_gulf',1)->orderBy('name','ASC')->pluck('name','id')->toArray();
    });    
    return Cache::get('allGulfCountry');
}

function getOtherCountries(){
    Cache::remember('allOtherCountry', config('constants.CACHING_TIME'), function() {
        return Country::where('status',1)->where('is_gulf',0)->orderBy('name','ASC')->pluck('name','id')->toArray();
    });    
    return Cache::get('allOtherCountry');
}

function getSettings(){
    Cache::remember('settingData', config('constants.CACHING_TIME'), function() {
        return Setting::first();
    });
    return Cache::get('settingData');
}
function getPageBanner($route_path=null,$segment=null){
    if($segment=='walkins-detail'){
        return PageBanner::where('id',6)->first();
    }
     return PageBanner::where('route_path',$route_path)->first();
}

function getFooterData($flag){
    if($flag='country'){
        Cache::remember('countryList', config('constants.CACHING_TIME'), function() {
            return Country::limit(6)->get();
        });
        return Cache::get('countryList');
    } else if($flag='industry'){
        Cache::remember('industryList', config('constants.CACHING_TIME'), function() {
            return Industry::limit(6)->get();
        });
        return Cache::get('industryList');
    }
    
}

function getPremiumJobs(){
    return PostJob::with('employer_info','industry','jobs_in_city')->where('expiry_date','>=',date('Y-m-d'))->whereIn('job_type',[2,3])->whereStatus(1)->limit(8)->orderBy('id','DESC')->get();
}
function getRecentJobs($job_ids=null){
    return PostJob::with('employer_info','industry','jobs_in_city')->where('expiry_date','>=',date('Y-m-d'))->whereNotIn('id',$job_ids)->whereStatus(1)->limit(8)->orderBy('id','DESC')->get();
}
function getRelatedJobs($job_id=0){
    $job = PostJob::where('id',$job_id)->first();

    $jobList = PostJob::with('employer_info','industry','jobs_in_city')->where('expiry_date','>=',date('Y-m-d'))->where('id','!=',$job_id)->whereStatus(1)->limit(6)->orderBy('id','DESC');
    $key_skills = explode(',',$job->key_skill);
    
    if($job->key_skill && count($key_skills)>0){
            foreach($key_skills as $k=>$val){
                 if($k==0) $jobList->whereRaw('FIND_IN_SET("'.$val.'",key_skill)');
                 else $jobList->orWhereRaw('FIND_IN_SET("'.$val.'",key_skill)');
            }   
        }
    return $jobList->get();
}

function getLatestNews($limit=5){
    return LatestNews::whereStatus(1)->limit($limit)->orderBy('id','DESC')->get();
}
function jobAppliedOrNot($job_id){
    return AppliedJob::where('apply_by',Auth::user()->id)->where('job_id',$job_id)->count();
}
