<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostJobsCandidateCity extends Model
{
    protected $guarded = ['id'];
}
