<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WalkinPosition extends Model
{
    public $with=['designation'];
    function designation(){
        return $this->hasOne('App\Designation','id','designation_id');
    }
}
