<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppliedJob extends Model
{
    function jobs(){
    	return $this->hasOne('App\PostJob','id','job_id');
    }
    function user(){
	 	return $this->hasOne('App\User','id','apply_by')->with('additional_info');
	}
	function user_education(){
        return $this->hasOne('App\UserEducation','user_id','apply_by')->with(['basic_course','basic_specialization','master_course','master_specialization']);
    }
}
