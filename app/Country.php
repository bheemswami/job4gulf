<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Country extends Model
{
	protected $guarded=['id'];
	function cities(){
		return $this->hasMany('App\City','country_id','id')->orderBy('name','ASC');
	}
	function job_post(){
		return $this->hasMany('App\PostJob','job_in_country','id')->whereStatus(1)->where('expiry_date','>=',date('Y-m-d'));
	}
	function consultant(){
		return $this->hasMany('App\Employer','comp_country','id')->where('comp_type',1)->where('is_deleted',0)->where('status',1);
	}
	function employer(){
		return $this->hasMany('App\Employer','comp_country','id')->where('comp_type',2)->where('status',1);
	}
}
