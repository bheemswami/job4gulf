<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable
{
    use Notifiable;
    use EntrustUserTrait;

    protected $guarded=['id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $with=['city:id,country_id,name'];
    function employer_info(){
        return $this->hasOne('App\Employer','user_id','id')->with(['industry:id,name','country:id,name,phonecode','city:id,name','designations:id,name']);
    }

    function additional_info(){
        return $this->hasOne('App\UserAdditionalInfo','user_id','id')->with(['industry','function_area','company']);
    }
    function education(){
        return $this->hasOne('App\UserEducation','user_id','id')->with(['basic_course','basic_specialization','master_course','master_specialization']);
    }
    function country(){
        return $this->hasOne('App\Country','id','country_id');
    }
    function city(){
        return $this->hasOne('App\City','id','city_id');
    }
}
