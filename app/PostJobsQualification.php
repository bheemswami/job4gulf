<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostJobsQualification extends Model
{
  protected $guarded = ['id'];
}
