<?php

namespace App\Http\Middleware;

use Closure;

class CheckUserSession
{

    public function handle($request, Closure $next)
    {
        if (!$request->session()->exists('auth')) {
            // user value cannot be found in session
            return redirect('/admin/');
        }

        return $next($request);
    }

}