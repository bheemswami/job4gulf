<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Coupon;
class CouponController extends Controller
{
    private $core;
    public $data=[];
    public function __construct()
    {
        $this->middleware('usersession');
        $this->core=app(\App\Http\Controllers\CoreController::class);
    }
    public function index()
    {
        $this->data['coupons']=Coupon::where('status', 1)->get();
        return view('admin::coupon.coupon_list',$this->data);
    }
    public function addCoupon(){
        return view('admin::coupon.add_edit_coupon');
    }
    public function updateCoupon(Request $request){
        $this->data['coupon_data']=Coupon::whereId($request->id)->first();
        return view('admin::coupon.add_edit_coupon',$this->data);
    }
    public function saveCoupon(Request $request) {
        if($request->id>0){
            $success = config('constants.FLASH_REC_UPDATE_1');
            $error = config('constants.FLASH_REC_UPDATE_0');
        } else {
            $success = config('constants.FLASH_REC_ADD_1');
            $error = config('constants.FLASH_REC_ADD_0');
        }

        if(Coupon::updateOrCreate(['id'=>$request->id],$request->except(['_token']))){
            return redirect()->route('coupon-list')->with(['success' => $success]);
        }
        return redirect()->route('coupon-list')->with(['error' => $error]);
    }
    public function deleteCoupon(Request $request){
        if(Coupon::whereId($request->id)->update(['status'=>0])){
            return redirect()->back()->with(['success' => config('constants.FLASH_REC_DELETE_1')]);
        }
        return redirect()->back()->with(['error' => config('constants.FLASH_REC_DELETE_0')]);
    }
}
