<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Auth,Hash,DB;
use Session,Validator;
use App\Role;
use App\User,App\RoleUser;

class LoginController extends Controller
{
    public function index(Request $request){
        return view('admin_panel.login');
    }
    public function loginProcess(Request $request){
        if(Auth::attempt(['email'=>$request->email,'password'=>$request->password])){
            if(Auth::user()->hasRole('admin')){
                return redirect()->route('dashboard')->with(['success' => config('constants.FLASH_SUCCESS_LOGIN')]);;
            }
        }
        return redirect()->back()->with(['error' => config('constants.FLASH_INVALID_CREDENTIAL')]);
    }
    public function logout(Request $request){
    	Auth::logout();
    	return redirect()->route('login')->with(['error' => config('constants.FLASH_SUCCESS_LOGOUT')]);
    }
}
