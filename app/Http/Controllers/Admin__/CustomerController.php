<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Hash;
use App\User,App\UserAddress;
class CustomerController extends Controller
{
    private $core;
    public $data=[];
    public function __construct()
    {
        $this->middleware('usersession');
        $this->core=app(\App\Http\Controllers\CoreController::class);
        $this->core->adminSessionCheck();
    }
    public function index()
    {
        $this->data['customers']=User::where('status', '!=' ,2)->get();
        return view('admin::customer.customer_list',$this->data);
    }
    public function addCustomer(){
        return view('admin::customer.add_edit_customer');
    }
    public function updateCustomer(Request $request){
        $this->data['customer_data']=User::whereId($request->id)->first();
        return view('admin::customer.add_edit_customer',$this->data);
    }
    public function viewCustomer(Request $request){
        $this->data['customer_data']=User::whereId($request->id)->first();
        return view('admin::customer.customer_details',$this->data);
    }
    public function saveCustomer(Request $request) {
        if(emailExistUsers($request->email,$request->id)){
            return redirect()->back()->with(['error' => config('constants.FLASH_EMAIL_ADDRESS_EXIST').' ('.$request->email.')']);
        }

        if($request->id>0){
            $success = config('constants.FLASH_REC_UPDATE_1');
            $error = config('constants.FLASH_REC_UPDATE_0');
            if($request->new_password!='')
                $request->merge(['password'=>Hash::make($request->new_password)]);
        } else {
            $success = config('constants.FLASH_REC_ADD_1');
            $error = config('constants.FLASH_REC_ADD_0');
            $request->merge(['password'=>Hash::make($request->new_password)]);
        }
       
      
        if($request->hasFile('img')){
            $filename=$this->core->fileUpload($request->file('img'),'uploads/user_img');
            $request->merge(['image'=>$filename]);
        }

        $savedRes = User::updateOrCreate(['id'=>$request->id],$request->except(['_token','img','new_password','confirm_password','address']));
        if($savedRes){
            if(isset($request->address_ids)){
                UserAddress::where('user_id',$savedRes->id)->whereNotIn('id',$request->address_ids)->delete();
            } else {
                UserAddress::where('user_id',$savedRes->id)->delete();
            }
            if(!empty($request->address) && $request->address[0]!=null){
                $userAddress=[];
                foreach($request->address as $val){
                    $latLong=getLatLong($val);
                    $userAddress[]=['user_id'=>$savedRes->id,'address_line_1'=>$val,'lat'=>$latLong['lat'],'long'=>$latLong['long']];
                }
                UserAddress::insert($userAddress);
            }               
            return redirect()->route('customer-list')->with(['success' => $success]);
        }
        return redirect()->route('customer-list')->with(['error' => $error]);
    }
    public function deleteCustomer(Request $request){
        if(User::whereId($request->id)->update(['status'=>2])){
            return redirect()->back()->with(['success' => config('constants.FLASH_REC_DELETE_1')]);
        }
        return redirect()->back()->with(['error' => config('constants.FLASH_REC_DELETE_0')]);
    }

     public function listCustomer(Request $request)
    {
        if(isset($request->all()['query']) && $request->all()['query']!=''){

                $list = User::where('first_name','like','%'.$request->all()['query'].'%')->orWhere('phone','like','%'.$request->all()['query'].'%')->select('id as value','first_name as text','phone')->get();
        } else {
            $list = User::select('id as value','first_name as text')->get();
        }
        return response()->json($list);
    }

}
