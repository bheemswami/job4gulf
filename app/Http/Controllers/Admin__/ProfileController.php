<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Login,Session;
class ProfileController extends Controller
{
    private $data = [];
    public function __construct(){
        $this->middleware('usersession');
        $this->core=app(\App\Http\Controllers\CoreController::class);
    }
    public function index(){
        if(Session::get('auth')){
            return view('admin::profile.profile',$this->data);
        }
        return redirect()->route('logMeOut');
    }
}
