<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Nationals;
use App\Country,App\City,App\State;
use App\User;
use App\Employer;
use App\Company;
use App\Designation;
use App\Industry;
use App\FunctionalArea;
use App\SkillKeyword;
use App\Degree,App\Course;
use App\Specialization;
use App\Institute;
use App\JobPlan;
use App\AppliedJob;
class AjaxController extends Controller
{
    
    public $data=[];
    private $core;
    public function __construct()
    {
       // $this->middleware('auth');
        $this->core=app(\App\Http\Controllers\CoreController::class);
    }

    public function getSkillKeywordList(){
       return response()->json(SkillKeyword::where('status',1)->where('name','like','%'.$_GET['q'].'%')->select('id','name')->get());
    }
    public function getNationalityList(Request $request)
    {
       return response()->json(Nationals::where('status',1)->where('name','like','%'.$_GET['q'].'%')->select('id','name')->get());
    }
    public function getStateList(Request $request)
    {
        //dd($request->all());
        if($request->country_id>0){
            $cities=State::where('status',1)->where('country_id',$request->country_id)->orderBy('name','ASC')->select('id','name')->get();
        } else {
            $cities=State::where('status',1)->where('name','like','%'.$_GET['q'].'%')->orderBy('name','ASC')->select('id','name')->get();
        }
        return response()->json($cities);
    }
    public function getCityList(Request $request)
    {   
        if($request->country_id>0 && $request->by_country==1){
            $cities=City::where('status',1)->where('country_id',$request->country_id)->orderBy('name','ASC')->select('id','name')->get();
        } else {
            $cities=City::where('status',1)->where('country_id',$request->country_id)->where('name','like','%'.$_GET['q'].'%')->orderBy('name','ASC')->select('id','name')->get();
        }
        return response()->json($cities);
    }

    public function getGulfCityList(Request $request)
    {   
        if($request->country_id>0){
            $cities=City::where('is_gulf',1)->where('status',1)->where('country_id',$request->country_id)->orderBy('name','ASC')->select('id','name')->get();
        } else {
            $cities=City::where('is_gulf',1)->where('status',1)->where('name','like','%'.$_GET['q'].'%')->orderBy('name','ASC')->select('id','name')->get();
        }
        return response()->json($cities);
    }
    public function getCoumpanyList(Request $request){
        return response()->json(Company::where('status',1)->where('name','like','%'.$_GET['q'].'%')->orderBy('name','ASC')->select('id','name')->get());
    }

    public function getInstituteList(Request $request){
        return response()->json(Institute::where('status',1)->where('name','like','%'.$_GET['q'].'%')->orderBy('name','ASC')->select('id','name')->get());
    }

    public function getDesignationList(Request $request){
        return response()->json(Designation::where('status',1)->where('name','like','%'.$_GET['q'].'%')->orderBy('name','ASC')->select('id','name')->get());
    }

    public function getFunctionalAreaList(Request $request){
        return response()->json(FunctionalArea::where('status',1)->where('name','like','%'.$_GET['q'].'%')->orderBy('name','ASC')->select('id','name')->get());
    }

    public function getIndustryList(Request $request){
        return response()->json(Industry::where('status',1)->where('name','like','%'.$_GET['q'].'%')->orderBy('name','ASC')->select('id','name')->get());
    }

    public function getCourseList(Request $request)
    {
        $data=[];
        if($request->degree_id>0){
            $data=Course::where('status',1)->where('degree_id',$request->degree_id)->orderBy('name','ASC')->select('id','name')->get();
        } 
        return response()->json($data);
    }

    

    public function getStateListByCountry(Request $request)
    {
        $cities=[];
        if($request->country_id>0){
            $country_code=Country::where('status',1)->where('id',$request->country_id)->select('id','phonecode')->first();
            $cities=State::where('status',1)->where('country_id',$request->country_id)->select('id','name')->get();
        }
        return response()->json(['cities'=>$cities,'country_code'=>$country_code->phonecode]);
    }

    public function getSpecializationList(Request $request)
    {
        $data=[];
        if(is_array($request->course_ids)){
            $data=Course::with('specialization')->where('status',1)->whereIn('id',$request->course_ids)->orderBy('name','ASC')->select('id','name')->get();
        } else {
            $data=Specialization::where('status',1)->where('course_id',$request->course_id)->orderBy('name','ASC')->select('id','name')->get();
        }
        return response()->json($data);
    }

    public function getSpecialization(Request $request){
         $degreeList=Degree::where('status',1)->with('courses')->orderBy('name','ASC')->get();
        foreach($degreeList as $k=>$val){
            if($val->courses!=null){
                foreach($val->courses as $c){
                $educations[$val->name][$c->id]=$c->name;

                }
            }
        }
        return response()->json($emp);
    }

    public function getFilterdEmployer(Request $request){
    	$emp['list'] = Employer::all();
    	return response()->json($emp);
    }

    public function checkEmailExist(Request $request)
    {
        if($this->core->checkUserExist($request->email,$request->user_id)>0){
            return response()->json(false);
        } 
        return response()->json(true);
    }

    public function cvMoveTo(Request $request)
    {return response()->json(['status'=>$request->all()]); 
        $applied_job =AppliedJob::whereIn('id',$request->ids)->where('job_id',$request->job_id);
        if($applied_job->update(['move_to'=>$request->move_to])){
            return response()->json(['status'=>200]);  
        }
        return response()->json(['status'=>400]);  
    }

// use function in admin section   
 public function getPlanDetailsById(Request $request){
       $planDetails = JobPlan::with('features')->where('id',$request->plan_id)->first();
        foreach($planDetails->features as $k=>$vals){
            $planFeatures[]=['feature_id'=>$vals->feature_id,'units'=>$vals->units,'title'=>str_replace(['##VALUE##'], [$vals->units],config('constants.jobplan_features')[$vals->feature_id]['name'])];
        }
        return response()->json(['planDetails'=>$planDetails,'planFeatures'=>$planFeatures]);
    }
}
