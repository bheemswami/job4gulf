<?php

namespace App\Http\Controllers;
use Auth,DB;
use Illuminate\Http\Request;
use App\User,App\UserAdditionalInfo,App\UserEducation;
use App\Employer;
use App\Country,App\City,App\Nationals;
use App\Industry,App\Company,App\Designation,App\FunctionalArea,App\Degree,App\Institute;
use App\SkillKeyword;
use App\PostJob,App\PostJobsCity;
use App\Course;
use App\Enquiry;
use App\Service;
use App\JobPlan;
class SearchController extends Controller
{
    public $data=[];
    private $core;
    public function __construct()
    {
        $this->core=app(\App\Http\Controllers\CoreController::class);
    }

   public function searchJob(Request $request){
        $countryIds=$industryIds=[];
        $country_search_key=$city_search_key=$industry_search_key='';
       
        $jobs=PostJob::with('employer_info','industry')->orderBy('job_type','DESC');
        $jobs->where('status',1);
        $jobs->where('expiry_date','>=',date('Y-m-d'));

        $key_skills = ($request->key_skills!='') ? explode(',',$request->key_skills) : [];        
        if($request->key_skills && count($key_skills)>0){
            foreach($key_skills as $k=>$val){
                 if($k==0) $jobs->whereRaw('FIND_IN_SET("'.$val.'",key_skill)');
                 else $jobs->orWhereRaw('FIND_IN_SET("'.$val.'",key_skill)');
            }  
        }
        if($request->exp_year){
            $jobs->where('min_experience', '>=' ,(int)$request->exp_year);
        }
        if($request->country_ids){            
            $jobs->whereIn('job_in_country',explode(',',$request->country_ids));
            
            $country_search_key = DB::table('countries')->select(DB::raw("group_concat(countries.name) as country" ))->whereIn('id',explode(',',$request->country_ids))->get();
            $country_search_key = $country_search_key[0]->country;
        }
        if($request->city_ids){
            $job_ids=PostJobsCity::whereIn('city_id',explode(',',$request->city_ids))->pluck('post_job_id','id');
            $jobs->whereIn('id',$job_ids);
            
            $city_search_key = DB::table('cities')->select(DB::raw("group_concat(cities.name) as city"))->whereIn('id',explode(',',$request->city_ids))->get();
            $city_search_key = $city_search_key[0]->city;
        }
        if($request->industry_ids){
            $jobs->whereIn('industry_id',explode(',',$request->industry_ids));
            
            $industry_search_key = DB::table('industries')->select(DB::raw("group_concat(industries.name) as industry"))->whereIn('id',explode(',',$request->industry_ids))->get();
            $industry_search_key = $industry_search_key[0]->industry;
        }

        $this->data['jobs']=$jobs->paginate(3)->appends(request()->query());        
        $this->data['search_tag']=$country_search_key.','.$city_search_key.','.$industry_search_key;
        if ($request->ajax()) {
            return view('front_panel.job.job_listing_paginate', $this->data)->render();  
        } else {
            $job_cites=PostJobsCity::where('status',1)->pluck('city_id','id');
            $jobList=PostJob::where('status',1)->where('expiry_date','>=',date('Y-m-d'))->get();
            foreach($jobList as $val){
                $countryIds[]=$val->job_in_country;
                $industryIds[]=$val->industry_id;
            }
            $this->data['country_list']=Country::with('job_post')->where('status',1)->whereIn('id',$countryIds)->orderBy('name','ASC')->get();
            $this->data['city_list']=City::with('active_jobs')->whereIn('id',$job_cites)->orderBy('name','ASC')->get();
            $this->data['industry_list']=Industry::with('job_post')->whereIn('id',$industryIds)->orderBy('name','ASC')->get();
        }
        return view('front_panel.job.search_jobs',$this->data);
    }

    public function searchConsultant(Request $request){
        $countryIds=$industryIds=$cityIds=[];
        $country_search_key=$city_search_key=$industry_search_key='';
        $result = Employer::with(['industry','country','job_post'=>function($q){
            return $q->count();
        }]);

        $result->where('comp_type',1)->where('is_deleted',0)->where('status',1);

        $empList=$result->get();
        if(count($empList)>0){
            foreach($empList as $val){
                $countryIds[]=$val->comp_country;
                $industryIds[]=$val->comp_industry;
                $cityIds[]=$val->comp_city;
            }
        }
        
        if($request->country_ids){            
            $result->whereIn('comp_country',explode(',',$request->country_ids));
            
            $country_search_key = DB::table('countries')->select(DB::raw("group_concat(countries.name) as country" ))->whereIn('id',explode(',',$request->country_ids))->get();
            $country_search_key = $country_search_key[0]->country;
        }
        if($request->city_ids){
            $result->whereIn('comp_city',explode(',',$request->city_ids));
            
            $city_search_key = DB::table('cities')->select(DB::raw("group_concat(cities.name) as city"))->whereIn('id',explode(',',$request->city_ids))->get();
            $city_search_key = $city_search_key[0]->city;
        }
        if($request->industry_ids){
            $result->whereIn('comp_industry',explode(',',$request->industry_ids));
            
            $industry_search_key = DB::table('industries')->select(DB::raw("group_concat(industries.name) as industry"))->whereIn('id',explode(',',$request->industry_ids))->get();
            $industry_search_key = $industry_search_key[0]->industry;
        }

        $this->data['consultants']=$result->paginate(3)->appends(request()->query());
        $this->data['search_tag']=$country_search_key.','.$city_search_key.','.$industry_search_key;
        if ($request->ajax()) {
            return view('front_panel.consultant.consultant_list', $this->data)->render();  
        } else {
            $this->data['country_list']=Country::with('consultant')->where('status',1)->whereIn('id',$countryIds)->orderBy('name','ASC')->get();
            $this->data['city_list']=City::with('consultant')->whereIn('id',$cityIds)->orderBy('name','ASC')->get();
            $this->data['industry_list']=Industry::with('consultant')->whereIn('id',$industryIds)->orderBy('name','ASC')->get();

        }
       
        return view('front_panel.consultant.consultant',$this->data);
    }

    public function topEmployers(Request $request){
        $countryIds=$industryIds=$cityIds=[];
        $country_search_key=$city_search_key=$industry_search_key='';
        $result = Employer::with(['country','job_post'=>function($q){
            return $q->count();
        }]);

        $result->where('comp_type',2)->where('is_deleted',0)->where('status',1);

        $empList=$result->get();
        if(count($empList)>0){
            foreach($empList as $val){
                $countryIds[]=$val->comp_country;
                $industryIds[]=$val->comp_industry;
                $cityIds[]=$val->comp_city;
            }
        }

        if($request->country_ids){            
            $result->whereIn('comp_country',explode(',',$request->country_ids));
            
            $country_search_key = DB::table('countries')->select(DB::raw("group_concat(countries.name) as country" ))->whereIn('id',explode(',',$request->country_ids))->get();
            $country_search_key = $country_search_key[0]->country;
        }
        if($request->city_ids){
            $result->whereIn('comp_city',explode(',',$request->city_ids));
            
            $city_search_key = DB::table('cities')->select(DB::raw("group_concat(cities.name) as city"))->whereIn('id',explode(',',$request->city_ids))->get();
            $city_search_key = $city_search_key[0]->city;
        }
        if($request->industry_ids){
            $result->whereIn('comp_industry',explode(',',$request->industry_ids));
            
            $industry_search_key = DB::table('industries')->select(DB::raw("group_concat(industries.name) as industry"))->whereIn('id',explode(',',$request->industry_ids))->get();
            $industry_search_key = $industry_search_key[0]->industry;
        }
         
        $this->data['top_employers']=$result->paginate(3)->appends(request()->query());
        $this->data['search_tag']=$country_search_key.','.$city_search_key.','.$industry_search_key;
        if ($request->ajax()) {
            return view('front_panel.employer.top_employer_list', $this->data)->render();  
        } else {
            $this->data['country_list']=Country::with('employer')->where('status',1)->whereIn('id',$countryIds)->orderBy('name','ASC')->get();
            $this->data['city_list']=City::with('employer')->whereIn('id',$cityIds)->orderBy('name','ASC')->get();
            $this->data['industry_list']=Industry::with('employer')->whereIn('id',$industryIds)->orderBy('name','ASC')->get();
        }
        return view('front_panel.employer.top_employer',$this->data);
    }

}
