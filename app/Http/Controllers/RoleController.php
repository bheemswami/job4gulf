<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;
use App\Permission;
use DB;
use Auth;
class RoleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function roleList()
    {
        $data['permission']=Permission::get();
        $data['role']=Role::where('active',1)->get();
        return view('entrust_view.manage_roles',$data);
    }
   public function storeRole(Request $request)
    {
        $role = new Role();
        $role->name = $request->role_name;
        $role->display_name = $request->role_name;
        $role->description = $request->description;
        $flag=$role->save();
        foreach ($request->permission as $key => $value) {
            $role->attachPermission($value);
        }
        if($flag)
             return redirect()->back()->with(['success' => config('constants.FLASH_REC_ADD_1')]);
        else
            return redirect()->back()->with(['error' => config('constants.FLASH_REC_ADD_0')]);
    }
    public function editRole($id)
    {
         $data['role']=Role::find($id);
         $data['permission']=Permission::whereIn('id', DB::table('permission_role')->where('role_id',$id)->pluck('permission_id'))->pluck('id');
         $data['all_permission']=Permission::get();
         return view('entrust_view.edit_role',$data);
    }
    public function updateRole(Request $request,$id)
    {
        $role = Role::find($id);
        $role->name=$request->display_name;
        $role->display_name = $request->display_name;
        $role->description = $request->description;
        $flag=$role->save();
        DB::table("permission_role")->where("permission_role.role_id",$id)->delete();
        foreach ($request->permission as $key => $value) {
            $role->attachPermission($value);
        }
        if($flag)
            return redirect()->back()->with(['success' => config('constants.FLASH_REC_UPDATE_1')]);
        else
            return redirect()->back()->with(['error' => config('constants.FLASH_REC_UPDATE_0')]);
    }
    public function deleteRole($id)
    {
        $flag=Role::where('id',$id)->update(['active'=>1]);
        if($flag)
            return redirect()->back()->with(['success' => config('constants.FLASH_REC_DELETE_1')]);
        else
           return redirect()->back()->with(['success' => config('constants.FLASH_REC_DELETE_0')]);
    }
}