<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash,DB,Auth,Session;
use Validator;
use Carbon\Carbon;
use App\User,App\UserAdditionalInfo,App\UserEducation;
use App\Role,App\RoleUser;
use App\Employer;
use App\Country,App\City,App\Nationals;
use App\Industry,App\Institute,App\FunctionalArea;
use App\SkillKeyword;
use App\PostJob,App\PostJobsCity,App\PostJobsKeyword,App\PostJobsQualification,App\PostJobsSpecialization,App\PostJobsNationality,App\PostJobsCandidateCountry,App\PostJobsCandidateCity;
use App\Degree,App\Course;
use App\Enquiry;
use App\Service;
use App\JobPlan,App\AdvertisePlanFeature;
use App\ForgetPassword;
use App\TransactionHistory,App\PurchasePlan;
use App\TmpUser;

class HomeController extends Controller
{
    public $data=[];
    private $core;
    public function __construct()
    {
        $this->core=app(\App\Http\Controllers\CoreController::class);
    }

    public function index()
    {
        $this->data['candidates']=User::with(['additional_info','country','city'])->whereRole(2)->where('is_approved',1)->orderBy('id','DESC')->get();
        $this->data['recent_jobs']=PostJob::with('employer_info','industry','jobs_in_city')->get();
        $this->data['top_employers']=Employer::with('job_post','country')->where('status',1)->get();
        $this->data['countries']=Country::where('status',1)->where('is_gulf',1)->select('id','name','icon')->orderBy('name','ASC')->get();
        $this->data['industries']=Industry::where('status',1)->inRandomOrder()->limit(8)->get();
        return view('front_panel.index',$this->data);
    }
    public function login(){
        return redirect('/');
    }
    public function register(){
        return redirect('/');
    }
    public function userLogout(){
        Auth::logout();
        return redirect('/')->with(['success'=>config('constants.FLASH_SUCCESS_LOGOUT')]);
    }
    public function candidateLoginByEmail(Request $request)
    {
        $status=0;
        if(Auth::attempt(['email'=>$request->email,'password'=>$request->password,'status'=>1,'role'=>2])){
           $status=1;
        } else {
            $status=2;
        }
        return response()->json(['status'=>$status]);
    }

    public function candidateRegistartion()
    { 
        $basicEducations=$masterEducations=[];
        $degreeList=Degree::with('courses:id,degree_id,name,education_type')->where('status',1)->orderBy('name','ASC')->get();
        foreach($degreeList as $k=>$val){
            if($val->courses!=null){
                foreach($val->courses as $c){
                    if($c->education_type==1){
                        $basicEducations[$val->name][$c->id]=$c->name;
                    } else {
                        $masterEducations[$val->name][$c->id]=$c->name;
                    }
                    
                }
            }
        }
        $this->data['basic_courses']=$basicEducations;
        $this->data['master_courses']=$masterEducations;
        $this->data['institutes']=Institute::where('status',1)->pluck('name','id');
        $this->data['industry_list']=$this->core->getIndustryList('pluck');
        $this->data['functional_area']=FunctionalArea::where('status',1)->orderBy('name','ASC')->pluck('name','id');
        $this->data['skill_keywords']=SkillKeyword::where('status',1)->select('name','id')->get();
        $this->data['country_codes']=Country::select(
                DB::raw('CONCAT(phonecode, "~", id) AS phonecode'),
                DB::raw('CONCAT(sortname, " (+", phonecode,")") AS codecountry'))
                ->where('status',1)->where('phonecode','!=','')
                ->orderBy('sortname','ASC')
                ->pluck('codecountry','phonecode');
        return view('front_panel.candidate_register',$this->data);
    }
    public function saveCandidate(Request $request){
        $interestedIn='';
        $userEducation=[];
        if($request->interested_in!=null){
            $interestedIn=join(',',$request->interested_in);
        }
        if($request->hasFile('user_img')){
            $filename=$this->core->fileUpload($request->file('user_img'),'uploads/user_img');
            $request->merge(['avatar'=>$filename]);
        }  
        if($request->hasFile('resume_file')){
            $filename=$this->core->fileUpload($request->file('resume_file'),'uploads/user_resume');
            $request->merge(['resume'=>$filename]);
        } 

        if($request->plan_id=='1')
        {
            $tmp_id=TmpUser::insertGetId(["post_data"=>json_encode($request->all())]);
            return redirect()->route('payment-gateway',['id'=>$tmp_id]);
        }

        
        DB::beginTransaction(); 
        $userData=[
            'email'=>$request->email,
            'password'=>Hash::make($request->password),
            'name'=>$request->name,
            'dob'=>dateConvert($request->dob,'Y-m-d'),
            'title'=>$request->name_title,
            'gender'=>config('constants.genders')[$request->name_title],
            'mobile'=>$request->phone,
            'avatar'=>$request->avatar,
            'nationality'=>$request->nationality,
            'city_id'=>$request->city,
            'country_id'=>$request->country,
            'state_id'=>$request->state,
            'country_code'=>explode('~',$request->country_code)[0],
        ];        
        //save user data
        $user=User::create($userData);        
        if($user){
            $lastUserId=$user->id;
            $user->attachRole(getRoleId('candidate'));
            
            //user additional information
            $userAdditionalInfo=[
                'user_id'=>$lastUserId,
                'exp_level'=>$request->experience_level,
                'exp_year'=>$request->year_experiance,
                'exp_month'=>$request->month_experiance,
                'current_company'=>$request->current_company,
                'current_position'=>$request->current_position,
                'current_industry'=>$request->current_industry,
                'functional_area'=>$request->functional_area,
                'key_skills'=>$request->key_skills,
                'interested_in'=>$interestedIn,
                'salary'=>$request->salary,
                'salary_in'=>$request->currency,
                'resume_headline'=>$request->resume_headline,
                'resume'=>$request->resume,
            ];
            $lastIdInfo=UserAdditionalInfo::insertGetId($userAdditionalInfo);

            //user education
            $userEducation=[
                'user_id'=>$lastUserId,
                'basic_course_id'=>$request->basic_course_id,
                'basic_specialization_id'=>$request->basic_specialization_id,
                'basic_comp_year'=>$request->basic_comp_year,
                'master_course_id'=>$request->master_course_id,
                'master_specialization_id'=>$request->master_specialization_id,
                'master_comp_year'=>$request->master_comp_year,
                'institute'=>$request->institute,
            ];
            $lastIdEducation=UserEducation::insertGetId($userEducation);

            DB::commit();
            $mailData['params']=['email'=>$request->email,'user_name'=>$request->name,'subject'=>'Welcome to Job4Gulf','template'=>'welcome_email'];
            $this->core->SendEmail($mailData);

            $mailData['params']=['email'=>$request->email,'user_id'=>$lastUserId,'user_name'=>$request->name,'subject'=>'Candidate Profile - Confirm your e-mail','template'=>'verify_email'];
            $this->core->SendEmail($mailData);

            //Auto Login User
            Auth::attempt(['email'=>$request->email,'password'=>$request->password]);
             
            return redirect()->route('thank-you')->with(['success' => 'We have sent a confirmation E-mail to '.$user->email.'. Please verify account.']);
        } else {
            DB::rollBack();
        }
    }

    public function paymentGateway(Request $request){
        
        $tmpUser = TmpUser::whereId($request->id)->first();
        if($tmpUser){
            $date = Carbon::now();
            $request->merge(json_decode($tmpUser->post_data,true));
            $interestedIn='';
            $userEducation=[];
            if($request->interested_in!=null){
                $interestedIn=join(',',$request->interested_in);
            }
            if($request->hasFile('user_img')){
                $filename=$this->core->fileUpload($request->file('user_img'),'uploads/user_img');
                $request->merge(['avatar'=>$filename]);
            }  
            if($request->hasFile('resume_file')){
                $filename=$this->core->fileUpload($request->file('resume_file'),'uploads/user_resume');
                $request->merge(['resume'=>$filename]);
            } 
            DB::beginTransaction(); 
            $userData=[
                'email'=>$request->email,
                'password'=>Hash::make($request->password),
                'name'=>$request->name,
                'dob'=>dateConvert($request->dob,'Y-m-d'),
                'title'=>$request->name_title,
                'gender'=>config('constants.genders')[$request->name_title],
                'mobile'=>$request->phone,
                'avatar'=>$request->avatar,
                'nationality'=>$request->nationality,
                'city_id'=>$request->city,
                'country_id'=>$request->country,
                'state_id'=>$request->state,
                'country_code'=>explode('~',$request->country_code)[0],
                'premium_acc_validity'=>$date->addDays(config('constants.jobseeker_features')[9]['validity_days'])->format('Y-m-d'),
            ];        
            //save user data
            $user=User::create($userData);        
            if($user){
                $lastUserId=$user->id;
                $user->attachRole(getRoleId('candidate'));
                
                //user additional information
                $userAdditionalInfo=[
                    'user_id'=>$lastUserId,
                    'exp_level'=>$request->experience_level,
                    'exp_year'=>$request->year_experiance,
                    'exp_month'=>$request->month_experiance,
                    'current_company'=>$request->current_company,
                    'current_position'=>$request->current_position,
                    'current_industry'=>$request->current_industry,
                    'functional_area'=>$request->functional_area,
                    'key_skills'=>$request->key_skills,
                    'interested_in'=>$interestedIn,
                    'salary'=>$request->salary,
                    'salary_in'=>$request->currency,
                    'resume_headline'=>$request->resume_headline,
                    'resume'=>$request->resume,
                ];
                $lastIdInfo=UserAdditionalInfo::insertGetId($userAdditionalInfo);

                //user education
                $userEducation=[
                    'user_id'=>$lastUserId,
                    'basic_course_id'=>$request->basic_course_id,
                    'basic_specialization_id'=>$request->basic_specialization_id,
                    'basic_comp_year'=>$request->basic_comp_year,
                    'master_course_id'=>$request->master_course_id,
                    'master_specialization_id'=>$request->master_specialization_id,
                    'master_comp_year'=>$request->master_comp_year,
                    'institute'=>$request->institute,
                ];
                $lastIdEducation=UserEducation::insertGetId($userEducation);

                DB::commit();
                $mailData['params']=['email'=>$request->email,'user_name'=>$request->name,'subject'=>'Welcome to Job4Gulf','template'=>'welcome_email'];
                $this->core->SendEmail($mailData);

                $mailData['params']=['email'=>$request->email,'user_id'=>$lastUserId,'user_name'=>$request->name,'subject'=>'Candidate Profile - Confirm your e-mail','template'=>'verify_email'];
                $this->core->SendEmail($mailData);

                //Auto Login User
                Auth::attempt(['email'=>$request->email,'password'=>$request->password]);
                 
                return redirect()->route('thank-you')->with(['success' => 'We have sent a confirmation E-mail to '.$user->email.'. Please verify account.']);
            } else {
                DB::rollBack();
            }

        }
    }
    public function employerLoginByEmail(Request $request)
    {
        $status=0;
        if(Auth::attempt(['email'=>$request->email,'password'=>$request->password,'status'=>1,'role'=>3])){
           $status=1;
        } else {
            $status=2;
        }
        return response()->json(['status'=>$status]);
    }
    public function employerRegistartion()
    {
       $this->data['country_codes']=getCountryCodes();
       $this->data['industry_list']=$this->core->getIndustryList('pluck');
       return view('front_panel.employer_register',$this->data);
    }
    public function saveEmployer(Request $request){
        $messages = [
            'cp_name.required' => 'Contact person name field is required.',
            'cp_designation.required' => 'Contact person designation field is required.',
            'comp_type.required' => 'Member type field is required.',
            'comp_name.required' => 'Company name field is required.',
            'comp_about.required' => 'Company about field is required.',
            'comp_industry.required' => 'Company industry field is required.',
            'comp_country.required' => 'Company country field is required.',
            'comp_city.required' => 'Company city field is required.',
            'comp_address.required' => 'Company address field is required.',
        ];
        $rules = [
            'password' => 'required|min:6',
            'email' => 'required|unique:users',
            'cp_name' => 'required',
            'cp_designation' => 'required',
            'dial_code' => 'required',
            'comp_type' => 'required',
            'comp_name' => 'required',
            'comp_about' => 'required',
            'comp_industry' => 'required',
            'comp_country' => 'required',
            'comp_city' => 'required',
            'comp_address' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect('register-employer')->withErrors($validator)->withInput();
        }
       DB::beginTransaction();
        $user = User::create([
            'role'=>3,
            'mobile'=>$request->cp_mobile,
            'name'=>$request->cp_name,
            'email'=>$request->email,
            'password'=>Hash::make($request->password)
        ]);
        if($user){                      
            if($request->hasFile('comp_img')){
                $filename=$this->core->fileUpload($request->file('comp_img'),'uploads/employer_logo');
                $request->merge(['comp_logo'=>$filename]);
            }
            $request->merge(['user_id'=>$user->id]);
            
            if(Employer::insert($request->except(['_token','password','comp_img','email']))){
                $user->attachRole(getRoleId('employer'));  

                //Activate free job plan
                $params=['plan_id'=>1,'user_id'=>$user->id,'type'=>'','remark'=>'Job Plan Activate On Registration Time'];
                $this->core->addNewPlan($params);

                DB::commit();

                $mailData['params']=['email'=>$request->email,'user_name'=>$request->name,'subject'=>'Welcome to Job4Gulf','template'=>'welcome_email_employer'];
                $this->core->SendEmail($mailData);

                $mailData['params']=['email'=>$request->email,'user_id'=>$user->id,'user_name'=>$request->name,'subject'=>'Employer Profile - Confirm your e-mail','template'=>'verify_email'];
                $this->core->SendEmail($mailData);
                //Auto Login User
                Auth::attempt(['email'=>$request->email,'password'=>$request->password]);

                return redirect()->route('thank-you')->with(['success' => 'We have sent a confirmation E-mail to '.$user->email.'. Please verify account.']);
            } else {
                DB::rollBack();
            }
        } else {
            DB::rollBack();
        }
        
    }

    public function consultantRegistration(){
        $this->data['skill_keywords']=SkillKeyword::where('status',1)->select('name','id')->get();
        $this->data['country_codes']=getCountryCodes();
        $this->data['country']=$this->core->getCountryList();
        $this->data['industry_list']=$this->core->getIndustryList();
        return view('front_panel.consultant_register',$this->data);
    }
    public function saveConsultant(Request $request){
        DB::beginTransaction();
        $user = User::create(['role'=>3,'name'=>$request->name,'email'=>$request->email,'password'=>Hash::make($request->password)]);
        if($user){                      
            if($request->hasFile('comp_img')){
                $filename=$this->core->fileUpload($request->file('comp_img'),'uploads/employer_logo');
                $request->merge(['comp_logo'=>$filename]);
            }
            $request->merge(['user_id'=>$user->id]);
            
            if(Employer::insert($request->except(['_token','password','comp_img','email','terms_and_conditions']))){
                $user->attachRole(getRoleId('employer')); 

                //Activate free job plan
                $params=['plan_id'=>1,'user_id'=>$user->id,'type'=>'','remark'=>'Job Plan Activate On Registration Time'];
                $this->core->addNewPlan($params);
 
                DB::commit();

                $mailData['params']=['email'=>$request->email,'user_name'=>$request->name,'subject'=>'Welcome to Job4Gulf','template'=>'welcome_email_employer'];
                $this->core->SendEmail($mailData);

                $mailData['params']=['email'=>$request->email,'user_id'=>$user->id,'user_name'=>$request->name,'subject'=>'Employer Profile - Confirm your e-mail','template'=>'verify_email'];
                $this->core->SendEmail($mailData);

                return redirect()->route('thank-you')->with(['success' => 'We have sent a confirmation E-mail to '.$user->email.'. Please verify account.']);
            } else {
                DB::rollBack();
            }
        } else {
            DB::rollBack();
        }
    }

    public function jobPostingPlans(){
        $this->data['plan_list']=config('constants.advertise_plan_category');
        $this->data['features']=AdvertisePlanFeature::all();
        //$this->data['plan_list']=JobPlan::with('features')->whereStatus(1)->get();
        return view('front_panel.job.job_plans',$this->data);
    }
     public function jobDetails(Request $request){
        $this->data['job_detail']=PostJob::with('employer_info','industry','functional_area','job_applicants','skills')->where('id',$request->jobid)->first();
        return view('front_panel.job.job_details',$this->data);
    }

    public function searchEmployerView(){
        $this->data['country_list']=$this->core->getCountryList();
        $this->data['city_list']=$this->core->getCityList();
        $this->data['industry_list']=$this->core->getIndustryList();
         return view('front_panel.company.search_company',$this->data);
    }

    public function topCompanies(){
        return view('front_panel.company.top_companies',$this->data);
    }

    //company detail page 
    public function companyPage(Request $request){
        $this->data['employer_details']=Employer::with('slider_images','job_post','country','city','designations')->findOrFail($request->id);
        return view('front_panel.employer.web_page',$this->data);
    }

    //consultant detail page
    public function consultantDetail($id){
        $this->data['consultant'] = Employer::with(['country','city','industry','job_post'])->where('id',$id)->first();
        $this->data['country_list']=$this->core->getCountryList();
        $this->data['city_list']=$this->core->getCityList();
        $this->data['industry_list']=$this->core->getIndustryList();
        $this->data['country_codes']=Country::whereStatus(1)->get();
       return view('front_panel.consultant.template_1.consultant_detail',$this->data);
    }

    public function jobsBy(Request $request){
        $job_cites=PostJobsCity::where('status',1)->pluck('city_id','id');
        $jobList=PostJob::where('status',1)->where('expiry_date','>=',date('Y-m-d'))->get();
        foreach($jobList as $val){
            $countryIds[]=$val->job_in_country;
            $industryIds[]=$val->industry_id;
        }

        if($request->type=='country'){
             $this->data['list']=Country::with('job_post')->where('status',1)->whereIn('id',$countryIds)->orderBy('name','ASC')->select('id','name','icon')->get();
             $this->data['title']='Country';
        } else if($request->type=='city'){
            $this->data['list']=City::with('active_jobs')->whereIn('id',$job_cites)->where('status',1)->select('id','name','icon')->get();
            $this->data['title']='City';
        } else if($request->type=='industry'){
            $this->data['list']=Industry::with('job_post')->whereIn('id',$industryIds)->get();
            $this->data['title']='Industry';
        } 
        return view('front_panel.job.job_by',$this->data);
    }
    
    public function thankYou(Request $request){
        return view('front_panel.thankyou',$this->data);
    }
    public function emailVerify(Request $request){
        $user = User::where('id',base64_decode($request->uid))->where('email',base64_decode($request->email))->first();
        if($user){
            $user->email_verified=1;
            $user->save();
        }       
        return redirect('/')->with(['success' => config('constants.FLASH_EMAIL_VERIFY_1')]);

        //return view('front_panel.thankyou',$this->data);
    }
    

    public function saveEnquiry(Request $request){
        if(Enquiry::insert($request->except(['_token','email_to']))){
            $request->merge(['template'=>'enquiry_email','subject'=>'New Query','email'=>$request->email_to,'email_user'=>$request->email]);
            $mailData['params']=$request;
            $this->core->SendEmail($mailData);
            return redirect()->back()->with(['success' => config('constants.FLASH_ENQUERY_SUBMIT_1')]);
        }
        return redirect()->back()->with(['error' => config('constants.FLASH_ENQUERY_SUBMIT_0')]);
    }

    public function ourPlans(){ 
        $this->data['service_list']=Service::with('features')->whereStatus(1)->get();      
        return view('front_panel.our_plans',$this->data);
    }


    public function saveServiceEnquiry(Request $request){
        $request->merge(['type'=>'1']);
        if(Enquiry::insert($request->except(['_token']))){
            return redirect()->back()->with(['success' => config('constants.FLASH_ENQUERY_SUBMIT_1')]);
        }
        return redirect()->back()->with(['error' => config('constants.FLASH_ENQUERY_SUBMIT_0')]);
    }

    //Forgot Password Functions
    public function forgot_password(){
        return view('front_panel.forgot_password.forgot_pass_step1');
    }
    public function forgot_password_step1(Request $request){
        $minute=config('constants.OTP_LIMIT'); //valid time duration
        $user=User::where('email',$request->email)->first();
        if($user){
            $date=date('Y-m-d H:i:s');
            $otp=genRandomNum(6);
            $end_date = date('Y-m-d H:i:s',strtotime('+ '.$minute .' minutes',strtotime($date)));  
            ForgetPassword::where('email',$request->email)->delete();
            ForgetPassword::insert([
                'user_id'=>$user->id,
                'email'=>$user->email,
                'otp'=>$otp,
                'end_date'=>$end_date
            ]);
            $mailData['params']=['email'=>$user->email,'user_name'=>$user->name,'subject'=>'Job4Gulf account recovery code','otp'=>$otp,'template'=>'forgot_password_otp'];
            $this->core->SendEmail($mailData);
            session(['user_email' => $user->email]);
            return redirect('otp-verify-view');
        }
        else
            return redirect()->back()->with(['error' => 'Email address not exist']);
    }
    public function otp_verify_view(){
        if(!session('user_email'))
        {
            return redirect('/');
        }
        return view('front_panel.forgot_password.forgot_pass_step2');
    }
    public function otp_verify(Request $request){
        if(session('user_email'))
        {
            if(ForgetPassword::where('email',session('user_email'))
                            ->where('otp',$request->otp)
                            ->where('end_date','>',date('Y-m-d H:i:s'))
                            ->first())
                return view('front_panel.forgot_password.forgot_pass_step3');

            else
                return redirect()->back()->with(['error' => 'OTP is incorrect.Please enter valid OTP.']); 
        }
        else
            return redirect()->back()->with(['error' => 'Email address not exist']);
    }
    public function updateForgotPassword(Request $request)
    {
        if($request->password!=''){
            $user=User::where('email',session('user_email'))->update(['password'=>Hash::make($request->password)]);
            if($user)
            {
                Session::forget('user_email');
                return redirect('/')->with(['success'=>'Your password updated successfully.']);
            }
            else{
                return redirect()->back()->with(['error'=>'Your password updated Falied.Try again.']);
            }

        }
        else
            return redirect()->back()->with(['error'=>'Password field can`t be blank.']);
    }

    public function aboutUs(){
        return view('front_panel.about_us');
    }

    public function contactUs(){
        $this->data['settings'] = getSettings();
        return view('front_panel.contact_us',$this->data);
    }
    public function contactUsMessage(Request $request){
       
         $mailData['params']=['email'=>$request->email,'name'=>$request->name,'title'=>$request->subject,'message'=>$request->message,'subject'=>'New Query (Contact Us)','template'=>'contact_us_message'];
         $this->core->SendEmail($mailData);
         return redirect()->back()->with(['success'=>'Thank You ! Your email has been delivered.']);
    }
    public function siteMap(){
        $this->data['countries']=Country::where('status',1)->select('id','name','icon')->limit(5)->get();
        $this->data['cities']=City::where('status',1)->select('id','name','icon')->limit(5)->get();
        $this->data['industries']=Industry::where('status',1)->inRandomOrder()->limit(5)->get();
        return view('front_panel.site_map',$this->data);
    }

    //End Forgot Password Functions
   
   public function postFreeJob(){
         Auth::logout();         
        $this->data['skill_keywords']=SkillKeyword::where('status',1)->select('name','id')->get();
        $educations=[];
        $degreeList=Degree::where('status',1)->with('courses:id,degree_id,name')->orderBy('name','ASC')->get();
        foreach($degreeList as $k=>$val){
            if($val->courses!=null){
                foreach($val->courses as $c){
                    $educations[$val->name][$c->id]=$c->name;
                }
            }
        }
        $this->data['educations']=$educations;
        $this->data['functional_area']=FunctionalArea::where('status',1)->orderBy('name','ASC')->pluck('name','id');
        $this->data['country_codes']=getCountryCodes();
        $this->data['industry_list']=$this->core->getIndustryList('pluck');

    return view('front_panel.post_free_job',$this->data);
   }

   public function saveFreeJob(Request $request){
        $userId=0;
        $filename='';
        // start validations
            $messages = [
                'job_title.required' => 'Job title field is required.',
                'job_description.required' => 'Job description field is required.',
            ];
            $rules = [
                'job_title' => 'required',
                'job_description' => 'required',
            ];

            $validator = Validator::make($request->all(), $rules, $messages);

            if ($validator->fails()) {
                return redirect('job-posting')->withErrors($validator)->withInput();
            }
        // end validations

        // check user email exist
            if($request->emp_registered==0 && $this->core->checkUserExist($request->email)>0){
                return redirect()->back()->with(['error' => config('constants.FLASH_EMAIL_ADDRESS_EXIST')]);
            } 
            
        
            if($request->emp_registered==1){
                if(Auth::attempt(['email'=>$request->user_email,'password'=>$request->user_password,'status'=>1,'role'=>3])){
                    $userId=Auth::user()->id;
                } else {
                    return redirect()->back()->with(['error' => config('constants.FLASH_INVALID_CREDENTIAL')]);
                }
            } else {
                DB::beginTransaction();
                $user = User::create([
                    'role'=>3,'name'=>$request->cp_name,'mobile'=>$request->cp_mobile, 'email'=>$request->user_email_unique,'password'=>Hash::make($request->user_password)
                ]);
                if($user){  
                    $userId=$user->id;                    
                    if($request->hasFile('comp_img')){
                        $filename=$this->core->fileUpload($request->file('comp_img'),'uploads/employer_logo');
                    }                
                    $empData=[
                        'user_id'=>$userId,
                        'comp_type'=>$request->comp_type,
                        "comp_name" => $request->comp_name,
                        "comp_about" => $request->comp_about,
                        "comp_website" => $request->comp_website,
                        "comp_industry" => $request->comp_industry,
                        "comp_logo" => $filename,
                        "comp_country" => $request->comp_country,
                        "comp_city" => $request->comp_city,
                        "comp_address" => $request->comp_address,
                        "zip_code" => $request->zip_code,
                        "cp_title" => $request->cp_title,
                        "cp_name" => $request->cp_name,
                        "cp_designation" => $request->cp_designation,
                        "dial_code" => $request->dial_code,
                        "cp_mobile" => $request->cp_mobile,
                    ];
                    $lastEmpId = Employer::insertGetId($empData);
                    if($lastEmpId>0){
                        $user->attachRole(getRoleId('employer')); 
                        //Activate free job plan
                            $params=['plan_id'=>1,'user_id'=>$userId,'type'=>'post_job','remark'=>'Job Plan Activat On Registration Time'];
                            $this->core->addNewPlan($params);

                        //auto login current employer
                            Auth::attempt(['email'=>$user->email,'password'=>$request->user_password,'status'=>1,'role'=>3]);

                        DB::commit();
                        $mailData['params']=['email'=>$request->email,'user_name'=>$request->name,'subject'=>'Welcome to Job4Gulf','template'=>'welcome_email_employer'];
                        $this->core->SendEmail($mailData);

                        $mailData['params']=['email'=>$request->email,'user_id'=>$user->id,'user_name'=>$request->name,'subject'=>'Employer Profile - Confirm your e-mail','template'=>'verify_email'];
                        $this->core->SendEmail($mailData);
                        
                    } else {
                        DB::rollBack();
                    }
                } else {
                    DB::rollBack();
                }
            }
             
        //post new job
        if(!Auth::check()){
            return redirect()->back()->with(['error' => config('constants.FLASH_SUCCESS_LOGIN_ALERT')]);
        } else {
            $date = Carbon::now();
            $jobData=[
                'user_id'=>$userId,
                'job_type'=>1,
                'expiry_date'=>$date->addDays(30)->format('Y-m-d'),
                'job_title'=>$request->job_title,
                'job_description'=>$request->job_description,
                "total_vacancy" =>$request->total_vacancy,
                "salary_min" =>$request->salary_min,
                "salary_max" =>$request->salary_max,
                "show_salary" =>$request->show_salary,
                "job_in_country" =>$request->job_in_country,
                "industry_id" => $request->industry_id,
                "functional_area_id" =>$request->functional_area_id,
                "gender"=>$request->gender,
                "min_experience" =>$request->min_experience,
                "max_experience" =>$request->max_experience,
                'response_by'=>$request->response_by,
                'response_email'=>$request->response_email,
                'walkin_address'=>$request->walkin_address,
                'walkin_date_from'=>dateConvert($request->walkin_date_from,'Y-m-d'),
                'walkin_date_to'=>dateConvert($request->walkin_date_to,'Y-m-d'),
                'walkin_time'=>$request->walkin_start_time.' '.$request->time_meridiem,
                'cp_title'=>$request->job_cp_title,
                'cp_name'=>$request->job_cp_name,
                'cp_mobile'=>$request->job_cp_mobile,
                'dial_number'=>$request->job_dial_number,
            ];

            // save job data in database
            $postJobId=PostJob::insertGetId($jobData);
            
            $jobsCityArr=$keywordArr=$quilificationArr=$specializationArr=$nationalityArr=$candidateCountryArr=$candidateCityArr=[];
            if($postJobId){

                // save jobs city
                if($request->job_in_city!=''){
                    $cities = explode(',', $request->job_in_city);
                    foreach($cities as $val){
                        $jobsCityArr[]=['post_job_id'=>$postJobId,'city_id'=>$val];
                    }
                    PostJobsCity::insert($jobsCityArr);
                }  

                // save job keywords
                if($request->keywords!=''){
                    $keywords = explode(',', $request->keywords);
                    foreach($keywords as $val){
                        $keywordArr[]=['post_job_id'=>$postJobId,'skill_id'=>$val];
                    }
                    PostJobsKeyword::insert($keywordArr);
                }

                // save job qualifications
                if(is_array($request->quilification)){
                    foreach($request->quilification as $val){
                        $quilificationArr[]=['post_job_id'=>$postJobId,'course_id'=>$val];
                    }
                    PostJobsQualification::insert($quilificationArr);
                }

                // save job specialization
                if(is_array($request->specialization)){
                    foreach($request->specialization as $val){
                        $exp = explode('~', $val);
                        $specializationArr[]=['post_job_id'=>$postJobId,'course_id'=>$exp[1],'specialization_id'=>$exp[0]];
                    }
                    PostJobsSpecialization::insert($specializationArr);
                }

                // save job nationalities
                if(is_array($request->nationality_id)){
                    foreach($request->nationality_id as $val){
                        $nationalityArr[]=['post_job_id'=>$postJobId,'nationality_id'=>$val];
                    }
                    PostJobsNationality::insert($nationalityArr);
                }

                // save candidate current country AND current city
                if(is_array($request->candidate_current_country)){
                foreach($request->candidate_current_country as $k=>$val){
                    $candidateCountryArr[]=['post_job_id'=>$postJobId,'country_id'=>$val];

                    if(isset($request->candidate_current_city[$k])){
                        foreach($request->candidate_current_city[$k] as $valCity){
                            $candidateCityArr[]=['post_job_id'=>$postJobId,'country_id'=>$val,'city_id'=>$valCity];
                        }
                    }
                }
                if(!empty($candidateCityArr)) PostJobsCandidateCity::insert($candidateCityArr);
                if(!empty($candidateCountryArr)) PostJobsCandidateCountry::insert($candidateCountryArr);
            }  
                 return redirect()->route('employer-dashboard')->with(['success' => config('constants.FLASH_JOB_POST_1')]);
            } 
        }
        return redirect()->back()->with(['error' => config('constants.FLASH_INVALID_PARAMS')]);
   }

   function getPurchasePlanData(){
        return PurchasePlan::with('job_plan')->where('user_id',Auth::user()->id)->where('remaining_jobs','>',0)->where('plan_end_date','>=',date('Y-m-d'))->orderBy('plan_id','DESC')->first();
    }   


    public function template_test(Request $request){
        $mailData['params']=['email'=>'bheemswami808@gmail.com','user_id'=>1,'user_name'=>'Bheem','subject'=>'Candidate Profile - Confirm your e-mail','msg'=>'','template'=>'thanks_verify_email'];
        //return view('email_templates.verify_email',$this->data);
        $mailData['params']=['email'=>'bheemswami808@gmail.com','user_name'=>'Bheem','otp'=>'198564','subject'=>'Welcome to Job4Gulf','template'=>'welcome_email_employer'];
        return view('email_templates.unpaid_invoice',$mailData);
    }
}