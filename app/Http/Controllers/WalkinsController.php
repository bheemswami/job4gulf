<?php

namespace App\Http\Controllers;
use Carbon\Carbon;
use Auth;
use Illuminate\Http\Request;
use App\PostJob,DB;
use App\AppliedWalkin;
use App\Walkin,App\WalkinInterviewDetail,App\WalkinPosition,App\City,App\Industry;
class WalkinsController extends Controller
{
     public $data=[];
    private $core;
    public function __construct()
    {
       // $this->middleware('auth');
        $this->core=app(\App\Http\Controllers\CoreController::class);
    }

    public function index(Request $request){
        $date = Carbon::now();
        $industryIds=[];
        $country_search_key=$city_search_key=$industry_search_key=$emp_type='';

        $result = Walkin::with('industry','country','walkin_interview','walkin_position');
        $result->where('status',1);

        $walkinList=$result->get();
        if(count($walkinList)>0){
            foreach($walkinList as $val){
                $industryIds[]=$val->comp_industry;
            }
        }
        if($request->this_week){
            $result->whereBetween('created_at', [$date->startOfWeek()->format('Y-m-d'), $date->endOfWeek()->format('Y-m-d')]);
        }
        if($request->next_week){
            $result->whereBetween('created_at', [$date->parse('this monday')->toDateString(), $date->endOfWeek()->addDays(7)->format('Y-m-d')]);
        }
        if($request->this_month){
            $result->whereMonth('created_at',date('m'));
        }
        if($request->post_date){
            $result->whereDate('created_at',dateConvert($request->post_date,'Y-m-d'));
        }
       
        if($request->city_ids){
            $walkin_ids=WalkinInterviewDetail::whereIn('city_id',explode(',',$request->city_ids))->pluck('walkin_id','id');
            $result->whereIn('id',$walkin_ids);
            
            $city_search_key = DB::table('cities')->select(DB::raw("group_concat(cities.name) as city"))->whereIn('id',explode(',',$request->city_ids))->get();
            $city_search_key = $city_search_key[0]->city;
        }
        if($request->industry_ids){
            $result->whereIn('comp_industry',explode(',',$request->industry_ids));
            
            $industry_search_key = DB::table('industries')->select(DB::raw("group_concat(industries.name) as industry"))->whereIn('id',explode(',',$request->industry_ids))->get();
            $industry_search_key = $industry_search_key[0]->industry;
        }
       
        if($request->employer_type){
            $exp=explode(',',$request->employer_type);
            $result->whereIn('employer_type',$exp);
            $empType=[1=>'Company Jobs',2=>'Consultancy Jobs'];
            foreach($exp as $val){
                $sVal[]=$empType[$val];
            }
            $emp_type=implode(',', $sVal);
        }
         
        $this->data['walkins']=$result->paginate(3)->appends(request()->query());
        $this->data['search_tag']=$city_search_key.','.$industry_search_key.','.$emp_type;
        if ($request->ajax()) {
            return view('front_panel.walkins.walkins_list', $this->data)->render();  
        } else {
            $cityIds=WalkinInterviewDetail::where('status',1)->pluck('city_id');
            $this->data['city_list']=City::with('walkins')->whereIn('id',$cityIds)->orderBy('name','ASC')->get();
            $this->data['industry_list']=Industry::with('walkins')->whereIn('id',$industryIds)->orderBy('name','ASC')->get();
            $this->data['emp_type'] = DB::select("select count(*) as total from walkins group by employer_type");

        }
        return view('front_panel.walkins.walkins',$this->data);

    }

    public function walkinsDetail(Request $request){
         $this->data['country_list']=$this->core->getCountryList();
        $this->data['city_list']=$this->core->getCityList();
        $this->data['industry_list']=$this->core->getIndustryList();
        $this->data['job_detail']=PostJob::with('employer_info','industry','functional_area')->first();
        $this->data['walkins']=Walkin::with('industry','country','walkin_interview','walkin_position')->where('status',1)->where('id',$request->walkin_id)->first();
        
        return view('front_panel.walkins.walkins_details',$this->data);
    }

    public function applyWalkin(Request $request){
        if(!Auth::check()){
            return redirect()->back()->with(['error' => config('constants.FLASH_SUCCESS_LOGIN_ALERT')]);
        }
        if(Auth::user()->role!=2){
            return redirect()->back()->with(['error' => config('constants.FLASH_WALKIN_APPLY_ONLY_CANDIDATE')]);
        }
        if(AppliedWalkin::where('apply_by',Auth::user()->id)->where('walkin_id',$request->walkin_id)->count()>0){
            return redirect()->back()->with(['error' => config('constants.FLASH_ALREADY_APPLIED_WALKINS')]);
        }
        $request->merge(['apply_by'=>Auth::user()->id]);
        if(AppliedWalkin::insertGetId($request->except(['_token']))){
            return redirect()->back()->with(['success'=>config('constants.FLASH_WALKIN_APPLY_MSG')]);
        } else {
            return redirect()->back()->with(['error' => config('constants.FLASH_INVALID_PARAMS')]);
        }
    }
    
}
