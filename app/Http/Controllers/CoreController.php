<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use PHPMailerAutoload; 
use PHPMailer\PHPMailer\PHPMailer;
use Mail;
use Carbon\Carbon;
use DB,File;
use Excel;
use App\User,App\RoleUser;
use App\Language;
use App\Industry;
use App\Country,App\City;
use App\JobPlan;
use App\TransactionHistory,App\PurchasePlan;
class CoreController extends Controller
{   
    function adminSessionCheck(){
        if(!session()->has('auth')){
         return redirect(url('/admin'));
        }
    }
    function getUserIdsByRole($type){
        return RoleUser::where('role_id',getRoleId($type))->pluck('user_id');
    }
    function fileUpload($file,$path,$original_name=0)
    {
        $filename='';
        $full_path='public/'.$path;
        File::isDirectory($full_path) or File::makeDirectory($full_path, 0777, true, true);
        File::isDirectory($full_path.'/thumbnail') or File::makeDirectory($full_path.'/thumbnail', 0777, true, true);
        if($file!=''){
            if($original_name==1){
                $filename=$file->getClientOriginalName();                
            } else {
                $filename=md5($file->getClientOriginalName()).'_'.time().'.'.$file->getClientOriginalExtension();                
            }
            switch($path){
                case "uploads/walkin_comp_logo":
                    $this->resizeImage($file,$path.'/thumbnail',$filename,100,38);
                    $file->move($full_path, $filename);
                    break;
                case "uploads/country_img":
                    $this->resizeImage($file,$path.'/thumbnail',$filename,48,30);
                    $file->move($full_path, $filename);
                    break;
                case "uploads/banner_img":
                    $this->resizeImage($file,$path.'/thumbnail',$filename,2000,250);
                    $file->move($full_path, $filename);
                    break;
                case "uploads/user_img":
                    $this->resizeImage($file,$path.'/thumbnail',$filename,100,100);
                    $file->move($full_path, $filename);
                    break;
                case "uploads/industry_img":
                    //$this->resizeImage($file,$path.'/thumbnail',$filename,48,30);
                    $file->move($full_path, $filename);
                    break;
                default:
                    $file->move($full_path, $filename);
                    break;
            }
            
        }
       
        return $filename;
   
    }

    function resizeImage($file,$path,$filename,$w,$h){
         $image_resize = Image::make($file->getRealPath());              
        $image_resize->resize($w, $h);
        $image_resize->save(public_path($path.'/'.$filename));
    }
    function unlinkImg($img,$path) {
        if($img !=null || $img !='')
        {
            $path='public/'.$path.'/';
            $image_path = app()->basePath($path.$img);
            if(File::exists($image_path)) 
                unlink($image_path);
        }       
    }
    // public function SendEmail($data){
    //     if(isset($data['params']['template']) && $data['params']['template']!=''){
    //      $send = Mail::queue('email_templates/'.$data['params']['template'], $data, function ($m) use ($data) {
    //         $m->from(config('constants.email_config')['from_email'], config('constants.email_config')['from_name']);

    //         $m->to($data['params']['email'], null);
    //         //$m->sender($address, $name = null);
    //         //$m->cc($address, $name = null);
    //         //$m->bcc($address, $name = null);
    //         $m->replyTo(config('constants.email_config')['reply_to'], 'Email Notification');
    //         $m->subject($data['params']['subject']);
    //         //$m->priority($level);
    //     });
    //     }
    //     if(!$send) { 
    //         $txt = FALSE;            
    //     } else {
    //         $txt = TRUE;
    //     }
    //     return $txt;
    // }
    public function SendEmail($data){
        $mail = $this->mailConfig();
        $mail->addAddress($data['params']['email']);
        $mail->Subject = $data['params']['subject'];
        if(isset($data['params']['template']) && $data['params']['template']!=''){
            $mail->Body = view('email_templates/'.$data['params']['template'],$data);
        } else {
            $mail->Body = $data['params']['msg'];
        }
        $mail->isHTML(true);
        if(!$mail->Send()) {            
           $res = FALSE;            
        } else {
            $res = TRUE;
        }
        return $res;
     }

     public function SendMassEmail($data,$template=null){
        $mail = $this->mailConfig();
        $mail->Subject = $data['mail']['subject'];
        if(!empty($data['mail']['emails'])){
            foreach($data['mail']['emails'] as $email){
                $mail->addAddress($email); 
            }
        }
        if($template==null){
            $mail->Body= $data['mail']['msg'];
        } else {
            $mail->Body=view('email_template.'.$template,$data);
        }
        $mail->isHTML(true);
        if(!$mail->Send()) { 
            $txt = FALSE;            
        } else {
            $txt = TRUE;
        }
       // return $txt;
     }

    function mailConfig(){
        $mail = new PHPMailer;
        $mail->isSMTP();
        //$mail->SMTPDebug  = 1; 
        $mail->SMTPAuth = true;
        $mail->Host = config('constants.email_config')['host'];
        $mail->Username = config('constants.email_config')['username'];
        $mail->Password = config('constants.email_config')['password'];
        $mail->SMTPSecure = config('constants.email_config')['smtp_secure'];
        $mail->Port = 465;
        $mail->From = config('constants.email_config')['from_email'];
        $mail->FromName = config('constants.email_config')['from_name'];
        $mail->addReplyTo(config('constants.email_config')['reply_to'], 'Email Notification');  
        return $mail;
    }

    function exportFiles($data_array,$view){
        Excel::create('excel', function($excel) use ($data_array,$view){
            $excel->sheet('Excel sheet', function($sheet) use ($data_array,$view) {
                //$sheet->setOrientation('landscape');
                //$sheet->fromArray($course);
                $sheet->loadView('excel_view/'.$view)->with($data_array);
                });
            })->export('xls');
    }

     function checkUserExist($email=null,$except_id=null){
        if($email!=null){
            if($except_id!=null)
                return User::where('id','!=',$except_id)->where('email',$email)->get()->count();
            else
                return User::where('email',$email)->get()->count();
        }
        return 0;

     }
    function getIndustryList($type=null){
        if($type=='pluck')
            return Industry::where('status',1)->orderBy('name','ASC')->pluck('name','id');
        else
            return Industry::where('status',1)->orderBy('name','ASC')->get();
    }
    function getCountryList($type=null){
        if($type=='pluck')
            return Country::where('status',1)->orderBy('name','ASC')->pluck('name','id');
        else
            return Country::where('status',1)->orderBy('name','ASC')->get();
    }
    function getCountryCodeList($type=null){
        if($type=='pluck')
            return Country::where('status',1)->orderBy('name','ASC')->pluck('phonecode');
    }
    function getCityList($type=null){
        if($type=='pluck')
            return City::where('status',1)->orderBy('name','ASC')->pluck('name','id');
        else
            return City::where('status',1)->orderBy('name','ASC')->get();
    }

    function addNewPlan($params){
        $date = Carbon::now();
        $planStartDate = $date->format('Y-m-d');

        $job_plan=JobPlan::with('features')->whereId($params['plan_id'])->first();
        $plan_features = getPlanFeatureData($job_plan);        
        
        if($params['type']=='new_job_plan'){
            $currentPlan = PurchasePlan::with('job_plan')->whereStatus(1)->where('user_id',$params['user_id'])->where('plan_end_date','>=',date('Y-m-d'))->orderBy('plan_end_date','DESC')->first();
            $date = Carbon::parse($currentPlan->plan_end_date);

            //set new plan start date of current plan endDate+1
            $planStartDate = $date->addDays(1)->format('Y-m-d');
        }

        $purchaseId = PurchasePlan::insertGetId([
            'user_id'=>$params['user_id'],
            'plan_id'=>$job_plan->id,
            'total_basic_jobs'=>$job_plan->basic_jobs,
            'remaining_basic_jobs'=>($params['type']=='post_job') ? $job_plan->basic_jobs-1 : $job_plan->basic_jobs,
            'total_premium_jobs'=>$job_plan->premium_jobs,
            'remaining_premium_jobs'=>$job_plan->premium_jobs,
            'basic_job_live'=>$plan_features['basic_job_live'],
            'premium_job_live'=>$plan_features['premium_job_live'],
            'plan_validity_days'=>$plan_features['plan_validity_days'],
            'straight_to_inbox'=>$plan_features['straight_to_inbox'],
            'job_alert_emails'=>$plan_features['job_alert_emails'],
            'plan_validity_days'=>$plan_features['plan_validity_days'],
            'job_email_upto'=>$plan_features['job_email_upto'],
            'unlimited_space'=>$plan_features['unlimited_space'],
            'use_email_ids'=>$plan_features['use_email_ids'],
            'job_refreshed'=>$plan_features['job_refreshed'],
            'greater_visibility'=>$plan_features['greater_visibility'],
            'job_promote'=>$plan_features['job_promote'],
            'share_jobs'=>$plan_features['share_jobs'],
            'bulk_downloads'=>$plan_features['bulk_downloads'],
            'manage_response'=>$plan_features['manage_response'],
            'redirect_to_website'=>$plan_features['redirect_to_website'],
            'plan_start_date'=>$planStartDate,
            'plan_end_date'=>$date->addDays($plan_features['plan_validity_days'])->format('Y-m-d'),
            'plan_feature_ids'=>$plan_features['plan_feature_ids'],
            'remark'=>$params['remark'],
        ]);
               
        $trasId = TransactionHistory::insertGetId([
            'user_id'=>$params['user_id'],
            'plan_id'=>$job_plan->id,
            'order_id'=>'',
            'payment_id'=>genTransactionId(24,'PAY-'),
            'intent'=>'',
            'payment_state'=>'',
            'cart'=>'',
            'currency'=>'USD',
            'plan_price'=>$job_plan->price,
            'quantity'=>($job_plan->basic_jobs+$job_plan->premium_jobs),
            'total_amount'=>$job_plan->price,
            'payer_payment_method'=>'',
            'payer_status'=>'',
            'transaction_for'=>$params['remark'],
            'payment_details'=>''
        ]);
        if($purchaseId>0 && $params['type']!='new_job_plan'){
            PurchasePlan::where('id','<>',$purchaseId)->where('user_id',$params['user_id'])->update(['status'=>0]);
        }      
        return $trasId;

   } 
}