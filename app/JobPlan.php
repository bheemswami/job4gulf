<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobPlan extends Model
{
    protected $guarded=['id'];
     function features(){
        return $this->hasMany('App\JobPlanFeature','job_plan_id','id');
    }
}
