<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Degree extends Model
{
	protected $guarded=['id'];
	function courses(){
	 	return $this->hasMany('App\Course','degree_id','id');
	}
}
