<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class PurchasePlan extends Model
{
	protected $guarded=['id'];
	function job_plan(){
	 	return $this->hasOne('App\JobPlan','id','plan_id');
	}

}
