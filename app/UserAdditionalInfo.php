<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAdditionalInfo extends Model
{
    public $table='user_additional_info';
    protected $fillable=[
    	'id', 'user_id', 'exp_level', 'exp_year', 'exp_month', 'current_company', 'current_position', 'current_industry', 'functional_area', 'key_skills', 'salary', 'salary_in', 'interested_in', 'resume_headline', 'resume', 'status'
    ];
    function user(){
        return $this->hasOne('App\User','id','user_id')->where('email_verified',0)->whereStatus(1);
    }
    function industry(){
        return $this->hasOne('App\Industry','id','current_industry');
    }
    function company(){
        return $this->hasOne('App\Company','id','current_company');
    }
    function function_area(){
        return $this->hasOne('App\FunctionalArea','id','functional_area');
    }
}
