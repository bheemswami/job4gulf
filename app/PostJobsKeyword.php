<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostJobsKeyword extends Model
{
    protected $guarded = ['id'];
    protected $with=['skill_keywords'];
    function skill_keywords(){
        return $this->hasOne('App\SkillKeyword','id','skill_id')->select('id','name');
    }
}
