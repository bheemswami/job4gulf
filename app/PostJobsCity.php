<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostJobsCity extends Model
{
    public $with=['city'];
    function city(){
        return $this->hasOne('App\City','id','city_id')->select('id','name');
    }
    function jobs(){
        return $this->hasOne('App\PostJob','id','post_job_id')->where('status',1);
    }
}
