<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class TransactionHistory extends Model
{
	protected $guarded=['id'];
	protected $table='transaction_history';
	function user(){
	 	return $this->hasOne('App\User','id','user_id');
	}
	function job_plan(){
	 	return $this->hasOne('App\JobPlan','id','plan_id');
	}
}
