 $(document).ready(function() {
        $('#service_desc,#coupon_terms,#blog_desc').summernote({
        	height: 160,
        });
        $('.datetimepicker').datetimepicker({
            format: 'YYYY-MM-DD h:mm'
        });
        $(document).ready(function() {
            $('#example,#datatbl').DataTable();
        });
 });
if($(".service-form")[0] != undefined){
     $(".service-form").validate({
        rules: {
            title: {
                required: true,
                normalizer: function(value) {
                    return $.trim(value);
                }
            }
        }
    });
}
if($(".service-addon-form")[0] != undefined){
     $(".service-addon-form").validate({
        rules: {
            service_id: {
                required: true,
            },
             title: {
                required: true,
            },
        }
    });
}
if($(".service-type-form")[0] != undefined){
     $(".service-type-form").validate({
        rules: {
        	service_id: {
                required: true,
            },
            title: {
                required: true,
            }
        }
    });
}
if($(".add-manager-form")[0] != undefined){
    $(".add-manager-form").validate({
        rules: {
            name: {
                required: true,
            },
            email: {
                required: true,
                email: true,
            },
            new_password: {
                required: true,
                minlength: 6,
            },
            confirm_password: {
            	required: true,
                minlength: 6,
          		equalTo: "#password"
       		}
        }
    });
}
if($(".update-manager-form")[0] != undefined){
    $(".update-manager-form").validate({
        rules: {
            name: {
                required: true,
            },
            email: {
                required: true,
                email: true,
            },
            new_password: {
                minlength: 6,
            },
            confirm_password: {
            	minlength: 6,
          		equalTo: "#password"
       		}
        }
    });
}

if($(".add-customer-form")[0] != undefined){
    $(".add-customer-form").validate({
        rules: {
            first_name: {
                required: true,
            },
            email: {
                required: true,
               email: true,
            },
            new_password: {
                required: true,
                minlength: 6,
            },
            confirm_password: {
                required: true,
                minlength: 6,
                equalTo: "#password"
            }
        }
    });
}
if($(".update-customer-form")[0] != undefined){
    $(".update-customer-form").validate({
        rules: {
            first_name: {
                required: true,
            },
            email: {
                required: true,
                email: true,
            },
            new_password: {
                minlength: 6,
            },
            confirm_password: {
                minlength: 6,
                equalTo: "#password"
            }
        }
    });
}
if($(".coupon-form")[0] != undefined){
    $(".coupon-form").validate({
        rules: {
            title: {
                required: true,
            },
            code: {
                required: true,
            },
            rate: {
                required: true,
                number: true
            },
            start_datetime: {
                required: true,
                date: true
            },
            end_datetime: {
                required: true,
                date: true
            },
            
        }
    });
}
if($(".blog-form")[0] != undefined){
    $(".blog-form").validate({
        rules: {
            title: {
                required: true,
            },
            subtitle: {
                required: true,
            },
            
        }
    });
}
//alerts
$('.delete_btn').click(function(){
	var deleteUrl = $(this).data('url');
	swal({
	  title: 'Are you sure?',
	  text: "You won't be able to revert this!",
	  type: 'warning',
	  showCancelButton: true,
	  confirmButtonColor: '#3085d6',
	  cancelButtonColor: '#d33',
	  confirmButtonText: 'Yes, delete it!'
	}).then(function (result) {
	  if (result.value) {
	    window.location.href=deleteUrl;
	  }
	});
});
