  @php 
  $settings = getSettings();
  //session()->put('CALL_US',$settings->mobile);
  @endphp
  <!doctype html>
  <html>
    <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
      <meta property="og:url" content="@yield('og_url')" />
      <meta property="og:image" content="@yield('og_image')" />
      <meta property="og:title" content="@yield('og_title')" />
      <title>{{$settings->meta_title}} @yield('page_title')</title>
      <script>
        var base_url="{{url('/').'/'}}";
        var csrf_token="{{ csrf_token() }}";
        var globalFunc = {};
        var globalVar = {};
        var currentThis = '';
        var minChars = 2;
      </script>
      <link rel="shortcut icon" type="image/png" href="{{url('public/images/favicon.png')}}"/>
      <link rel="stylesheet" href="{{URL::asset('public/plugins/selectize/selectize.css')}}">
      <link rel="stylesheet" type="text/css" href="{{URL::asset('public/css/bootstrap.min.css')}}">
      <link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css">
      <link rel="stylesheet" type="text/css" href="{{URL::asset('public/css/owl.carousel.css')}}">
      <link rel="stylesheet" type="text/css" href="{{URL::asset('public/css/owlcarousel-theme.css')}}">
      <link rel="stylesheet" type="text/css" href="{{URL::asset('public/css/style.css')}}">
      <link rel="stylesheet" type="text/css" href="{{URL::asset('public/css/style-new.css')}}">
      <link rel="stylesheet" type="text/css" href="{{URL::asset('public/css/media.css')}}">
      <link rel="stylesheet" type="text/css" href="{{URL::asset('public/css/bootstrap-multiselect.css')}}">
      <link rel="stylesheet" type="text/css" href="{{URL::asset('public/plugins/Font-Awesome/css/font-awesome.css')}}" />
      <link rel="stylesheet" rel="stylesheet" href="{{URL::asset('public/css/slick.css')}}"  />        
      <link rel="stylesheet" rel="stylesheet" href="{{URL::asset('public/css/slick-theme.css')}}"  /> 
      <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

      <link rel="stylesheet" type="text/css" href="{{URL::asset('public/plugins/multiSelect/jquery.multiselect.css')}}" />
      
      <script src="{{URL::asset('public/js/jquery-1.11.2.min.js')}}"></script>
      <script src="{{URL::asset('public/js/bootstrap.min.js')}}"></script>
      <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/moment.min.js"></script>
      <script type="text/javascript" src="http://rawgithub.com/botmonster/jquery-bootpag/master/lib/jquery.bootpag.min.js"></script>
      <script src="http://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
      <script src="http://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>

      <script src="{{URL::asset('public/js/owl.carousel.min.js')}}"></script>
      <script src="{{URL::asset('public/js/bootstrap-multiselect.js')}}"></script>
      <script src="{{URL::asset('public/js/custom.js')}}"></script>
      <script src="{{URL::asset('public/js/smoothscroll.js')}}"></script>
      <script src="{{URL::asset('public/js/jquery.validate.min.js')}}" type="text/javascript"></script>
      <script src="http://jqueryvalidation.org/files/dist/additional-methods.min.js"></script>
      
      <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
      <script src="{{URL::asset('public/plugins/selectize/selectize.js')}}"></script>
      <script src="{{URL::asset('public/plugins/multiSelect/jquery.multiselect.js')}}"></script>
      
  </head>

  <body>

  <!-- Header Start -->
  <header id="header" style="position: inherit;">
    <div class="container">
      {{-- <div id="loader">
        <img class="loader_image" src="{{url('public/images/image_1153550.gif')}}"/>
      </div> --}}
      <div class="row">
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-4 logo">
          <a href="{{url('/')}}"><img src="{{checkFile($settings->logo,'/uploads/site_logo/','logo.png')}}" class="img-responsive"></a>
        </div>
        <div class="col-xs-12 col-sm-9 col-md-9 col-lg-8 menu-sec">
          <nav class="navbar navbar-default " role="navigation">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>            
            </div>
            <div class="collapse navbar-collapse navbar-ex1-collapse nopadding">
              <ul class="nav navbar-nav navbar-right">
                <li class="no-vertical-line dropdown"><a>Browse Jobs</a>
                  <ul class="dropdown-content">
                      <li class="no-vertical-line"><a href="{{route('search-jobs')}}">Search Jobs</a></li>
                      <li class="no-vertical-line"><a href="{{route('jobs-by',['country'])}}">Jobs by Country</a></li>
                      <li class="no-vertical-line"><a href="{{route('jobs-by',['city'])}}">Jobs by City</a></li>
                      <li class="no-vertical-line"><a href="{{route('jobs-by',['industry'])}}">Jobs by Industry</a></li>
                  </ul>
                </li>
                <li class="no-vertical-line"><a href="{{route('consultant')}}">Search Consultant</a></li> 
                <li  class="no-vertical-line"><a href="{{route('walkins')}}">Walkins</a></li>
                
                @if(Auth::check() && Auth::user()->role==2)
                  <li class="no-vertical-line dropdown"><a href="#"><i class="fa fa-user" aria-hidden="true"></i> {{ucfirst(Auth::user()->name)}}</a>
                    <ul class="dropdown-content">
                      <li class="no-vertical-line"><a href="{{route('candidate-dashboard')}}">Dashboard</a></li>
                      <li class="register-btn no-vertical-line dropdown"><a href="{{route('logout')}}">Logout</a></li>
                    </ul>
                  </li>
                @else 
                  <li class="no-vertical-line dropdown"><a href="#"><i class="fa fa-user" aria-hidden="true"></i> Job Seeker</a>
                    <ul class="dropdown-content">
                      <li class="no-vertical-line"><a href="#" data-toggle="modal" data-target="#userLoginModal">Login</a></li>
                      <li class="register-btn no-vertical-line dropdown"><a href="{{route('candidate-register')}}">Register</a></li>
                    </ul>
                  </li>
                @endif

                @if(Auth::check() && Auth::user()->role==3)
                  <li class="no-vertical-line dropdown"><a href="#"><i class="fa fa-user" aria-hidden="true"></i> {{ucfirst(Auth::user()->name)}}</a>
                    <ul class="dropdown-content">
                      <li class="no-vertical-line"><a href="{{route('employer-dashboard')}}">Dashboard</a></li>
                      <li class="no-vertical-line"><a href="{{route('emp-profile-view')}}">Profile</a></li>
                      <li class="register-btn no-vertical-line dropdown"><a href="{{route('logout')}}">Logout</a></li>
                    </ul>
                  </li>
                @else 
                  <li class="no-vertical-line dropdown"><a href="#"><i class="fa fa-users" aria-hidden="true"></i> Employers</a>
                  <ul class="dropdown-content">
                    <li class="no-vertical-line"><a href="#" data-toggle="modal" data-target="#employerLoginModal">Login</a></li>
                    <li class="no-vertical-line"><a href="{{route('employer-register')}}">Register</a></li>
                    <li class="no-vertical-line"><a href="{{route('our-plans')}}">Our Plans</a></li>                  
                  </ul>
                </li>
                @endif
                <li class="no-vertical-line"><a href="{{route('contact-us')}}">Contact Us</a></li> 
                @if(Auth::check())
                <img class="userimg" src="{{checkFile(Auth::user()->avatar,'uploads/user_img/','user_img.png')}}">
                @endif
              </ul>

            </div>
          </nav>
        </div>
      </div>
    </div>
  </header>
  <!-- Header End -->
    
    @include('layouts.flash_msg')
    @yield('content')
  <!-- Start Modal Candidate Login-->
    <div class="modal fade" id="userLoginModal" tabindex="-1" role="dialog" aria-labelledby="userLoginModalLabel" aria-hidden="true">
      <div class="modal-dialog login-modal" role="document">
        <div class="modal-content">
          <div class="modal-header">
              <h4 class="modal-title">Job Seeker Login</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
          <div class="modal-body flexbox">

             <!--div class="col-xs-12 col-sm-4 hidden-xs" style="background: url('{{url('public/images/login-img.jpg')}}') no-repeat center top;margin-top: -1px; background-size:cover;">
               
                <div class="socialLogin">
                  <p class="txtSignUp">Log In With</p>
                  <a class="socialBtn fbLogin ico" href="#" id="fbLogin"><img src="{{url('public/images/facebook.png')}}">
                  </a>
                  <a class="socialBtn gpLogin ico" href="#" id="gpLogin"><img src="{{url('public/images/gplus.png')}}">
                  </a>
                </div>

              </div-->
              <div class="col-xs-12 col-sm-12 right">
                <img src="{{url('public/images/image_1153550.gif')}}" class="login_model_loader hide-elem"/>
              <div id="success_error_msg" class="jobseeker-msg text-center"></div>
                <form class="login-form form-1" id="login-form" autocomplete="on">
                  <div class="form-group clearfix">
                    <label class="col-xs-12 nopadding">Username</label>
                    <div class="col-xs-12 nopadding">
                      <input type="email" name="candidate_username" value="" id="cand_login_username" class="form-control" placeholder="Enter Username" title="Please Emter Username" required/>
                    </div>
                  </div>
                  <div class="form-group clearfix">
                    <label class="col-xs-12 nopadding">Password</label>
                    <div class="col-xs-12 nopadding">
                      <input type="password" name="candidate_password" value="" id="cand_login_password" class="form-control" placeholder="Enter Password" required/>
                    </div>
                  </div>
                  <div class="form-group clearfix">
                    <button type="submit">Login</button>
                  </div>
                  <a href="{{route('forgot-password')}}">Forgot Password?</a>
                </form>
              </div>
          </div>
        </div>
      </div>
    </div>
  <!-- End Modal Candidate Login-->

  <!-- Start Modal Employer Login-->
    <div class="modal fade" id="employerLoginModal" tabindex="-1" role="dialog" aria-labelledby="employerLoginModalLabel" aria-hidden="true">
      <div class="modal-dialog login-modal" role="document">
        <div class="modal-content">
          <div class="modal-header">
              <h4 class="modal-title">Employer Login</h4>
               <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
          <div class="modal-body flexbox">
              <div class="col-xs-12 col-sm-12 right">
                <img src="{{url('public/images/image_1153550.gif')}}" class="login_model_loader hide-elem">
                <div id="success_error_msg" class="employer-msg text-center"></div>
                <form class="login-form form-2" id="login-form" autocomplete="on">
                  <div class="form-group clearfix">
                    <label class="col-xs-12 nopadding">Username</label>
                    <div class="col-xs-12 nopadding">
                      <input type="email" name="employer_username" value="" id="emp_login_username" class="form-control" placeholder="Enter Username" title="Please Enter Username" required/>
                    </div>
                  </div>
                  <div class="form-group clearfix">
                    <label class="col-xs-12 nopadding">Password</label>
                    <div class="col-xs-12 nopadding">
                      <input type="password" name="employer_password" value="" id="emp_login_password" class="form-control" placeholder="Enter Password" required/>
                    </div>
                  </div>
                  <div class="form-group clearfix">
                    <button type="submit">Login</button>
                  </div>
                  <a href="{{route('forgot-password')}}">Forgot Password?</a>
                </form>
              </div>
          </div>
        </div>
      </div>
    </div>
  <!-- End Modal Employer Login-->
  @php
    $countryList=getFooterData('country');
    $industryList=getFooterData('industry');
  @endphp
  <footer id="footer" class="inner-footer">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 left">
          <h3>Jobs By</h3>
          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 footer-menu">

            <h4>Location</h4>
            <ul>            
              @foreach($countryList as $val)
                <li><a href="{{url('search-jobs?country_ids='.$val->id)}}" >Jobs in {{$val->name}}</a></li>
              @endforeach
              <li class="footer-menu-more"><a href="{{route('jobs-by',['country'])}}">More <i class="fa fa-fw fa-angle-double-right text-primary"></i></a></li>
            </ul>
          </div>
          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 footer-menu">
            <h4>Category</h4>
            <ul>
              @foreach($industryList as $val)
                <li><a href="{{url('search-jobs?industry_ids='.$val->id)}}" > {{$val->name}}</a></li>
              @endforeach
             <li class="footer-menu-more"><a href="{{route('jobs-by',['industry'])}}">More <i class="fa fa-fw fa-angle-double-right text-primary"></i></a></li>
            </ul>
          </div>
          
        </div>

        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 left">
          <h3>Users</h3>
          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 footer-menu">
            <h4>Job Seekers</h4>
            <ul>
            @if(Auth::check() && Auth::user()->role==2)
                <li><a href="#">{{ucfirst(Auth::user()->name)}}</a>
                <li><a href="{{route('logout')}}">Logout</a></li>
            @else 
                <li><a href="#" data-toggle="modal" data-target="#userLoginModal">Login</a></li>
                <li><a href="{{route('candidate-register')}}">Register</a></li>
            @endif 
            </ul>         
          </div>

          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 footer-menu">
            <h4>Employers</h4>
            <ul>
              @if(Auth::check() && Auth::user()->role==2)
                <li><a href="#">{{ucfirst(Auth::user()->name)}}</a>
                <li><a href="{{route('logout')}}">Logout</a></li>
              @else 
                  <li><a href="#" data-toggle="modal" data-target="#employerLoginModal">Login</a></li>
                  <li><a href="{{route('employer-register')}}">Register</a></li>
              @endif 
              <li><a href="{{route('our-plans')}}">Our Plans</a></li>
            </ul>
          </div>
          
        </div>
        
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 nopadding right">
          <h3>Jobs4Gulf</h3>
          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 footer-menu">
            <h4>Quick Links</h4>
            <ul>
              <li><a href="{{route('about-us')}}">About Us</a></li>
              <li><a href="{{route('contact-us')}}">Contact Us</a></li>
              <li><a href="{{route('site-map')}}">Site Map</a></li>
            </ul>
          </div>
          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 nopadding footer-social">
              <h4>Follow Us</h4>
              <ul>
                @if($settings->fb_link!='') <li><a target="_blank" href="{{$settings->fb_link}}"><img src="{{url('public/images/facebook.png')}}"></a></li> @endif
                @if($settings->twitter_link!='')<li><a target="_blank" href="{{$settings->twitter_link}}"><img src="{{url('public/images/twitter.png')}}"></a></li> @endif
                @if($settings->gplus_link!='')<li><a target="_blank" href="{{$settings->gplus_link}}"><img src="{{url('public/images/gplus.png')}}"></a></li> @endif
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="clearfix footer-bottom">
          <div class="text-center"> All rights reserved © {{date('Y')}} Jobs4Gulf</div>
        </div>
      </div>
    </div>
  </footer>
  <script src="{{URL::asset('public/js/custom-file-input.js')}}"></script>
  <link href="{{URL::asset('public/plugins/summernote-0.8.8/dist/summernote.css')}}" rel="stylesheet">
  <script src="{{URL::asset('public/plugins/summernote-0.8.8/dist/summernote.js')}}"></script>
  <script src="{{URL::asset('public/js/frontpanel.js')}}"></script>
  <script src="{{URL::asset('public/js/global_custom.js')}}"></script>
  <script src="{{URL::asset('public/js/slick.js')}}" type="text/javascript"></script>

   <script type="text/javascript">
          $(document).ready(function(){
            $('.data_table').dataTable(
              {
                'aoColumnDefs': [{
                  'bSortable': false,
                  'aTargets': ['nosort']
                }]
              }
            );
            $('.banner-callus-button').html("{{'Questions? Call us: '.$settings->mobile}}");
            $('.contact_no_job').html("{{$settings->mobile}}");

            $(".lazy").slick({
              lazyLoad: 'ondemand', // ondemand progressive anticipated
              infinite: true,
              dots: true,
              arrows : false,
              autoplay: false,
            });
          });

          $(document).on('click','#select_all_checkbox',function(){
            if(this.checked){
                $('.data_checkbox').each(function(){
                    this.checked = true;
                });
            }else{
                 $('.data_checkbox').each(function(){
                    this.checked = false;
                });
            }
          });
          
          $(document).on('click','.data_checkbox',function(){
              if($('.data_checkbox:checked').length == $('.data_checkbox').length){
                  $('#select_all_checkbox').prop('checked',true);
              }else{
                  $('#select_all_checkbox').prop('checked',false);
              }
          });
      </script>
  </body>

  </html>