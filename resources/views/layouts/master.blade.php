<!DOCTYPE html>

<html lang="en">
     <head>
        <meta charset="UTF-8" />
        <title> Admin Panel </title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        
        <link rel="stylesheet" href="{{URL::asset('public/extra_plugins/fastselect/fastselect.min.css')}}">
         
        <link rel="stylesheet" rel="stylesheet" href="{{URL::asset('public/assets/plugins/bootstrap/css/bootstrap.css')}}" />
        <link rel="stylesheet" rel="stylesheet" href="{{URL::asset('public/assets/plugins/dataTables/dataTables.bootstrap.css')}}" />
        <link rel="stylesheet" rel="stylesheet" href="{{URL::asset('public/assets/css/main.css')}}" />
        <link rel="stylesheet" rel="stylesheet" href="{{URL::asset('public/assets/css/theme.css')}}" />
        <link rel="stylesheet" rel="stylesheet" href="{{URL::asset('public/assets/css/MoneAdmin.css')}}" />
        <link rel="stylesheet" rel="stylesheet" href="{{URL::asset('public/assets/plugins/Font-Awesome/css/font-awesome.css')}}" />
        <!--END GLOBAL STYLES -->
        
        <!-- PAGE LEVEL STYLES -->
        {{-- <link rel="stylesheet" rel="stylesheet" href="{{URL::asset('public/assets/plugins/chosen/chosen.min.css')}}" />
        <link rel="stylesheet" rel="stylesheet" href="{{URL::asset('public/assets/plugins/tagsinput/jquery.tagsinput.css')}}" /> --}}
               

        <link rel="stylesheet" rel="stylesheet" href="{{URL::asset('public/assets/css/layout2.css')}}"  />        
        <link rel="stylesheet" rel="stylesheet" href="{{URL::asset('public/css/style.css')}}" />
        <!-- END THEME LAYOUT STYLES -->


        <script type="text/javascript" src="{{URL::asset('public/js/jquery.min.js')}}"></script>
        <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/moment.min.js"></script>
        <script type="text/javascript" src="{{URL::asset('public/assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>


        <link rel="stylesheet" rel="stylesheet" href="{{URL::asset('public/extra_plugins/sweetalert2/sweetalert2.min.css')}}" />
        <script type="text/javascript" src="{{URL::asset('public/extra_plugins/sweetalert2/sweetalert2.all.js')}}" ></script>
       
       <script>
        var base_url="{{url('/').'/'}}";
        var csrf_token="{{ csrf_token() }}";
        var globalFunc = {};
        var globalVar = {};
        var currentThis = '';
       </script>
        

    </head>
    <body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap" >
        @php 
        $site_settings= getSiteSetting();
        @endphp

        <!-- HEADER SECTION -->
        <div id="top">
            <nav class="navbar navbar-inverse navbar-fixed-top " style="padding-top: 10px;">
                <a href="{{url('/')}}"><img src="{{checkFile($site_settings->site_logo,'uploads/site_logo/','site_logo')}}" id="logoimg" alt="Logo" /></a>
                <a data-original-title="Show/Hide Menu" data-placement="bottom" data-tooltip="tooltip" class="accordion-toggle btn btn-primary btn-sm visible-xs" data-toggle="collapse" href="#menu" id="menu-toggle">
                    <i class="icon-align-justify"></i>
                </a>
                <!-- LOGO SECTION -->
                <header class="navbar-header">
                    <a href="index.html" class="navbar-brand"><img src="assets/img/logo.png" alt="" /> </a>
                </header>
                <!-- END LOGO SECTION -->
                <ul class="nav navbar-top-links navbar-right">
                    <!--ADMIN SETTINGS SECTIONS -->

                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="icon-user "></i>&nbsp; <i class="icon-chevron-down "></i>
                        </a>

                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="{{route('profile')}}"><i class="icon-user"></i> User Profile </a></li>
                            <li class="divider"></li>  
                            <li><a href="{{route('manage-settings')}}"><i class="icon-gear"></i> Settings </a></li>
                            <li class="divider"></li>                            
                            <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"> Logout </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>

                    </li>
                    <!--END ADMIN SETTINGS -->
                </ul>

            </nav>
               
        </div>
        <!-- END HEADER SECTION -->



        <!-- MENU SECTION -->
       <div id="left" >
            <div class="media user-media well-small">
                <a class="user-link" href="#">
                     <img class="media-object img-thumbnail user-profile-img" alt="User Picture" src="{{checkFile(Auth::user()->image,'uploads/user_img/','profile')}}"/>
                </a>
                <br />
                <div class="media-body">
                    <h5 class="media-heading"> {{ucfirst(Auth::user()->fname)}}</h5>
                   
                </div>
                <br />
            </div>
            @php
                $dashboard= $mCertif=$mCertifTemp=$volunt=$person=$users=$manager=$language=$branch=$csv='';
                if(Request::route()->uri=='dashboard')
                    $dashboard='active';
                else if(Request::route()->uri=='manage-certificates')
                    $mCertif='active';         
                else if(Request::route()->uri=='manage-temp-certificates')
                    $mCertifTemp='active';               
                else if(Request::route()->uri=='manage-volunteers')
                    $volunt='active';
                else if(Request::route()->uri=='manage-persons')
                    $person='active';
                else if(Request::route()->uri=='manage-users')
                    $users='active';
                else if(Request::route()->uri=='manage-managers')
                    $manager='active';
                else if(Request::route()->uri=='manage-language')
                    $language='active';
                else if(Request::route()->uri=='manage-branch')
                    $branch='active';
                else if(Request::route()->uri=='upload-csv')
                    $csv='active';
            @endphp
            <ul id="menu" class="collapse">
                <li class="panel {{$dashboard}}"><a href="{{route('dashbaord')}}" ><i class="icon-table"></i> Dashboard</a></li>
                <li class="panel {{$mCertif}}"><a href="{{route('manage-certificates')}}" ><i class="icon-table"></i> Manage Certificates</a></li>
                <li class="panel {{$mCertifTemp}}"><a href="{{route('manage-temp-certificates')}}" ><i class="icon-table"></i> Manage Temp Certificates</a></li>
                <li class="panel {{$manager}}"><a href="{{route('manage-managers')}}" ><i class="icon-table"></i> Manage Managers</a></li>
                <li class="panel {{$users}}"><a href="{{route('manage-users')}}" ><i class="icon-table"></i> Manage Users</a></li>
                <li class="panel {{$person}}"><a href="{{route('manage-persons')}}" ><i class="icon-table"></i> Manage Persons</a></li>
                <li class="panel {{$volunt}}"><a href="{{route('manage-volunteers')}}" ><i class="icon-table"></i> Manage Volunteers</a></li>
                <li class="panel {{$language}}"><a href="{{route('manage-language')}}" ><i class="icon-table"></i> Manage Language</a></li>
                <li class="panel {{$branch}}"><a href="{{route('manage-branch')}}" ><i class="icon-table"></i> Manage Branch</a></li>
                {{-- <li class="panel"><a href="{{route('manage-permission')}}" ><i class="icon-table"></i> Manage Permission</a></li> --}}
                <li class="panel"><a href="{{route('manage-roles')}}" ><i class="icon-table"></i> Manage Roles</a></li>
                <li class="panel {{$csv}}"><a href="{{route('upload-csv')}}" ><i class="icon-table"></i> Upload CSV</a></li>

            </ul>

        </div>
        <!--END MENU SECTION -->
        
        <!--PAGE CONTENT -->
        <div id="content">
            <div class="inner" style="min-height: 700px;">
                 @include('layouts.flash_msg')

                @yield('content')
           </div>
        </div>
        <!--END PAGE CONTENT -->
        <!-- RIGHT STRIP  SECTION -->
            @yield('right_content')
        <!-- END RIGHT STRIP  SECTION -->
         
    </div>

    <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
    <div id="footer">
        <p>&copy;  ManageCertificates &nbsp;2017 &nbsp;</p>
    </div>
    <!--END FOOTER -->
    <script src="{{URL::asset('public/js/custom.js')}}" type="text/javascript"></script>
    <script src="{{URL::asset('public/js/jquery.validate.min.js')}}" type="text/javascript"></script>
    <script src="{{URL::asset('public/js/validation_mc.js')}}" type="text/javascript"></script>
   
    <script src="{{URL::asset('public/assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js')}}"></script>
    
    <script src="{{URL::asset('public/assets/plugins/dataTables/jquery.dataTables.js')}}"></script>
    <script src="{{URL::asset('public/assets/plugins/dataTables/dataTables.bootstrap.js')}}"></script>
    <script src="{{URL::asset('public/assets/plugins/tagsinput/jquery.tagsinput.min.js')}}" type="text/javascript"></script>


     <script src="{{URL::asset('public/assets/js/jquery-ui.min.js')}}"></script>
     <script src="{{URL::asset('public/assets/plugins/uniform/jquery.uniform.min.js')}}"></script>
    <script src="{{URL::asset('public/assets/plugins/inputlimiter/jquery.inputlimiter.1.3.1.min.js')}}"></script>
    <script src="{{URL::asset('public/assets/plugins/chosen/chosen.jquery.min.js')}}"></script>
    <script src="{{URL::asset('public/assets/plugins/colorpicker/js/bootstrap-colorpicker.js')}}"></script>
    <script src="{{URL::asset('public/assets/plugins/tagsinput/jquery.tagsinput.min.js')}}"></script>
    <script src="{{URL::asset('public/assets/plugins/validVal/js/jquery.validVal.min.js')}}"></script>
    <script src="{{URL::asset('public/assets/plugins/daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{URL::asset('public/assets/plugins/daterangepicker/moment.min.js')}}"></script>
    <script src="{{URL::asset('public/assets/plugins/datepicker/js/bootstrap-datepicker.js')}}"></script>
    <script src="{{URL::asset('public/assets/plugins/timepicker/js/bootstrap-timepicker.min.js')}}"></script>
    <script src="{{URL::asset('public/assets/plugins/switch/static/js/bootstrap-switch.min.js')}}"></script>
    <script src="{{URL::asset('public/assets/plugins/jquery.dualListbox-1.3/jquery.dualListBox-1.3.min.js')}}"></script>
    <script src="{{URL::asset('public/assets/plugins/autosize/jquery.autosize.min.js')}}"></script>
    <script src="{{URL::asset('public/assets/plugins/jasny/js/bootstrap-inputmask.js')}}"></script>

    {{-- <script src="{{URL::asset('public/assets/plugins/autosize/jquery.autosize.min.js')}}" type="text/javascript"></script>
    <script src="{{URL::asset('public/assets/js/formsInit.js')}}" type="text/javascript"></script>
    <script>
            $(function () { formInit(); });
    </script>
     --}}

    @yield('footer_js')
   
</body>

        
       
    

</html>
