@include('email_templates.header')

<style>
.content{
    padding: 8px 31px !important;
    font-size: 13px  !important;
}
</style>
 <tr>
    <th>{{ ($params['job_data']->employer_info!=null) ? $params['job_data']->employer_info->comp_name : '' }}</th>
</tr>
<tr>
    <th><h6> You have successfully applied for this job </h6></th>
</tr>
<tr>
    <td class="content">
        <table>
            <tr>
                <th class="text-left">Job Title&nbsp;:</th>
                <td>&nbsp;{{$params['job_data']->job_title}}</td>
            </tr>
            <tr>
                <th class="text-left">Experience&nbsp;:</th>
                <td>&nbsp;{{$params['job_data']->min_experience}}-{{$params['job_data']->max_experience}} years</td>
            </tr>
            <tr>
                <th class="text-left">No. of Vacancy&nbsp;:</th>
                <td>&nbsp;{{$params['job_data']->total_vacancy}}</td>
            </tr>
            @if($params['job_data']->functional_area!=null)
            <tr>
                <th class="text-left">Functional Area&nbsp;:</th>
                <td>&nbsp;{{ ($params['job_data']->functional_area!=null) ? $params['job_data']->functional_area->name : '' }}</td>
            </tr>
            @endif
            @if($params['job_data']->industry!=null)
            <tr>
                <th class="text-left">Industry&nbsp;:</th>
                <td>&nbsp;{{ ($params['job_data']->industry!=null) ? $params['job_data']->industry->name : '' }}</td>
            </tr>
            @endif
        </table>
    </td>
</tr>

<tr>
    <td class="content"></td>
</tr>
<tr>
    <th><h6>Key Skills</h6></th>
</tr>
<tr>
    <td class="content">{{$params['job_data']->key_skill}}</td>
</tr>
<tr>
    <th><h6>Job Description</h6></th>
</tr>
<tr>
    <td class="content">{!! limit_words($params['job_data']->job_description,150) !!}</td>
</tr>


@include('email_templates.footer')