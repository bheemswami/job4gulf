@extends('/layouts/front_panel_master')
@section('content')

{{-- @include('front_panel/includes/page_banner') --}}
 
        <div class="jumbotron text-center">
            <img src="{{url('/public/images/access_denied.png')}}" width="50px">
          <h2 class="display-3">Access Denied</h2>
          <p class="lead">You do not have access to the page you requested!</p>
          <hr>
          <p>
           <a href="{{route('home')}}">Return to home page</a>
          </p>
        </div>
  

@endsection