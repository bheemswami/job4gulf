<style>
table th{background: #efeb58;border: 1px solid;}
table td{border: 1px solid;text-align: left;}
</style>
<table class="table table-bordered" style="border: 2px solid;">
                  <tr>
                    <th>Job Name</th>
                    <td">{{$job_detail->job_title}}</td>
                  </tr>
                  <tr>
                    <th>Job Type</th>
                    <td">{{($job_detail->job_plan!=null) ? $job_detail->job_plan->name : '' }}</td>
                  </tr>
                  <tr>
                    <th>Total Vacancy</th>
                    <td">{{$job_detail->total_vacancy}}</td>
                  </tr>
                  <tr>
                    <th>Date of Download</th>
                     <td">{{ date('d-M-Y') }} Year</td>
                   </tr>
                   <tr>
                    <th>Total Resumes</th>
                    <td">{{ count($candidates) }}</td>
                  </tr>
              </table>                              
                       <table class="table table-bordered">
                          <thead>
                            <tr>
                              <th>S.No.</th>
                              <th>Name</th>
                              <th>Gender</th>
                              <th>Telephone numbers (Mobile)</th>
                              <th>Date of Birth</th>
                              <th>E-mail ID</th>
                              <th>Location</th>
                              <th>Date of Apply</th>
                              <th>Total Experience</th>
                              <th>Status</th>
                              <th>Resume Title</th>
                              <th>Key Skills</th>
                              <th>Functional Area</th>
                              <th>Current Designation</th>
                              <th>Annual Salary</th>
                              <th>UG Course</th>
                              <th>UG specialization</th>
                              <th>PG Course</th>
                              <th>PG Specialization</th>
                              <th>Download Cv</th>
                            </tr>
                          </thead>
                          <tbody>
                            @forelse($candidates as $k=>$val)
                                <tr>
                                  <td>{{ $k+1 }}</td>
                                  <td>{{ $val->user->name }}</td>
                                  <td>{{ $val->user->gender }}</td>
                                  <td>{{ $val->user->mobile }}</td>
                                  <td>{{ dateConvert($val->user->dob,'d-M-Y') }}</td>
                                  <td>{{ $val->user->email }}</td>
                                  <td>{{ ($val->user->city!=null) ? $val->user->city->name : '' }}</td>
                                  <td>{{ dateConvert($val->created_at,'d-M-Y') }}</td>
                                  <td> 
                                    @if($val->user->additional_info!=null)
                                        {{ ($val->user->additional_info->exp_level==1) ? @$val->user->additional_info->exp_year.' Year(s) '.@$val->user->additional_info->exp_month.' Month(s)' : 'Fresher' }}
                                    @endif
                                  </td>
                                  <td>{{ config('constants.folder_names')[$val->move_to] }}</td>
                                  <td>{{ ($val->user->additional_info!=null) ? $val->user->additional_info->resume_headline : '' }}</td>
                                  <td>{{ ($val->user->additional_info!=null) ? $val->user->additional_info->key_skills : '' }}</td>
                                  <td>
                                    @if($val->user->additional_info!=null)
                                    {{ ($val->user->additional_info->function_area!=null) ? $val->user->additional_info->function_area->name : '' }}
                                    @endif
                                    </td>
                                  <td>{{ ($val->user->additional_info!=null) ? $val->user->additional_info->current_position : '' }}</td>
                                  <td>{{ ($val->user->additional_info!=null) ? $val->user->additional_info->salary : '' }}</td>
                                  <td>
                                    @if($val->user->user_education!=null)
                                        {{ ($val->user_education->basic_course!=null) ? $val->user_education->basic_course->name : '' }}
                                    @endif
                                    </td>
                                  <td>
                                    @if($val->user_education!=null)
                                        {{ ($val->user_education->basic_specialization!=null) ? $val->user_education->basic_specialization->name : '' }}
                                    @endif
                                    </td>
                                  <td>
                                    @if($val->user_education!=null)
                                        {{ ($val->user_education->master_course!=null) ? $val->user_education->master_course->name : '' }}
                                    @endif
                                    </td>
                                  <td>
                                    @if($val->user_education!=null)
                                        {{ ($val->user_education->master_specialization!=null) ? @$val->user_education->master_specialization->name : '' }}
                                    @endif
                                    </td>
                                  <td>
                                    @if($val->user->additional_info!=null)
                                        @if($val->user->additional_info->resume!='')
                                            <a href="{{url('public/uploads/user_resume/'.$val->user->additional_info->resume)}}">Download Resume</a>
                                        @endif
                                    @endif
                                       
                                  </td>
                                
                                  </tr>
                            @empty
                            <tr>
                              <td class="bg-danger" colspan="4">Record Not Found</td>
                            </tr>
                            @endforelse
                          </tbody>
                        </table>
                    
@php
    function additionalInfoExist(){

    }
@endphp