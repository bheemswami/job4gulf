@extends('layouts.master')
@section('content')
    <!--PAGE CONTENT -->
            <div class="row">
                <div class="col-lg-12">
                    <h3> Manage Roles</h3>
                </div>
            </div>  
            <hr />
            <div class="row collapse" id="roles_form_sec">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Add Role
                        </div>
                        <div class="panel-body">
                            {!! Form::open(array('url'=>'storeRole','class'=>'form-horizontal','id'=>'roleForm')) !!}
                                <div class="form-group">
                                    <div class="col-sm-6">
                                        {!! Form::label('Role Name',null,array('class'=>'')) !!}
                                        {!! Form::text('role_name',null,array('class'=>'form-control','placeholder'=>'Enter Role Name')) !!}
                                    </div>
                                                
                                    <div class="col-sm-6">
                                    {!! Form::label('Description',null,array('class'=>'')) !!}
                                        {!! Form::text('description',null,array('class'=>"form-control",'placeholder'=>"Enter Description")) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        {!! Form::label('Select Permissions',null,array('class'=>'')) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                    @foreach($permission as $value)
                                        {!! '<div class="col-sm-6">' !!} {{ Form::checkbox('permission[]',$value->id)}} {{$value->display_name}} {!! "</div>" !!}
                                    @endforeach
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-8 col-sm-offset-10">
                                        {{Form::submit('Add Role',array('class'=>'btn btn-success add_btn','id'=>'subBtn')) }}
                                    </div>  
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
              </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            All Roles
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                              {{-- @permission(('display-role')) --}}
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                     <tr>
                                        <th>S.No</th>
                                        <th>Name</th>
                                        <th>Display Name</th>
                                        <th>Description</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i=1;?>
                                    @foreach($role as $value)
                                        <tr>
                                            <td>{{$i++}}</td>
                                            <td>{{$value->name}}</td>
                                            <td>{{$value->display_name}}</td>
                                            <td>{{$value->description}}</td>
                                            <td>
                                            {{--  @permission(('role-edit')) --}}
                                                <a href="{{route('edit-role',[$value->id])}}"><i class="fa fa-edit" aria-hidden="true"></i>Edit</a>
                                            {{-- @endpermission
                                            @permission(('role-delete')) --}}
                                                <a href="{{url('deleteRole/'.$value->id)}}" onclick="return confirm('Are You Sure to delete this Role ?')"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                            {{-- @endpermission --}}
                                            </td>
                                        </tr>
                                   
                                    @endforeach
                                    </tbody>
                                </table>
                                {{-- @endpermission --}}
                            </div>                           
                        </div>
                    </div>
                </div>
            </div>
       
    <!--END PAGE CONTENT -->

    <!-- RIGHT STRIP  SECTION -->
    {{-- @section('right_content')
    <div id="right">
        <div class="well well-small">
            <button class="btn btn-inverse btn-block" data-toggle="collapse" data-target="#roles_form_sec">Add New Role</button>
         </div>        
    </div>
    @endsection --}}
     <!-- END RIGHT STRIP  SECTION -->
@endsection
