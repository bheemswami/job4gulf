@extends('/layouts/front_panel_master')
@section('content')

@include('front_panel/includes/page_banner')
<div class="taber ">
  <div class="container">
    <div class="row">
        <div class="col-lg-9">    
          <div class="ear">
            <div class="clearfix job-detail-inner">
                <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 text-left">
                  <h3>{{$job_detail->job_title}}</h3>
                  <h5>{{ ($job_detail->employer_info!=null) ? $job_detail->employer_info->comp_name : '' }}</h5>
                  <div class="post-details">
                  <p> <span><i class="fa fa-suitcase" aria-hidden="true"></i>{{$job_detail->min_experience}}-{{$job_detail->max_experience}} years</span> 
                      <span><i class="fa fa-map-marker" aria-hidden="true"></i> <!-- {{$job_detail->job_in_city}} - --> {{ ($job_detail->country!=null) ? $job_detail->country->name : '' }} </span></p>
                  </div>
                </div>
                <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 text-right">
                  @if($job_detail->employer_info!=null)
                      <img src="{{checkFile($job_detail->employer_info->comp_logo,'uploads/employer_logo/','company_logo.png')}}" class="com">
                    @else 
                      <img src="{{url('public/images/default/company_logo.png')}}" class="com">
                    @endif
                </div>
            </div>
             <div class="haf post-details">
              <p> <span><i class="fa fa-clock-o" aria-hidden="true"></i>Posted : {{dateConvert($job_detail->created_at,'jS M Y')}}</span> 
                    <span> Job Applicants: <strong>{{count($job_detail->job_applicants)}}</strong>  </span>
                    <span> No. of Vacancies: <strong>{{$job_detail->total_vacancy}}</strong>  </span></p>
            </div>
          </div>

          <div class="ear">
            <div class="clearfix job-detail-inner">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left job-desc">
                  <h3 class="heading"><span class="grey">Job Description</span></h3>
                  <div class="post-details">
                    {!! $job_detail->job_description !!}
                  </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left job-desc">
                  <h3 class="heading"><span class="grey">Key Skills</span></h3>
                  <div class="post-details">
                    @php
                    $skills=[];
                    if($job_detail->skills!=null){
                      foreach($job_detail->skills as $val){
                          if($val->skill_keywords!=null){
                            $skills[]=$val->skill_keywords->name;
                          }
                      }
                    }
                    @endphp
                  <p> {{join(', ',$skills)}}</p>
                  </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left job-desc">
                  <h3 class="heading"><span class="grey">Job Summary</span></h3>
                  <div class="post-details">
                    
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="row mgbt-xs-0">
                          <label class="col-md-3 control-label">Functional Area:</label>
                          <div class="col-xs-7 controls">{{ ($job_detail->functional_area!=null) ? $job_detail->functional_area->name : '' }}</div>
                          <!-- col-sm-10 --> 
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="row mgbt-xs-0">
                          <label class="col-md-3 control-label">Industry:</label>
                          <div class="col-xs-7 controls">{{ ($job_detail->industry!=null) ? $job_detail->industry->name : '' }}</div>
                          <!-- col-sm-10 --> 
                        </div>
                      </div>
              
                      <div class="col-sm-12">
                        <div class="row mgbt-xs-0">
                          <label class="col-md-3 control-label">Gender:</label>
                          <div class="col-xs-7 controls"> {{$job_detail->gender}}</div>
                          <!-- col-sm-10 --> 
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="row mgbt-xs-0">
                          <label class="col-md-3 control-label">Number of Vacancy:</label>
                          <div class="col-xs-7 controls"> {{$job_detail->total_vacancy}}</div>
                          <!-- col-sm-10 --> 
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="row mgbt-xs-0">
                          <label class="col-md-3 control-label">Job Country:</label>
                          <div class="col-xs-7 controls">{{ ($job_detail->country!=null) ? $job_detail->country->name : '' }}</div>
                          <!-- col-sm-10 --> 
                        </div>
                      </div>
                      
                     
                    </div>

                  </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-left job-desc">
                  <h3 class="heading"><span class="grey">You Can Contact</span></h3>
                  @if($job_detail->response_email!='')
                    @php
                      $emails=[];
                      $expEmails = explode(',', $job_detail->response_email);
                      if(!empty($expEmails)){
                        foreach($expEmails as $val){
                          $emails[]='<a href="mailto:'.$val.'">'.$val.'</a>';
                        }
                      }
                    @endphp                  
                    <div class="col-sm-12">
                        <div class="row mgbt-xs-0">
                          <label class="col-md-3 control-label">Email:</label>
                          <div class="col-xs-7 controls">{!! join(', ',$emails) !!}</div>
                        </div>
                    </div>
                  @endif

                  @if($job_detail->cp_name!='')
                    <div class="col-sm-12">
                        <div class="row mgbt-xs-0">
                          <label class="col-md-3 control-label">Contact Person:</label>
                          <div class="col-xs-7 controls">{{ $job_detail->cp_title }} {{ $job_detail->cp_name }}</div>
                        </div>
                    </div>
                  @endif

                  @if($job_detail->cp_mobile!='')
                    <div class="col-sm-12">
                        <div class="row mgbt-xs-0">
                          <label class="col-md-3 control-label">Contact Number:</label>
                          <div class="col-xs-7 controls">+{{ $job_detail->dial_number }}-{{ $job_detail->cp_mobile }}</div>
                        </div>
                    </div>
                  @endif

                  @if($job_detail->walkin_address!='')
                    <div class="col-sm-12">
                        <div class="row mgbt-xs-0">
                          <label class="col-md-3 control-label">Address:</label>
                          <div class="col-xs-7 controls">{{ $job_detail->walkin_address }}</div>
                        </div>
                    </div>
                  @endif

                </div>
            </div>
             
          </div>
    
            <div class="refer">
              <div class="col-md-12 nopadding">
                  <ul> 
                    @if(Auth::check())
                      @if(Auth::user()->role=='2') 
                        @if(jobAppliedOrNot($job_detail->id)==0)  
                            <li><a onclick="return confirm('Are you sure you want to apply?')" href="{{url('apply-job/'.$job_detail->id)}}">Apply Now</a>
                        @else
                            <li><a href="#">Already Applied</a>
                        @endif
                      @endif
                    @else
                      <li><a data-toggle="modal" data-target="#userLoginModal">Apply Now</a>    
                   @endif
                    {{-- <li><a href="#">Refer to a Friend</a></li> --}}
                  </ul>
              </div>
        </div>
        </div>
    
@php 
  $latestNews = getLatestNews();
@endphp    
    <div class="col-lg-3">
    
    <div class="latest">
      <h3 class="heading"><span class="grey">Latest News</span></h3>
        <ul>
          @forelse($latestNews as $val)
            <li><i class="fa fa-flash"></i> <p>{!! $val->title !!}</p></li>
          @empty
            <li><i class="fa fa-flash"></i> <p>No Latest News</p></li>
          @endforelse
        </ul>   
    </div>
    
    </div>
    
    </div>
</div>
</div>
  
@php 
  $rJobs = getRelatedJobs($job_detail->id);
@endphp

<div class="carr waz">
    <div class="container">
      <h3 class="heading"><span class="grey">Related Jobs</span></h3>
      <div class="row">
        @foreach($rJobs as $k=>$val)
        <div class="col-xs-12 col-md-6">
          <div class=" job-box">
            <div class="row flexbox">
              <div class="col-xs-3 col-sm-2 left">
                <div class="div-table">
                  <div class="table-cell-div">
                    @if($val->employer_info!=null)
                      <img src="{{checkFile($val->employer_info->comp_logo,'uploads/employer_logo/','company_logo.png')}}" class="center-block">
                    @else 
                      <img src="{{url('public/images/default/company_logo.png')}}" class="center-block">
                    @endif

                  </div>
                </div>
              </div>
              <div class="col-xs-9 col-sm-10 right">
                <h2><a href="{{route('job-details',['jobid'=>$val->id])}}">{{$val->job_title}}</a></h2>
                <h5>{{ ($val->employer_info!=null) ? $val->employer_info->comp_name : '' }}</h5>
                <div class="post-details">
                  <p>{{limit_text($val->key_skill,80)}}</p>
                  <p> 
                    <span><i class="fa fa-suitcase" aria-hidden="true"></i>{{$val->min_experience}}-{{$val->max_experience}} years</span> 
                    <span><i class="fa fa-map-marker" aria-hidden="true"></i>                     
                     {{ getCityNames($val->jobs_in_city) }} - {{ ($val->country!=null) ? $val->country->name : '' }}
                    </span>
                  </p>
                </div>
              </div>
            </div>
            <div class="job_optwrap">
              <p>Posted : {{dateConvert($val->created_at,'jS M Y')}}</p>
            </div>
          </div>
        </div>  
      @endforeach

    </div>
  </div>

    </div>

    {{-- <div class="acds">
        <div class="container">
          <div class="row">
            
          </div>
        </div>
    </div> --}}
      

<div class="clearfix"></div>
@endsection