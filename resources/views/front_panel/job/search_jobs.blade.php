
@extends('/layouts/front_panel_master')
@section('content')

@php
$paramCountryIds=$paramCityIds=$paramIndustryIds=[];
if(isset(Request::all()['country_ids'])){
  $paramCountryIds=explode(',',Request::all()['country_ids']);
}
// if(isset(Request::all()['country_id'])){
//   $paramCountryIds[]=Request::all()['country_id'];
// }
if(isset(Request::all()['city_ids'])){
  $paramCityIds=explode(',',Request::all()['city_ids']);
}
// if(isset(Request::all()['city_id'])){
//   $paramCityIds[]=Request::all()['city_id'];
// }
if(isset(Request::all()['industry_ids'])){
  $paramIndustryIds=explode(',',Request::all()['industry_ids']);
}
// if(isset(Request::all()['industry_id'])){
//   $paramIndustryIds[]=Request::all()['industry_id'];
// }
@endphp
<section id="inner-banner" style="background: url({{url('public/images/inner-banner.jpg')}}) no-repeat center top;">
<div class="overlay"></div>
 @include('front_panel/includes/search_job_section')
</section>
<section class="main-inner-page">
  <section id="search-section">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-12">
            <div class="top-block col-lg-12">
              <div class="pull-left">
                <h5>You Searched : </h5>
                <div>
                  @php
                  $c=[];
                    if(Request::get('key_skills')){
                      $c=Request::get('key_skills');
                      $c=explode(',',$c);
                    }
                    if(!empty($c)){
                    foreach($c as $k=>$val){
                      echo '<span class="label label-info">'.$val.'</span>';
                    }
                    }
                  @endphp

                </div>
                <div id='search_key'>  </div>
              </div>
              
              <div class="clearfix text-right">
               <span id="total_get_jobs">0 Job </span>
                {{-- <div class="shorting-field">
                  <select>
                    <option value="" style="display: none;">Short By</option>
                    <option value="">Ascending Order</option>
                    <option value="">Descending Order</option>
                  </select>
                </div> --}}
              </div>
            </div>
          
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-3 sidebar-parent">
          <div class="left-sidebar">
            {{ Form::open(array('url'=>route('search-jobs'),'method'=>'GET','class'=>'','id'=>'filter_form'))}}
              <div class="search">
                <h4 class="text-uppercase">Refine Search</h4>
              </div>
           
              <div class="country-filter filter-block">
                <h4 class="btn {{(!empty($paramCountryIds)) ? '' : 'collapsed'}}" data-toggle="collapse" data-target="#country">Jobs By Country</h4>
                <div id="country" class="collapse {{(!empty($paramCountryIds)) ? 'in' : ''}}">
                  <ul>
                  	@foreach($country_list as $k=>$val)
                     <li>
                      <label class="checkbox-btn">
                        @if(in_array($val->id, $paramCountryIds))
                          <input type="checkbox" name="country_ids" value="{{$val->id}}" class="checkbox" checked/> 
                        @else
                          <input type="checkbox" name="country_ids" value="{{$val->id}}" class="checkbox"/> 
                        @endif
                        <b class="listbo">{{$val->name}}</b>
                      </label>
                      <div class="count">({{count($val->job_post)}})</div>
                    </li>
                    @endforeach
                  </ul>
                </div>
              </div>
              <div class="city-filter filter-block">
                <h4 class="btn {{(!empty($paramCityIds)) ? '' : 'collapsed'}}" data-toggle="collapse" data-target="#city">Jobs By City</h4>
                <div id="city" class="collapse {{(!empty($paramCityIds)) ? 'in' : ''}}">
                 <ul>
                  @foreach($city_list as $k=>$val)
                     <li>
                      <label class="checkbox-btn">
                        @if(in_array($val->id, $paramCityIds))
                          <input type="checkbox" name="city_ids" value="{{$val->id}}" class="checkbox" checked/> 
                        @else
                          <input type="checkbox" name="city_ids" value="{{$val->id}}" class="checkbox"/> 
                        @endif
                        <b class="listbo">{{$val->name}}</b>
                      </label>
                      @php
                      $total_jobs=0;
                      if($val->active_jobs!=null){                        
                          foreach($val->active_jobs as $vals){
                            if($vals->jobs!=null ){
                                $total_jobs=$total_jobs+1;
                            }
                            
                          }
                      }
                      @endphp
                      <div class="count">({{$total_jobs}})</div>
                    </li>
                    @endforeach
                  </ul>
                </div>
              </div>
              
              <div class="industry-filter filter-block">
                <h4 class="btn {{(!empty($paramIndustryIds)) ? '' : 'collapsed'}}" data-toggle="collapse" data-target="#industry">Jobs By Industry</h4>
                <div id="industry" class="collapse {{(!empty($paramIndustryIds)) ? 'in' : ''}}">
                  <ul>
                      @foreach($industry_list as $k=>$val)
                     <li>
                      <label class="checkbox-btn">
                        @if(in_array($val->id, $paramIndustryIds))
                          <input type="checkbox" name="industry_ids" value="{{$val->id}}" class="checkbox" checked/> 
                        @else
                          <input type="checkbox" name="industry_ids" value="{{$val->id}}" class="checkbox"/> 
                        @endif
                        <b class="listbo">{{$val->name}}</b>
                      </label>
                      <div class="count">({{count($val->job_post)}})</div>
                    </li>
                    @endforeach
                  </ul>
                </div>
              </div>
            
              {{-- <div class="">
                <button type="submit" class="btn btn-info btn-sm btn-refine">Refine</button>
              </div> --}}

            {{ Form::close() }}
          </div>
        </div>
        <div id="load" style="position: relative;">
            @include('front_panel.job.job_listing_paginate')
        </div>
      </div>
    </div>
    <!-- </div> -->
  </section>
</section>
<script>
  var url = base_url;
  $(document).on('click','.custom-pagination a',renderJobs);

  $(document).on('click','.checkbox',function(e){
     
    var form = $('#filter_form').serializeArray();
    var industry_ids = [];
    var city_ids = [];
    var country_ids = [];
    $.each(form,function(index,value){
      console.log(value);
      if(value.name=='country_ids'){
        country_ids.push(value.value);
      }else if(value.name=='city_ids'){
        city_ids.push(value.value);
      }else if(value.name=='industry_ids'){
        industry_ids.push(value.value);
      }
    });
    url=base_url+'search-jobs?country_ids='+country_ids+'&city_ids='+city_ids+'&industry_ids='+industry_ids;
    window.history.pushState("", "", url);
    var data = {'country_ids':country_ids.join(),'city_ids':city_ids.join(),'industry_ids':industry_ids.join()};
    ajaxCall(data);
  });
  
  

  function renderJobs(e){
      e.preventDefault();
      //$('#load').append('<img class="" src="'+global_var.loader+'" />');
      url = $(this).attr('href'); 
      ajaxCall();
      window.history.pushState("", "", url);
  }
  function ajaxCall(data=NaN){
    $.ajax({
          url : url,
          data : data,
      }).done(function (data) {
        console.log(data);
          $('#load').html(data); 
      }).fail(function () {
        
      }).complete(function(){
            });

     //window.history.pushState("", "", url);
  }
  
</script>
@endsection