<div class="col-xs-12 col-sm-9">
          @forelse($jobs as $k=>$val)
            <div class="col-xs-12 col-md-12 nopadding">
              <div class=" job-box">
                <div class="row flexbox">
                  <div class="col-xs-3 col-sm-2 left">
                    <div class="div-table">
                      <div class="table-cell-div">
                        @if($val->employer_info!=null)
                          <img src="{{checkFile($val->employer_info->comp_logo,'uploads/employer_logo/','company_logo.png')}}" class="center-block">
                        @else 
                          <img src="{{url('public/images/default/company_logo.png')}}" class="center-block">
                        @endif

                      </div>
                    </div>
                  </div>
                  <div class="col-xs-9 col-sm-10 right">
                    <h2><a href="{{route('job-details',['jobid'=>$val->id])}}">{{$val->job_title}}</a></h2>
                    <h5>{{ ($val->employer_info!=null) ? $val->employer_info->comp_name : '' }}</h5>
                    <div class="post-details">
                      <p>{{$val->key_skill}}</p>
                      <p> 
                        <span><i class="fa fa-suitcase" aria-hidden="true"></i>{{$val->min_experience}}-{{$val->max_experience}} years</span> 
                        <span><i class="fa fa-map-marker" aria-hidden="true"></i>{{-- {{$val->job_in_city}} -  --}}{{ ($val->country!=null) ? $val->country->name : '' }}</span>
                      </p>
                    </div>
                  </div>
                </div>
                <div class="job_optwrap">
                  <p>Posted : {{dateConvert($val->created_at,'jS M Y')}}</p>
                </div>
              </div>
            </div> 
          @empty 
              <div class="col-xs-12 col-md-12 nopadding">
              <div class=" job-box">
                <div class="row flexbox">
                  
                  <div class="col-xs-12 col-sm-12">
                    
                    <img src="{{url('public/images/default/nojobsfound.png')}}" class="center-block" width="240px" style="margin-top: 5%;">
                    <div class="post-details">
                      
                    </div>
                  </div>
                </div>
                
              </div>
            </div> 
          @endforelse
        <div class="custom-pagination">
            {{ $jobs->links() }}
        </div>
        
        </div>
@php
$totalRec = count($jobs);
  if($totalRec>0){
    $totalRec = $jobs->firstItem().' - '.$jobs->lastItem().' of '.$jobs->total();
  }
@endphp
<script>

  var records_of ='{{$totalRec}}';
$('#total_get_jobs').html('<span>'+records_of+' Job </span>');
var search='{{$search_tag}}';
var array = search.split(',');

if(search)
{
  $('#search_key').html('');
  $.each( array, function( key, value ) {
    if(value )
    $('#search_key').append('<span class="label label-info">'+value+'</span>');
  });
}

</script>