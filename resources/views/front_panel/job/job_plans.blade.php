@extends('/layouts/front_panel_master')
@section('content')
@include('front_panel/includes/page_banner')

      <section id="pricing-1" >
        <div class="container">
            <div class="pricing-inner">
                <div class="title common text-center">
                    <h2>choose your plan</h2>
                </div>
                <div class="main">
                    <div class="row">
                        <div class="membership-pricing-table">
                            <table>
                                <tbody>
                                    <tr>
                                        <th class="plan-header plan-header-blue">
                                            <div class="pricing-plan-name">FEATURES</div>
                                        </th>
                                        <th class="plan-header plan-header-free">
                                            <div class="pricing-plan-name">BASIC</div>
                                        </th>
                                        <th class="plan-header plan-header-standard">
                                            <div class="header-plan-inner">
                                                <span class="recommended-plan-ribbon">RECOMMENDED</span>
                                                <div class="pricing-plan-name">PREMIUM</div>
                                            </div>
                                        </th>
                                    </tr>
                                @foreach($features as $k=>$val)
                                <tr>
                                    <td>{!! $val->feature_title !!}:</td>
                                    <td>
                                        @if($val->val_type==0)
                                            {{ $val->basic_benefits}}
                                        @else
                                            {!! ($val->basic_benefits==1) ? '<span class="icon-yes fa fa-check fa-2x"></span>' : '<span class="icon-no fa fa-times"></span>' !!}
                                        @endif
                                    </td>
                                    <td>
                                        @if($val->val_type==0)
                                            {{ $val->premium_benefits}}
                                        @else
                                            {!! ($val->premium_benefits==1) ? '<span class="icon-yes fa fa-check"></span>' : '<span class="icon-no fa fa-times"></span>' !!}
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                               
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<div class="whyus-area">
<div class="container">
  <div class="row">
    <div class="col-lg-6 col-lg-offset-6 col-md-6 col-md-offset-6 col-sm-offset-6 col-sm-6 col-xs-12">
      <div class="whyus-content">
        <h2>Why Choose Jobs4Gulf ?</h2>
        <h3>More Candidates</h3>
        <p>149,000+ new registration every month</p>
        <h3>More Response</h3>
        <p>3.5 million applications a month</p>
        <h3>More Relevance</h3>
        <p>Target the best candidates quickly</p>

      </div>
    </div>
  </div>
</div>
</div>


@endsection