@extends('/layouts/front_panel_master')
@section('content')

@include('front_panel/includes/page_banner')

<section class="main-inner-page lite-greyBg">
 
    <div class="container">
      <div class="title-style-3 clearfix text-center">
      {{-- <h4>In Middle East</h4> --}}
    </div>
    @if($title=='Country')
      <div class="row">
        @foreach($list as $val)
          <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3 category-parent-box">
            <div class="country-block">
              <a href="{{url('search-jobs?country_ids='.$val->id)}}" >
                {{-- <i class="flag flag-AE"></i> --}}
                @if($val->icon!='')
                  <img class="flag" alt="image" src="{{$val->icon}}">
                @else
                  <img class="flag" alt="image" src="{{checkFile($val->icon,'uploads/country_img/','small-logo.png')}}">
                @endif
                <h4>{{$val->name}}</h4>
                <h4>({{ (isset($val->job_post) && $val->job_post!=null) ? count($val->job_post) : 0 }})</h4>
              </a>
            </div>
          </div>
        @endforeach
      </div>
    @endif
    @if($title=='City')
      <div class="row">
        @foreach($list as $val)
          <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3 category-parent-box">
            <div class="country-block">
              <a href="{{url('search-jobs?city_ids='.$val->id)}}" >
                <i class="fa  fa-building-o  fa-AE"></i>
                <h4>{{$val->name}}</h4>
                <h4>({{ (isset($val->job_post) && $val->job_post!=null) ? count($val->job_post) : 0 }})</h4>
              </a>
            </div>
          </div>
        @endforeach
      </div>
    @endif
    @if($title=='Industry')
      <div class="row">
        @foreach($list as $val)
          <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3 category-parent-box">
            <div class="country-block">
              <a href="{{url('search-jobs?industry_ids='.$val->id)}}" >
                <i class="fa  fa-industry  fa-AE"></i>
                <h4>{{$val->name}}</h4>
                <h4>({{ (isset($val->job_post) && $val->job_post!=null) ? count($val->job_post) : 0 }})</h4>
              </a>
            </div>
          </div>
        @endforeach
      </div>
    @endif

      </div>
<br><br>
     {{--  <div class="container">
      <div class="title-style-3 clearfix text-center">
      <h4>Outside Middle East</h4>
    </div>
      <div class="row">
      <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3 category-parent-box">
        <div class="country-block">
          <a href="#" >
            <i class="flag flag-IN"></i>
            <h4>India</h4>
            <h4>(1000)</h4>
          </a>
        </div>
      </div>
      </div>
    </div>
 --}}
    </div>
</section>

@endsection