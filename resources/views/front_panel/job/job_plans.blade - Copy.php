@extends('/layouts/front_panel_master')
@section('content')
@include('front_panel/includes/page_banner')

      <section id="pricing-1" >
        <div class="container">
            <div class="pricing-inner">
                <div class="title common text-center">
                    <h2>choose your plan</h2>
                </div>
                <div class="main">
                    <div class="row">
                        @foreach($plan_list as $k=>$val)
                            @php
                                $plans_data[$val->id]=['price'=>$val->price,'offer_price'=>$val->price];

                                $ids=$units=[];
                                if($val->features!=null){
                                  foreach($val->features as $f_val){
                                    $ids[]=$f_val->feature_id;  
                                    $units[$f_val->feature_id]=$f_val->units;                                          
                                  }
                                }
                            @endphp 
                        {{ Form::open(array('url'=>route('purchase-plan'),'class'=>'job-planlist-form'))}}
                        {{ Form::hidden('plan_id',$val->id)}}    
                        <div class="col-md-4 col-sm-6">
                            <div class="item">
                                <div class="item-title text-center">
                                    <h2>{{$val->name}}</h2>
                                </div>
                                <div class="item-price">
                                    <div class="item-bor">
                                        <div class="price-inner">
                                            <div class="text">
                                                <span>${{$val->price}}</span>
                                            </div>
                                        </div>                                        
                                    </div>
                                </div>
                               <div class="item-offers">
                                    <div class="adverstise-basket-controller">
                                        @if($val->plan_type==1)
                                            <span class="basket-final-amount pull-left">Free</span>
                                        @else 
                                            <span class="basket-input-minus" data-plan-id={{$val->id}}><i class="fa fa-minus"></i></span>
                                            <input name="jobs" type="number" value="1" class="basket-input-number" readonly="type">
                                            <span class="basket-input-plus" data-plan-id={{$val->id}}><i class="fa fa-plus"></i></span>
                                            <span class="basket-final-amount" id="plan_{{$val->id}}">${{$val->price}}</span>
                                        @endif
                                        
                                    </div>
                                    <p class="jobads-content-text">&nbsp;
                                        <span class="info_msg_{{$val->id}} hide-elem">To buy more than 20 jobs, call <strong class="contact_no_job">0845 241 9293</strong></span>
                                    </p>
                                    <hr/>
                                </div>                             
                                    <div class="item-offers">
                                        <ul class="no-style">
                                            @foreach(config('constants.jobplan_features') as $vals)
                                                <li>
                                                    @if(in_array($vals['id'],$ids))
                                                        <span class="icon yes fa fa-check"></span>
                                                    @else
                                                        <span class="icon no">&times;</span> 
                                                    @endif
                                                    {{str_replace(['##VALUE##'], array(@$units[$vals['id']]), $vals['name'])}}
                                                </li>
                                            @endforeach                                           
                                        </ul>
                                    </div>
                                
                                <div class="get-btn">
                                    @if($val->plan_type!=1)
                                        <button type="submit" class="btn btn-default">Post Job</button>
                                    @else
                                        @if(Auth::check())
                                            <a href="{{route('job-post')}}" class="btn btn-default">Post Job</a>
                                        @else
                                            <a href="{{route('free-job-post')}}" class="btn btn-default">Post Job</a>
                                        @endif
                                    @endif
                                    
                                </div>
                            </div>
                        </div>
                        {{ Form::close() }}
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </section>
<div class="whyus-area">
<div class="container">
  <div class="row">
    <div class="col-lg-6 col-lg-offset-6 col-md-6 col-md-offset-6 col-sm-offset-6 col-sm-6 col-xs-12">
      <div class="whyus-content">
        <h2>Why Choose Jobs4Gulf ?</h2>
        <h3>More Candidates</h3>
        <p>149,000+ new registration every month</p>
        <h3>More Response</h3>
        <p>3.5 million applications a month</p>
        <h3>More Relevance</h3>
        <p>Target the best candidates quickly</p>

      </div>
    </div>
  </div>
</div>
</div>

<script>
var plansData={!! json_encode($plans_data) !!};
$('.basket-input-plus').click(function(){
    var planId=$(this).data('plan-id');
    var inputVal=parseInt($(this).siblings('.basket-input-number').val());
    if(inputVal==20){
        $('.info_msg_'+planId).show();
    } else {
        var inputValPlus=inputVal+1;
        var amount = inputValPlus*parseInt(plansData[planId].offer_price);
        $(this).siblings('.basket-input-number').val(inputValPlus);
        $('#plan_'+planId).text('$'+amount);
        $('.info_msg_'+planId).hide();
    }
});
$('.basket-input-minus').click(function(){
    var planId=$(this).data('plan-id');
    var inputVal=parseInt($(this).siblings('.basket-input-number').val());
    var inputValMinus=(inputVal>1) ? inputVal-1 : 1;
    var amount = inputValMinus*parseInt(plansData[planId].offer_price);
    $(this).siblings('.basket-input-number').val(inputValMinus);
    $('#plan_'+planId).text('$'+amount);
    $('.info_msg_'+planId).hide();
});
</script>

@endsection