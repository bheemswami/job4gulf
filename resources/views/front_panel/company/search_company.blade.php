
@extends('/layouts/front_panel_master')
@section('content')
  <section id="inner-banner" style="background: url({{url('public/img/ban.jpg')}}) no-repeat center top;">
<div class="overlay"></div>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1">
        <form action="" method="GET" autocomplete="off" id="search-job" class="inner-search-form">
          <div class="col-xs-12 col-sm-5 form-block">
            <input type="text" placeholder="Job Title, Keywords or Company" name="key_skill" id="skills" value="" class="job-title">
          </div>
          <div class="col-xs-12 col-sm-5 form-block">
            <input type="text" placeholder="City, State, or Zip" name="location" id="location" value="" class="job-location">
          </div>
          <div class="col-xs-12 col-sm-2 form-block">
            <button type="search" class="">Find Jobs</button>
          </div>
        </form>
        
      </div>
    </div>
  </div>
</section>
<section style="margin-bottom: 0px; margin-top: 40px;">
  <div class="container">
    <div class="row">
      <h2 class="title-widget-sidebar note2 color-is" style="color:black;margin-bottom: 42px;">FILTERED BY</h2> 
      <div class="col-md-3">
        <div class="left-sidebar">
          <form method="post" action="" id="filter_form">
              <div class="search">
                <h4 class="text-uppercase">Search</h4>
                <input type="text" name="key_skill" value="" class="form-control"/>
              </div>
              
              <div class="country-filter filter-block">
                <h4>Country</h4>
                <div class="">
                  <ul>
                    @foreach($country_list as $k=>$val)
                     <li>
                      <label class="checkbox-btn">
                        <input type="checkbox" id="" name="country_ids[]" value="{{$val->id}}" class=""/> <b class="listbo">{{$val->name}}</b>
                      </label>
                      <div class="count">(22)</div>
                    </li>
                    @endforeach
                  </ul>
                </div>
              </div>
              
              <div class="city-filter filter-block">
                <h4>City</h4>
                <div class="">
                 <ul>
                     @foreach($city_list as $k=>$val)
                     <li>
                      <label class="checkbox-btn">
                        <input type="checkbox" id="" name="city_ids[]" value="{{$val->id}}" class=""/> <b class="listbo">{{$val->name}}</b>
                      </label>
                      <div class="count">(22)</div>
                    </li>
                    @endforeach
                  </ul>
                </div>
              </div>
              
              <div class="city-filter filter-block">
                <h4>Industry</h4>
                <div class="">
                  <ul>
                      @foreach($industry_list as $k=>$val)
                     <li>
                      <label class="checkbox-btn">
                        <input type="checkbox" id="" name="industry_ids[]" value="{{$val->id}}" class=""/> <b class="listbo">{{$val->name}}</b>
                      </label>
                      <div class="count">(22)</div>
                    </li>
                    @endforeach
                  </ul>
                </div>
              </div>
            </form>
          </div>              
    </div>
    <div class="col-md-9" style="border:1px solid #eee;border-radius: 4px; background-color: #fff;">    
      <div class="tabbed-about-us tabbed-about-us-v2">
          <div class="row">
            <div class="col-sm-12">
              <ul class="tabs-nav comp_by_cat" role="tablist">
                
              </ul>
            </div> 
          </div>
        </div>
    </div>
    </div>
    </div>
    </section>

    <section style="margin-top: 26px;margin-bottom: 197px;">
      <div class="container">
        <!-- ========== SECTION HEADER ========== -->
        <h2 class="title-widget-sidebar note2 color-is" style="color:black;    margin-bottom: 42px;">LATEST ARTICLES</h2>
        <div class="row">
          <!-- SINGLE-POSTS -->
          <div class="col-md-12 col-lg-6" style="margin-bottom: -38px;">
            <div class="row">
              <div class="col-sm-6" style="margin-bottom: -38px;">
                <!-- single-post -->
                <article class="single-post small post-details-effect single-post-index">
                  <div class="img-wrapper">
                    <a href="single-v2.html"><img src="{{url('public/img/1.jpg')}}" alt="img"></a>
                  </div>
                  <div class="info">
                    <p class="tag">science</p>
                    <a href="single-v2.html" class="title">
                      Lose yourself in videos of people burning things ily phone lets kids.
                    </a>
                    <div class="article-info">
                      <ul>
                        <li class="author-name">By <a href="#">Jamie Doe</a></li>
                        <li>May 20, 2015</li>
                      </ul>
                    </div> <!-- //.article-info -->
                  </div> <!-- //.info -->
                </article> <!-- //single-post -->
              </div> <!-- //.col-sm-6 -->

              <div class="col-sm-6" style="    margin-bottom: -38px;">
                <!-- single-post -->
                <article class="single-post small post-details-effect single-post-index">
                  <div class="img-wrapper">
                    <a href="single-v2.html"><img src="{{url('public/img/1.jpg')}}" alt="img"></a>
                  </div>
                  <div class="info">
                    <p class="tag">Life style</p>
                    <a href="single-v2.html" class="title">
                      The Ily phone lets kids make video calls Lose yourself in videos.
                    </a>
                    <div class="article-info">
                      <ul>
                        <li class="author-name">By <a href="#">Jamie Doe</a></li>
                        <li>May 20, 2015</li>
                      </ul>
                    </div><!-- //.article-info -->
                  </div> <!-- //.info -->
                </article> <!-- //single-post -->
              </div> <!-- //.col-sm-6 -->

              <div class="col-sm-6">
                <!-- single-post -->
                <article class="single-post small post-details-effect single-post-index">
                  <div class="img-wrapper">
                    <a href="single-v2.html"><img src="{{url('public/img/2.jpg')}}" alt="img"></a>
                  </div>
                  <div class="info">
                    <p class="tag">Reviews</p>
                    <a href="single-v2.html" class="title">
                      Syfy is working on a series about Superman's home Lose yourself in videos.
                    </a>
                    <div class="article-info">
                      <ul>
                        <li class="author-name">By <a href="#">Jamie Doe</a></li>
                        <li>May 20, 2015</li>
                      </ul>
                    </div><!-- //.article-info -->
                  </div> <!-- //.info -->
                </article> <!-- //single-post -->
              </div> <!-- //.col-sm-6 -->

              <div class="col-sm-6">
                <!-- single-post -->
                <article class="single-post small post-details-effect single-post-index">
                  <div class="img-wrapper">
                    <a href="single-v2.html"><img src="{{url('public/img/1.jpg')}}" alt="img"></a>
                  </div>
                  <div class="info">
                    <p class="tag">Tech</p>
                    <a href="single-v2.html" class="title">
                      Apple Watch one year in: My (kinda sorta) everyday phone lets kids.
                    </a>
                    <div class="article-info">
                      <ul>
                        <li class="author-name">By <a href="#">Jamie Doe</a></li>
                        <li>May 20, 2015</li>
                      </ul>
                    </div><!-- //.article-info -->
                  </div> <!-- //.info -->
                </article> <!-- //single-post -->
              </div> <!-- //.col-sm-6 -->
            </div> <!-- //row -->
          </div> <!-- //.col-md-12 col-lg-6 -->
          
          <!-- FEATURED-POST -->
          <div class="col-md-12 col-lg-6">
            <!-- featured-post -->
            <article class="featured-post single-post">
              <div class="img-wrapper">
                <a href="single-v2.html"><img src="{{url('public/img/Business-Photos-1.jpg')}}" alt="img"></a>
              </div>
              <div class="info">
                <p class="tag">Nature</p>
                <a href="single-v2.html" class="title">
                  Hyperloop Transportation says it will use a ‘cheaper, safer’ form of magnetic levitation.
                </a>
                <div class="article-info">
                  <ul>
                    <li class="author-name">By <a href="#">Jamie Doe</a></li>
                    <li>May 20, 2015</li>
                  </ul>
                </div><!-- //.article-info -->
              </div> <!-- //.info -->
            </article> <!-- //featured-post -->
          </div> <!-- //.col-md-12 col-lg-6 -->
          
        </div> <!-- //.row -->
      </div> <!-- //container -->
    </section>

  <script type="text/javascript">
  $(document).ready(function(e) {
      getCompList();
  });

  $("#filter_form").on("change", "input:checkbox", function(e){
      getCompList();
  });
  function getCompList(){
      globalFunc.ajaxCall('api/filter-employer', $('#filter_form').serialize(), 'POST', globalFunc.before, globalFunc.listOfCompanies, globalFunc.error, globalFunc.complete);
  }
</script>
@endsection