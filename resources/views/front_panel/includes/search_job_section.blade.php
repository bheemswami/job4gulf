@php
if(!isset($form_class)){
  $form_class='';
}
$country = getAllCountries();
 foreach($country as $val){
     $all_countries[$val->id]=$val->name;
    if($val->is_gulf==1){
      $gulf_countries[$val->id]=$val->name;
    }
  }
@endphp
@if(Request::segment(1)==null)
    <div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1 banner-form">
        <div class="search-header"><h2>SEARCH JOBS</h2></div>
        {{ Form::open(array('url'=>route('search-jobs'),'class'=>$form_class,'method' =>'GET','id'=>'search-job'))}}
              <div class="col-xs-12 col-sm-4 form-block nopadding">
                {{ Form::text('key_skills',null,['id'=>'skill','class'=>'job-title','placeholder'=>'Job Title, Keywords']) }}
              </div>
              <div class="col-xs-12 col-sm-3 form-block nopadding">
                {{ Form::select('exp_year',getDigitRange(0,5),null,['id'=>'exp','class'=>'job-exp','placeholder'=>'Exp (Years)']) }}
              </div>
              <div class="col-xs-12 col-sm-3 form-block nopadding">
                {{ Form::select('country_ids',['Gulf Country'=>$gulf_countries,'All Country'=>$all_countries],null,['id'=>'location','class'=>'job-location','placeholder'=>'Location']) }}
              </div>
              <div class="col-xs-12 col-sm-2 form-block nopadding">
                <button type="search" class="">Find Jobs</button>
              </div>
        {{ Form::close() }}
    </div>  
@else
 <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1">        
        {{ Form::open(array('url'=>route('search-jobs'),'class'=>$form_class,'method' =>'GET','id'=>'search-job'))}}
          <div class="col-xs-12 col-sm-4 form-block nopadding">
            {{ Form::text('key_skills',null,['id'=>'skill','class'=>'job-title','placeholder'=>'Enter keywords (e.g. Architect )']) }}
          </div>
          <div class="col-xs-12 col-sm-3 form-block nopadding">
            {{ Form::select('exp_year',getDigitRange(0,5),null,['id'=>'exp','class'=>'job-exp','placeholder'=>'Exp (Years)']) }}
          </div>
          <div class="col-xs-12 col-sm-3 form-block nopadding">
            {{ Form::select('country_ids',['Gulf Country'=>$gulf_countries,'All Country'=>$all_countries],null,['id'=>'location','class'=>'job-location','placeholder'=>'All Countries']) }}
          </div>
          <div class="col-xs-12 col-sm-2 form-block nopadding">
            <button type="search" class="">Find Jobs</button>
          </div>
        {{ Form::close() }}
        
      </div>
    </div>
  </div>
@endif