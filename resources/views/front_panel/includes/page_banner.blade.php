@php
  $banner = getPageBanner(Request::path(),Request::segment(1));
@endphp
<section id="inner-banner" class="custom-banner" style="background: url({{checkFile($banner->image,'/uploads/banner_img/','default_banner.png')}}) no-repeat center top;">
     {{-- <div class="overlay"> --}}
        <div class="container">
        <div class="banner-contents">
                <h1>{{$banner->title}}</h1>
                <span class="banner-text-fs_medium">{{$banner->description}}</span>
                <span class="banner-text-fs_small">{{$banner->description_2}}</span>
                <div class="banner-callus-button">Questions? Call us: </div>
        </div>
    </div>
  {{-- </div> --}}
</section>