{{-- /* start Job Post Form JS */ --}}
<script>
  var loc=0;
  var countryOptions = '';
  $(document).ready(function(){
    initCityMultiselect('c_city0');
    //***** start create country list select box *****//
        var array1 = {!! json_encode($gulfCountry) !!};
        var array2 = {!! json_encode($otherCountry) !!};
        
        //countryOptions+='<select name="candidate_current_country[]" class="form-control curr_loc" placeholder="Select Country">';
          countryOptions+='<option>Select Country</option>';
          countryOptions+='<optgroup label="Gulf Country">';
            $.each(array1,function(index,val){
                countryOptions+='<option value="'+index+'">'+val+'</option>';
            });
          countryOptions+='</optgroup>';

          countryOptions+='<optgroup label="Other Country">';
            $.each(array2,function(index,val){
                countryOptions+='<option value="'+index+'">'+val+'</option>';
            });
          countryOptions+='</optgroup>';
        //countryOptions+='</select>';
    //***** end create country list select box *****//
  });

  $('#exp_year_from').change(function(){
      var array = JSON.parse('{!! json_encode(getDigitRange(0,30,'year')) !!}');
      var value = parseInt($(this).val());

      $('#exp_year_to').find('option').not(':first').remove();
      $.each(array,function(index,val){
        if(value <= index){
          $('#exp_year_to').append('<option value="'+index+'">'+val+'</option>');
        }
      });
  });
</script>
<script>
  $('#key_skills').selectize({
      plugins: ['remove_button'],
      maxItems: 15,
      valueField: 'id',
      labelField: 'name',
      searchField: 'name',
      create: false,
      options: {!! json_encode($skill_keywords) !!},
    
  });
 
  $('.res_manage').click(function(e){
    $('.manage_sec_1,.manage_sec_2,.manage_sec_3').hide();
    switch($(this).val()){
      case "Email":
        $('.manage_sec_1').show();
        break;
      case "ContactDetails":
        $('.manage_sec_2').show();
        break;
      case "Walkin":
        $('.manage_sec_3').show();
        break;
      case "All":
        $('.manage_sec_1,.manage_sec_2,.manage_sec_3').show();
        break;
    }
  });

  $('#qualifications').multiselect({
     texts: {
        placeholder    : 'Select Qualification',
      },
      onOptionClick : function( element, option ){
        globalVar.qIds=$(element).val();
        $.ajax({
                url: base_url+'api/specialization-list',
                type: 'POST',
                dataType: 'json',
                data: {
                    course_ids: globalVar.qIds,
                },           
                success: function(res) {
                    $('#specialization').find('option').remove();
                    $('#specialization').find('optgroup').remove();
                      if(res){
                        var str='';
                          $.each(res,function(index,val){
                            if(val.specialization.length>0){
                              str+='<optgroup label="'+val.name+'">';
                                $.each(val.specialization,function(index2,val2){
                                  str+='<option value="'+val2.id+'~'+val.id+'">'+val2.name+'</option>';
                                });
                              str+='</optgroup>';
                            }
                          });
                          $('#specialization').append(str);
                          $('#specialization').multiselect( 'reload' );
                    }
                },
                error: function(error) {
                   
                }
            });
      },
  });

  $('#specialization').multiselect({
     texts: {
        placeholder    : 'Select Specialization',
      },
  });
  $('#nationality').multiselect({
     texts: {
        placeholder    : 'Select Nationality',
      },
  });

  $('#job_country').change(function(){
        globalVar.jobCountryId = $(this).val();
        var selectize = $('#job_city')[0].selectize; 
        selectize.clear();
        selectize.clearOptions();
        selectize.renderCache['option'] = {}; 
  });

  $('#job_city').selectize({
      plugins: ['remove_button'],
      maxItems: 5,
      valueField: 'id',
      labelField: 'name',
      searchField: 'name',
      create: false,
      closeAfterSelect: true,
      options: [],
      render: {
          option: function(item, escape) {           
              return '<div>' +escape(item.name)+'</div>';
          }
      },
      load: function(query, callback) {
          if (!query.length) return callback();
          if(query.length>=1){
           $.ajax({
                url: base_url+'/api/city-suggestion',
                type: 'GET',
                dataType: 'json',
                data: {
                    country_id: globalVar.jobCountryId,
                    q: query
                },           
                success: function(res) {
                    callback(res);
                },
                error: function(error) {
                    callback();
                }
            });
          } else {
            var selectize =$('#job_city')[0].selectize; 
           selectize.renderCache['option'] = {}; 
           return callback();
          }
      }
  });

  $(document).on('change','.curr_loc',function(){
        var elem_id = $(this).parents('div').siblings('div').children('.curr_city').attr('id');
        cityListByCountry($(this).val(),'#'+elem_id); 
  });
  function cityListByCountry(country_id=0,elem_id){
     $.ajax({
          url: base_url+'/api/city-suggestion',
          type: 'GET',
          dataType: 'json',
          data: {
              by_country: 1,
              country_id: country_id,
          },           
          success: function(res) {
               $(elem_id).find('option').remove();
               if(res){
                var str='';
                $.each(res,function(index,val){
                  str+='<option value="'+val.id+'">'+val.name+'</option>';
                });
                $(elem_id).append(str);
                $(elem_id).multiselect( 'reload' );
            }
          },
          error: function(error) { }
      });
  }

  $('.add_more').on('click',function(){
      var elem = $(this).siblings('div').children('.curr_city').attr('class');
      addMoreLocation();
  });

  function addMoreLocation(){
      loc=loc+1;
      if(loc>10){
        alert('Allow Only 10 Current Location of the Candidate.');
      } else {
        var id ='c_city_'+loc;
        var str=' <div class="form-group row" id="d_'+loc+'">\
                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label"></label>\
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">\
                     <select name="candidate_current_country['+loc+']" class="form-control curr_loc" placeholder="Select Country">\
                     '+countryOptions+'\
                     </select>\
                    </div>\
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">\
                     <select name="candidate_current_city['+loc+'][]" id="'+id+'" class="form-control curr_city" multiple></select>\
                    </div>\
                    <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1 rmv" data-id="d_'+loc+'"><i class="fa fa-minus fa-icon-red" aria-hidden="true"></i></div>\
                  </div>';
          $('.current_location_section').append(str);
          initCityMultiselect(id);
      }
  }

  function initCityMultiselect(elem=null){
      $('#'+elem).multiselect({
         texts: { placeholder : 'Select City' }
      });
  }

  $(document).on('click','.rmv',function(){
   $('#'+$(this).data('id')).remove();
  });


  $( function() {
      var dateFormat = "dd-mm-yy",
      from = $( "#wdf" ).datepicker({
          defaultDate: "+1w",
          dateFormat: "dd-mm-yy",
          changeMonth: true,
          numberOfMonths: 1,
          minDate:'0',
      })
      .on( "change", function() {
            to.datepicker( "option", "minDate", getDate( this ) );
       }),
       to = $( "#wdt" ).datepicker({
          defaultDate: "+1w",
          dateFormat: "dd-mm-yy",
          changeMonth: true,
          numberOfMonths: 1
      })
        .on( "change", function() {
          from.datepicker( "option", "maxDate", getDate( this ) );
        });
   
      function getDate( element ) {
        var date;
        try {
          date = $.datepicker.parseDate( dateFormat, element.value );
        } catch( error ) {
          date = null;
        }
   
        return date;
      }
  });
</script>

{{-- /* End Job Post Form JS */ --}}

{{-- /* start Employer Reg Form JS */ --}}
  
<script>
  $('.emp_registered_rbtn').click(function(e){
      if($(this).val()==0){
        $('.emp_no_registered').show();
        $('.emp_yes_registered').hide();
      } else {
        $('.emp_no_registered').hide();
        $('.emp_yes_registered').show();

      }
  });
  $(document).on('click','#signin_now',function(){
      $("input[name=emp_registered][value='1']").prop("checked",true);
      $('.emp_no_registered').hide();
      $('.emp_yes_registered').show();
  });
 $("#comp_logo").change(function(){
    previewImage(this,'.user-default','{{url('public/images/default/no_logo.png')}}');
  });
  $('.rmv_img').click(function () {
    $("#comp_logo").val(null);
    $(".user-default").attr('src','{{url('public/images/default/no_logo.png')}}');
  });

  $('#designation').selectize({
        maxItems: 1,
        valueField: 'id',
        labelField: 'name',
        searchField: 'name',
        create: false,
        options: [],
        render: {
            option: function(item, escape) {           
                return '<div>' +escape(item.name)+'</div>';
            }
        },
        load: function(query, callback) {
            if (!query.length) return callback();
            $.ajax({
                url: base_url+'/api/designation-suggestion',
                type: 'GET',
                dataType: 'json',
                data: {
                    q: query,
                },           
                success: function(res) {
                    callback(res);
                },
                error: function(error) {
                    callback();
                }
            });
        }
    });
  $('#comp_country').change(function(){
    globalVar.countryIdComp = $(this).val();
    var selectize = $("#comp_city")[0].selectize; 
    selectize.clear();
    selectize.clearOptions();
    selectize.renderCache['option'] = {}; 
});

$('#comp_city').selectize({
    plugins: ['remove_button'],
    maxItems: 1,
    valueField: 'id',
    labelField: 'name',
    searchField: 'name',
    create: false,
    closeAfterSelect: true,
    options: [],
    render: {
            option: function(item, escape) {           
                return '<div>' +escape(item.name)+'</div>';
            }
        },
        load: function(query, callback) {
            if (!query.length) return callback();
            if(query.length>=1){
             $.ajax({
                  url: base_url+'/api/city-suggestion',
                  type: 'GET',
                  dataType: 'json',
                  data: {
                      country_id: globalVar.countryIdComp,
                      q: query
                  },           
                  success: function(res) {
                      callback(res);
                  },
                  error: function(error) {
                      callback();
                  }
              });
            } else {
              var selectize = $("#comp_city")[0].selectize; 
             selectize.renderCache['option'] = {}; 
             return callback();
            }
        }
 });
  </script>

{{-- /* End Employer Reg Form JS */ --}}