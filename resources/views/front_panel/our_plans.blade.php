 @extends('/layouts/front_panel_master')
  @section('content')
@include('front_panel/includes/page_banner')

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      
      <div class="modal-body">
         <img src="{{url('public/images/screenshot.png')}}" class="img-responsive">
      </div>
      
    </div>
  </div>
</div>

<div class="clearfix"></div>
<section class="main-inner-page" id="search-section">
        <div class="container">
            <!-- top section-->
           
            <!-- top section-->

             <!-- main row -->
            <div class="row">
                <!--sidebar-->
                
                 <!-- content section --> 
                <div class="col-xs-12 col-sm-12 mt-15">
                        <div class="row">
                            <div class="col-sm-12"> 
                                @foreach($service_list as $k=>$val)
                                    <div class="col-xs-12 col-md-6 col-lg-6 plr10">
                                        <div class="walkins-card listing-card plans-card">
                                            <span class="listing-highlight big-heading">{{$val->title}}
                                            <a class="listing-card-header" href="job-plans">
                                                <img src="{{checkFile($val->icon,'/uploads/service_img/','no-img.png')}}" class="card-img-top">
                                            </a>
                                            </span>
                                            <div class="listing-card-block">
                                                <p class="listing-card-text mb20 text-center">{{$val->description}}</p>
                                                <div class="key-feature col-xs-12 col-lg-8 nopadding">
                                                    <h4 class="listing-card-title list">Key Features</h4>
                                                    @if($val->features!=null)
                                                        <ul class="list">
                                                            @foreach($val->features as $key=>$value)
                                                                <li>{{$value->title}}</li>
                                                            @endforeach
                                                        </ul>
                                                    @endif                                                    
                                                </div>
                                                <div class="col-lg-4 col-xs-12">
                                                    <img class="screenshot-img" src="{{checkFile($val->image,'/uploads/service_img/','no-thumbnail.jpg')}}" data-toggle="modal" data-target="#exampleModal">
                                                </div>
                                            </div>
                                            <div class="listing-card-footer">
                                                @if($val->show_btn==2)
                                                    <a href="job-plans"  class="hvr-sweep-to-top"> Find Out More</a>
                                                @elseif($val->show_btn==1)
                                                    <a data-toggle="modal" data-target="#enquiryModel" class="hvr-sweep-to-top"> Find Out More</a>
                                                @endif
                                            </div>
                                        </div>
                                    </div> 
                                @endforeach
                                                         
                            </div>
                               
                        </div>
                </div>
            </div> 
             <!-- main row -->
                     </div> <!-- container -->

</section>

<div class="whyus-area">
<div class="container">
  <div class="row">
    <div class="col-lg-6 col-lg-offset-6 col-md-6 col-md-offset-6 col-sm-offset-6 col-sm-6 col-xs-12">
      <div class="whyus-content">
        <h2>Why Choose Jobs4Gulf?</h2>
        <h3>More Candidates</h3>
        <p>149,000+ new registration every month</p>
        <h3>More Response</h3>
        <p>3.5 million applications a month</p>
        <h3>More Relevance</h3>
        <p>Target the best candidates quickly</p>

      </div>
    </div>
  </div>
</div>
</div>
<!-- Start Enquiry Modal -->
@php
$pro_interest_in=config('constants.pro_interest_in');
sort($pro_interest_in);
@endphp
  <div class="modal fade" id="enquiryModel" role="dialog" aria-labelledby="enquiryModel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title text-center">Your message will be sent to  </h4> 
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">       
                {{ Form::open(array('url'=>route('save-service-enquiry'),'class'=>'form-horizontal','id'=>'save-service-enquiry-form')) }}
                <div class="form-group row">
                    <label class="col-sm-12 form-label ">Name</label>
                    <div class="col-sm-12">
                        {{Form::text('name',null,array('class'=>'form-control','placeholder'=>'Enter Name'))}}  
                    </div>                                   
                </div> 
                <div class="form-group row">
                    <label class="col-sm-12 form-label ">Designation</label>
                    <div class="col-sm-12">
                        {{Form::text('designation',null,array('class'=>'form-control','placeholder'=>'Enter Designation'))}}  
                    </div>                                   
                </div> 
                <div class="form-group row">
                    <label class="col-sm-12 form-label">Email Id</label>
                    <div class="col-sm-12">
                        {{Form::text('email',null,array('class'=>'form-control','placeholder'=>'Enter Email Id'))}}  
                    </div>                                   
                </div>
                <div class="form-group row">
                    <label class="col-sm-12 form-label ">Mobile</label>
                    <div class="col-sm-12">
                        {{Form::text('mobile',null,array('class'=>'form-control','placeholder'=>'Enter Mobile No.'))}}  
                    </div>                                   
                </div>
                <div class="form-group row">
                    <label class="col-sm-12 form-label ">Landline</label>
                    <div class="col-sm-12">
                        {{Form::text('landline',null,array('class'=>'form-control','placeholder'=>'Enter Landline No.'))}}  
                    </div>                                   
                </div>
                <div class="form-group row">
                    <label class="col-sm-12 form-label ">Company</label>
                    <div class="col-sm-12">
                        {{Form::text('company',null,array('class'=>'form-control','placeholder'=>'Enter Company Name'))}}  
                    </div>                                   
                </div>
                <div class="form-group row">
                    <label class="col-sm-12 form-label ">Location</label>
                    <div class="col-sm-12">
                        {{Form::text('location',null,array('class'=>'form-control','placeholder'=>'Enter Location'))}}  
                    </div>                                   
                </div>
                <div class="form-group row">
                    <label class="col-sm-12 form-label ">Product Interested In</label>
                    <div class="col-sm-12">
                        {{Form::select('pro_interest_in',$pro_interest_in,null,array('class'=>'form-control','placeholder'=>'--Select--'))}}  
                    </div>                                   
                </div>
                <div class="form-group row">
                    <label class="col-sm-12 form-label ">Comments</label>
                    <div class="col-sm-12">
                        {{Form::textarea('comments',null,array('cols'=>'50','rows'=>3,'class'=>'form-control','placeholder'=>'Enter Comment'))}}  
                    </div>                                   
                </div>

                <div class="form-group row"> 
                    <div class="col-xs-12">            
                        <button type="submit" class="btn-danger">Submit</button>  
                    </div>
                </div>
                {{ Form::close() }}
        </div>
      </div>
    </div>
  </div>
<!-- End Enquiry Modal -->
@endsection