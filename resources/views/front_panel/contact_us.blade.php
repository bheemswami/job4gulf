@extends('/layouts/front_panel_master')
@section('content')

@include('front_panel/includes/page_banner')

<section class="main-inner-page lite-greyBg contact">
    <div class="container">
                <div class="card block-stacking-top mrg-btm-120" data-parallax='{"y": 70}'>
          <div class="content-block-2">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="text-content pdd-vertical-70">
                                        <h2 class="mrg-btm-40">Send Us a Message</h2>
                                        {{ Form::open(array('url'=>route('send-contact-message'),'class'=>'contact-form-wrapper','id'=>'contact_form'))}}
                                            <div class="row">
                                                <div class="form-group col-md-6">
                                                    <input type="text" class="form-control fill-this" name="name" placeholder="Enter Name">
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <input type="email" class="form-control input-email fill-this" name="email" placeholder="Enter Email">
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <input type="text" class="form-control fill-this" name="subject" placeholder="Enter Subject">
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <textarea class="form-control" name="message" rows="3" placeholder="Enter Message"></textarea>
                                                </div>                                               
                                                <div class="form-group col-md-12" id="submit">
                                                    <input class="btn btn-md btn-dark" type="submit" id="send_message" value="Send Message">
                                                </div>
                                            </div>
                                       {{ Form::close() }}
                                    </div>
                                </div>
                            </div>
                        </div>  
                        <div class="image-container col-md-4 col-md-offset-8">
                            <div class="background-holder theme-overlay has-content">
                                <div class="content pdd-horizon-50">
                                    <h2 class="mrg-btm-30">Contact Info</h2>
                                    <p class="text-light-gray">
                                        <b class="text-white">Contact Number:</b>
                                        <span id="contact_us_mobile">{{$settings->mobile}}</span>
                                    </p>
                                    <p class="text-light-gray">
                                        <b class="text-white">Email Adress:</b>
                                        <span id="contact_us_email">{{$settings->email}}</span>
                                    </p>
                                    <div class="row mrg-top-30">
                                        <div class="col-md-9">
                                            <p class="font-size-17 text-white"><b >Adress</b></p>
                                            <p class="text-light-gray" id="contact_us_address">{!!$settings->address!!}</p>
                                        </div>
                                    </div>
                                     <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 footer-social">
                                    <ul class="social-btn mrg-top-30 ">
                                        @if($settings->fb_link!='') <li><a class="btn icon-btn-md btn-white-inverse hover-facebook border-radius-round" target="_blank" href="{{$settings->fb_link}}"><img src="{{url('public/images/facebook.png')}}"></a></li> @endif
                                        @if($settings->twitter_link!='')<li><a class="btn icon-btn-md btn-white-inverse hover-twitter border-radius-round" target="_blank" href="{{$settings->twitter_link}}"><img src="{{url('public/images/twitter.png')}}"></a></li> @endif
                                        @if($settings->gplus_link!='')<li><a class="btn icon-btn-md btn-white-inverse hover-google-plus border-radius-round" target="_blank" href="{{$settings->gplus_link}}"><img src="{{url('public/images/gplus.png')}}"></a></li> @endif
                                    </ul>
                                </div>
                                </div><!-- content -->
                            </div><!-- /background-holder -->
                        </div><!-- /image-container -->
          </div>
        </div>
            </div>
</section>

@endsection