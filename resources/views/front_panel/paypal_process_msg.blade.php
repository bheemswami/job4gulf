@extends('/layouts/front_panel_master')
@section('content')

<section class="main-inner-page lite-greyBg contact">
    <div class="container">
        <div class="card block-stacking-top mrg-btm-120">
          <div class="content-block-2 text-center">
               <img class="" src="{{url('public/images/pay_success.png')}}" width="100px" style="margin-top: -10px;"/> <br/><br/>
               <p style="font-size:22px;font-weight: 600;margin-bottom: 22px;">Thank you for your payment</p>
               <a class="btn btn-primary" href="{{route('job-post')}}">Post Your Job</a>
          </div>
        </div>
    </div>
</section>

@endsection