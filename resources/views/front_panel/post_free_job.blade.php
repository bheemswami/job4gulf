@extends('/layouts/front_panel_master')
@section('content')

@php 
  $gulfCountry=getGulfCountries();
  $otherCountry=getOtherCountries();
@endphp

<section class="main-inner-page lite-greyBg">
  <section id="search-section">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-10 mid-sec-top">

          {{ Form::model(session()->get('tmpPostJobData'),array('url'=>route('save-free-job'),'class'=>'dasboard-form','id'=>'post-job-free-form','files'=>true))}}
          <!-- Start Job Details Form -->
          <div class="block pt-0"> 
            <div class="form">        
                <div class="form-section">
                  <div class="form-title">
                    <h4 class="jobdetail">Job Details</h4>
                  </div>

                  <div class="form-group row">
                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Job Title/Designation</label>
                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                      {{ Form::text('job_title',null,array('class'=>"form-control",'id'=>"job_title",'placeholder'=>"Enter Complete Designation")) }}                 
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Job Description</label>
                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                      {{ Form::textarea('job_description',null,array('class'=>"form-control",'id'=>"job_description",'placeholder'=>"Enter Job Description")) }}
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Job Location</label>
                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                      <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 leftnopadding">
                        {{ Form::select('job_in_country',
                          ['Gulf Country'=>$gulfCountry,'Other Country'=>$otherCountry],null,
                        array('class'=>"form-control",'id'=>"job_country",'placeholder'=>"Select Country")) }}
                      </div>
                     
                       <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 nopadding">
                        {{ Form::TEXT('job_in_city',null,array('class'=>"form-control",'id'=>"job_city",'autocompletion'=>'off','placeholder'=>"Enter City")) }} 
                         <span id="errorJobInCity"></span>                
                      </div>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Monthly Salary</label>
                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 leftnopadding">
                        {{ Form::select('salary_min',config('constants.min_salary'),null,array('class'=>"form-control",'id'=>"monthly_salary_min",'placeholder'=>"in US$")) }}
                         <span class="fieldlabel">Minimum</span>                 
                      </div>
                      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 nopadding">
                        {{ Form::select('salary_max',config('constants.max_salary'),null,array('class'=>"form-control",'id'=>"monthly_salary_max",'placeholder'=>"in US$")) }}
                         <span class="fieldlabel">Maximum</span>                 
                      </div>
                    </div>
                  </div>
                  
                  <div class="form-group row">
                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label"></label>  
                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                      <lable class="checkbox-btn btn2">{{Form::checkbox('show_salary', 1, true)}}Do not Display the Salary range to Job Seekers</lable>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">No. of vacancies</label>
                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                      {{ Form::number('total_vacancy',null,array('class'=>"form-control",'id'=>"total_vacancy",'placeholder'=>"Enter No. of Vacancies")) }}                 
                    </div>
                  </div>
                </div>
                
                <div class="form-section">
                  <div class="form-title">
                    <h4 class="filter">Job Classification</h4>
                  </div>

                  <div class="form-group row">
                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Industry Type</label>
                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                      {{ Form::select('industry_id',$industry_list,null,array('class'=>"form-control",'id'=>"job_industry",'placeholder'=>"Select Industry")) }}
                    </div>
                  </div>             

                  <div class="form-group row">
                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Functional Area</label>
                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                      {{ Form::select('functional_area_id',$functional_area,null,array('class'=>"form-control",'id'=>"functional_area",'placeholder'=>"Select Functional Area")) }}
                    </div>
                  </div>

                   <div class="form-group row">
                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Keywords</label>
                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                       {{ Form::textarea('keywords',null,array('class'=>"form-control",'id'=>"key_skills",'placeholder'=>"Enter important keywords that can describe this job")) }}
                    <span id="errorKeywords"></span>
                    </div>
                  </div>
                </div>
                            
                <div class="form-section">
                  <div class="form-title">
                    <h4 class="personal_detail">Eligiblity Criteria</h4>
                  </div>
                  <div class="form-group row">
                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Qualification</label>
                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                      {{ Form::select('quilification[]',$educations,null,array('class'=>"form-control",'id'=>"qualifications",'multiple'=>"multiple")) }}
                    </div>
                  </div>

                   <div class="form-group row">
                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Specialization</label>
                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                      {{ Form::select('specialization[]',[],null,array('class'=>"form-control",'id'=>"specialization",'multiple'=>"multiple")) }}
                    </div>
                  </div>


                  <div class="form-group row">
                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Gender</label>
                    <div class="col-xs-12 col-sm-9 left-pad3">
                      <div class="row">
                       <div class="radioStyle2" >
                          <label for="male">{{ Form::radio('gender','Male',false,array('class'=>"form-control",'id'=>"male")) }}Male</label>
                        </div>                    
                        <div class="radioStyle2">
                          <label for="female">{{ Form::radio('gender','Female',false,array('class'=>"form-control",'id'=>"female")) }}Female</label>
                        </div>
                        <div class="radioStyle2">
                          <label for="anyone">{{ Form::radio('gender','No Preference',false,array('class'=>"form-control",'id'=>"anyone")) }}No Preference</label>
                        </div>
                        <span id="errorGender"></span>
                     </div>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Work Experience</label>
                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 leftnopadding">
                        {{ Form::select('min_experience',getDigitRange(0,30,'year'),null,array('class'=>"form-control",'id'=>"exp_year_from",'placeholder'=>"in Year")) }}                 
                        <span class="fieldlabel">Minimum</span>
                      </div>
                      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 nopadding">
                        {{ Form::select('max_experience',[],null,array('class'=>"form-control",'id'=>"exp_year_to",'placeholder'=>"in Year")) }}                 
                        <span class="fieldlabel">Maximum</span>
                      </div>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label">Nationality</label>
                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                       {{ Form::select('nationality_id[]',config('constants.nationality'),null,array('class'=>"form-control",'id'=>"nationality",'multiple'=>"multiple")) }}
                    </div>
                  </div>

                  <div class="current_location_section">
                    <div class="location_elem">
                      <div class="form-group row">
                        <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label">Current Location of the Candidate</label>
                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                          {{ Form::select('candidate_current_country[0]',['Gulf Country'=>$gulfCountry,'Other Country'=>$otherCountry],null,array('class'=>"form-control curr_loc",'placeholder'=>"Select Country")) }} 
                        </div>
                        
                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                          {{ Form::select('candidate_current_city[0][]',[],null,array('class'=>"form-control curr_city",'id'=>'c_city0','multiple'=>"multiple")) }} 
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1 add_more"><i class="fa fa-plus fa-icon-green" aria-hidden="true"></i></div>
                      </div>
                    </div>

                  </div>
                </div>

                <div class="form-section">
                  <div class="form-title">
                    <h4 class="manageres">Manage Response</h4>
                  </div>
                  <div class="form-group row">
                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Seeker will Respond you at</label>
                    <div class="col-xs-12 col-sm-8">
                      <div class="row">                    
                        <div class="radioStyle2">
                          <label for="responce_1">{{ Form::radio('response_by','Email',false,array('class'=>"form-control res_manage",'id'=>'responce_1')) }}Email </label>
                        </div>                    
                        <div class="radioStyle2">
                          <label for="responce_2">{{ Form::radio('response_by','ContactDetails',false,array('class'=>"form-control res_manage",'id'=>'responce_2')) }}Contact Details</label>
                        </div>
                        <div class="radioStyle2">
                          <label for="responce_3">{{ Form::radio('response_by','Walkin',false,array('class'=>"form-control res_manage",'id'=>'responce_3')) }}WalkIn</label>
                        </div>
                        <div class="radioStyle2">
                          <label for="responce_4">{{ Form::radio('response_by','All',false,array('class'=>"form-control res_manage",'id'=>'responce_4')) }}All</label>
                        </div>
                      </div>
                      <span id="errorResponseBy"></span>
                    </div>
                  </div>

                  <div class="form-group row manage_sec_1 hide-elem">
                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Mailing E-Mail ID</label>
                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                      {{ Form::text('response_email',null,array('class'=>"form-control",'id'=>"mailing_email",'placeholder'=>"Mailing E-Mail ID (Comma seperated for multiple)")) }}                 
                    </div>
                  </div>
                  <div class="form-group row manage_sec_2 hide-elem">
                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Contact Person</label>
                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                      <div class="form-row">
                        <div class="col-xs-12 col-sm-4 form-child-block">
                          {{ Form::select('job_cp_title',config('constants.name_titles'),null,array('class'=>"form-control",'id'=>"job_cp_title")) }} 
                        </div>
                        <div class="col-xs-12 col-sm-8 form-child-block">
                          {{ Form::text('job_cp_name',null,array('class'=>"form-control",'hint-class'=>"fname_s",'id'=>"job_cp_name",'placeholder'=>"Enter Contact Person Name")) }}
                        </div>
                      </div>                  
                    </div>
                  
                  </div>
                  <div class="form-group row manage_sec_2 hide-elem">
                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Contact Number</label>
                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                      <div class="form-row">
                        <div class="col-xs-12 col-sm-4 form-child-block">
                          {{ Form::select('job_dial_number',$country_codes,'Male',array('class'=>"form-control",'id'=>"contact_code")) }} 
                        </div>
                        <div class="col-xs-12 col-sm-8 form-child-block">
                          {{ Form::text('job_cp_mobile',null,array('class'=>"form-control",'id'=>"contact_number",'placeholder'=>"Enter Contact Person Number")) }}
                        </div>
                      </div>                  
                    </div>
                  </div>

                  <div class="form-group row manage_sec_3 hide-elem">
                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Address</label>
                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                       {{ Form::textarea('walkin_address',null,array('class'=>"form-control",'id'=>"walkin_address",'placeholder'=>"Enter WalkIn Address")) }}
                    </div>
                  </div>
                  <div class="form-group row manage_sec_3 hide-elem">
                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">WalkIn Date</label>
                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                      <div class="form-row">
                        <div class="col-xs-12 col-sm-6 form-child-block">
                          {{ Form::text('walkin_date_from',null,array('class'=>"form-control","id"=>"wdf","readonly"=>true,'placeholder'=>'Select Date','autocomplete'=>'off')) }}
                      	  <span id="errorWalkinDateFrom"></span>
                        </div>
                        <div class="col-xs-12 col-sm-6 form-child-block">
                          {{ Form::text('walkin_date_to',null,array('class'=>"form-control","id"=>"wdt","readonly"=>true,'placeholder'=>'Select Date','autocomplete'=>'off')) }}
                      	  <span id="errorWalkinDateTo"></span>
                        </div>
                      </div>                  
                    </div>
                  </div>

                  <div class="form-group row manage_sec_3 hide-elem">
                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">WalkIn Starting Time</label>
                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                      <div class="form-row">
                        <div class="col-xs-12 col-sm-6 form-child-block">
                          {{ Form::select('walkin_start_time',getTimeRange('9:00', '19:30'),null,array('class'=>"form-control","id"=>"wst",'placeholder'=>'Select Time')) }}
                        </div>
                        <div class="col-xs-12 col-sm-6 form-child-block">
                          {{ Form::select('time_meridiem',['AM'=>'AM','PM'=>'PM'],null,array('class'=>"form-control")) }}
                      	  <span id="errorWalkinDateTo"></span>
                        </div>
                      </div>                  
                    </div>
                  </div>
                </div>
            </div>
          </div>
          <!-- End Job Details Form -->

          <!-- Start Account Details Form -->
          <div class="block pt-0">
          <div class="form">  
                <div class="form-section">
                  <div class="form-title">
                    <h4 class="logininfo">Account Information</h4>
                  </div>

                  <div class="form-group row">
                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Registered Employer</label>
                    <div class="col-xs-12 col-sm-9">
                      <div class="row">                    
                        <div class="radioStyle2">
                          <label>{{ Form::radio('emp_registered',0,true,array('class'=>"form-control emp_registered_rbtn")) }}No</label>
                        </div>                    
                        <div class="radioStyle2">
                          <label>{{ Form::radio('emp_registered',1,false,array('class'=>"form-control emp_registered_rbtn")) }}Yes</label>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="form-group row emp_no_registered">
                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Email ID</label>
                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                      {{ Form::email('user_email_unique',null,array('class'=>"form-control",'id'=>"user_email_unique",'placeholder'=>"Enter Company E-mail ID")) }}                
                    </div>
                  </div>

                  <div class="form-group row emp_yes_registered hide-elem">
                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Email ID</label>
                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                      {{ Form::email('user_email',null,array('class'=>"form-control",'id'=>"emp_email",'placeholder'=>"Enter Company E-mail ID")) }}                
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Password </label>
                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                      <div class="input-group pwd-grp">
                        {{ Form::password('user_password',array('class'=>"form-control pwd",'id'=>"emp_password",'placeholder'=>"Enter Password")) }}
                        <span class="pass-show input-group-btn"><button class="btn btn-default reveal" type="button"><i class="glyphicon glyphicon-eye-open"></i></button></span>
                      </div>
                    </div>
                  </div>
                </div>
            
              <div class="form-section emp_no_registered">
                <div class="form-title">
                  <h4 class="contactinfo">Contact Person Information</h4>
                </div>
                <div class="form-group row">
                  <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Contact Person</label>
                  <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                    <div class="form-row">
                      <div class="col-xs-12 col-sm-4 form-child-block">
                        {{ Form::select('cp_title',config('constants.name_titles'),null,array('class'=>"form-control",'id'=>"cp_title",'placeholder'=>"Select Title")) }}
                      </div>
                      <div class="col-xs-12 col-sm-8 form-child-block">
                        {{ Form::text('cp_name',null,array('class'=>"form-control",'id'=>"cp_name",'placeholder'=>"Enter Contact Person Name")) }}
                      </div>
                    </div>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Designation</label>
                  <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                    {{ Form::text('cp_designation',null,array('class'=>"form-control hintable",'hint-class'=>"",'id'=>"designation",'placeholder'=>"Enter Designation")) }}
                    <span id="errorCpDesignation"></span>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Contact Number</label>
                  <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                   <div class="form-row">
                     <div class="col-xs-12 col-sm-4 form-child-block">
                        {{ Form::select('dial_code',$country_codes,null,array('class'=>"form-control",'id'=>"dial_code",'placeholder'=>"Select Code")) }} 
                      </div>
                      <div class="col-xs-12 col-sm-8 form-child-block">
                        {{ Form::text('cp_mobile',null,array('class'=>"form-control",'id'=>"cp_mobile",'placeholder'=>"Enter Contact Number")) }}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
                        
              <div class="form-section emp_no_registered">
                <div class="form-title">
                  <h4 class="factory">Company Information</h4>
                </div>

                <div class="form-group row">
                  <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Member Type</label>
                  <div class="col-xs-12 col-sm-9 left-pad">                    
                      <div class="radioStyle2">
                        <label>{{ Form::radio('comp_type','1',false,array('class'=>"form-control")) }}Consultant</label>
                      </div>                    
                      <div class="radioStyle2">
                        <label>{{ Form::radio('comp_type','2',false,array('class'=>"form-control")) }}Employer</label>
                      </div>
                      <span id="errorCompType"></span>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Company Name</label>
                  <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                     {{ Form::text('comp_name',null,array('class'=>"form-control",'id'=>"company_name",'placeholder'=>"Enter Company Name")) }}
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">About Company</label>
                  <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                     {{ Form::textarea('comp_about',null,array('class'=>"form-control",'id'=>"company_about",'placeholder'=>"Enter About Company")) }}
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label">Company Website</label>
                  <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                     {{ Form::text('comp_website',null,array('class'=>"form-control",'id'=>"company_website",'placeholder'=>"Enter Company Website URL")) }}
                  </div>
                </div>

                <div class="form-group row">
                 <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Industry Type</label>
                  <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                    {{ Form::select('comp_industry',$industry_list,null,array('class'=>"form-control",'id'=>"industry_type",'placeholder'=>"Select Industry Type")) }} 
                  </div>
                </div>

                <div class="form-group row">  
                  <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label"> Company Logo</label>
                  <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                    <img src="{{url('public/images/default/no_logo.png')}}" class="user-default">
                    <div class="form-field-note">
                      Upload only Image (.png/.jpg/.jpeg) formats.
                    </div><br>
                    <div class="upload-btn-wrapper">
                      <lable class="label-button">SELECT LOGO</lable> 
                      {{ Form::file('comp_img',array('class'=>"upload",'id'=>"comp_logo")) }}
                    </div>
                    <div class="upload-btn-wrapper">
                    <lable class="remove rmv_img">REMOVE LOGO</lable>
                    </div>
                    <span id="errorCompImg"></span>
                  </div>
                </div>

                <h2>Mailing Address</h2>

                <div class="form-group row">
                  <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Country</label>
                  <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                     {{ Form::select('comp_country',
                      ['Gulf Country'=>getGulfCountries(),'Other Country'=>getOtherCountries()],null,
                      array('class'=>"form-control",'id'=>"comp_country",'placeholder'=>"Select Country")) }}
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">City</label>
                  <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                     {{ Form::text('comp_city',null,array('class'=>"form-control",'id'=>"comp_city",'autocompletion'=>'off','placeholder'=>"Enter City")) }}
                     <span id="errorCompCity"></span>
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Address</label>
                  <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                     {{ Form::textarea('comp_address',null,array('class'=>"form-control",'id'=>"company_address",'placeholder'=>"Enter Address")) }}  
                  </div>
                </div>

                <div class="form-group row">
                  <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label">PO Box/ Zip Code</label>
                  <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                     {{ Form::text('zip_code',null,array('class'=>"form-control",'id'=>"zip_code",'placeholder'=>"Enter PO Box/ Zip Code")) }}
                  </div>
                </div>
            </div>

                <div class="form-group row mt-20">
                  <div class="col-sm-8 col-sm-offset-4 nopadding">
                    <button type="submit" class="newfclassub">Post Job</button>
                  </div>
                </div>
                </div>
          </div>
          <!-- End Account Details Form -->
          {{ Form::close() }}
        </div>
        
        <div class="col-xs-12 col-sm-2">
          <img src="{{url('public/images/left-side-img.jpg')}}" class="img-responsive center-block">
        </div>
        
        <div class="col-xs-12">
          <img src="{{url('public/images/bottom-adv.jpg')}}" class="img-responsive center-block">
        </div>
      </div>
    </div>
  </section>
</section>

<script>
  globalVar.qIds={};
  globalVar.countryIdComp=0;
  globalVar.countryId=0;
  globalVar.jobCountryId=0;
  globalVar.currCityElem='';
 
</script>
@include('front_panel/includes/add_job_js_code')
@endsection
