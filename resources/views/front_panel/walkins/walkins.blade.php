 @extends('/layouts/front_panel_master') @section('content')
@include('front_panel/includes/page_banner')
<div class="clearfix"></div>
<section class="main-inner-page" id="search-section">
        <div class="container">
            <!-- top section-->
            <div class="row">
                <div class="col-xs-12 col-sm-12">
                    <div class="top-block col-lg-12">
                        <div class="pull-left">
                            <h5>You Searched </h5>
                             <div id='search_key'>  </div>
                        </div>
                        <div class="clearfix text-right">
                            <span id="total_get_walkins">0 Walkins </span>
                            
                        </div>
                    </div>

                </div>
            </div>
            <!-- top section-->

             <!-- main row -->
            <div class="row">
                <!--sidebar-->
                <div class="col-xs-12 col-sm-3 sidebar-parent">
                    <div class="left-sidebar">
                        <form method="post" action="" id="filter_form">
                            {{-- <div class="search">
                                <h4 class="text-uppercase">Refine Search</h4>
                                <input type="text" name="key_skill" value="" class="form-control" />
                            </div> --}}

                            <div class="country-filter filter-block">
                                <h4 class="btn" data-toggle="collapse" data-target="#country">Walk in Date</h4>
                                <div id="country" class="collapse in">
                                    <ul>
                                        <li>
                                            <label class="checkbox-btn">
                                                <input type="checkbox" name="this_week" value="this_week" class="checkbox"/> <b class="listbo">This Week</b>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="checkbox-btn">
                                                <input type="checkbox" name="next_week" value="next_week" class="checkbox"/> <b class="listbo">Next Week</b>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="checkbox-btn">
                                                <input type="checkbox" name="this_month" value="this_month" class="checkbox"/> <b class="listbo">This Month</b>
                                            </label>
                                        </li>
                                        <li>
                                            <label class="checkbox-btn date-checkbox">
                                                <input type="checkbox" name="post_date" value="" class="checkbox " id="post_date"/>
                                            </label>
                                                <input type="text" id="datepicker"  placeholder="Select Date" readonly="readonly" />
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div class="city-filter filter-block">
                                <h4 class="btn collapsed" data-toggle="collapse" data-target="#city">Jobs By City</h4>
                                <div id="city" class="collapse">
                                    <ul>
                                        @foreach($city_list as $k=>$val)
                                        <li>
                                            <label class="checkbox-btn">
                                                <input type="checkbox" name="city_ids" value="{{$val->id}}" class="checkbox" /> <b class="listbo">{{$val->name}}</b>
                                            </label>
                                            <div class="count">({{count($val->walkins)}})</div>
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            <div class="industry-filter filter-block">
                                <h4 class="btn collapsed" data-toggle="collapse" data-target="#industry">Jobs By Industry</h4>
                                <div id="industry" class="collapse">
                                    <ul>
                                        @foreach($industry_list as $k=>$val)
                                        <li>
                                            <label class="checkbox-btn">
                                                <input type="checkbox" name="industry_ids" value="{{$val->id}}" class="checkbox" /> <b class="listbo">{{$val->name}}</b>
                                            </label>
                                            <div class="count">({{count($val->walkins)}})</div>
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            <div class="employer_type-filter filter-block">
                                <h4 class="btn collapsed" data-toggle="collapse" data-target="#employer_type">Employer Type</h4>
                                <div id="employer_type" class="collapse">
                                    <ul>
                                        <li>
                                            <label class="checkbox-btn">
                                                <input type="checkbox" name="employer_type" value="1" class="checkbox" /> <b class="listbo">Company Jobs</b>
                                            </label>
                                            <div class="count">({{@$emp_type[0]->total}})</div>
                                        </li>
                                        <li>
                                            <label class="checkbox-btn">
                                                <input type="checkbox" name="employer_type" value="2" class="checkbox" /> <b class="listbo">Consultancy Jobs</b>
                                            </label>
                                            <div class="count">({{@$emp_type[1]->total}})</div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                 <!-- content section --> 
                <div class="col-xs-12 col-sm-9 mt-15">
                    <div class="row">
                        <div id="load" style="position: relative;">
                        @include('front_panel/walkins/walkins_list')
                    </div>
                    </div>
                </div>
            </div> 
             <!-- main row -->
                     </div> <!-- container -->

</section>
<script type="text/javascript">
    var url = '{{route('walkins')}}';
    $(document).on('click','.custom-pagination a',renderData);
    $(document).on('click','.checkbox',function(){
       setValues();
    });

    function setValues(){
         var form = $('#filter_form').serializeArray();
        console.log(form);
        var industry_ids = [];
        var city_ids = [];
        var employer_type = [];
        var duration = [];
        var this_week='';
        var next_week='';
        var this_month='';
        var post_date='';
        $.each(form,function(index,value){
          if(value.name=='city_ids'){
            city_ids.push(value.value);
          } else if(value.name=='industry_ids'){
            industry_ids.push(value.value);
          } else if(value.name=='employer_type'){
            employer_type.push(value.value);
          } else if(value.name=='this_week'){
            this_week=value.value;
          }  else if(value.name=='next_week'){
            next_week=value.value;
          }  else if(value.name=='this_month'){
            this_month=value.value;
          }  else if(value.name=='post_date'){
            post_date=value.value;
          }
        });
        var data = {'city_ids':city_ids.join(),'industry_ids':industry_ids.join(),'employer_type':employer_type.join(),this_week:this_week,next_week:next_week,this_month:this_month,post_date:post_date};
        ajaxCall(data);
    }
    function renderData(e){
      e.preventDefault();
      url = $(this).attr('href');
      ajaxCall();
      window.history.pushState("", "", url);
    }
    function ajaxCall(data=NaN){
    $.ajax({
          url : url,
          data : data,
      }).done(function (data) {
        console.log(data);
          $('#load').html('').html(data); 
      }).fail(function () {
        
      }).complete(function(){
    });
    }

    $( "#datepicker" ).datepicker({
      changeMonth: true,
      changeYear: true,
      showOn: "button",
      buttonImage: "{{url('public/images/icons/calendar.png')}}",
      buttonImageOnly: true,
      buttonText: "Select date",
      altField: "#post_date",
      dateFormat: "dd-mm-yy",
      onSelect: function (date) {setValues();
      }
    });

    
</script>
@endsection