<div class="col-sm-12"> 
    @forelse ($walkins as $key=>$vals)
        <div class="col-xs-12 col-md-6 col-lg-4 plr10">
            <div class="walkins-card listing-card">
                <span class="listing-highlight">{{ $vals->title }}</span>
                <a class="listing-card-header" href="{{url('walkins-detail/'.$vals->id)}}">
                    <img src="{{checkFile($vals->comp_logo,'uploads/walkin_comp_logo/','job_role_default.png')}}" class="card-img-top busimg">
                    <h4 class="listing-card-title">{{ $vals->comp_name }}</h4>
                </a>
                <div class="listing-card-block">
                    <p class="listing-card-text mb20">{!! limit_text($vals->about_us,120) !!}</p>
                   
                    @if($vals->walkin_interview!=null)
                        @php  $interview_city=[]; @endphp
                        @foreach($vals->walkin_interview as $k=>$v)
                            @php
                                if($v->city!=null){
                                    $interview_city[]= $v->city->name;                                                             
                                }
                            @endphp
                            @if($k==0)
                                <p class="walkin-listng-info"> <strong>Walk-in Date:</strong> <span>{{dateConvert($v->date_from,'dS')}}-{{dateConvert($v->date_to,'dS M Y')}}</span></p>
                                <p class="walkin-listng-info"> <strong>Walk-in Time:</strong> <span>{{dateConvert($v->date_from,'h:i A')}} to {{dateConvert($v->date_to,'h:i A')}}</span></p>
                            @endif
                        @endforeach
                            <p class="walkin-listng-info"> <strong>Walk-in Location:</strong> <span>{{join(',',$interview_city)}}</span></p>
                    @endif

                    @if($vals->walkin_position!=null)
                        <p class="listing-card-text mt20">
                            @php $designations=[];  @endphp
                            @foreach($vals->walkin_position as $d)
                                @php 
                                if($d->designation!=null)
                                    $designations[]= $d->designation->name; 
                                @endphp
                            @endforeach
                            <strong>Positions:</strong>
                            <span>{{ limit_text(join(',',$designations),140) }}<span>                                                    
                        </p>
                    @endif
                </div>
                <div class="listing-card-footer">
                    <a href="{{url('walkins-detail/'.$vals->id)}}" target="_blank" class="hvr-sweep-to-top"> view more detail</a>
                </div>
            </div>
        </div> 
    @empty
            <div class="col-xs-12 col-md-12 nopadding">
              <div class=" job-box">
                <div class="row flexbox">
                  
                  <div class="col-xs-12 col-sm-12">
                    
                    <img src="{{url('public/images/default/nojobsfound.png')}}" class="center-block" width="240px" style="margin-top: 5%;">
                    <div class="post-details">
                      
                    </div>
                  </div>
                </div>
                
              </div>
            </div> 
    @endforelse                       

    <div class="custom-pagination">
        {{$walkins->links()}}
    </div>
</div>

@php
    $totalRec = count($walkins);
  if($totalRec>0){
    $totalRec = $walkins->firstItem().' - '.$walkins->lastItem().' of '.$walkins->total();
  }
@endphp
<script>

var records_of ='{{$totalRec}}';
$('#total_get_walkins').html('<span>'+records_of+' Walkins </span>');

var search='{{$search_tag}}';
var array = search.split(',');

if(search)
{
  $('#search_key').html('');
  $.each( array, function( key, value ) {
    if(value )
    $('#search_key').append('<span class="label label-info">'+value+'</span>');
  });
}
</script>