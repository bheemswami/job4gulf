@extends('/layouts/front_panel_master')
@section('content')
@include('front_panel/includes/page_banner')

<section class="main-inner-page lite-greyBg">
  <section id="search-section">
    <div class="container">
      <div class="row">
       
        <div class="col-xs-12 col-sm-10 mid-sec-top">
          
          <div class="block pt-0">          
           
            {{ Form::open(array('url'=>route('otp-verify'),'class'=>'form','id'=>'forget-password-form2'))}}
            <div class="form-section">
              <div class="form-title">
                <h4 class="logininfo">Enter Security Code</h4>
              </div>
              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">OTP</label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                  {{ Form::text('otp',null,array('class'=>"form-control",'id'=>"",'placeholder'=>"Enter OTP")) }}                 
                </div>
              </div>
            </div>

            <div class="form-group row mt-20">
              <div class="col-sm-9 col-sm-offset-3 nopadding">
                <button type="submit" class="newfclassub">Submit</button>
              </div>
            </div>
          </form>
          </div>
        </div>
        
        <div class="col-xs-12 col-sm-2">
          <img src="{{url('public/images/left-side-img.jpg')}}" class="img-responsive center-block">
        </div>
        
        
      </div>
    </div>
  </section>
</section>
@endsection