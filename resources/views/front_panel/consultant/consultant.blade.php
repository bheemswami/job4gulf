 @extends('/layouts/front_panel_master') 
 @section('content')
@include('front_panel/includes/page_banner')

<div class="clearfix"></div>
<section class="main-inner-page"  id="search-section">
        <div class="container">
            <!-- top section-->
            <div class="row">
                <div class="col-xs-12 col-sm-12">
                    <div class="top-block col-lg-12">
                        <div class="pull-left">
                            <h5>Consultants: </h5>
                            
                          <div id='search_key'>  </div>
                        </div>
                        <div class="clearfix text-right">
                            <span id="total_get_consultants">0 Consultants </span>
                        </div>
                    </div>

                </div>
            </div>
            <!-- main row -->
            <div class="row">
                <!--sidebar-->
                  <div class="col-xs-12 col-sm-3 sidebar-parent">
                      <div class="left-sidebar">
                          <form method="get" id="filter_form">

                              <div class="country-filter filter-block">
                                  <h4 class="btn" data-toggle="collapse" data-target="#country"> Country</h4>
                                  <div id="country" class="collapse in">
                                      <ul>
                                          @foreach($country_list as $k=>$val)
                                          <li>
                                              <label class="checkbox-btn">
                                                  <input type="checkbox" name="country_ids" value="{{$val->id}}" class="checkbox" /> <b class="listbo">{{$val->name}}</b>
                                              </label>
                                              <div class="count">({{count($val->consultant)}})</div>
                                          </li>
                                          @endforeach
                                      </ul>
                                  </div>
                              </div>

                              <div class="city-filter filter-block">
                                  <h4 class="btn collapsed" data-toggle="collapse" data-target="#city"> City</h4>
                                  <div id="city" class="collapse">
                                      <ul>
                                          @foreach($city_list as $k=>$val)
                                          <li>
                                              <label class="checkbox-btn">
                                                  <input type="checkbox" name="city_ids" value="{{$val->id}}" class="checkbox" /> <b class="listbo">{{$val->name}}</b>
                                              </label>
                                              <div class="count">({{count($val->consultant)}})</div>
                                          </li>
                                          @endforeach
                                      </ul>
                                  </div>
                              </div>

                              <div class="industry-filter filter-block">
                                  <h4 class="btn collapsed" data-toggle="collapse" data-target="#industry"> Category</h4>
                                  <div id="industry" class="collapse">
                                      <ul>
                                          @foreach($industry_list as $k=>$val)
                                          <li>
                                              <label class="checkbox-btn">
                                                  <input type="checkbox" name="industry_ids" value="{{$val->id}}" class="checkbox" /> <b class="listbo">{{$val->name}}</b>
                                              </label>
                                              <div class="count">({{count($val->consultant)}})</div>
                                          </li>
                                          @endforeach
                                      </ul>
                                  </div>
                              </div>
                          </form>
                      </div>
                  </div>
                  
                <!-- content section --> 
                  <div class="col-xs-12 col-sm-9 mt-15">
                          <div class="row">
                              <div id="load" style="position: relative;">
                                @include('front_panel/consultant/consultant_list')
                              </div>
                          </div>
                  </div>
            </div> <!-- main row-->
        </div> <!-- container -->
</section>
<script>
  var url = '{{route('consultant')}}';
  $(document).on('click','.custom-pagination a',renderData);
  

  $(document).on('click','.checkbox',function(){
    var form = $('#filter_form').serializeArray();
    var industry_ids = [];
    var city_ids = [];
    var country_ids = [];
    $.each(form,function(index,value){
      if(value.name=='country_ids'){
        country_ids.push(value.value);
      }else if(value.name=='city_ids'){
        city_ids.push(value.value);
      }else if(value.name=='industry_ids'){
        industry_ids.push(value.value);
      }
    });
    var data = {'country_ids':country_ids.join(),'city_ids':city_ids.join(),'industry_ids':industry_ids.join()};
    ajaxCall(data);
  });

  function renderData(e){
      e.preventDefault();
      url = $(this).attr('href');
      ajaxCall();
      window.history.pushState("", "", url);
  }
  function ajaxCall(data=NaN){

    $.ajax({
          url : url,
          data : data,
      }).done(function (data) {
          $('#load').html(data); 
      }).fail(function () {
        
      }).complete(function(){
    });
  }
  
</script>
@endsection