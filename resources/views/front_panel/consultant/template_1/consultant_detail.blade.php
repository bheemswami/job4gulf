@extends('/front_panel/consultant/template_1/master')
@section('content')
<!-- Start Query Modal -->

  <div class="modal fade" id="queryModel" role="dialog" aria-labelledby="queryModel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
            
            <h4 class="modal-title text-center">Your message will be sent to {{$consultant->comp_name}}</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
        <div class="modal-body">   
            <h6>Please share your details to connect the Placement Conultant</h6>        
                {{ Form::open(array('url'=>route('save-enquiry'),'class'=>'form-horizontal','id'=>'save-enquiry-form')) }}
                {{ Form::hidden('email_to',@$consultant->user->email) }}
                <div class="form-group">
                    <label class="label-inline">Are you :</label>                  
                    <label class="radio-inline"><input type="radio" name="are_you" value="Jobseeker">Jobseeker</label>
                    <label class="radio-inline"><input type="radio" name="are_you" value="Employer/Corporate">Employer/Corporate</label>
                </div>
                <div class="form-group"> 
                <div class="row">                
                  <div class="col-sm-6">
                    <label class="col-xs-12 nopadding">Your Email</label>
                    <input type="email" name="email" class="form-control" placeholder="Your E-mail"/>
                  </div>
                  <div class="col-sm-6">
                    <label class="col-xs-12 nopadding">Your Name</label>
                    <input type="text" name="name" class="form-control" placeholder="Your Name"/>
                  </div>
                </div>
                </div>

                <div class="form-group"> 
                    <div class="row">
                      <div class="col-sm-4">
                        <label class="col-xs-12 nopadding">Mobile</label>
                        <select name="country_code" class="form-control">
                            @foreach($country_codes as $val)
                                <option value="{{$val->phonecode}}">{{$val->sortname}} (+{{$val->phonecode}})</option>
                            @endforeach
                        </select>                
                      </div>                
                      <div class="col-sm-4">
                        <label class="col-xs-12 nopadding">&nbsp;</label>
                        <input type="text" name="mobile" class="form-control" placeholder="Mobile No."/>
                      </div>
                      <div class="col-sm-4">
                        <label class="col-xs-12 nopadding">Location</label>
                        <input type="text" name="location" class="form-control" placeholder="Location"/>
                      </div>
                    </div>
                </div>

                <div class="form-group">  
                    <div class="row">                           
                      <div class="col-xs-12">
                        <label class="col-xs-12 nopadding">Enter your query in brief</label>
                       <textarea name="query" class="form-control" rows="6"></textarea>
                      {{--  <span class="subtitle">50 to 1000Characters</span> --}}
                     </div>
                    
                    <div class="form-group">                             
                      <div class="col-xs-12">
                       <label class="checkbox-inline"><input type="checkbox" name="forward_query" value="1">Interested to forward my query to more corporate</label>
                     </div>
                    </div>
                    </div>
                </div>

               <div class="row"> 
                <div class="col-xs-12">            
                  <button type="submit" class="btn-danger">Submit</button>
                </div>
                </div>
                {{ Form::close() }}
        </div>
      </div>
    </div>
  </div>
<!-- End Query Modal -->
<nav class="cd-navbar navbar navbar-default navbar-expand-lg bg-secondary fixed-top text-uppercase" id="mainNav">
    <div class="container">
        <a class="navbar-brand cd js-scroll-trigger" href="{{route('home')}}">
            <img src="{{checkFile($consultant->comp_logo,'uploads/employer_logo/','company_logo.png')}}" class="card-img-top busimg">
        </a>
        <button class="navbar-toggler navbar-toggler-right text-uppercase bg-primary text-white rounded" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            Menu
            <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item mx-0 mx-lg-1">
                    <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#about">About Us</a>
                </li>
                <li class="nav-item mx-0 mx-lg-1">
                    <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#career">Career</a>
                </li>
                <li class="nav-item mx-0 mx-lg-1">
                    <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#contact">Contact</a>
                </li>
                <li class="nav-item mx-0 mx-lg-1">
                    <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#visit">Visit</a>
                </li>
            </ul>
            <div class="cd-socila-links">
                <h4 class="">Follow Us!</h4>
                    <ul class="list-inline mb-0">
                        <li class="list-inline-item">
                            <a class="btn btn-outline-light btn-social text-center rounded-circle" href="{{$consultant->twitter_link}}">
                                <i class="fa fa-fw fa-twitter"></i>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a class="btn btn-outline-light btn-social text-center rounded-circle" href="{{$consultant->fb_link}}">
                                <i class="fa fa-fw fa-facebook"></i>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a class="btn btn-outline-light btn-social text-center rounded-circle" href="{{$consultant->gplus_link}}">
                                <i class="fa fa-fw fa-google-plus"></i>
                            </a>
                        </li>
                    </ul>
                </div>
        </div>
    </div>
</nav>
<!-- Slider Section -->
<section id="page-top" class="custom-banner">
    <div id="first-slider">
        <div id="carousel-example-generic" class="carousel slide carousel-fade">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
            </ol>
            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <!-- Item 1 -->
                <div class="item active slide1" style="background-image: url({{url('public/img/job-interview-tips.jpg')}}">
                    <div class="row">
                        <div class="container">
                        </div>
                    </div>
                </div>
                <!-- Item 2 -->
                <div class="item  slide2" style="background-image: url({{url('public/img/job-interview.jpg')}}">
                    <div class="row">
                        <div class="container">
                        </div>
                    </div>
                </div>
                <!-- End Item 4 -->
                <div class="item  slide3" style=" background-image: url({{url('public/img/Business-Photos-1.jpg')}}">
                    <div class="row">
                        <div class="container">
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Wrapper for slides-->
            <div class="banner-callus-button" data-toggle="modal" data-target="#queryModel">Questions? Send an Enquiry</div>
        </div>
    </div>
</section>

<section class="mb-0 about-consultant" id="about">
    <div class="container">
        <h1 class="about-consultant-title">About Us<span></span></h1>
        <div class="about-consultant-content">
            <div class="more">
                {!! $consultant->about !!}
            </div>
            <div class="consultant-company-profile">
                <h3>Company Profile</h3>
                <p><lable>Location:</lable><span>{{($consultant->city!=null) ? $consultant->city->name : ''}}</span>
                </p>
                <p><lable>Industries we serve:</lable><span>{{ ($consultant->industry!=null) ? $consultant->industry->name : ''}}</span>
                </p>
                <p><lable>Skills/ Roles we hire for:</lable><span>{{$consultant->skills}}</span>
                </p>
            </div>
        </div>
    </div>
</section>
@if(count($consultant->job_pos)>0)
<section class="mb-0 career-consultant" id="career">
    <div class="container">
        <h1 class="about-consultant-title">Career<span></span></h1>
        @foreach($consultant->job_post as $k=>$y)
            <div class=" col-xs-12 col-md-6 related-block">
                <div class="clearfix nopadding lcop">
                    <div class=" job-box">
                        <div class="row flexbox">
                            <div class="col-xs-3 col-sm-2 left">
                                <div class="div-table">
                                    <div class="table-cell-div">
                                        <img src="{{url('public/images/job_icon.png')}}" class="center-block">
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-9 col-sm-10 right">
                                <h2><a href="{{route('job-details',['jobid'=>$y->id])}}">{{$y->job_title}}</a></h2>
                                <h5>{{$y->functional_area->name}}</h5>
                                <div class="post-details">
                                    <p>Keyskills: {{$y->key_skill}}</p>
                                    <p> <span><i class="fa fa-suitcase" aria-hidden="true"></i>{{$y->min_experience}}-{{$y->max_experience}} years</span>
                                        <span><i class="fa fa-map-marker" aria-hidden="true"></i> {{$y->country->name}} </span></p>
                                </div>
                            </div>
                        </div>
                        <div class="job_optwrap">
                            <p>Posted : {{date('dS M Y',strtotime($y->created_at))}}</p>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</section>
@endif
<section class="contactus-section" id="contact">
    <div class="container">
        <h1 class="about-consultant-title">Contact us<span></span></h1>
        <div class="col-xs-12 col-md-6 col-lg-8">
            <div class="col-xs-12 col-md-6 col-lg-6 mb20">
                <h3>Personal Details:</h3>
                <p>{{$consultant->contact_person}}</p>
                <p>
                    <lable>Email:</lable> <span>{{($consultant->user!=null) ? $consultant->user->email : '' }}</span></p>
                <p>
                    <lable>Skype ID:</lable> <span>Not Available</span></p>
            </div>
            <div class="col-xs-12 col-md-6 col-lg-6 mb20">
                <h3>Address:</h3>
                <p>{{$consultant->comp_address}}</p>
            </div>
            <div class="col-xs-12 col-md-12 col-lg-12">
                <h3>Contact Number:</h3>
                <p><lable>Phone:</lable> <span>{{$consultant->phone}}</span></p>
            </div>
            <div class="mt10 mb10"><button type="button" data-toggle="modal" data-target="#queryModel" class="btn jobs-info-share-button">Questions? Send an Enquiry</button></div>
        </div>
        
        <div class="col-xs-12 col-md-6 col-lg-4 map-block" id="map">
        </div>
        

    </div>

    </section>

    <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
    <div class="scroll-to-top d-lg-none position-fixed ">
        <a class="js-scroll-trigger d-block text-center text-white rounded" href="#page-top">
            <i class="fa fa-chevron-up"></i>
        </a>
    </div>
<style>
  #map {
    height: 200px;
  }
</style>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDXkuWv5uEZFVDx6XG3PQU5XwrFqUQj_CM&callback=initMap"></script>

<script>
     var geocoder;
  var map;
  var address ="Junagarh Fort Road, Bikaner Fort, Rath Khana Colony, Bikaner, Rajasthan 334001";
  function initMap() {
    geocoder = new google.maps.Geocoder();
    var latlng = new google.maps.LatLng(-34.397, 150.644);
    var myOptions = {
      zoom: 8,
      center: latlng,
    mapTypeControl: true,
    mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
    navigationControl: true,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById("map"), myOptions);
    if (geocoder) {
      geocoder.geocode( { 'address': address}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {
          map.setCenter(results[0].geometry.location);

            var infowindow = new google.maps.InfoWindow(
                { content: '<b>'+address+'</b>',
                  size: new google.maps.Size(150,50)
                });

            var marker = new google.maps.Marker({
                position: results[0].geometry.location,
                map: map, 
                title:address
            }); 
            google.maps.event.addListener(marker, 'click', function() {
                infowindow.open(map,marker);
            });

          } else {
            alert("No results found");
          }
        } else {
          alert("Geocode was not successful for the following reason: " + status);
        }
      });
    }
  }
</script>
@endsection