<div class="col-sm-12">
  
    @forelse($consultants as $k=>$val)
            <div class="col-xs-12 col-md-6 col-lg-4 plr10">
                <div class="listing-card">
                  <a class="listing-card-header" href="consultant-detail/{{$val->id}}">
                      <img class="card-img-top busimg" src="{{checkFile($val->comp_logo,'uploads/employer_logo/','company_logo.png')}}">
                      <h4 class="listing-card-title">{{$val->comp_name}}</h4>
                  </a>
                    <div class="listing-card-block">
                        <ul class="listing-card-tags"><li>{{count($val->job_post)}} Active Jobs</li><li>1 industries cover</li></ul>
                        <h5 class="listing-subcard-title">{{$val->country->name}}</h5>
                        <p class="listing-card-text mb10">We hire for : {{($val->industry!=null) ? $val->industry->name : '' }}</p>
                    </div>
                    <div class="listing-card-footer">
                        <a href="consultant-detail/{{$val->id}}" class="hvr-sweep-to-top">View more detail</a>
                    </div>
                </div>
            </div>
    @empty
      <div class="col-xs-12 col-md-12 nopadding">
              <div class=" job-box">
                <div class="row flexbox">
                  
                  <div class="col-xs-12 col-sm-12">
                    
                    <img src="{{url('public/images/default/nojobsfound.png')}}" class="center-block" width="240px" style="margin-top: 5%;">
                    <div class="post-details">
                      
                    </div>
                  </div>
                </div>
                
              </div>
            </div> 
    @endforelse                                    

  <div class="custom-pagination">
  {{$consultants->links()}}
  </div>
</div>
@php
    $totalRec = count($consultants);
  if($totalRec>0){
    $totalRec = $consultants->firstItem().' - '.$consultants->lastItem().' of '.$consultants->total();
  }
@endphp
<script>

var records_of ='{{$totalRec}}';
$('#total_get_consultants').html('<span>'+records_of+' Consultants </span>');

var search='{{$search_tag}}';
var array = search.split(',');

if(search)
{
  $('#search_key').html('');
  $.each( array, function( key, value ) {
    if(value )
    $('#search_key').append('<span class="label label-info">'+value+'</span>');
  });
}
</script>
                             