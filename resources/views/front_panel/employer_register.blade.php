@extends('/layouts/front_panel_master')
@section('page_title') | Register @endsection
@section('content')
@include('front_panel/includes/page_banner')
@include('front_panel/includes/validation_msg')

<section class="main-inner-page lite-greyBg">
  <section id="search-section">
    <div class="container">
      <div class="row">
       
        <div class="col-xs-12 col-sm-10 mid-sec-top">
           <div class="section-title text-center">
               <h2><span class="textgreen">Become an Employer</h2>
            </div>
          <div class="block pt-0">  
            {{ Form::open(array('url'=>route('save-employer'),'class'=>'form','id'=>'employer-registration-form','files'=>true))}}
            <div class="form-section">
              <div class="form-title">
                <h4 class="logininfo">Login Information</h4>
              </div>
              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Company E-mail ID</label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                  {{ Form::email('email',null,array('class'=>"form-control",'id'=>"emp_email",'placeholder'=>"This Will Become Your Username For Login")) }}                 
                </div>
              </div>

              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Password 
                <span class="help-tip"><p>Password is case sensitive. Min 6 characters </p></span>
              </label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                  <div class="input-group pwd-grp">
                    {{ Form::password('password',array('class'=>"form-control pwd",'id'=>"emp_password",'placeholder'=>"Enter Password")) }}
                    <span class="pass-show input-group-btn"><button class="btn btn-default reveal" type="button"><i class="glyphicon glyphicon-eye-open"></i></button></span>
                  </div>
                </div>
              </div>

            </div>
            
            <div class="form-section">
              <div class="form-title">
                <h4 class="contactinfo">Contact Person Information</h4>
              </div>
              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Contact Person</label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                  <div class="form-row">
                    <div class="col-xs-12 col-sm-4 form-child-block">
                      {{ Form::select('cp_title',config('constants.name_titles'),null,array('class'=>"form-control",'id'=>"cp_title",'placeholder'=>"Select Title")) }}
                    </div>
                    <div class="col-xs-12 col-sm-8 form-child-block">
                      {{ Form::text('cp_name',null,array('class'=>"form-control",'id'=>"cp_name",'placeholder'=>"Enter Contact Person Name")) }}
                    </div>
                  </div>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Designation</label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                  {{ Form::text('cp_designation',null,array('class'=>"form-control hintable",'hint-class'=>"",'id'=>"designation",'placeholder'=>"Enter Designation")) }}
                 <span id="errorDesignation"></span>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Contact Number</label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                 <div class="form-row">
                   <div class="col-xs-12 col-sm-4 form-child-block">
                      {{ Form::select('dial_code',$country_codes,null,array('class'=>"form-control",'id'=>"dial_code",'placeholder'=>"Select Code")) }} 
                    </div>
                    <div class="col-xs-12 col-sm-8 form-child-block">
                      {{ Form::text('cp_mobile',null,array('class'=>"form-control",'id'=>"cp_mobile",'placeholder'=>"Enter Contact Number")) }}
                    </div>
                  </div>
                </div>
              </div>
            </div>
                        
            <div class="form-section">
              <div class="form-title">
                <h4 class="factory">Company Information</h4>
              </div>

              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Member Type</label>
                <div class="col-xs-12 col-sm-9 left-pad">                    
                    <div class="radioStyle2">
                      <label>{{ Form::radio('comp_type','1',false,array('class'=>"form-control")) }}Consultant</label>
                    </div>                    
                    <div class="radioStyle2">
                      <label>{{ Form::radio('comp_type','2',false,array('class'=>"form-control")) }}Employer</label>
                    </div>
                     <span id="errorCompType"></span>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Company Name</label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                   {{ Form::text('comp_name',null,array('class'=>"form-control",'id'=>"company_name",'placeholder'=>"Enter Company Name")) }}
                </div>
              </div>

              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">About Company</label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                   {{ Form::textarea('comp_about',null,array('class'=>"form-control",'id'=>"company_about",'placeholder'=>"Enter About Company")) }}
                </div>
              </div>

              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label">Company Website</label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                   {{ Form::text('comp_website',null,array('class'=>"form-control",'id'=>"company_website",'placeholder'=>"Enter Company Website URL")) }}
                </div>
              </div>

              <div class="form-group row">
               <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Industry Type</label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                  {{ Form::select('comp_industry',$industry_list,null,array('class'=>"form-control",'id'=>"industry_type",'placeholder'=>"Select Industry Type")) }} 
                </div>
              </div>

              <div class="form-group row">  
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label"> Company Logo</label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                  <img src="{{url('public/images/default/no_logo.png')}}" class="user-default">
                  <div class="form-field-note">
                    Upload only Image (.png/.jpg/.jpeg) formats.
                  </div><br>
                  <div class="upload-btn-wrapper">
                    <lable class="label-button">SELECT LOGO</lable> 
                    {{ Form::file('comp_img',array('class'=>"upload",'id'=>"comp_logo")) }}
                  </div>
                  <div class="upload-btn-wrapper">
                  <lable class="remove rmv_img">REMOVE LOGO</lable>
                  </div>
                  <span id="errorCompImg"></span>
                </div>
              </div>

              <h2>Mailing Address</h2>

              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Country</label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                   {{ Form::select('comp_country',
                    ['Gulf Country'=>getGulfCountries(),'Other Country'=>getOtherCountries()],null,
                    array('class'=>"form-control",'id'=>"country",'placeholder'=>"Select Country")) }}
                </div>
              </div>

              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">City</label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                   {{ Form::text('comp_city',null,array('class'=>"form-control",'id'=>"city",'autocompletion'=>'off','placeholder'=>"Enter City")) }}
                   <span id="errorCompCity"></span>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Address</label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                   {{ Form::textarea('comp_address',null,array('class'=>"form-control",'id'=>"company_address",'placeholder'=>"Enter Address")) }}
                </div>
              </div>

              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label">PO Box/ Zip Code</label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                   {{ Form::text('zip_code',null,array('class'=>"form-control",'id'=>"zip_code",'placeholder'=>"Enter PO Box/ Zip Code")) }}
                </div>
              </div>
            </div>
            
            <div class="form-group row mt-20">
                <div class="col-sm-8 col-sm-offset-4 nopadding">
                  <button type="submit" class="newfclassub">Create Account</button>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-9 col-sm-offset-2">
                  <label class="checkbox-btn full-width mr-0">
                    By creating an account you will be agree the <a href="{{route('terms-employer')}}" target="_blank">Terms and Conditions</a>
                </div>
            </div>
            {{ Form::close() }}
          </div>
        </div>
        
        <div class="col-xs-12 col-sm-2">
          <img src="{{url('public/images/left-side-img.jpg')}}" class="img-responsive center-block"/>
        </div>
        
        
      </div>
    </div>
  </section>
</section>
<script>
  globalVar.countryId=0;
  $("#comp_logo").change(function(){
    previewImage(this,'.user-default','{{url('public/images/default/no_logo.png')}}');
  });
  $('.rmv_img').click(function () {
    $("#comp_logo").val(null);
    $(".user-default").attr('src','{{url('public/images/default/no_logo.png')}}');
  });

    $('#designation').selectize({
        maxItems: 1,
        valueField: 'id',
        labelField: 'name',
        searchField: 'name',
        create: false,
        options: [],
        render: {
            option: function(item, escape) {           
                return '<div>' +escape(item.name)+'</div>';
            }
        },
        load: function(query, callback) {
            if (!query.length) return callback();
            $.ajax({
                url: base_url+'/api/designation-suggestion',
                type: 'GET',
                dataType: 'json',
                data: {
                    q: query,
                },           
                success: function(res) {
                    callback(res);
                },
                error: function(error) {
                    callback();
                }
            });
        }
    });

   
  $('#country').change(function(){
      globalVar.countryId = $(this).val();
      var selectize = $("#city")[0].selectize; 
      selectize.clear();
      selectize.clearOptions();
      selectize.renderCache['option'] = {}; 
  });

  $('#city').selectize({
    plugins: ['remove_button'],
    maxItems: 1,
    valueField: 'id',
    labelField: 'name',
    searchField: 'name',
    create: false,
    closeAfterSelect: true,
    options: [],
    render: {
            option: function(item, escape) {           
                return '<div>' +escape(item.name)+'</div>';
            }
        },
        load: function(query, callback) {
            if (!query.length) return callback();
            if(query.length>=3){
             $.ajax({
                  url: base_url+'/api/city-suggestion',
                  type: 'GET',
                  dataType: 'json',
                  data: {
                      country_id: globalVar.countryId,
                      q: query
                  },           
                  success: function(res) {
                      callback(res);
                  },
                  error: function(error) {
                      callback();
                  }
              });
            } else {
              var selectize = $("#city")[0].selectize; 
             selectize.renderCache['option'] = {}; 
             return callback();
            }
        }
  });
  

</script>
@endsection