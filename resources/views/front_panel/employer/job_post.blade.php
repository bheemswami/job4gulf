  @extends('/layouts/front_panel_master')
@section('content')
@include('front_panel.includes.employer_page_banner')
@php 
  $gulfCountry=getGulfCountries();
  $otherCountry=getOtherCountries();
@endphp
<section class="main-inner-page">
  <section id="search-section">
    <div class="container">
      <div class="row">
	      @include('front_panel.includes.employer_dashboard_sidebar')        
        <div class="col-sm-9">
            <div class="tabs widget profile-area">
		          <div class="tab-content">
                  <div class="row">          
                      <div class="col-xs-12 col-sm-12 mid-sec-top">
                        <div class="block pt-0">          
                          {{ Form::open(array('url'=>route('save-job-post'),'class'=>'dasboard-form','id'=>'post-job-form'))}}
                                   
                                <div class="form-section">
                                  <div class="form-title">
                                    <h4 class="jobdetail">Job Details</h4>
                                  </div>

                                  <div class="form-group row">
                                  <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Job Type</label>
                                  <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                     {{ Form::select('job_type',[1=>'Basic',2=>'Premium'],null,array('class'=>"form-control",'placeholder'=>'Select Job Type')) }}
                                  </div>
                                </div>

                                  <div class="form-group row">
                                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Job Title/Designation</label>
                                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                      {{ Form::text('job_title',null,array('class'=>"form-control",'id'=>"job_title",'placeholder'=>"Enter Complete Designation")) }}                 
                                    </div>
                                  </div>

                                  <div class="form-group row">
                                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Job Description</label>
                                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                      {{ Form::textarea('job_description',null,array('class'=>"form-control",'id'=>"job_description",'placeholder'=>"Enter Job Description")) }}
                                    </div>
                                  </div>

                                  <div class="form-group row">
                                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Job Location</label>
                                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                      <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 leftnopadding">
                                        {{ Form::select('job_in_country',
                                          ['Gulf Country'=>$gulfCountry,'Other Country'=>$otherCountry],null,
                                        array('class'=>"form-control",'id'=>"job_country",'placeholder'=>"Select Country")) }}
                                      </div>
                                     
                                       <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 nopadding">
                                        {{ Form::TEXT('job_in_city',null,array('class'=>"form-control",'id'=>"job_city",'autocompletion'=>'off','placeholder'=>"Enter City")) }} 
                                         <span id="errorJobInCity"></span>                
                                      </div>
                                    </div>
                                  </div>

                                  <div class="form-group row">
                                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Monthly Salary</label>
                                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 leftnopadding">
                                        {{ Form::select('salary_min',config('constants.min_salary'),null,array('class'=>"form-control",'id'=>"monthly_salary_min",'placeholder'=>"in US$")) }}
                                         <span class="fieldlabel">Minimum</span>                 
                                      </div>
                                      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 nopadding">
                                        {{ Form::select('salary_max',config('constants.max_salary'),null,array('class'=>"form-control",'id'=>"monthly_salary_max",'placeholder'=>"in US$")) }}
                                         <span class="fieldlabel">Maximum</span>                 
                                      </div>
                                    </div>
                                  </div>
                                  
                                  <div class="form-group row">
                                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label"></label>  
                                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                      <lable class="checkbox-btn btn2">{{Form::checkbox('show_salary', 1, true)}}Do not Display the Salary range to Job Seekers</lable>
                                    </div>
                                  </div>

                                  <div class="form-group row">
                                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">No. of vacancies</label>
                                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                      {{ Form::number('total_vacancy',null,array('class'=>"form-control",'id'=>"total_vacancy",'placeholder'=>"Enter No. of Vacancies")) }}                 
                                    </div>
                                  </div>
                                </div>
                                
                                <div class="form-section">
                                  <div class="form-title">
                                    <h4 class="filter">Job Classification</h4>
                                  </div>

                                  <div class="form-group row">
                                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Industry Type</label>
                                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                      {{ Form::select('industry_id',$industry_list,null,array('class'=>"form-control",'id'=>"job_industry",'placeholder'=>"Select Industry")) }}
                                    </div>
                                  </div>             

                                  <div class="form-group row">
                                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Functional Area</label>
                                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                      {{ Form::select('functional_area_id',$functional_area,null,array('class'=>"form-control",'id'=>"functional_area",'placeholder'=>"Select Functional Area")) }}
                                    </div>
                                  </div>

                                   <div class="form-group row">
                                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Keywords</label>
                                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                       {{ Form::textarea('keywords',null,array('class'=>"form-control",'id'=>"key_skills",'placeholder'=>"Enter important keywords that can describe this job")) }}
                                    <span id="errorKeywords"></span>
                                    </div>
                                  </div>
                                </div>
                                            
                                <div class="form-section">
                                  <div class="form-title">
                                    <h4 class="personal_detail">Eligiblity Criteria</h4>
                                  </div>
                                  <div class="form-group row">
                                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Qualification</label>
                                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                      {{ Form::select('quilification[]',$educations,null,array('class'=>"form-control",'id'=>"qualifications",'multiple'=>"multiple")) }}
                                    </div>
                                  </div>

                                   <div class="form-group row">
                                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Specialization</label>
                                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                      {{ Form::select('specialization[]',[],null,array('class'=>"form-control",'id'=>"specialization",'multiple'=>"multiple")) }}
                                    </div>
                                  </div>


                                  <div class="form-group row">
                                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Gender</label>
                                    <div class="col-xs-12 col-sm-9 left-pad3">
                                      <div class="row">
                                       <div class="radioStyle2" >
                                          <label for="male">{{ Form::radio('gender','Male',false,array('class'=>"form-control",'id'=>"male")) }}Male</label>
                                        </div>                    
                                        <div class="radioStyle2">
                                          <label for="female">{{ Form::radio('gender','Female',false,array('class'=>"form-control",'id'=>"female")) }}Female</label>
                                        </div>
                                        <div class="radioStyle2">
                                          <label for="anyone">{{ Form::radio('gender','No Preference',false,array('class'=>"form-control",'id'=>"anyone")) }}No Preference</label>
                                        </div>
                                        <span id="errorGender"></span>
                                     </div>
                                    </div>
                                  </div>

                                  <div class="form-group row">
                                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Work Experience</label>
                                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 leftnopadding">
                                        {{ Form::select('min_experience',getDigitRange(0,30,'year'),null,array('class'=>"form-control",'id'=>"exp_year_from",'placeholder'=>"in Year")) }}                 
                                        <span class="fieldlabel">Minimum</span>
                                      </div>
                                      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 nopadding">
                                        {{ Form::select('max_experience',[],null,array('class'=>"form-control",'id'=>"exp_year_to",'placeholder'=>"in Year")) }}                 
                                        <span class="fieldlabel">Maximum</span>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="form-group row">
                                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label">Nationality</label>
                                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                       {{ Form::select('nationality_id[]',config('constants.nationality'),null,array('class'=>"form-control",'id'=>"nationality",'multiple'=>"multiple")) }}
                                    </div>
                                  </div>

                                  <div class="current_location_section">
                                    <div class="location_elem">
                                      <div class="form-group row">
                                        <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label">Current Location of the Candidate</label>
                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                          {{ Form::select('candidate_current_country[0]',['Gulf Country'=>$gulfCountry,'Other Country'=>$otherCountry],null,array('class'=>"form-control curr_loc",'placeholder'=>"Select Country")) }} 
                                        </div>
                                        
                                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                                          {{ Form::select('candidate_current_city[0][]',[],null,array('class'=>"form-control curr_city",'id'=>'c_city0','multiple'=>"multiple")) }} 
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1 add_more"><i class="fa fa-plus fa-icon-green" aria-hidden="true"></i></div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="form-section">
                                  <div class="form-title">
                                    <h4 class="manageres">Manage Response</h4>
                                  </div>
                                  <div class="form-group row">
                                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Seeker will Respond you at</label>
                                    <div class="col-xs-12 col-sm-8">
                                      <div class="row">                    
                                        <div class="radioStyle2">
                                          <label for="responce_1">{{ Form::radio('response_by','Email',false,array('class'=>"form-control res_manage",'id'=>'responce_1')) }}Email </label>
                                        </div>                    
                                        <div class="radioStyle2">
                                          <label for="responce_2">{{ Form::radio('response_by','ContactDetails',false,array('class'=>"form-control res_manage",'id'=>'responce_2')) }}Contact Details</label>
                                        </div>
                                        <div class="radioStyle2">
                                          <label for="responce_3">{{ Form::radio('response_by','Walkin',false,array('class'=>"form-control res_manage",'id'=>'responce_3')) }}WalkIn</label>
                                        </div>
                                        <div class="radioStyle2">
                                          <label for="responce_4">{{ Form::radio('response_by','All',false,array('class'=>"form-control res_manage",'id'=>'responce_4')) }}All</label>
                                        </div>
                                      </div>
                                      <span id="errorResponseBy"></span>
                                    </div>
                                  </div>

                                  <div class="form-group row manage_sec_1 hide-elem">
                                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Mailing E-Mail ID</label>
                                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                      {{ Form::text('response_email',null,array('class'=>"form-control",'id'=>"mailing_email",'placeholder'=>"Mailing E-Mail ID (Comma seperated for multiple)")) }}                 
                                    </div>
                                  </div>
                                  <div class="form-group row manage_sec_2 hide-elem">
                                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Contact Person</label>
                                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                      <div class="form-row">
                                        <div class="col-xs-12 col-sm-4 form-child-block">
                                          {{ Form::select('job_cp_title',config('constants.name_titles'),null,array('class'=>"form-control",'id'=>"job_cp_title")) }} 
                                        </div>
                                        <div class="col-xs-12 col-sm-8 form-child-block">
                                          {{ Form::text('job_cp_name',null,array('class'=>"form-control",'hint-class'=>"fname_s",'id'=>"job_cp_name",'placeholder'=>"Enter Contact Person Name")) }}
                                        </div>
                                      </div>                  
                                    </div>
                                  
                                  </div>
                                  <div class="form-group row manage_sec_2 hide-elem">
                                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Contact Number</label>
                                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                      <div class="form-row">
                                        <div class="col-xs-12 col-sm-4 form-child-block">
                                          {{ Form::select('job_dial_number',$country_codes,'Male',array('class'=>"form-control",'id'=>"contact_code")) }} 
                                        </div>
                                        <div class="col-xs-12 col-sm-8 form-child-block">
                                          {{ Form::text('job_cp_mobile',null,array('class'=>"form-control",'id'=>"contact_number",'placeholder'=>"Enter Contact Person Number")) }}
                                        </div>
                                      </div>                  
                                    </div>
                                  </div>

                                  <div class="form-group row manage_sec_3 hide-elem">
                                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Address</label>
                                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                       {{ Form::textarea('walkin_address',null,array('class'=>"form-control",'id'=>"walkin_address",'placeholder'=>"Enter WalkIn Address")) }}
                                    </div>
                                  </div>
                                  <div class="form-group row manage_sec_3 hide-elem">
                                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">WalkIn Date</label>
                                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                      <div class="form-row">
                                        <div class="col-xs-12 col-sm-6 form-child-block">
                                          {{ Form::text('walkin_date_from',null,array('class'=>"form-control","id"=>"wdf","readonly"=>true,'placeholder'=>'Select Date','autocomplete'=>'off')) }}
                                          <span id="errorWalkinDateFrom"></span>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 form-child-block">
                                          {{ Form::text('walkin_date_to',null,array('class'=>"form-control","id"=>"wdt","readonly"=>true,'placeholder'=>'Select Date','autocomplete'=>'off')) }}
                                          <span id="errorWalkinDateTo"></span>
                                        </div>
                                      </div>                  
                                    </div>
                                  </div>

                                  <div class="form-group row manage_sec_3 hide-elem">
                                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">WalkIn Starting Time</label>
                                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                      <div class="form-row">
                                        <div class="col-xs-12 col-sm-6 form-child-block">
                                          {{ Form::select('walkin_start_time',getTimeRange('9:00', '19:30'),null,array('class'=>"form-control","id"=>"wst",'placeholder'=>'Select Time')) }}
                                        </div>
                                        <div class="col-xs-12 col-sm-6 form-child-block">
                                          {{ Form::select('time_meridiem',['AM'=>'AM','PM'=>'PM'],null,array('class'=>"form-control")) }}
                                          <span id="errorWalkinDateTo"></span>
                                        </div>
                                      </div>                  
                                    </div>
                                  </div>
                                </div>

                                
                            <div class="form-group row mt-20">
                              <div class="col-sm-9">
                                <button type="submit" class="newfclassub">Post Job</button>
                              </div>
                            </div>
                          {{ Form::close() }}
                        </div>
                      </div>
                  </div>
              </div>
            </div>            
        </div>
      </div>
    </div>
  </section>
</section>

<script>
  globalVar.qIds={};
  globalVar.countryIdComp=0;
  globalVar.countryId=0;
  globalVar.jobCountryId=0;
  globalVar.currCityElem='';
</script>
@include('front_panel/includes/add_job_js_code')
@endsection
