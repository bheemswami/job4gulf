  
@extends('/layouts/front_panel_master')
@section('content')
@include('front_panel.includes.employer_page_banner')

<section class="main-inner-page">
  <section id="search-section">
    <div class="container">
      <div class="row">
	@include('front_panel.includes.employer_dashboard_sidebar')        
            <div class="col-sm-9">
                <div class="tabs widget profile-area">
				          <div class="tab-content">
                    
      
                          <div class="row">          
                            <div class="col-xs-12 col-sm-12 mid-sec-top">
                              <div class="block pt-0">          
                                {{ Form::model($user,array('url'=>route('candidate-profile-update'),'class'=>'dasboard-form','id'=>'canditate-update-form','files'=>true))}}            
                                    <div class="form-section">
                                      <div class="form-title">
                                        <h4 class="acc_info">Account Information</h4>
                                      </div>
                                      <div class="form-group row">
                                        <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Your Email ID</label>
                                        <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                          {{ Form::email('email',null,array('class'=>"form-control",'id'=>"candidate_email",'placeholder'=>"This will become your Username for login")) }}
                                         
                                        </div>
                                      </div>
                                      <div class="form-group row">
                                        <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Password</label>
                                        <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                          {{ Form::password('password',array('class'=>"form-control",'id'=>"candidate_password",'placeholder'=>"Please enter your password")) }}
                                          <span class="pass-show"><i class="fa fa-eye"></i></span>
                                        </div>
                                      </div>
                                    </div>
            
                                    <div class="form-section">
                                      <div class="form-title">
                                        <h4 class="personal_detail">Your Personal Details</h4>
                                      </div>
                                      <div class="form-group row">
                                        <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Full Name</label>
                                        <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                          {{ Form::text('name',null,array('class'=>"form-control hintable",'hint-class'=>"fname_s",'id'=>"name",'data-toggle'=>"tooltip",'placeholder'=>"Please enter your name")) }}
                                         <div class="tooltip-area fname_s">
                                            We will issue cheques in the name you register. Make sure you enter your <strong>real name</strong>.<span class="ttip-arw"></span>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="form-group row">
                                        <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Date of Birth</label>
                                        <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                          @php 
                                            $exp_dob=explode('-',$user->dob);
                                          @endphp
                                          <div class="form-row">
                                            <div class="col-xs-12 col-sm-4 form-child-block">
                                              {{ Form::select('day',getDigitRange(1,31),@$exp_dob[2],array('class'=>"form-control",'id'=>"day",'placeholder'=>"Day")) }}                     
                                            </div>
                                            <div class="col-xs-12 col-sm-4 form-child-block">
                                              {{ Form::select('month',config('constants.month_list'),@$exp_dob[1],array('class'=>"form-control",'id'=>"month",'placeholder'=>"Month")) }} 
                                            </div>
                                            <div class="col-xs-12 col-sm-4 form-child-block">
                                              {{ Form::select('year',getDigitRange((date('Y')-35),date('Y')),@$exp_dob[0],array('class'=>"form-control",'id'=>"year",'placeholder'=>"Year")) }}  
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="form-group row">
                                        <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Gender</label>
                                        <div class="col-xs-12 col-sm-9">                  
                                            <div class="radioStyle2">
                                              <label for="male">{{ Form::radio('gender','Male',false,array('class'=>"form-control",'id'=>"male")) }}Male</label>
                                            </div>                    
                                            <div class="radioStyle2">
                                              <label for="female">{{ Form::radio('gender','Female',false,array('class'=>"form-control",'id'=>"female")) }}Female</label>
                                            </div>
                                        </div>
                                      </div>                                    
                                      <div class="form-group row">
                                        <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Nationality</label>
                                        <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                          {{ Form::select('nationality',config('constants.nationality'),null,array('class'=>"form-control",'id'=>"nationality",'placeholder'=>"Select your nationality")) }} 
                                        </div>
                                      </div>
                                      <div class="form-group row">
                                        <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Currnet Location</label>
                                        <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                          <div class="col-xs-6 col-sm-6 leftnopadding">
                                             {{ Form::select('country',$countries,$user->country_id,array('class'=>"form-control",'id'=>"country",'placeholder'=>"Select Country")) }} 
                                          </div>
                                          <div class="col-xs-6 col-sm-6 nopadding">
                                            {{ Form::select('city',[],null,array('class'=>"form-control",'id'=>"candidate_city",'placeholder'=>"Select City")) }}
                                          </div>
                                        </div>
                                      </div>              
                                      <div class="form-group row">
                                        <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Mobile Number</label>
                                        <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                          <div class="col-xs-4 col-sm-4 leftnopadding">
                                             {{ Form::text('country_code',null,array('class'=>"form-control",'id'=>"country_code",'placeholder'=>"Country Code")) }} 
                                          </div>
                                          <div class="col-xs-8 col-sm-8 nopadding">
                                            {{ Form::text('phone',$user->mobile,array('class'=>"form-control",'id'=>"phone",'placeholder'=>"Mobile Number")) }}
                                            <div class="tooltip-area mobile_s">
                                              Privacy assured. Employers prefer calling you on your mobile. So make sure you enter the right number.
                                              <span class="ttip-arw"></span>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                            
                                    <div class="form-section">
                                        <div class="form-title">
                                          <h4 class="edu_detail">Your Education Details</h4>
                                        </div>
        
                                        <div class="form-group row">
                                          <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Basic Education</label>
                                          <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                           {{ Form::select('basic_course_id',$basic_courses,$user->education->basic_course_id,array('class'=>"form-control","id"=>"basic_course",'placeholder'=>"Select your course")) }} 
                                          </div>
                                        </div>

                                        <div class="basic-elem hide-elem">
                                          <div class="form-group row">
                                            <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">
                                              Specialization
                                            </label>
                                            <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                              {{ Form::select('basic_specialization_id',[],null,array('class'=>"form-control","id"=>"basic_specialization",'placeholder'=>"Select your specialization")) }} 
                                            </div>
                                          </div>
                                          <div class="form-group row">
                                            <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">
                                              Completion Year
                                            </label>
                                            <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                              <div class="form-row">
                                                <div class="col-xs-6 col-sm-4 form-child-block">
                                                   {{ Form::select('basic_comp_year',getDigitRange(1960,date('Y')),$user->education->basic_comp_year,array('class'=>"form-control",'id'=>"year_of_graduation",'placeholder'=>"Year")) }} 
                                                </div>
                                              </div>
                                            </div>              
                                          </div>
                                        </div>

                                        <div class="form-group row">
                                          <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Master Education</label>
                                          <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                           {{ Form::select('master_course_id',$master_courses,$user->education->master_course_id,array('class'=>"form-control","id"=>"master_course",'placeholder'=>"Select your course")) }} 
                                          </div>
                                        </div>
                                      <div class="master-elem hide-elem">
                                        <div class="form-group row">
                                          <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">
                                            Specialization
                                          </label>
                                          <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                            {{ Form::select('master_specialization_id',[],null,array('class'=>"form-control","id"=>"master_specialization",'placeholder'=>"Select your specialization")) }} 
                                          </div>
                                        </div>
                                        <div class="form-group row">
                                          <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">
                                            Completion Year
                                          </label>
                                          <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                            <div class="form-row">
                                              <div class="col-xs-6 col-sm-4 form-child-block">
                                                 {{ Form::select('master_comp_year',getDigitRange(1960,date('Y')),$user->education->master_comp_year,array('class'=>"form-control",'id'=>"year_of_graduation",'placeholder'=>"Year")) }} 
                                              </div>
                                            </div>
                                          </div>              
                                        </div>
                                      </div>
                                      <div class="form-group row">
                                      <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">
                                        Institute / University
                                      </label>
                                      <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                         {{ Form::text('institute',null,array('class'=>"form-control institutes",'placeholder'=>"Select your institution")) }} 
                                       
                                      </div>
                                      </div>
                                    </div>
                                    
                                    
                                      <div class="form-section">
                                        <div class="form-title">
                                            <h4 class="work_exp">Your Current Employment Details</h4>
                                        </div>
                                        <div class="form-group row">
                                          <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Experiance Level</label>
                                          <div class="col-xs-12 col-sm-9">                 
                                              <div class="radioStyle2">
                                                <label>{{ Form::radio('experience_level',1,($user->additional_info->exp_level==1) ? true : false,array('class'=>"form-control exp_level")) }}I have work Experience</label>
                                              </div>
                                              <div class="radioStyle2">
                                                <label>{{ Form::radio('experience_level',0,($user->additional_info->exp_level==0) ? true : false,array('class'=>"form-control exp_level")) }}I am a Fresher</label>
                                              </div>                    
                                          </div>
                                        </div>

                                        <div class="exp_level_fields hide-elem">
                                            <div class="form-group row">
                                              <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">
                                                Your Total Work Experiance
                                              </label>
                                              <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                                <div class="form-row">
                                                  <div class="col-xs-12 col-sm-6 form-child-block">
                                                    {{ Form::select('year_experiance',getDigitRange(0,30),$user->additional_info->exp_year,array('class'=>"form-control",'id'=>"year_experiance",'placeholder'=>"Select")) }}
                                                    <span class="fieldlabel">Years</span>
                                                  </div>
                                                  <div class="col-xs-12 col-sm-6 form-child-block">
                                                    {{ Form::select('month_experiance',getDigitRange(0,12),$user->additional_info->exp_month,array('class'=>"form-control",'id'=>"month_experiance",'placeholder'=>"Select")) }}
                                                    <span class="fieldlabel">Moths</span>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                            <div class="form-group row">
                                              <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">
                                                Current Employer Name
                                              </label>
                                              <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                                {{ Form::text('current_company',null,array('class'=>"form-control",'id'=>"current_company",'data-toggle'=>"tooltip",'placeholder'=>"Enter Company Name")) }}
                                              </div>
                                            </div>
                                            <div class="form-group row">
                                              <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">
                                                Your Current Position
                                              </label>
                                              <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                                {{ Form::text('current_position',$user->additional_info->current_position,array('class'=>"form-control",'id'=>"current_position",'placeholder'=>"Mention complete designation")) }}
                                              </div>
                                            </div>
                                            <div class="form-group row">
                                              <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">
                                                Current Employer's Industry
                                              </label>
                                              <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                                {{ Form::text('current_industry',null,array('class'=>"form-control",'id'=>"current_industry",'placeholder'=>"Select")) }}
                                              </div>
                                            </div>
                                            <div class="form-group row">
                                              <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">
                                                Your Monthly Salary
                                              </label>
                                              <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                                <div class="form-row">
                                                  <div class="col-xs-12 col-sm-6 form-child-block">
                                                    {{ Form::text('salary',$user->additional_info->salary,array('class'=>"form-control",'id'=>"salary",'placeholder'=>"Select")) }}
                                                    </div>
                                                  <div class="col-xs-12 col-sm-6 form-child-block">
                                                    {{ Form::select('currency',config('constants.currency'),$user->additional_info->salary_in,array('class'=>"form-control",'id'=>"currency",'placeholder'=>"Select")) }}
                                                    </div>
                                                </div>
                                              </div>
                                            </div>
                                            <div class="form-group row">
                                              <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">
                                                Functional Area / Department
                                              </label>
                                              <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                                {{ Form::text('functional_area',null,array('class'=>"form-control",'id'=>"functional_area",'placeholder'=>"Select")) }}
                                              </div>
                                            </div>
                                        </div>
                                        <div class="form-group row fresher_level_fields hide-elem">
                                          <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">
                                            Key Skills
                                          </label>
                                          <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                            {{ Form::text('key_skills',null,array('class'=>"form-control hintable",'hint-class'=>"keyskills_s",'id'=>"key_skills",'data-toggle'=>"tooltip",'placeholder'=>"Key Skills")) }}
                                          <div class="form-field-note row">
                                              <div class="col-xs-12 text-right">
                                                Max 250 characters
                                              </div>
                                            <div class="tooltip-area keyskills_s">
                                              Enter your expertise or specialities here separated by commas.
                                              <span class="ttip-arw"></span>
                                            </div>
                                            </div>
                                          </div>
                                        </div>
                                        <hr/>
                                        <div class="form-group row">
                                          <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">
                                            Resume Headline
                                          </label>
                                          <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                             {{ Form::text('resume_headline',$user->additional_info->resume_headline,array('class'=>"form-control hintable",'hint-class'=>"headline_s",'id'=>"resume_headline",'data-toggle'=>"tooltip",'placeholder'=>"Key Skills")) }}
                                            <div class="tooltip-area headline_s">
                                              Please enter a title for your resume. <strong>Eg:</strong> Java Programmer,4 yrs exp,B.Tech
                                              <span class="ttip-arw"></span>
                                            </div>
                                          </div>
                                        </div>
                                        <div class="form-group row">
                                          <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label">
                                            Upload Resume
                                          </label>
                                          <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                            <div class="upload-btn-wrapper">
                                              <button class="btn">Upload Your Resume</button>
                                              {{ Form::file('resume_file',null,array('class'=>"",'id'=>"resume_file")) }}    
                                            </div>                 
                                            <div class="form-field-note">
                                              Upload only Word (.doc/.docx), Pdf (.pdf) or Text (.txt) formats.
                                            </div>
                                          </div>
                                        </div>

                                        <div class="form-group row">
                                          <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label">
                                            Upload Your Photo
                                          </label>
                                          <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                            
                                            <div class="upload-btn-wrapper">
                                              <button class="btn">Upload Your Photo</button>
                                              {{ Form::file('user_img',array('class'=>"",'id'=>"user_img")) }}
                                            </div>
                                            <div class="form-field-note">
                                              A Profile with Photo has a 40% higher chance of getting viewed by Employers
                                            </div>
                                          </div>
                                        </div>

                                        <div class="form-group row">
                                          <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">
                                            Interested in
                                          </label>
                                          <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                            @foreach(config('constants.interested_in') as $k=>$val)
                                              <label class="checkbox-btn">
                                                {{ Form::checkbox('interested_in[]',$k,false,array('class'=>"")) }}
                                                {{ $val }}
                                              </label>
                                            @endforeach
                                           
                                          </div>
                                        </div>
                                      </div>              
                                      <div class="form-group row mt-20">
                                        <div class="col-sm-9">
                                          <button type="submit" class="newfclassub">Submit Resume</button>
                                        </div>
                                      </div>
                                  {{ Form::close() }}
                                </div>
                            </div>
                          </div>
                      
                   
                  </div>
  <!-- tab-content --> 
                </div>
<!-- tabs-widget -->              
            </div>
      </div>
    </div>
  </section>
</section>

<script>
globalVar.city_id='{{$user->city_id}}';
globalVar.basic_s_id='{{$user->education->basic_specialization_id}}';
globalVar.master_s_id='{{$user->education->master_specialization_id}}';

$("#user_img").change(function(){
    previewImage(this,'.user-default');
});
  $(function(){
      $("#country").change();
      $("#basic_course").change();
      $("#master_course").change();
      expLevelElem({{$user->additional_info->exp_level}});

    $('#current_company').selectize({
        maxItems: 1,
        valueField: 'id',
        labelField: 'name',
        searchField: 'name',
        create: false,
        options: {!! json_encode($companies) !!},
        onInitialize: function(){
          var selectize = this;
          selectize.setValue({{$user->additional_info->current_company}});
        }
    });

    $('#current_position').selectize({
        maxItems: 1,
        valueField: 'name',
        labelField: 'name',
        searchField: 'name',
        create: true,
        options: [],
        render: {
            option: function(item, escape) {           
                return '<div>' +escape(item.name)+'</div>';
            }
        },
        load: function(query, callback) {
            if (!query.length) return callback();
            $.ajax({
                url: base_url+'api/designation-suggestion',
                type: 'GET',
                dataType: 'json',
                data: {
                    q: query,
                },           
                success: function(res) {
                    callback(res);
                },
                error: function(error) {
                    callback();
                }
            });
        }
    });

   $('#functional_area').selectize({
        maxItems: 1,
        valueField: 'id',
        labelField: 'name',
        searchField: 'name',
        create: false,
        options: {!! json_encode($f_area) !!},
        onInitialize: function(){
          var selectize = this;
          selectize.setValue({{$user->additional_info->functional_area}});
        }
    });

   $('#current_industry').selectize({
        maxItems: 1,
        valueField: 'id',
        labelField: 'name',
        searchField: 'name',
        create: false,
        options: {!! json_encode($industry) !!},
        onInitialize: function(){
          var selectize = this;
          selectize.setValue({{$user->additional_info->current_industry}});
        }
    });    
   
  });

  $('#key_skills').selectize({
      plugins: ['remove_button'],
      maxItems: 15,
      valueField: 'name',
      labelField: 'name',
      searchField: 'name',
      create: true,
      options: {!! json_encode($skill_keywords) !!},
      onInitialize: function(){
          var selectize = this;
          var skill='{{$user->additional_info->key_skills}}';
          var splitVal=skill.split(',');
          var selected_items=splitVal;
          selectize.setValue(splitVal);
      }
  });
  $('.institutes').selectize({
      maxItems: 1,
      valueField: 'name',
      labelField: 'name',
      searchField: 'name',
      create: true,
      options: {!! json_encode($institutes) !!},
      onInitialize: function(){
          var selectize = this;
          selectize.setValue(['{{$user->education->institute}}']);
      }
  });

  function rePopulateCourse(data=null){
    var selectize = $(".course")[0].selectize; 
    selectize.clear();
    selectize.clearOptions();
    selectize.renderCache['option'] = {};
    if(data!=null)
      selectize.addOption(data);
  }

  function rePopulateSpecialization(data=null){
    var selectize = $(".specialization")[0].selectize; 
    selectize.clear();
    selectize.clearOptions();
    selectize.renderCache['option'] = {};
    if(data!=null)
      selectize.addOption(data);
  }


  $('.exp_level').click(function(e){
      expLevelElem($(this).val());
  });
  function expLevelElem(input_val){
    if(input_val==0){
      $('.exp_level_fields').hide();
    } else {
      $('.exp_level_fields').show();
    }
    $('.fresher_level_fields').show();
  }

$('#country').change(function(e){
    var post_data={country_id:$(this).val()};
    globalFunc.ajaxCall('api/city-by-country', post_data, 'POST', globalFunc.before, globalFunc.listOfCities, globalFunc.error, globalFunc.complete);
});

$('#basic_course').change(function(e){
    if($(this).val()>0){
      $('.basic-elem').show();
    } else {
      $('.basic-elem').hide();
    }
    var post_data={course_id:$(this).val()};
    globalFunc.ajaxCall('api/specialization-by-course', post_data, 'POST', globalFunc.before, globalFunc.listOfSpecializationBasic, globalFunc.error, globalFunc.complete);
});
$('#master_course').change(function(e){
    if($(this).val()>0){
      $('.master-elem').show();
    } else {
      $('.master-elem').hide();
    }
    var post_data={course_id:$(this).val()};
    globalFunc.ajaxCall('api/specialization-by-course', post_data, 'POST', globalFunc.before, globalFunc.listOfSpecializationMaster, globalFunc.error, globalFunc.complete);
});


</script>
@endsection
