<div class="col-sm-12">
              <ul class="tabs-nav comp_by_cat" role="tablist">
                @foreach($top_employers as $k=>$val)
                  <li role="presentation" style="">
                    <a href="{{url('companies/'.$val->id)}}">   
                        <span class="icon">       
                        <img src="{{checkFile($val->comp_logo,'uploads/employer_logo/','company_logo.png')}}" class="busimg">     
                        </span>{{ $val->comp_name }}
                    </a>
                        <span class="bgcolor-major-gradient-overlay"></span>
                  </li>
                @endforeach
              </ul>
            </div> 
<div class="custom-pagination">
{{$top_employers->links()}}
</div>
@php
    $totalRec = count($top_employers);
  if($totalRec>0){
    $totalRec = $top_employers->firstItem().' - '.$top_employers->lastItem().' of '.$top_employers->total();
  }
@endphp
<script>

var records_of ='{{$totalRec}}';
$('#total_get_employers').html('<span>'+records_of+' Employers </span>');
  var search='{{$search_tag}}';
var array = search.split(',');

if(search)
{
  $('#search_key').html('');
  $.each( array, function( key, value ) {
    if(value )
    $('#search_key').append('<span class="label label-info">'+value+'</span>');
  });
}
</script>
                             