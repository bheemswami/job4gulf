
@extends('/layouts/front_panel_master')
@section('content')

@include('front_panel.includes.employer_page_banner')
<style>
.dataTables_filter { display: none; }
</style>
<section class="main-inner-page">
  <section id="search-section">
    <div class="container">
      <div class="row">
         @include('front_panel.includes.employer_dashboard_sidebar')
        <div class="col-sm-9">

           <div class="tabs widget profile-area">
                  <div class="tab-content">
                    <div class="row">          
                      <div class="col-xs-12 col-sm-12 mid-sec-top">
                        <div class="block pt-0"> 
                           {{ Form::open(array('url'=>route('add-slider-image'),'class'=>'dasboard-form','id'=>'slider-img-form','files'=>true))}}
                            {{ Form::hidden('employer_id',$emp_data->id)}}
                           <div class="form-section">
                             <div class="form-title">
                               <h4 class="icon_landscape">Add Image</h4>
                             </div>
                             

                             <div class="form-group row">  
                                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label"> Company Logo</label>
                                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                  <img src="{{url('public/images/default.png')}}" class="user-default" style="width:150px;">
                                  <div class="upload-btn-wrapper">
                                    <button class="btn">Upload Logo</button>
                                    {{ Form::file('slider_image',array('class'=>"upload",'id'=>"slider_img")) }}
                                  </div>
                                  <div class="form-field-note">
                                    Upload only Image (.png/.jpg/.jpeg) formats.
                                  </div>
                                </div>
                              </div>

                             {{-- <div class="form-group row">
                               <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Sort Order</label>
                               <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                 {{ Form::number('sort_order',1,array('class'=>"form-control",'id'=>"contact_person",'min'=>"1")) }}
                               </div>
                             </div>
                             <div class="form-group row">
                               <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Status</label>
                               <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                  {{ Form::select('status',[0=>'Inactive',1=>'Active'],1,array('class'=>"form-control",'id'=>"",'placeholder'=>"Select Status")) }}
                               </div>
                             </div>
                           </div> --}}
                             
                           <div class="form-group row mt-20">
                             <div class="col-sm-9 col-sm-offset-3 nopadding">
                               <button type="submit" class="newfclassub">Add</button>
                             </div>
                           </div>
                          {{ Form::close() }}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>  

          <div class="tabs widget profile-area">
            <div class="tab-content">
              <div class="form-section">
                 <div class="form-title">
                   <h4 class="icon_landscape">All Images</h4>
                 </div>
                  <table class="table table-bordered table-striped data_table hide-searchbox">
                    <thead>
                      <tr>
                        <th>S.No</th>
                        <th class="nosort">Image</th>
                        {{-- <th>Sort Order</th> --}}
                       {{--  <th>Status</th> --}}
                        <th class="nosort">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @if($emp_data->slider_images!=null)
                      @forelse($emp_data->slider_images as $k=>$val)
                      <tr>
                        <td>{{++$k}}.</td>
                        <td><img style="width:100px" src="{{checkFile($val->image,'uploads/slider_image/','no_images.png')}}" class="card-img-top busimg"></td>
                       {{--  <td>{{$val->sort_order}}</td> --}}
                       {{--  <td>{!!($val->status==1) ? '<span class="btn btn-xs btn-success">Active</span>' : '<span class="btn btn-xs btn-default">Inactive</span>'!!}</td> --}}
                        <td>
                          <a href="{{route('delete-slider-image',['id'=>$val->id])}}" onclick="return confirm('Are you sure?');" title="Delete Slider Image"><i class="fa fa-trash-o fa-red" aria-hidden="true"></i></a>
                        </td>
                      </tr>
                     @empty
                     @endforelse
                     @endif
                    </tbody>
                  </table>
              </div>
            </div>
          </div>
        </div>
    </div>
  </section>
</section>
<script>
   $("#slider_img").change(function(){
    previewImage(this,'.user-default','{{url('public/images/default.png')}}');
});
</script>
@endsection