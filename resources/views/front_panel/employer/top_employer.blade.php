@extends('/layouts/front_panel_master')
@section('content')
<section id="inner-banner" style="background: url({{url('public/images/inner-banner.jpg')}}) no-repeat center top;">
<div class="overlay"></div>
 @include('front_panel/includes/search_job_section')
</section>
<section class="main-inner-page">
  <section id="search-section">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-12">
            <div class="top-block col-lg-12">
              <div class="pull-left">
                <h5>Top Employeres: </h5>
                 <div id='search_key'>  </div>
              </div>
              <div class="clearfix text-right">
                <span id="total_get_employers">0 Employers </span>
                {{-- <div class="shorting-field">
                  <select>
                    <option value="" style="display: none;">Short By</option>
                    <option value="">Ascending Order</option>
                    <option value="">Descending Order</option>
                  </select>
                </div> --}}
              </div>
            </div>
          
        </div>
      </div>
      <div class="row">
         <div class="col-xs-12 col-sm-3 sidebar-parent">
                      <div class="left-sidebar">
                          <form method="get" id="filter_form">

                              <div class="country-filter filter-block">
                                  <h4 class="btn" data-toggle="collapse" data-target="#country"> Country</h4>
                                  <div id="country" class="collapse in">
                                      <ul>
                                          @foreach($country_list as $k=>$val)
                                          <li>
                                              <label class="checkbox-btn">
                                                  <input type="checkbox" name="country_ids" value="{{$val->id}}" class="checkbox" /> <b class="listbo">{{$val->name}}</b>
                                              </label>
                                              <div class="count">({{count($val->employer)}})</div>
                                          </li>
                                          @endforeach
                                      </ul>
                                  </div>
                              </div>

                              <div class="city-filter filter-block">
                                  <h4 class="btn collapsed" data-toggle="collapse" data-target="#city"> City</h4>
                                  <div id="city" class="collapse">
                                      <ul>
                                          @foreach($city_list as $k=>$val)
                                          <li>
                                              <label class="checkbox-btn">
                                                  <input type="checkbox" name="city_ids" value="{{$val->id}}" class="checkbox" /> <b class="listbo">{{$val->name}}</b>
                                              </label>
                                              <div class="count">({{count($val->employer)}})</div>
                                          </li>
                                          @endforeach
                                      </ul>
                                  </div>
                              </div>

                              <div class="industry-filter filter-block">
                                  <h4 class="btn collapsed" data-toggle="collapse" data-target="#industry"> Category</h4>
                                  <div id="industry" class="collapse">
                                      <ul>
                                          @foreach($industry_list as $k=>$val)
                                          <li>
                                              <label class="checkbox-btn">
                                                  <input type="checkbox" name="industry_ids" value="{{$val->id}}" class="checkbox" /> <b class="listbo">{{$val->name}}</b>
                                              </label>
                                              <div class="count">({{count($val->employer)}})</div>
                                          </li>
                                          @endforeach
                                      </ul>
                                  </div>
                              </div>
                          </form>
                      </div>
                  </div>

                  <div class="col-xs-12 col-sm-9 mt-15">
                    <div class="tabbed-about-us tabbed-about-us-v2">
                          <div class="row">
                              <div id="load" style="position: relative;">
                                @include('front_panel/employer/top_employer_list')
                              </div>
                          </div>
                    </div>
                  </div>

      </div>
    </div>
    <!-- </div> -->
  </section>
</section>

<script>
  var url = '{{route('top-employers')}}';
  $(document).on('click','.custom-pagination a',renderData);
  

  $(document).on('click','.checkbox',function(){
    var form = $('#filter_form').serializeArray();
    var industry_ids = [];
    var city_ids = [];
    var country_ids = [];
    $.each(form,function(index,value){
      if(value.name=='country_ids'){
        country_ids.push(value.value);
      }else if(value.name=='city_ids'){
        city_ids.push(value.value);
      }else if(value.name=='industry_ids'){
        industry_ids.push(value.value);
      }
    });
    var data = {'country_ids':country_ids.join(),'city_ids':city_ids.join(),'industry_ids':industry_ids.join()};
    ajaxCall(data);
  });

  function renderData(e){
      e.preventDefault();
      url = $(this).attr('href');
      ajaxCall();
      window.history.pushState("", "", url);
  }
  function ajaxCall(data=NaN){

    $.ajax({
          url : url,
          data : data,
      }).done(function (data) {
          $('#load').html(data); 
      }).fail(function () {
        
      }).complete(function(){
    });
  }
  
</script>
@endsection