
@extends('/layouts/front_panel_master')
@section('content')

<section id="home-banner" style="background: url('{{url('public/images/home-slider/banner1.jpg')}}') 0 -20px; background-size:cover;">
  <div class="container">
    <div class="row">
      
      <div class="col-xs-8 col-sm-8 banner-text col-sm-offset-1">
        <h1>Find Your Dream Job in Gulf</h1>
        <h3>Join <span>Top</span> Companies Currently Hiring</h3>
      </div>
      <div class="col-xs-2 col-sm-2 banner-login text-center">
        <i class="fa fa-user-circle-o" aria-hidden="true"></i>
        <h5>Job Seeker</h5>
        <ul>
          <li><a href="{{route('candidate-register')}}">Register CV</a></li>
        </ul>
      </div>
    <div class="row home-banner-form">
        @include('front_panel/includes/search_job_section')
    </div>
  </div>
</section>

<section id="employers-hiring-section">
  <div class="container">
    <div class="title-style-1 clearfix">
      <h1 class="text-center">Top Consultants Hiring</h1>
    </div>
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-10 col-lg-offset-1">
        <div id="employers-slider" class="">
          @foreach($top_employers as $k=>$val)
            @if($val->comp_type==2)
              <div class="item">
                <div class="employers-box" style="border-top: 6px solid #f98a9a;">                 
                 <a href="{{url('companies/'.$val->id)}}">
                    <img src="{{checkFile($val->comp_logo,'uploads/employer_logo/','company_logo.png')}}" class="img-responsive center-block">
                </a>
                  <div class="content">{{ $val->comp_name }} <span>{{ ($val->job_post!=null) ? count($val->job_post) : 0 }} job(s)</span></div>
                </div>
              </div>
            @endif
          @endforeach
          
        </div>
      </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-10 col-lg-offset-1 text-right link-section">
      <a href="{{route('top-employers')}}" class="view-more-btn">More Companies <i class="fa fa-fw fa-angle-double-right text-primary"></i></a>
    </div>
  </div>
</section>

<section id="employers" style="background: url('{{url('public/images/employeers.jpg')}}') no-repeat center top;">
  <div class="overlay-bg">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          
          <div class="col-xs-12 col-sm-7 col-md-12 right">
            <div class="title-style2 clearfix">
              <h1>Employers</h1>
            </div>
            <h3>Advertise your job to get Qualified applicants</h3>
            <form class="post-free-job-form" action="" method="post">
              <div class="form-group">
                <div class="form-checkbox">
                  <input type="checkbox" name="" checked disabled="disabled">
                  <label for="">Receive unlimited response for your job ad</label>
                </div>
                <div class="form-checkbox">
                  <input type="checkbox" name="" checked disabled="disabled">
                  <label for="">Each of your Ad remainse active for 60 days </label>
                </div>
                <div class="form-checkbox">
                  <input type="checkbox" name="" checked disabled="disabled">
                  <label for="">Apply to over 10000 jobs in gulf</label>
                </div>
              </div>
              @if(Auth::check() && Auth::user()->role==3)
                <button type="submit" class=""><a href="{{route('job-post')}}">Post a Free Job</a></button>
              @elseif(Auth::check() && Auth::user()->role==2)
                <button type="submit" class=""><a  href="#" data-toggle="modal" data-target="#alertModal">Post a Free Job</a></button>
              @else
                <button type="submit" class=""><a href="{{route('free-job-post')}}">Post a Free Job</a></button>
              @endif
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="jobs-section">
  <div class="container">
    <div class="title-style-1 clearfix">
      <h1 class="text-center">Premium Jobs</h1>
    </div>
    <div class="row">
      @php 
        $pJobs = getPremiumJobs();
        $pJobIds=[];
      @endphp
      @foreach($pJobs as $k=>$val)
        @php 
        $pJobIds[]=$val->id;
        @endphp
        <div class="col-xs-12 col-md-6">
          <div class=" job-box">
            <div class="row flexbox">
              <div class="col-xs-3 col-sm-2 left">
                <div class="div-table">
                  <div class="table-cell-div">
                    @if($val->employer_info!=null)
                      <img src="{{checkFile($val->employer_info->comp_logo,'uploads/employer_logo/','company_logo.png')}}" class="center-block">
                    @else 
                      <img src="{{url('public/images/default/company_logo.png')}}" class="center-block">
                    @endif

                  </div>
                </div>
              </div>
              <div class="col-xs-9 col-sm-10 right">
                <h2><a href="{{route('job-details',['jobid'=>$val->id])}}">{{$val->job_title}}</a></h2>
                <h5>{{ ($val->employer_info!=null) ? $val->employer_info->comp_name : '' }}</h5>
                <div class="post-details">
                  <p>{{limit_text($val->key_skill,80)}}</p>
                  <p> 
                    <span><i class="fa fa-suitcase" aria-hidden="true"></i>{{$val->min_experience}}-{{$val->max_experience}} years</span> 
                    <span><i class="fa fa-map-marker" aria-hidden="true"></i>                     
                     {{ getCityNames($val->jobs_in_city) }} - {{ ($val->country!=null) ? $val->country->name : '' }}
                    </span>
                  </p>
                </div>
              </div>
            </div>
            <div class="job_optwrap">
              <p>Posted : {{dateConvert($val->created_at,'jS M Y')}}</p>
            </div>
          </div>
        </div>  
      @endforeach
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right link-section nopadding">
      <a href="{{route('search-jobs')}}" class="view-more-btn">Browse More <i class="fa fa-fw fa-angle-double-right text-primary"></i></a>
    </div>
  </div>
</section>

<section id="job-seekers" style="background: url('{{url('public/images/job-seeker.jpg')}}') no-repeat center top;">
  <div class="overlay-bg">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          
          <div class="col-xs-12 col-sm-7 col-md-6 left">
            <div class="title-style2 clearfix">
              <h1>Job Seekers</h1>
            </div>
            <h3>Advertise your job to get Qualified applicants</h3>
            <form class="post-free-job-form" action="" method="post">
              <div class="form-group">
                <div class="form-checkbox">
                  <input type="checkbox" name="" checked disabled="disabled">
                  <label for="">Receive unlimited response for your job ad</label>
                </div>
                <div class="form-checkbox">
                  <input type="checkbox" name="" checked disabled="disabled">
                  <label for="">Each of your Ad remainse active for 60 days </label>
                </div>
                <div class="form-checkbox">
                  <input type="checkbox" name="" checked disabled="disabled">
                  <label for="">Apply to over 10000 jobs in gulf</label>
                </div>
              </div>
             
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="recent-jobs">
  <div class="container">
    <div class="title-style-1 clearfix">
    <h1 class="text-center">Recently Posted Jobs</h1>
    </div>
    <div class="row">
      @php 
        $rJobs = getRecentJobs($pJobIds);
        
      @endphp
      @foreach($rJobs as $k=>$val)
        <div class="col-xs-12 col-md-6">
          <div class=" job-box">
            <div class="row flexbox">
              <div class="col-xs-3 col-sm-2 left">
                <div class="div-table">
                  <div class="table-cell-div">
                    @if($val->employer_info!=null)
                      <img src="{{checkFile($val->employer_info->comp_logo,'uploads/employer_logo/','company_logo.png')}}" class="center-block">
                    @else 
                      <img src="{{url('public/images/default/company_logo.png')}}" class="center-block">
                    @endif

                  </div>
                </div>
              </div>
              <div class="col-xs-9 col-sm-10 right">
                <h2><a href="{{route('job-details',['jobid'=>$val->id])}}">{{$val->job_title}}</a></h2>
                <h5>{{ ($val->employer_info!=null) ? $val->employer_info->comp_name : '' }}</h5>
                <div class="post-details">
                  <p>{{limit_text($val->key_skill,80)}}</p>
                  <p> 
                    <span><i class="fa fa-suitcase" aria-hidden="true"></i>{{$val->min_experience}}-{{$val->max_experience}} years</span> 
                    <span><i class="fa fa-map-marker" aria-hidden="true"></i>                     
                     {{ getCityNames($val->jobs_in_city) }} - {{ ($val->country!=null) ? $val->country->name : '' }}
                    </span>
                  </p>
                </div>
              </div>
            </div>
            <div class="job_optwrap">
              <p>Posted : {{dateConvert($val->created_at,'jS M Y')}}</p>
            </div>
          </div>
        </div>  
      @endforeach
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right link-section nopadding">
       <a href="{{route('search-jobs')}}" class="view-more-btn">Browse More <i class="fa fa-fw fa-angle-double-right text-primary"></i></a>
    </div>
  </div>
</section>


<section id="map-sec">
    <div class="title-style2 clearfix col-md-6 nopadding" id="job-country">
      <h1 class="text-center">Find Jobs By Country</h1>
      
        @foreach($countries as $val)
        <div class="col-xs-6 col-sm-4 col-md-4 col-lg-3 category-parent-box">
          <div class="country-block">
            <a href="{{url('search-jobs?country_ids='.$val->id)}}">
              <img class="flag" alt="image" src="{{$val->icon}}">
              <h3>{{$val->name}}</h3>
              <h4>({{ (isset($val->job_post) && $val->job_post!=null) ? count($val->job_post) : 0 }})</h4>
            </a>
          </div>
        </div>
        @endforeach
      </div>
    <div class="title-style2 clearfix text-center col-md-6 nopadding" id="job-consultant">
      <h1>Top Consultants</h1>
      <section class="lazy slider" data-sizes="50vw">
       @php $i=1; @endphp
        @foreach($top_employers as $k=>$val)
            @if($val->comp_type==1)
              @if($i==1)
                <div>
              @endif

                <div class="col-xs-6 col-sm-4 col-md-4 col-lg-3 cimg">
                  <a href="consultant-detail/{{$val->id}}"><img src="{{checkFile($val->comp_logo,'uploads/employer_logo/','company_logo.png')}}" class="center-block">
                    <h3>{{limit_text($val->comp_name,30)}}</h3>
                    <p>{{ ($val->country!=null) ? '('.$val->country->name.')' : '' }}</p>
                    <div class="cons-info">
                        <div class="cons-text">
                          <h3>{{$val->comp_name}}</h3>
                        <p>({{ ($val->job_post!=null) ? count($val->job_post) : 0 }} jobs)</p>
                      </div>
                    </div>
                  </a>

                </div>
                 @php                  
                 if($i%8==0){
                    $i=0; 
                    echo '</div>';
                 }
                 $i++; @endphp
            @endif           
          @endforeach
      </section>
    </div>
</section>

<section id="popular-categories">
  <div class="container">
    <div class="title-style-1 clearfix text-center">
      <h1>Popular Categories</h1>
    </div>
    <div class="row">
      @foreach($industries as $val)
          <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3 category-parent-box">
            <div class="category-block">
              <a href="{{url('search-jobs?industry_ids='.$val->id)}}" >
                <img src="{{url('public/images/icon3.jpg')}}" class="img-responsive center-block">
                <h4>{{$val->name}}</h4>
                <h4>({{ (isset($val->job_post) && $val->job_post!=null) ? count($val->job_post) : 0 }})</h4>
              </a>
            </div>
          </div>
        @endforeach

      
      
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right link-section nopadding">
      <a href="" class="view-more-btn">Browse More <i class="fa fa-fw fa-angle-double-right text-primary"></i></a>
    </div>
  </div>
</section>

@if(count($candidates)>0)
<section id="condidate-section">
  <div class="container">
    <div class="title-style-1 clearfix text-center">
      <h1>Recently Registerd Candidate</h1>
    </div>
    <div id="condidate-slider" class="">
      @foreach($candidates as $k=>$user)
      <div class="item">
        <div class="condidate-box">
          <div class="center-block condidate-img" style="background: url('{{checkFile($user->avatar,'uploads/user_img/','user_img.png')}}') no-repeat center top;"></div>
          <h3 class="condidate-name">{{$user->name}}</h3>
          <h4 class="condidate-post">{{ ($user->additional_info!=null) ? $user->additional_info->current_position : '' }}</h4>          
        </div>
      </div>
      @endforeach
     {{--  <div class="item">
        <div class="condidate-box">
          <div class="center-block condidate-img" style="background: url('{{url('public/images/profile.jpg')}}') no-repeat center top;"></div>
          <h3 class="condidate-name">John Doe</h3>
          <h4 class="condidate-post">Producer Manager</h4>
          <img class="img-responsive candidate-logo center-block" src="{{url('public/images/microsoft.png')}}">
        </div>
      </div> --}}
    </div>
  </div>
</section>
@endif

<!-- Start Modal Alert -->
    <div class="modal fade" id="alertModal" tabindex="-1" role="dialog" aria-labelledby="userLoginModalLabel" aria-hidden="true">
      <div class="modal-dialog login-modal" role="document">
        <div class="modal-content">
          <div class="modal-header">
              <h4 class="modal-title">Post Job</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
          <div class="modal-body flexbox">
              <div class="col-xs-12 col-sm-12 right text-center">
                  <div class="form-group clearfix">
                    <label class="col-xs-12 nopadding alert-msg">Please login as employer to post a job. </label>
                    </div>
                  <div class="form-group clearfix">
                    <a class="link-btn" href="{{route('free-job-post')}}">Ok</a>
                    <button type="submit" class="closemodal">Cancel</button>
                  </div>
              </div>
          </div>
        </div>
      </div>
    </div>
  <!-- End Modal Alert -->

<script>
  $('.closemodal').click(function() {
    $('#alertModal').modal('hide');
});
  $(function() {
    // for key skill //
    $( "#skills" ).autocomplete({
      source: '{{url("listing/get_key")}}'
    });
  });
  $(function() {
    // for location //
    $( "#location" ).autocomplete({
      source: '{{url("listing/country_state_city")}}'
    });
  });
 
  </script>
@endsection
