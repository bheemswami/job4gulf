@extends('/layouts/front_panel_master')
@section('page_title') | Register @endsection
@section('content')
@include('front_panel/includes/page_banner')

<section class="main-inner-page lite-greyBg">
  <section id="search-section">
    <div class="container">
      <div class="row">
       
        
        <div class="col-xs-12 col-sm-10 mid-sec-top">
          <div class="section-title text-center">
                <h2><span class="textgreen">Become a Jobs4Gulf Member,</span> it's free</h2>
                <ul>
                  <li>Build an effective professional profile</li>
                  <li>Apply to over 50,000 live jobs</li>
                  <li>Have full control over your privacy settings</li>
                  <li>Get headhunted by top employers in Gulf</li>
                  <li>Receive relevant Job Alerts in your email</li>

                   
                </ul>
            </div>
          <div class="block pt-0"> 
          @php
            $features = config('constants.jobseeker_features');
          @endphp         
            {{ Form::open(array('url'=>route('save-candidate'),'class'=>'form','id'=>'canditate-registration-form','files'=>true))}}            
            <div class="form-section">
            <table class="table table-bordered">
                <tbody>
                    <tr>
                        <th>Benifits</th>
                        <th>Premium</th>
                        <th>Free</th>
                    </tr>
                @foreach($features as $k=>$val)
                  @if($val['id']==1)
                    <tr class="text-info">
                        <th>
                          <b>{!! $val['heading'] !!}</b> {!! $val['description'] !!}  {!! ($val['note']!='') ? '<br/><span class="text-danger">'.$val['note'].'</span>' : ''; !!}
                        </th>
                        <th>&#8377; {{ $val['price'] }}</th>
                        <th>Free</th>                    
                    </tr>
                  @else
                      <tr>
                          <td>
                            <b>{!! $val['heading'] !!}</b> {!! $val['description'] !!}
                            {!! ($val['note']!='') ? '<br/><span class="text-danger">'.$val['note'].'</span>' : ''; !!}
                          </td>
                          <td>{!! ($val['is_premium']==1) ? '<span class="icon-yes fa fa-check"></span>' : '<span class="icon-no fa fa-times"></span>' !!}</td>
                          <td>{!! ($val['is_free']==1) ? '<span class="icon-yes fa fa-check"></span>' : '<span class="icon-no fa fa-times"></span>' !!}</td>                    
                      </tr>
                  @endif
                @endforeach
                  
                <tr class="text-success">
                  <th>Select Membership type*: <span id="errorPlanId"></span></th>
                  <td><label class="radio-inline">{{ Form::radio('plan_id',1,false,array('class'=>"")) }}</label></td>
                  <td><label class="radio-inline">{{ Form::radio('plan_id',0,false,array('class'=>"")) }}</label></td>
                </tr>            
                </tbody>
            </table>
              <div class="form-title">
                <h4 class="acc_info">Account Information</h4>
              </div>
              
              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Your Email ID</label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                  {{ Form::email('email',null,array('class'=>"form-control",'id'=>"candidate_email",'placeholder'=>"This Will Become Your Username For Login")) }}
                 
                </div>
              </div>
              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Password
                <span class="help-tip"><p>Password is case sensitive. Min 6 characters </p></span>
              </label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                  <div class="input-group pwd-grp">
                    {{ Form::password('password',array('class'=>"form-control pwd",'id'=>"candidate_password",'placeholder'=>"Enter Password")) }}
                    <span class="pass-show input-group-btn"><button class="btn btn-default reveal" type="button"><i class="glyphicon glyphicon-eye-open"></i></button></span>
                  </div>
                </div>
              </div>
            </div>
          


            <div class="form-section">
              <div class="form-title">
                <h4 class="personal_detail">Your Personal Details</h4>
              </div>
              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Name</label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                  <div class="form-row">
                    <div class="col-xs-12 col-sm-4 form-child-block">
                      {{ Form::select('name_title',config('constants.name_titles'),'Mr.',array('class'=>"form-control",'id'=>"name_title")) }} 
                    </div>
                    <div class="col-xs-12 col-sm-8 form-child-block">
                      {{ Form::text('name',null,array('class'=>"form-control",'hint-class'=>"fname_s",'id'=>"name",'data-toggle'=>"tooltip",'placeholder'=>"Enter Full Name")) }}
                    </div>
                  </div>                  
                </div>
              </div>
              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Date of Birth</label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                  <div class="form-row">
                    <div class="col-xs-12 col-sm-12 form-child-block">
                      {{ Form::text('dob',null,array('class'=>"form-control dob_pick",'id'=>"datepicker","readonly"=>true,'autocomplete'=>'off')) }}
                      <span id="errorDOB"></span>
                     </div>
                   </div>
                </div>
              </div>  

               <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Mobile Number</label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                  <div class="col-xs-4 col-sm-4 leftnopadding">
                     {{ Form::select('country_code',$country_codes,null,array('class'=>"form-control",'id'=>"country_code",'placeholder'=>"Select Code")) }} 
                  </div>
                  <div class="col-xs-8 col-sm-8 nopadding">
                    {{ Form::text('phone',null,array('class'=>"form-control",'id'=>"phone",'placeholder'=>"Enter Mobile Number")) }}
                    <div class="tooltip-area mobile_s">
                      Privacy assured. Employers prefer calling you on your mobile. So make sure you enter the right number.
                      <span class="ttip-arw"></span>
                    </div>
                  </div>
                </div>
              </div>
             
              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Current location</label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                  <div class="col-xs-12 col-sm-6 leftnopadding">
                     {{ Form::select('country',['Gulf Country'=>getGulfCountries(),'Other Country'=>getOtherCountries()],null,array('class'=>"form-control",'id'=>"country",'placeholder'=>"Select Country")) }} 
                  </div>
                  <div class="col-xs-12 col-sm-6 nopadding">
                    {{ Form::text('city',null,array('class'=>"form-control",'id'=>"city",'placeholder'=>"Enter City")) }}
                    <span id="errorCity"></span>
                  </div>
                </div>
              </div>              
             
             <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Nationality</label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                  {{ Form::select('nationality',config('constants.nationality'),null,array('class'=>"form-control",'id'=>"nationality",'placeholder'=>"Select Nationality")) }} 
                </div>
              </div>
             
            </div>
                          
            <div class="form-section">
              <div class="form-title">
                <h4 class="edu_detail">Your Education Details</h4>
              </div>
        
              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Basic Education</label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                 {{ Form::select('basic_course_id',$basic_courses,null,array('class'=>"form-control","id"=>"basic_course",'placeholder'=>"Select")) }} 
                </div>
              </div>

              <div class="basic-elem hide-elem">
                <div class="form-group row">
                  <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">
                    Specialization
                  </label>
                  <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                    {{ Form::select('basic_specialization_id',[],null,array('class'=>"form-control","id"=>"basic_specialization",'placeholder'=>"Select")) }} 
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">
                    Completion Year
                  </label>
                  <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                    <div class="form-row">
                      <div class="col-xs-6 col-sm-4 form-child-block">
                         {{ Form::select('basic_comp_year',getDigitRange(1960,2018),null,array('class'=>"form-control",'id'=>"year_of_graduation",'placeholder'=>"Select")) }} 
                      </div>
                    </div>
                  </div>              
                </div>
              </div>

              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label">Master Education</label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                 {{ Form::select('master_course_id',$master_courses,null,array('class'=>"form-control","id"=>"master_course",'placeholder'=>"Select")) }} 
                </div>
              </div>
            <div class="master-elem hide-elem">
              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label">
                  Specialization
                </label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                  {{ Form::select('master_specialization_id',[],null,array('class'=>"form-control","id"=>"master_specialization",'placeholder'=>"Select")) }} 
                </div>
              </div>

              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label">
                  Completion Year
                </label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                  <div class="form-row">
                    <div class="col-xs-6 col-sm-4 form-child-block">
                       {{ Form::select('master_comp_year',getDigitRange(1960,2018),null,array('class'=>"form-control",'id'=>"year_of_postgraduation",'placeholder'=>"Select")) }} 
                    </div>
                  </div>
                </div>              
              </div>
            </div>
              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">
                  Institute / University
                </label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                   {{ Form::select('institute',$institutes,null,array('class'=>"form-control institutes",'placeholder'=>"Select Institute")) }} 
                </div>
              </div>

            </div>
            <div class="form-section">
              <div class="form-title">
                <h4 class="work_exp">Your Current Employment Details</h4>
            </div>
            <div class="form-section">
             

              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Experience Level</label>
                <div class="col-xs-12 col-sm-9 left-pad">                 
                    <div class="radioStyle2">
                      <label>{{ Form::radio('experience_level',1,false,array('class'=>"form-control exp_level")) }}I have work Experience</label>
                    </div>
                    <div class="radioStyle2">
                      <label>{{ Form::radio('experience_level',0,false,array('class'=>"form-control exp_level")) }}I am a Fresher</label>
                    </div>                    
                </div>
              </div>
              <div class="exp_level_fields hide-elem">
                  <div class="form-group row">
                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">
                      Your Total Work Experience
                    </label>
                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                      <div class="form-row">
                        <div class="col-xs-12 col-sm-6 form-child-block">
                          {{ Form::select('year_experiance',getDigitRange(0,30),null,array('class'=>"form-control",'id'=>"year_experiance",'placeholder'=>"Select")) }}
                         {{--  <span class="fieldlabel">Years</span> --}}
                        </div>
                        <div class="col-xs-12 col-sm-6 form-child-block">
                          {{ Form::select('month_experiance',getDigitRange(0,12),null,array('class'=>"form-control",'id'=>"month_experiance",'placeholder'=>"Select")) }}
                         {{--  <span class="fieldlabel">Months</span> --}}
                        </div>
                      </div>
                    </div>
                  </div>

                  

                  <div class="form-group row">
                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">
                      Current Employer Name
                    </label>
                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                      {{ Form::text('current_company',null,array('class'=>"form-control",'hint-class'=>"",'id'=>"current_company",'placeholder'=>"Enter Company Name")) }}
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">
                      Your Current Position
                    </label>
                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                      {{ Form::text('current_position',null,array('class'=>"form-control",'id'=>"current_position",'placeholder'=>"Select")) }}
                       <span id="errorCurrentPosition"></span>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">
                      Current Employer's Industry
                    </label>
                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                      {{ Form::select('current_industry',$industry_list,null,array('class'=>"form-control",'id'=>"current_industry",'placeholder'=>"Select Industry")) }}
                    </div>
                  </div>
                  @php
                  $currencyList=config('constants.currency');
                  sort($currencyList);
                  @endphp
                  <div class="form-group row">
                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">
                      Your Monthly Salary
                    </label>
                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                      <div class="form-row">
                        <div class="col-xs-12 col-sm-6 form-child-block">
                          {{ Form::text('salary',null,array('class'=>"form-control",'id'=>"salary",'placeholder'=>"Enter Salary")) }}
                          </div>
                        <div class="col-xs-12 col-sm-6 form-child-block">
                          {{ Form::select('currency',$currencyList,null,array('class'=>"form-control",'id'=>"currency",'placeholder'=>"Select")) }}
                          </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">
                      Functional Area / Department
                    </label>
                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                      {{ Form::select('functional_area',$functional_area,null,array('class'=>"form-control",'id'=>"functional_area",'placeholder'=>"Select Functional Area")) }}
                    </div>
                  </div>
              </div>
              <div class="form-group row fresher_level_fields hide-elem">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">
                  Key Skills
                </label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                  {{ Form::text('key_skills',null,array('class'=>"form-control",'hint-class'=>"keyskills_s",'id'=>"key_skills",'data-toggle'=>"tooltip",'placeholder'=>"Enter Key Skills")) }}
                  <span id="errorKeySkills"></span>
                </div>
              </div>
              <hr/>
              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label">
                  Resume Headline
                  <span class="help-tip"><p>Ex:  Award-Winning Editor Skilled in Web Design .</p></span>
                </label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                   {{ Form::text('resume_headline',null,array('class'=>"form-control",'id'=>"resume_headline",'data-toggle'=>"tooltip",'placeholder'=>"Enter Resume Headline")) }}
                </div>
              </div>
              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label">
                  Upload Resume
                </label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                  <div class="upload-btn-wrapper">
                  <button class="button" style="padding: 8px;">Upload Your Resume</button>
                  <span id="curr_selected_file"></span>   
                  {{ Form::file('resume_file',null,array('class'=>"",'id'=>"resume_file")) }} 
                </div>                 
                  <div class="form-field-note">
                    Upload only Word (.doc/.docx), Pdf (.pdf) or Text (.txt) formats.
                  </div>
                </div>
              </div>

              <div class="form-group row">  
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label"> Upload Your Photo</label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                  <img src="{{url('public/images/deuser.png')}}" class="user-default">
                  <p class="instruction">A Profile with Photo has a 40% higher chance of getting viewed by Employers</p>
                  <div class="upload-btn-wrapper">
                  <button class="button" style="padding: 8px;">Upload Your Photo</button>
                  {{ Form::file('user_img',array('class'=>"",'id'=>"user_img")) }}
                </div>
                </div>
              </div>

              <div class="form-group row">
                <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label">
                  Interested in
                </label>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                  @foreach(config('constants.interested_in') as $k=>$val)
                    <label class="checkbox-btn">
                      {{ Form::checkbox('interested_in[]',$k,false,array('class'=>"")) }}
                      {{ $val }}
                    </label>
                  @endforeach
                 
                </div>
              </div>
            </div>
              
            <div class="form-group row mt-20">
              <div class="col-sm-8 col-sm-offset-4">
                <button type="submit" class="newfclassub">Submit Resume</button>
              </div>
            </div>
          </form>
          </div>
        </div>
        
        
        
       
      </div>
      <div class="col-xs-12 col-sm-2">
          <img src="{{url('public/images/left-side-img.jpg')}}" class="img-responsive center-block">
        </div>
    </div>
  </section>
</section>
<script>
  var defaultImg = '{{url('public/images/deuser.png')}}';
$("input[name=resume_file]").change(function () {
    printSelectedFileName($(this).val(),'#curr_selected_file');
});
$("#user_img").change(function(){
    previewImage(this,'.user-default','{{url('public/images/deuser.png')}}');
});
  $(function(){
    $('#current_position').selectize({
        maxItems: 1,
        valueField: 'name',
        labelField: 'name',
        searchField: 'name',
        create: true,
        options: [],
        render: {
            option: function(item, escape) {           
                return '<div>' +escape(item.name)+'</div>';
            }
        },
        load: function(query, callback) {
            if (!query.length) return callback();
            $.ajax({
                url: base_url+'api/designation-suggestion',
                type: 'GET',
                dataType: 'json',
                data: {
                    q: query,
                },           
                success: function(res) {
                    callback(res);
                },
                error: function(error) {
                    callback();
                }
            });
        }
    });

   
  });
  $('#key_skills').selectize({
      plugins: ['remove_button'],
      maxItems: 15,
      valueField: 'name',
      labelField: 'name',
      searchField: 'name',
      create: true,
      options: {!! json_encode($skill_keywords) !!},
    
  });

  $('.institutes').selectize({
      maxItems: 1,
      valueField: 'name',
      labelField: 'name',
      searchField: 'name',
      create: true,
  });


  $('.exp_level').click(function(e){
      if($(this).val()==0){
        $('.exp_level_fields').hide();
      } else {
        $('.exp_level_fields').show();
      }
      $('.fresher_level_fields').show();
  });


$('#basic_course').change(function(e){
    if($(this).val()>0){
      $('.basic-elem').show();
    } else {
      $('.basic-elem').hide();
    }
    var post_data={course_id:$(this).val()};
    globalFunc.ajaxCall('api/specialization-by-course', post_data, 'POST', globalFunc.before, globalFunc.listOfSpecializationBasic, globalFunc.error, globalFunc.complete);
});
$('#master_course').change(function(e){
    if($(this).val()>0){
      $('.master-elem').show();
    } else {
      $('.master-elem').hide();
    }
    var post_data={course_id:$(this).val()};
    globalFunc.ajaxCall('api/specialization-by-course', post_data, 'POST', globalFunc.before, globalFunc.listOfSpecializationMaster, globalFunc.error, globalFunc.complete);
});

$('#country_code').change(function(){
      globalVar.countryId = $(this).val().split('~')[1];
      $('#country').val(globalVar.countryId);
      $('#country').change();
});
 $('#country').change(function(){
      globalVar.countryId = $(this).val();
      var selectize = $("#city")[0].selectize; 
      selectize.clear();
      selectize.clearOptions();
      selectize.renderCache['option'] = {}; 
  });

  $('#city').selectize({
    plugins: ['remove_button'],
    maxItems: 1,
    valueField: 'id',
    labelField: 'name',
    searchField: 'name',
    create: false,
    closeAfterSelect: true,
    options: [],
    render: {
            option: function(item, escape) {           
                return '<div>' +escape(item.name)+'</div>';
            }
        },
        load: function(query, callback) {
            if (!query.length) return callback();
            if(query.length>=3){
             $.ajax({
                  url: base_url+'/api/city-suggestion',
                  type: 'GET',
                  dataType: 'json',
                  data: {
                      country_id: globalVar.countryId,
                      q: query
                  },           
                  success: function(res) {
                      callback(res);
                  },
                  error: function(error) {
                      callback();
                  }
              });
            } else {
              var selectize = $("#city")[0].selectize; 
             selectize.renderCache['option'] = {}; 
             return callback();
            }
        }
  });
  
 $( "#datepicker" ).datepicker({
      changeMonth: true,
      changeYear: true,
      yearRange: "-58:+0",
      showOn: "both",
      buttonImage: "{{url('public/images/icons/calendar.png')}}",
      buttonImageOnly: true,
      buttonText: "Select date",
      dateFormat: "dd-mm-yy",
      maxDate:'0'
      
    });
</script>
@endsection