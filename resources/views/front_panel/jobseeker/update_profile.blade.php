
@extends('/layouts/front_panel_master')
@section('content')

<section id="inner-banner" class="dashboard-banner" style="background: url({{url('public/images/inner-banner.jpg')}}) no-repeat center top;">
<div class="overlay"></div>
 @include('front_panel/includes/search_job_section')
</section>

<style>
.ui-datepicker-trigger{
  width: 16px !important;
}
</style>

<section class="main-inner-page">
  <section id="search-section">
    <div class="container">
      <div class="row">
          @include('front_panel.includes.candidate_dashboard_sidebar',['user'=>$user])     
            <div class="col-sm-9">
                <div class="tabs widget profile-area">
				          <div class="tab-content">
                          <div class="row">          
                            <div class="col-xs-12 col-sm-12 mid-sec-top">
                              <div class="block pt-0">          
                                {{ Form::model($user,array('url'=>route('candidate-profile-update'),'class'=>'dasboard-form','id'=>'canditate-update-form','files'=>true))}}            
                                    <div class="form-section">
                                      <div class="form-title">
                                        <h4 class="acc_info">Account Information</h4>
                                      </div>
                                      <div class="form-group row">
                                        <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Your Email ID</label>
                                        <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                          {{ Form::email('email',null,array('class'=>"form-control",'id'=>"candidate_email",'placeholder'=>"Enter E-mail ID",'readonly'=>true)) }}
                                         
                                        </div>
                                      </div>
                                      <div class="form-group row">
                                        <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Password</label>
                                        <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                          {{ Form::password('password',array('class'=>"form-control",'id'=>"candidate_password",'placeholder'=>"Enter Password")) }}
                                        </div>
                                      </div>
                                    </div>
            
                                    <div class="form-section">
                                      <div class="form-title">
                                        <h4 class="personal_detail">Your Personal Details</h4>
                                      </div>
                                      <div class="form-group row">
                                        <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Name</label>
                                        <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                          <div class="form-row">
                                            <div class="col-xs-12 col-sm-4 form-child-block">
                                              {{ Form::select('title',config('constants.name_titles'),null,array('class'=>"form-control",'id'=>"name_title")) }} 
                                            </div>
                                            <div class="col-xs-12 col-sm-8 form-child-block">
                                              {{ Form::text('name',null,array('class'=>"form-control",'hint-class'=>"fname_s",'id'=>"name",'data-toggle'=>"tooltip",'placeholder'=>"Enter Full Name")) }}
                                            </div>
                                          </div>                  
                                        </div>
                                      </div>
                                      <div class="form-group row">
                                        <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Date of Birth</label>
                                        <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                         
                                          <div class="form-row">
                                            <div class="col-xs-12 col-sm-10 form-child-block">
                                              {{ Form::text('dob',dateConvert($user->dob,'d-m-Y'),array('class'=>"form-control dob_pick",'id'=>"datepicker",'readonly'=>true)) }}                    
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                                   
                                      <div class="form-group row">
                                        <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Mobile Number</label>
                                        <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                          <div class="col-xs-4 col-sm-4 leftnopadding">
                                             {{ Form::select('country_code',$country_codes,null,array('class'=>"form-control",'id'=>"country_code",'placeholder'=>"Select Code")) }} 
                                          </div>
                                          <div class="col-xs-8 col-sm-8 nopadding">
                                            {{ Form::text('phone',$user->mobile,array('class'=>"form-control",'id'=>"phone",'placeholder'=>"Enter Mobile Number")) }}
                                            <div class="tooltip-area mobile_s">
                                              Privacy assured. Employers prefer calling you on your mobile. So make sure you enter the right number.
                                              <span class="ttip-arw"></span>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="form-group row">
                                        <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Current Location</label>
                                        <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                          <div class="col-xs-12 col-sm-6 leftnopadding">
                                             {{ Form::select('country_id',['Gulf Country'=>getGulfCountries(),'Other Country'=>getOtherCountries()],null,array('class'=>"form-control",'id'=>"country",'placeholder'=>"Select Country")) }} 
                                          </div>
                                          <div class="col-xs-12 col-sm-6 nopadding">
                                            {{ Form::select('city',[],null,array('class'=>"form-control",'id'=>"city",'placeholder'=>"Select City")) }}
                                          </div>
                                        </div>
                                      </div>  
                                      <div class="form-group row">
                                        <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Nationality</label>
                                        <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                          {{ Form::select('nationality',config('constants.nationality'),null,array('class'=>"form-control",'id'=>"nationality",'placeholder'=>"Select")) }} 
                                        </div>
                                      </div>
                                    </div>
                            
                                    <div class="form-section">
                                        <div class="form-title">
                                          <h4 class="edu_detail">Your Education Details</h4>
                                        </div>
        
                                        <div class="form-group row">
                                          <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Basic Education</label>
                                          <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                           {{ Form::select('basic_course_id',$basic_courses,$user->education->basic_course_id,array('class'=>"form-control","id"=>"basic_course",'placeholder'=>"Select")) }} 
                                          </div>
                                        </div>

                                        <div class="basic-elem hide-elem">
                                          <div class="form-group row">
                                            <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">
                                              Specialization
                                            </label>
                                            <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                              {{ Form::select('basic_specialization_id',[],null,array('class'=>"form-control","id"=>"basic_specialization",'placeholder'=>"Select")) }} 
                                            </div>
                                          </div>
                                          <div class="form-group row">
                                            <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">
                                              Completion Year
                                            </label>
                                            <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                              <div class="form-row">
                                                <div class="col-xs-6 col-sm-4 form-child-block">
                                                   {{ Form::select('basic_comp_year',getDigitRange(1960,date('Y')),$user->education->basic_comp_year,array('class'=>"form-control",'id'=>"year_of_graduation",'placeholder'=>"Select")) }} 
                                                </div>
                                              </div>
                                            </div>              
                                          </div>
                                        </div>

                                        <div class="form-group row">
                                          <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label">Master Education</label>
                                          <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                           {{ Form::select('master_course_id',$master_courses,$user->education->master_course_id,array('class'=>"form-control","id"=>"master_course",'placeholder'=>"Select")) }} 
                                          </div>
                                        </div>
                                      <div class="master-elem hide-elem">
                                        <div class="form-group row">
                                          <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label">
                                            Specialization
                                          </label>
                                          <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                            {{ Form::select('master_specialization_id',[],null,array('class'=>"form-control","id"=>"master_specialization",'placeholder'=>"Select")) }} 
                                          </div>
                                        </div>
                                        <div class="form-group row">
                                          <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label">
                                            Completion Year
                                          </label>
                                          <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                            <div class="form-row">
                                              <div class="col-xs-6 col-sm-4 form-child-block">
                                                 {{ Form::select('master_comp_year',getDigitRange(1960,date('Y')),$user->education->master_comp_year,array('class'=>"form-control",'id'=>"year_of_graduation",'placeholder'=>"Select")) }} 
                                              </div>
                                            </div>
                                          </div>              
                                        </div>
                                      </div>
                                      <div class="form-group row">
                                      <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">
                                        Institute / University
                                      </label>
                                      <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                         {{ Form::text('institute',$user->education->institute,array('class'=>"form-control institutes",'placeholder'=>"Select Institutes")) }} 
                                       
                                      </div>
                                      </div>
                                    </div>
                                    
                                    
                                      <div class="form-section">
                                        <div class="form-title">
                                            <h4 class="work_exp">Your Current Employment Details</h4>
                                        </div>
                                        <div class="form-group row">
                                          <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">Experience Level</label>
                                          <div class="col-xs-12 col-sm-9">                 
                                              <div class="radioStyle2">
                                                <label>{{ Form::radio('experience_level',1,($user->additional_info->exp_level==1) ? true : false,array('class'=>"form-control exp_level")) }}I have work Experience</label>
                                              </div>
                                              <div class="radioStyle2">
                                                <label>{{ Form::radio('experience_level',0,($user->additional_info->exp_level==0) ? true : false,array('class'=>"form-control exp_level")) }}I am a Fresher</label>
                                              </div>                    
                                          </div>
                                        </div>

                                        <div class="exp_level_fields hide-elem">
                                            <div class="form-group row">
                                              <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">
                                                Your Total Work Experience
                                              </label>
                                              <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                                <div class="form-row">
                                                  <div class="col-xs-12 col-sm-6 form-child-block">
                                                    {{ Form::select('year_experiance',getDigitRange(0,30),$user->additional_info->exp_year,array('class'=>"form-control",'id'=>"year_experiance",'placeholder'=>"Select")) }}
                                                    <span class="fieldlabel">Years</span>
                                                  </div>
                                                  <div class="col-xs-12 col-sm-6 form-child-block">
                                                    {{ Form::select('month_experiance',getDigitRange(0,12),$user->additional_info->exp_month,array('class'=>"form-control",'id'=>"month_experiance",'placeholder'=>"Select")) }}
                                                    <span class="fieldlabel">Months</span>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                            <div class="form-group row">
                                              <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">
                                                Current Employer Name
                                              </label>
                                              <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                                {{ Form::text('current_company',$user->additional_info->current_company,array('class'=>"form-control",'id'=>"current_company",'data-toggle'=>"tooltip",'placeholder'=>"Enter Company Name")) }}
                                              </div>
                                            </div>
                                            <div class="form-group row">
                                              <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">
                                                Your Current Position
                                              </label>
                                              <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                                {{ Form::text('current_position',$user->additional_info->current_position,array('class'=>"form-control",'id'=>"current_position",'placeholder'=>"Select")) }}
                                              </div>
                                            </div>
                                            <div class="form-group row">
                                              <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">
                                                Current Employer's Industry
                                              </label>
                                              <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                                {{ Form::select('current_industry',$industry,$user->additional_info->current_industry,array('class'=>"form-control",'id'=>"current_industry",'placeholder'=>"Select Industry")) }}
                                              </div>
                                            </div>
                                            <div class="form-group row">
                                              <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">
                                                Your Monthly Salary
                                              </label>
                                              <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                                <div class="form-row">
                                                  <div class="col-xs-12 col-sm-6 form-child-block">
                                                    {{ Form::text('salary',$user->additional_info->salary,array('class'=>"form-control",'id'=>"salary",'placeholder'=>"Select")) }}
                                                    </div>
                                                  <div class="col-xs-12 col-sm-6 form-child-block">
                                                    {{ Form::select('currency',config('constants.currency'),$user->additional_info->salary_in,array('class'=>"form-control",'id'=>"currency",'placeholder'=>"Select")) }}
                                                    </div>
                                                </div>
                                              </div>
                                            </div>
                                            <div class="form-group row">
                                              <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">
                                                Functional Area / Department
                                              </label>
                                              <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                                {{ Form::select('functional_area',$f_area,$user->additional_info->functional_area,array('class'=>"form-control",'id'=>"functional_area",'placeholder'=>"Select Functional Area")) }}
                                              </div>
                                            </div>
                                        </div>
                                        <div class="form-group row fresher_level_fields hide-elem">
                                          <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">
                                            Key Skills
                                          </label>
                                          <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                            {{ Form::text('key_skills',null,array('class'=>"form-control hintable",'hint-class'=>"keyskills_s",'id'=>"key_skills",'data-toggle'=>"tooltip",'placeholder'=>"Enter Key Skills")) }}
                                          <div class="form-field-note row">
                                              <div class="col-xs-12 text-right">
                                                Max 250 characters
                                              </div>
                                            <div class="tooltip-area keyskills_s">
                                              Enter your expertise or specialities here separated by commas.
                                              <span class="ttip-arw"></span>
                                            </div>
                                            </div>
                                          </div>
                                        </div>
                                        <hr/>
                                        <div class="form-group row">
                                          <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label">
                                            Resume Headline
                                          </label>
                                          <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                             {{ Form::text('resume_headline',$user->additional_info->resume_headline,array('class'=>"form-control",'id'=>"resume_headline",'placeholder'=>"Enter Resume Headline")) }}
                                            <div class="tooltip-area headline_s">
                                              Please enter a title for your resume. <strong>Eg:</strong> Java Programmer,4 yrs exp,B.Tech
                                              <span class="ttip-arw"></span>
                                            </div>
                                          </div>
                                        </div>
                                        <div class="form-group row">
                                          <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label">
                                            Upload Resume
                                          </label>
                                          <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                            <div class="upload-btn-wrapper">
                                              <button class="btn">Upload Your Resume</button>
                                               <span id="curr_selected_file"></span>   
                                              {{ Form::file('resume_file',null,array('class'=>"",'id'=>"resume_file")) }}    
                                            </div>                 
                                            <div class="form-field-note">
                                              Upload only Word (.doc/.docx), Pdf (.pdf) or Text (.txt) formats.
                                            </div>
                                          </div>
                                        </div>

                                        <div class="form-group row">
                                          <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label">
                                            Upload Your Photo
                                          </label>
                                          <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                                            
                                            <div class="upload-btn-wrapper">
                                              <button class="btn">Upload Your Photo</button>
                                              {{ Form::file('user_img',array('class'=>"",'id'=>"user_img")) }}
                                            </div>
                                            <div class="form-field-note">
                                              A Profile with Photo has a 40% higher chance of getting viewed by Employers
                                            </div>
                                          </div>
                                        </div>

                                        <div class="form-group row">
                                          <label class="col-xs-12 col-sm-12 col-md-3 col-lg-3 form-label required-field">
                                            Interested in
                                          </label>
                                          <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 left-pad">
                                            @php
                                              $interestedIn=explode(',', $user->additional_info->interested_in);
                                            @endphp
                                            @foreach(config('constants.interested_in') as $k=>$val)
                                              @php
                                                $flag=(in_array($k,$interestedIn)) ? true : false;
                                              @endphp
                                              <label class="checkbox-btn">
                                                {{ Form::checkbox('interested_in[]',$k,$flag,array('class'=>"")) }}
                                                {{ $val }}
                                              </label>
                                            @endforeach
                                           
                                          </div>
                                        </div>
                                      </div>              
                                      <div class="form-group row mt-20">
                                        <div class="col-sm-9">
                                          <button type="submit" class="newfclassub">Update</button>
                                        </div>
                                      </div>
                                  {{ Form::close() }}
                                </div>
                            </div>
                          </div>
                      
                   
                  </div>
  <!-- tab-content --> 
                </div>
<!-- tabs-widget -->              
            </div>
      </div>
    </div>
  </section>
</section>

<script>
globalVar.city_id='{{$user->city_id}}';
globalVar.basic_s_id='{{$user->education->basic_specialization_id}}';
globalVar.master_s_id='{{$user->education->master_specialization_id}}';
globalVar.countryId='{{$user->country_id}}';
 var defaultImg = '{{url('public/images/deuser.png')}}';

$("input[name=resume_file]").change(function () {
    printSelectedFileName($(this).val(),'#curr_selected_file');
});
$("#user_img").change(function(){
    previewImage(this,'.user-default');
});


  $(function(){
      $("#country").change();
      $("#basic_course").change();
      $("#master_course").change();
      expLevelElem({{$user->additional_info->exp_level}});

    $('#current_position').selectize({
        maxItems: 1,
        valueField: 'name',
        labelField: 'name',
        searchField: 'name',
        create: true,
        options: [],
        render: {
            option: function(item, escape) {           
                return '<div>' +escape(item.name)+'</div>';
            }
        },
        load: function(query, callback) {
            if (!query.length) return callback();
            $.ajax({
                url: base_url+'api/designation-suggestion',
                type: 'GET',
                dataType: 'json',
                data: {
                    q: query,
                },           
                success: function(res) {
                    callback(res);
                },
                error: function(error) {
                    callback();
                }
            });
        }
    });
   
  });
$('#country_code').change(function(){
      globalVar.countryId = $(this).val().split('~')[1];
      $('#country').val(globalVar.countryId);
      $('#country').change();
});
 $('#country').change(function(){

      globalVar.countryId = $(this).val();
      $("#country").val(globalVar.countryId);
      $('#city').find('option').not(':first').remove();
      $.ajax({
        url: base_url+'/api/city-suggestion',
        type: 'GET',
        dataType: 'json',
        data: {
            by_country: 1,
            country_id: globalVar.countryId,
        },           
        success: function(res) {
           $.each(res,function(key,val){
            if(globalVar.city_id==val.id){
              $('#city').append('<option value="'+val.id+'" selected>'+val.name+'</option>');
            } else {
              $('#city').append('<option value="'+val.id+'">'+val.name+'</option>');
            }
           });
        },
        error: function(error) {
            
        }
      });
  });



  $('#key_skills').selectize({
      plugins: ['remove_button'],
      maxItems: 15,
      valueField: 'name',
      labelField: 'name',
      searchField: 'name',
      create: true,
      options: {!! json_encode($skill_keywords) !!},
      onInitialize: function(){
          var selectize = this;
          var skill='{{$user->additional_info->key_skills}}';
          var splitVal=skill.split(',');
          var selected_items=splitVal;
          selectize.setValue(splitVal);
      }
  });
  
  $('.exp_level').click(function(e){
      expLevelElem($(this).val());
  });
  function expLevelElem(input_val){
    if(input_val==0){
      $('.exp_level_fields').hide();
    } else {
      $('.exp_level_fields').show();
    }
    $('.fresher_level_fields').show();
  }

$('#basic_course').change(function(e){
    if($(this).val()>0){
      $('.basic-elem').show();
    } else {
      $('.basic-elem').hide();
    }
    var post_data={course_id:$(this).val()};
    globalFunc.ajaxCall('api/specialization-by-course', post_data, 'POST', globalFunc.before, globalFunc.listOfSpecializationBasic, globalFunc.error, globalFunc.complete);
});
$('#master_course').change(function(e){
    if($(this).val()>0){
      $('.master-elem').show();
    } else {
      $('.master-elem').hide();
    }
    var post_data={course_id:$(this).val()};
    globalFunc.ajaxCall('api/specialization-by-course', post_data, 'POST', globalFunc.before, globalFunc.listOfSpecializationMaster, globalFunc.error, globalFunc.complete);
});

$( "#datepicker" ).datepicker({
      changeMonth: true,
      changeYear: true,
      yearRange: "-58:+0",
      showOn: "both",
      buttonImage: "{{url('public/images/icons/calendar.png')}}",
      buttonImageOnly: true,
      buttonText: "Select date",
      dateFormat: "dd-mm-yy",
      maxDate:'0'
      
    });
</script>
@endsection