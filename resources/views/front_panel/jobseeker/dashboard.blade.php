@extends('/layouts/front_panel_master')
@section('content')

<section id="inner-banner" class="dashboard-banner" style="background: url({{url('public/images/inner-banner.jpg')}}) no-repeat center top;">
<div class="overlay"></div>
 @include('front_panel/includes/search_job_section')
</section>

<section class="main-inner-page">
  <section id="search-section">
    <div class="container">

      @if($user->email_verified==0)
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
           <a href="{{ route('send-verify-mail') }}">Verify</a> your email to increase your visibility and search-ability.
        </div>
      @endif
     {{--  @if($user->mobile_verified==0)
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
           Verify your mobile number. increase your visibility and searchability. Click : <a href="">Verify Number</a>
        </div>
      @endif --}}
      
      <div class="row">      	
        @include('front_panel.includes.candidate_dashboard_sidebar',['user'=>$user])    
              
        <div class="col-sm-9">
          <div class="tabs widget profile-area">
				  <ul class="nav nav-tabs widget">
				    <li class="active"> <a data-toggle="tab" href="#profile-tab"> Profile <span class="menu-active"><i class="fa fa-caret-up"></i></span>  </a></li>
				    <li class=""> <a data-toggle="tab" href="#applied-tab"> Applied Jobs  <span class="menu-active"><i class="fa fa-caret-up"></i></span></a></li>    
				    <li> <a data-toggle="tab" href="#activity-tab"> Last Activity  <span class="menu-active"><i class="fa fa-caret-up"></i></span></a></li>
				    <li> <a data-toggle="tab" href="#rec-tab"> Recommended Jobs  <span class="menu-active"><i class="fa fa-caret-up"></i></span></a></li>
				  </ul>

<div class="tab-content">
  <div id="profile-tab" class="tab-pane active">
    <div class="pd-20">       
            <div class="vd_info tr"> <a class="btn vd_btn btn-xs vd_bg-yellow" href="{{route('candidate-profile')}}"> <i class="fa fa-pencil append-icon"></i> Edit </a> </div>
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mb-20">
            <div class="form-title">
              <h4 class="personal_detail">Your Personal Details</h4>
            </div>
            
            <div class="col-sm-12">
              <div class="row mgbt-xs-0">
                <label class="col-xs-6 control-label">Full Name:</label>
                <div class="col-xs-6 controls">{{ $user->title }} {{ ucfirst($user->name) }}</div>
                <!-- col-sm-10 --> 
              </div>
            </div>
            <div class="col-sm-12">
              <div class="row mgbt-xs-0">
                <label class="col-xs-6 control-label">User Name:</label>
                <div class="col-xs-6 controls">{{ $user->email }}</div>
                <!-- col-sm-10 --> 
              </div>
            </div>
            <div class="col-sm-12">
              <div class="row mgbt-xs-0">
                <label class="col-xs-6 control-label">City:</label>
                <div class="col-xs-6 controls">{{ ($user->city!=null) ? $user->city->name : '-' }}</div>
                <!-- col-sm-10 --> 
              </div>
            </div>
            <div class="col-sm-12">
              <div class="row mgbt-xs-0">
                <label class="col-xs-6 control-label">Country:</label>
                <div class="col-xs-6 controls">{{ ($user->country!=null) ? $user->country->name : '-' }}</div>
                <!-- col-sm-10 --> 
              </div>
            </div>
            <div class="col-sm-12">
              <div class="row mgbt-xs-0">
                <label class="col-xs-6 control-label">Date of Birth:</label>
                <div class="col-xs-6 controls">{{ dateConvert($user->dob,'d-m-Y') }}</div>
                <!-- col-sm-10 --> 
              </div>
            </div>
            <div class="col-sm-12">
              <div class="row mgbt-xs-0">
                <label class="col-xs-6 control-label">Phone:</label>
                <div class="col-xs-6 controls">{{ ($user->country_code!='') ? '+'.$user->country_code.'-' : '' }}{{ $user->mobile }}</div>
                <!-- col-sm-10 --> 
              </div>
            </div>
            {{-- <div class="col-sm-12">
              <div class="row mgbt-xs-0">
                <label class="col-xs-6 control-label">Gender:</label>
                <div class="col-xs-6 controls">{{ $user->gender }}</div>
              </div>
            </div> --}}
          </div>

          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mb-20">
            <div class="form-title">
                <h4 class="edu_detail">Your Education Details</h4>
            </div>
           
            <div class="col-sm-12">
              <div class="row mgbt-xs-0">                
                <div class="col-xs-12 controls">
                  <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th>Education</th>
                      <th>Course</th>
                      <th>Specialization</th>
                      <th>Year</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <th>Basic</th>
                      <td>{{ ($user->education !=null) ? @$user->education->basic_course->name : '' }}</td>
                      <td>{{ ($user->education !=null) ? @$user->education->basic_specialization->name : '' }}</td>
                      <td>{{ ($user->education !=null) ? @$user->education->basic_comp_year : '' }}</td>
                    </tr>
                     @if($user->education !=null && $user->education->master_course!=null)
                    <tr>
                      <th>Master</th>
                      <td>{{ ($user->education !=null) ? @$user->education->master_course->name : '' }}</td>
                      <td>{{ ($user->education !=null) ? @$user->education->master_specialization->name : '' }}</td>
                      <td>{{ ($user->education !=null) ? @$user->education->master_comp_year : '' }}</td>
                    </tr>
                    @endif
                  </tbody>
                </table>
                </div>
                <!-- col-sm-10 --> 
              </div>
            </div>
            <div class="col-sm-12">
              <div class="row mgbt-xs-0">
                <label class="col-xs-6 control-label">Institute / University:</label>
                <div class="col-xs-6 controls">{{ ($user->education !=null) ? $user->education->institute : '' }}</div>
                <!-- col-sm-10 --> 
              </div>
            </div>
          </div>
       
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mb-20">
            <div class="form-title">
              <h4 class="work_exp">Your Current Employment Details</h4>
            </div>
            @if($user->additional_info!=null && $user->additional_info->exp_level)
              <div class="col-sm-12">
                <div class="row mgbt-xs-0">
                  <label class="col-xs-6 control-label">Work Experiance:</label>
                  <div class="col-xs-6 controls">{{ $user->additional_info->exp_year }} Year {{ $user->additional_info->exp_month }} Month</div>
                  <!-- col-sm-10 --> 
                </div>
              </div>
              <div class="col-sm-12">
                <div class="row mgbt-xs-0">
                  <label class="col-xs-6 control-label">Employer Name:</label>
                  <div class="col-xs-6 controls">{{ ($user->additional_info!=null) ? $user->additional_info->current_company : 'None' }}</div>
                  <!-- col-sm-10 --> 
                </div>
              </div>
              <div class="col-sm-12">
                <div class="row mgbt-xs-0">
                  <label class="col-xs-6 control-label">Employer's Industry:</label>
                  <div class="col-xs-6 controls">{{ $user->additional_info->industry->name }}</div>
                  <!-- col-sm-10 --> 
                </div>
              </div>
              <div class="col-sm-12">
                <div class="row mgbt-xs-0">
                  <label class="col-xs-6 control-label">Position:</label>
                  <div class="col-xs-6 controls">{{ $user->additional_info->current_position }}</div>
                  <!-- col-sm-10 --> 
                </div>
              </div>
              <div class="col-sm-12">
                <div class="row mgbt-xs-0">
                  <label class="col-xs-6 control-label">Monthly Salary:</label>
                  <div class="col-xs-6 controls">{{ $user->additional_info->salary }} ({{ config('constants.currency')[$user->additional_info->salary_in] }})</div>
                  <!-- col-sm-10 --> 
                </div>
              </div>
              <div class="col-sm-12">
                <div class="row mgbt-xs-0">
                  <label class="col-xs-6 control-label">Functional Area / Department:</label>
                  <div class="col-xs-6 controls">{{ $user->additional_info->function_area->name }}</div>
                  <!-- col-sm-10 --> 
                </div>
              </div>
            @endif
              <div class="col-sm-12">
                <div class="row mgbt-xs-0">
                  <label class="col-xs-6 control-label">Key Skills:</label>
                  <div class="col-xs-6 controls">{{ ($user->additional_info!=null) ? $user->additional_info->key_skills : '' }}</div>
                  <!-- col-sm-10 --> 
                </div>
              </div>
          </div>

          {{-- <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mb-20">
            <div class="form-title">
              <h4 class="cv">This is how my CV appears in the database</h4>
            </div>
          </div> --}}
      </div>
    </div>
      <!-- pd-20 --> 
  </div>
    <!-- home-tab -->
    @php 
    $total_ojobs=$total_cjobs=0; 
    @endphp
    <div id="applied-tab" class="tab-pane">
      <div class="pd-20">       
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mb-20">
            <div class="form-title">
                <h4 class="open_job">Open Jobs (<jobcount id="ojob">0</jobcount>)</h4>
            </div>
            <div class="col-sm-12">
              <table class="table table-striped table-bordered bdata-table" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th>S.No</th>
                    <th>Job Title</th>
                    <th>Total Vacancy</th>
                    <th>Experience</th>
                    <th>Salary</th>
                    <th>Apply Date</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  @forelse($applied_jobs as $k=>$val)
                    @if($val->jobs!=null && strtotime($val->jobs->expiry_date)>=strtotime(date('Y-m-d')))
                      @php $total_ojobs=$total_ojobs+1; @endphp
                      <tr>
                        <td>{{++$k}}.</td>
                        <td>{{$val->jobs->job_title}}</td>
                        <td>{{$val->jobs->total_vacancy}}</td>
                        <td>{{$val->jobs->min_experience}}-{{$val->jobs->max_experience}} Year</td>
                        <td>{{$val->jobs->salary_min}}-{{$val->jobs->salary_max}} {{$val->jobs->currency}}</td>
                        <td>{{dateConvert($val->created_at,'d M Y')}}</td>
                        <td><a href="{{route('job-details',['jobid'=>$val->jobs->id])}}" class="btn btn-info btn-xs">Detail</a></td>
                      </tr>
                    @endif
                  @empty
                  @endforelse               
                </tbody>
              </table>
            </div>
          </div>

          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mb-20">
            <div class="form-title">
                <h4 class="close_job">Close Jobs (<jobcount id="cjob">0</jobcount>)</h4>
            </div>
            <div class="col-sm-12">
              <table class="table table-striped table-bordered bdata-table" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th>S.No</th>
                    <th>Job Title</th>
                    <th>Total Vacancy</th>
                    <th>Experience</th>
                    <th>Salary</th>
                    <th>Expire Date</th>
                  </tr>
                </thead>
                <tbody>
                  @forelse($applied_jobs as $k=>$val)
                    @if($val->jobs!=null && strtotime($val->jobs->expiry_date)<strtotime(date('Y-m-d')))
                      @php $total_cjobs=$total_cjobs+1; @endphp
                      <tr>
                        <td>{{++$k}}.</td>
                        <td>{{$val->jobs->job_title}}</td>
                        <td>{{$val->jobs->total_vacancy}}</td>
                        <td>{{$val->jobs->min_experience}}-{{$val->jobs->max_experience}} Year</td>
                        <td>{{$val->jobs->salary_min}}-{{$val->jobs->salary_max}} {{$val->jobs->currency}}</td>
                        <td>{{dateConvert($val->jobs->expire_date,'d M Y')}}</td>
                      </tr>
                    @endif
                  @empty
                  @endforelse               
                </tbody>
              </table>
            </div>
          </div>

        </div>
      </div>
    </div>
    
    <div id="activity-tab" class="tab-pane">
    	<div class="row">
          <div class="col-sm-12">
            <h3 class="mgbt-xs-15 font-semibold"><i class="fa fa-globe mgr-10 profile-icon"></i> ACTIVITY</h3>
            <div class="">
              <div class="content-list">
                <div data-rel="scroll" class="mCustomScrollbar _mCS_6" style="overflow: hidden;"><div class="mCustomScrollBox mCS-light" id="mCSB_6" style="position: relative; height: 100%; overflow: hidden; max-width: 100%; max-height: 400px;"><div class="mCSB_container" style="position: relative; top: 0px;">
                  <ul class="list-wrapper">
                    <li> <span class="menu-icon vd_yellow"><i class="fa fa-suitcase"></i></span> <span class="menu-text"> Someone has give you a surprise <span class="menu-info"><span class="menu-date"> ~ 12 Minutes Ago</span></span> </span>  </li>
                    <li> <span class="menu-icon vd_blue"><i class=" fa fa-user"></i></span> <span class="menu-text"> Change your user profile details <span class="menu-info"><span class="menu-date"> ~ 1 Hour 20 Minutes Ago</span></span> </span> </li>
                    <li> <span class="menu-icon vd_red"><i class=" fa fa-cogs"></i></span> <span class="menu-text"> Your setting is updated <span class="menu-info"><span class="menu-date"> ~ 12 Days Ago</span></span> </span></li>
                    <li>  <span class="menu-icon vd_green"><i class=" fa fa-book"></i></span> <span class="menu-text"> Added new article <span class="menu-info"><span class="menu-date"> ~ 19 Days Ago</span></span> </span>  </li>
                    <li>  <span class="menu-icon vd_red"><i class=" fa fa-cogs"></i></span> <span class="menu-text"> Your setting is updated <span class="menu-info"><span class="menu-date"> ~ 12 Days Ago</span></span> </span>  </li>
                    <li>  <span class="menu-icon vd_green"><i class=" fa fa-book"></i></span> <span class="menu-text"> Added new article <span class="menu-info"><span class="menu-date"> ~ 19 Days Ago</span></span> </span> </li>
                  </ul>
                </div><div class="mCSB_scrollTools" style="position: absolute; display: block; opacity: 0;"><div class="mCSB_draggerContainer"><div class="mCSB_dragger" style="position: absolute; top: 0px; height: 352px;" oncontextmenu="return false;"><div class="mCSB_dragger_bar" style="position: relative; line-height: 352px;"></div></div><div class="mCSB_draggerRail"></div></div></div></div></div>
                <div class="closing text-center" style=""> <a href="#">See All Activities <i class="fa fa-angle-double-right"></i></a> </div>
              </div>
            </div>
          </div>
                   
        </div>	 
    </div> <!-- photos tab -->
    <div id="rec-tab" class="tab-pane">
	    	<h3 class="mgbt-xs-15 font-semibold"><i class="fa fa-suitcase mgr-10 profile-icon"></i> Recommended Jobs</h3>
	    
    	<ul>
        @forelse($recommended_jobs as $k=>$val)
    		<li>
    	   	 <p>{{++$k}}. <a href="{{route('job-details',['jobid'=>$val->id])}}" target="_blank">{{$val->job_title}} ( {{$val->min_experience}} - {{$val->max_experience}} yrs. ) </a></p>
			    <p><span class="text-secondary">{{$val->comp_name}} </span></p>
			    <p><span class="text-success">{{$val->country_name}}  - United Arab Emirates</span></p>
			 </li>
       @empty
       <li><p>No jobs available for now, Click here to <a href="{{route('search-jobs')}}" target="_blank">explore more</a></p></li>
       @endforelse
			 
		</ul>	 
    </div>  <!-- photos tab -->  
    
    
  </div>
  <!-- tab-content --> 
</div>
<!-- tabs-widget -->              
</div>
      </div>
    </div>
  </section>
</section>
<script>
  $(document).ready(function() {
    $('#ojob').text({{$total_ojobs}});
    $('#cjob').text({{$total_cjobs}});
    $('.bdata-table').DataTable({
        "paging": true,
        "bLengthChange": false,
        "ordering": false, 
        "searching":false
    });
});
</script>
@endsection