@extends('/layouts/front_panel_master')
@section('content')

@include('front_panel/includes/page_banner')
 
        <div class="jumbotron text-center">
            <i class="fa fa-thumbs-up" aria-hidden="true"></i>
          <h1 class="display-3">Thank You!</h1>
          <p class="lead"><strong>Please check your email</strong> for further instructions on how to complete your account setup.</p>
          <hr>
          <p>
            Having trouble? <a href="{{route('contact-us')}}">Contact us</a>
          </p>
        </div>
@php
$role=0;
if(Auth::check()){
	$role=Auth::user()->role;
}
$routeC = route('candidate-dashboard');
$routeE = route('employer-dashboard');
@endphp
 <script>
 		var userRole = {{$role}};
        var timer = setTimeout(function() {
        	if(userRole==2){
        		window.location='{{$routeC}}';
        	} else if(userRole==3){
        		window.location='{{$routeE}}';
        	}
        }, 10000);
    </script>
@endsection