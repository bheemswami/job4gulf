<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Cache,DB;
use Carbon\Carbon;
use Yajra\Datatables\Datatables;
use App\User,App\Employer;
use App\Service,App\ServiceFeature;
use App\JobPlan,App\JobPlanFeature;
use App\AdvertisePlanFeature;
use App\Enquiry;
use App\Setting;
use App\PageBanner;
use App\PurchasePlan,App\TransactionHistory;
use App\PostJob,App\PostJobsCity,App\PostJobsKeyword,App\PostJobsQualification,App\PostJobsSpecialization,App\PostJobsNationality,App\PostJobsCandidateCountry,App\PostJobsCandidateCity;

class AdminController extends Controller
{
    private $core;
    public $data=[];
    public function __construct()
    {
        $this->middleware('usersession');
        $this->core=app(\App\Http\Controllers\CoreController::class);
    }
    public function index(){
        return view('admin::login');
    }
    public function dashboard(){
        return view('admin::dashboard');
    }

    public function registeredEmployer(){
        $this->data['records']=Employer::where('comp_type',2)->orderBy('id','DESC')->get();
        return view('admin::employer.all_employer',$this->data);
    }

    public function registeredConsultant(){
        $this->data['records']=Employer::with(['country','city','industry','designations'])->where('comp_type',1)->orderBy('id','DESC')->get();
        return view('admin::employer.all_consultant',$this->data);
    }

    public function viewEmployer(Request $request){
        $this->data['employer_data']=Employer::with(['country','city','industry','designations'])->where('id',$request->id)->first();
        return view('admin::employer.view_employer',$this->data);
    }

    public function updateEmployerStatus(Request $request){
        if(Employer::whereIn('id',$request->ids)->update(['status'=>$request->status])){
            // if($request->status==1){
            //     $userIds = Employer::whereIn('id',$request->ids)->pluck('user_id');
            //     User::whereIn('id',$$userIds)->update(['is_email'=>1]);
            // }
            return redirect()->back()->with(['success' => config('constants.FLASH_REC_UPDATE_1')]);
        }
        return redirect()->back()->with(['success' => config('constants.FLASH_REC_UPDATE_0')]);
    }

    public function applyJobPlanEmployer(Request $request){
        $employer_data=Employer::where('id',$request->emp_id)->first();
        if($employer_data->user==null){
            return redirect()->back()->with(['error' => 'Invalid User Data']);
        }
        $this->data['user_id']=$employer_data->user->id;
        $this->data['purchase_plans']=PurchasePlan::with('job_plan')->where('user_id',$employer_data->user->id)->orderBy('plan_start_date','DESC')->get();
        $this->data['job_plans']=JobPlan::pluck('name','id');

        return view('admin::employer.apply_job_plan',$this->data);
    }
    public function saveJobPlanEmployer(Request $request){
        
       $date = Carbon::now();
       if($request->plan_id){
            $employer_data=Employer::where('id',$request->user_id)->first();
            if($employer_data->user==null){
                return redirect()->back()->with(['error' => 'User details not found.']);
            }

            DB::beginTransaction();
                $params=['plan_id'=>$request->plan_id,'user_id'=>$employer_data->user->id,'type'=>'new_job_plan','remark'=>'Job Plan Activate By Admin'];
                $trasId = $this->core->addNewPlan($params);
              
            if($trasId>0){
                DB::commit();
            } else {
                DB::rollBack();
            }
            return redirect()->back()->with(['success'=>'Plan Apply Successfully']);
        }
        return redirect()->back()->with(['error'=>'Missing Some Parameters.']);
    }

    public function manageTopEmployer(Request $request){
        $employer_data=Employer::where('id',$request->emp_id)->first();
        if($employer_data->user==null){
            return redirect()->back()->with(['error' => 'Invalid User Data']);
        }
        $this->data['user_id']=$employer_data->user->id;
        $this->data['purchase_plans']=PurchasePlan::with('job_plan')->where('user_id',$employer_data->user->id)->orderBy('id','DESC')->get();
        $this->data['job_plans']=JobPlan::whereNotIn('id',[1])->pluck('name','id');
        return view('admin::employer.make_top_employer',$this->data);
    }
    public function makeTopEmployer(Request $request){
         $date = Carbon::now();
        $employer_data=Employer::where('id',$request->emp_id)->first();
        if($employer_data->user==null){
            return redirect()->back()->with(['error' => 'Invalid User Data']);
        }
        $employer_data->no_of_walkins=$request->no_of_walkins;
        $employer_data->walkins_end_date=$date->addDays($request->acc_validity)->format('Y-m-d');
        $employer_data->account_validity=$date->addDays($request->acc_validity)->format('Y-m-d');
        $employer_data->save();

        $params=['plan_id'=>$request->plan_id,'user_id'=>$employer_data->user->id,'type'=>'','remark'=>'Top Employer Job Plan Activate'];
        $trasId = $this->core->addNewPlan($params);
        return redirect()->back()->with(['success' => config('constants.FLASH_REC_UPDATE_1')]);
    }

    public function updateEmployerWalkins(Request $request){
        if(Employer::where('id',$request->emp_id)->update(['no_of_walkins'=>$request->no_of_walkins,'walkins_end_date'=>dateConvert($request->walkins_end_date)])){
            return redirect()->back()->with(['success' => config('constants.FLASH_REC_UPDATE_1')]);
        }
       return redirect()->back()->with(['error' => config('constants.FLASH_REC_UPDATE_0')]);
    }

/* **********************  Start Candidate function ********************* */
    public function registeredCandidates(){
        $this->data['candidates']=User::with(['additional_info','education','country','city'])->whereRole(2)->orderBy('id','DESC')->get();
        return view('admin::candidate.all_candidate',$this->data);
    }
    public function viewCandidate(Request $request){
        $this->data['candidate_data']=User::with(['additional_info','education','country','city'])->whereRole(2)->whereId($request->id)->first();
        return view('admin::candidate.view_candidate',$this->data);
    }
    public function updateCandidateStatus(Request $request){
        if(User::whereIn('id',$request->ids)->update(['is_approved'=>$request->status])){
            return redirect()->back()->with(['success' => config('constants.FLASH_REC_UPDATE_1')]);
        }
        return redirect()->back()->with(['success' => config('constants.FLASH_REC_UPDATE_0')]);
    }
/* **********************  Start Candidate function ********************* */

/* **********************  Start Services function ********************* */
    public function serviceIndex()
    {
        $this->data['service_list']=Service::with('features')->get();
        return view('admin::service.all_service',$this->data);
    }   
    public function updateService(Request $request){
        $this->data['service_data']=Service::with('features')->whereId($request->id)->first();
        return view('admin::service.edit_service',$this->data);
    }
    public function saveService(Request $request) {        
        if($request->hasFile('service_icon')){
            $filename=$this->core->fileUpload($request->file('service_icon'),'uploads/service_img');
            $request->merge(['icon'=>$filename]);
        }
        if($request->hasFile('service_image')){
            $filename=$this->core->fileUpload($request->file('service_image'),'uploads/service_img');
            $request->merge(['image'=>$filename]);
        }
        $res = Service::whereId($request->id)->update($request->except(['_token','new_title','key_features','service_icon','service_image']));
        if($res){
            ServiceFeature::where('service_id',$request->id)->delete();
            if(count($request->key_features)>0){
                foreach($request->key_features as $val){
                    $data[]=['service_id'=>$request->id,'title'=>$val];
                }
                ServiceFeature::insert($data);
            }
            return redirect()->back()->with(['success' => config('constants.FLASH_REC_UPDATE_1')]);
        }
        return redirect()->back()->with(['error' => config('constants.FLASH_REC_UPDATE_0')]);
    }
/* **********************  End Services function ********************* */

/* **********************  Start JobPlans function ********************* */
    public function jobPlansIndex()
    {
        $this->data['plan_list']=JobPlan::with('features')->get();
        return view('admin::job_plans.all_job_plans',$this->data);
    }   
    public function addJobPlan(Request $request){
        return view('admin::job_plans.add_job_plan',$this->data);
    }
    public function createJobPlan(Request $request) { 

        $planId = JobPlan::insertGetId($request->except(['_token','key_features','units']));
        if($planId){
            if(count($request->key_features)>0){
                foreach($request->key_features as $val){
                    $data[]=['job_plan_id'=>$planId,'feature_id'=>$val,'units'=>((isset($request->units[$val])) ? $request->units[$val] : null) ];
                }
                JobPlanFeature::insert($data);
            }
            return redirect()->back()->with(['success' => config('constants.FLASH_REC_ADD_1')]);
        }
        return redirect()->back()->with(['error' => config('constants.FLASH_REC_ADD_0')]);
    }
    public function updateJobPlan(Request $request){
        $this->data['plan_data']=JobPlan::with('features')->whereId($request->id)->first();
        return view('admin::job_plans.edit_job_plan',$this->data);
    }
    
    public function saveJobPlan(Request $request) { 
        $res = JobPlan::whereId($request->id)->update($request->except(['_token','key_features','units']));
        if($res){
            JobPlanFeature::where('job_plan_id',$request->id)->delete();
            if(count($request->key_features)>0){
                foreach($request->key_features as $val){
                    $data[]=['job_plan_id'=>$request->id,'feature_id'=>$val,'units'=>((isset($request->units[$val])) ? $request->units[$val] : null) ];
                }
                JobPlanFeature::insert($data);
            }
            return redirect()->back()->with(['success' => config('constants.FLASH_REC_UPDATE_1')]);
        }
        return redirect()->back()->with(['error' => config('constants.FLASH_REC_UPDATE_0')]);
    }

    public function deleteJobPlan($id)
    {
        if(JobPlan::where('id',$id)->where('is_deletable',1)->delete())
            return redirect()->back()->with(['success' => config('constants.FLASH_REC_DELETE_1')]);
        else
            return redirect()->back()->with(['error' => config('constants.FLASH_REC_DELETE_0')]);
    }
/* **********************  End JobPlans function ********************* */

/* **********************  Start PostedJobs function ********************* */
    public function postedJobs(Request $request){
        
        if($request->ajax==null){
            return view('admin::jobs.all_posted_jobs',$this->data);
        }
        DB::statement(DB::raw('set @rownum=0'));
        $jobData=PostJob::with('employer_info','industry','jobs_in_city','job_applicants')->select(DB::raw('@rownum := @rownum + 1 AS sno'),'post_jobs.*');
        //$jobData->where('job_type',1);
        if($request->is_approve!='all'){
            $jobData->where('is_approved',$request->is_approve);
        }
        $jobs = $jobData->orderBy('id','DESC')->get();
        return Datatables::of($jobs)
                ->addColumn('sno',function(PostJob $jobs){
                    return $jobs->sno.'.';
                })
                ->addColumn('checkbox',function(PostJob $jobs){
                    return '<input type="checkbox" name="ids[]" value="'.$jobs->id.'" class="data_checkbox">';
                })
                ->addColumn('job_type',function(PostJob $jobs){
                    return ($jobs->job_type==1) ? 'Basic(Free)' : 'Premium';
                })                
                ->addColumn('post_date',function(PostJob $jobs){
                    return dateConvert($jobs->created_at,'d-m-Y');
                })
                ->addColumn('expire_date',function(PostJob $jobs){
                    return dateConvert($jobs->expiry_date,'d-m-Y');
                })
                ->addColumn('is_approved', function (PostJob $jobs) {
                    $status = ($jobs->is_approved==1) ? '<span class="text-success"> Approved</span>' : '<span class="text-danger"> Un-Approve</span>';
                    return $status;
                })
                ->addColumn('action',function(PostJob $jobs){
                    return '<a href="'.route('job-details',['jobid'=>$jobs->id]).'" target="_blank"><span class="badge badge-success"> View </span></a>';
                })
                ->rawColumns(['checkbox','is_approved','action'])
            ->make(true);
    }
    public function viewJobDetail(Request $request){
        $this->data['candidate_data']=User::with(['additional_info','education','country','city'])->whereRole(2)->whereId($request->id)->first();
        return view('admin::candidate.view_candidate',$this->data);
    }
    public function isApproveJob(Request $request){
        if(PostJob::whereIn('id',$request->ids)->update(['is_approved'=>$request->status])){
            return redirect()->back()->with(['success' => config('constants.FLASH_REC_UPDATE_1')]);
        }
        return redirect()->back()->with(['success' => config('constants.FLASH_REC_UPDATE_0')]);
    }
/* **********************  Start PostedJobs function ********************* */

/* **********************  Start AdvertiseJobPlans function ********************* */
    public function advertisePlanFeatures()
    {
        $this->data['plan_list']=config('constants.advertise_plan_category');
        $this->data['features']=AdvertisePlanFeature::all();
        return view('admin::job_plans.add_advertise_features',$this->data);
    }
    public function saveAdvertiseFeature(Request $request) { 
        $postData = [
            "feature_title" => $request->feature_title,
            "val_type" => $request->val_type,
        ];
        if($request->val_type==0){
            $postData['basic_benefits']=$request->basic_benefits;
            $postData['premium_benefits']=$request->premium_benefits;
        } else {
            $postData['basic_benefits']=$request->basic_plan_value;
            $postData['premium_benefits']=$request->premium_plan_value;
        }
       
        $res = AdvertisePlanFeature::insertGetId($postData);
        if($res){
            return redirect()->back()->with(['success' => config('constants.FLASH_REC_ADD_1')]);
        }
        return redirect()->back()->with(['error' => config('constants.FLASH_REC_ADD_0')]);

    }
    public function deleteAdvertiseFeature($id)
    {
        if(AdvertisePlanFeature::where('id',$id)->delete())
            return redirect()->back()->with(['success' => config('constants.FLASH_REC_DELETE_1')]);
        else
            return redirect()->back()->with(['error' => config('constants.FLASH_REC_DELETE_0')]);
    }
/* **********************  End AdvertiseJobPlans function ********************* */

/* **********************  Start Settings function ********************* */
    public function settingsForm()
    {
        $this->data['settings']=Setting::first();
        return view('admin::settings.update_settings',$this->data);
    }   
   
    public function saveSettings(Request $request) {
        Cache::forget('settingData');   
        $res = Setting::whereId($request->id); 
        if($request->hasFile('site_logo')){
            $row_data= $res->first();
            unlinkImg($row_data->logo,'uploads/site_logo');
            $filename=$this->core->fileUpload($request->file('site_logo'),'uploads/site_logo');
            $request->merge(['logo'=>$filename]);
        }
       
        $res->update($request->except(['_token','site_logo']));
        if($res){            
            return redirect()->back()->with(['success' => config('constants.FLASH_REC_UPDATE_1')]);
        }
        return redirect()->back()->with(['error' => config('constants.FLASH_REC_UPDATE_0')]);
    }

    public function pageBannerForm()
    {
        $this->data['banners']=PageBanner::whereStatus(1)->get();
        return view('admin::settings.update_page_banners',$this->data);
    }   
   
    public function savePageBanner(Request $request) {
        $res = PageBanner::whereId($request->id); 
        if($request->hasFile('banner_img')){
            $row_data= $res->first();
            unlinkImg($row_data->image,'uploads/banner_img');
            $filename=$this->core->fileUpload($request->file('banner_img'),'uploads/banner_img');
            $request->merge(['image'=>$filename]);
        }
       
        $res->update($request->except(['_token','banner_img']));
        if($res){            
            return redirect()->back()->with(['success' => config('constants.FLASH_REC_UPDATE_1')]);
        }
        return redirect()->back()->with(['error' => config('constants.FLASH_REC_UPDATE_0')]);
    }
/* **********************  End Settings function ********************* */

    public function allEnquiry(){
        $this->data['enquiry']=Enquiry::get();
        return view('admin::all_enquiry',$this->data);
    }
    public function deleteEnquiry($id)
    {
        if(Enquiry::where('id',$id)->delete())
            return redirect()->back()->with(['success' => config('constants.FLASH_REC_DELETE_1')]);
        else
            return redirect()->back()->with(['error' => config('constants.FLASH_REC_DELETE_0')]);
    }

    public function transactionHistory(){
        $this->data['transactions']=TransactionHistory::with('user','job_plan')->orderBy('id','DESC')->get();
        return view('admin::transaction_history',$this->data);
    }

}
