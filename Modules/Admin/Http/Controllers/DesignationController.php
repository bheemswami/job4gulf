<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Designation;
class DesignationController extends Controller
{
   private $core;
    public $data=[];
    public function __construct()
    {
        $this->middleware('usersession');
        $this->core=app(\App\Http\Controllers\CoreController::class);
    }
    public function index()
    {
        $this->data['designations']=Designation::where('status', '!=' ,2)->get();
        return view('admin::designation.all_designation',$this->data);
    }
    public function addDesignation(){
        return view('admin::designation.add_edit_designation');
    }
    public function updateDesignation(Request $request){
        $this->data['designation_data']=Designation::whereId($request->id)->first();
        return view('admin::designation.add_edit_designation',$this->data);
    }
    public function saveDesignation(Request $request) {
        $res = Designation::updateOrCreate(['id'=>$request->id],$request->except(['_token']));
        if($request->id>0){
            $success = config('constants.FLASH_REC_UPDATE_1');
            $error = config('constants.FLASH_REC_UPDATE_0');
        } else {
            $success = config('constants.FLASH_REC_ADD_1');
            $error = config('constants.FLASH_REC_ADD_0');
        }
        
        if($res){
            return redirect()->back()->with(['success' => $success]);
        }
        return redirect()->back()->with(['error' => $error]);
    }
    public function deleteDesignation(Request $request){
        if(Designation::whereId($request->id)->update(['status'=>2])){
            return redirect()->back()->with(['success' => config('constants.FLASH_REC_DELETE_1')]);
        }
        return redirect()->back()->with(['error' => config('constants.FLASH_REC_DELETE_0')]);
    }
}
