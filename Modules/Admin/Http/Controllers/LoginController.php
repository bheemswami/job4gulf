<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Hash,DB;
use Session,Validator;
use App\Login;

class LoginController extends Controller
{
    public function index(Request $request){
        return view('admin::login');
    }
    public function loginProcess(Request $request){
        $user=Login::where(['email'=>$request->email])->first();
        if ($user && Hash::check($request->password,$user['password'])){
            $request->session()->put('auth',$user);
            return redirect()->route('dashboard')->with(['success' => config('constants.FLASH_SUCCESS_LOGIN')]);;
        } else {
        	return redirect()->back()->with(['error' => config('constants.FLASH_INVALID_CREDENTIAL')]);
        }
        return view('admin::login');
    }
    public function logYouOut(Request $request){
    	$request->session()->flush();
    	return view('admin::login');
    }
}
