<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Industry;
class IndustryController extends Controller
{
   private $core;
    public $data=[];
    public function __construct()
    {
        $this->middleware('usersession');
        $this->core=app(\App\Http\Controllers\CoreController::class);
    }
    public function index()
    {
        $this->data['industries']=Industry::where('status', '!=' ,2)->get();
        return view('admin::industry.all_industry',$this->data);
    }
    public function addIndustry(){
        return view('admin::industry.add_edit_industry');
    }
    public function updateIndustry(Request $request){
        $this->data['industry_data']=Industry::whereId($request->id)->first();
        return view('admin::industry.add_edit_industry',$this->data);
    }
    public function saveIndustry(Request $request) {
        
        if($request->id>0){
            $rec = Industry::find($request->id);
            $success = config('constants.FLASH_REC_UPDATE_1');
            $error = config('constants.FLASH_REC_UPDATE_0');
        } else {
            $success = config('constants.FLASH_REC_ADD_1');
            $error = config('constants.FLASH_REC_ADD_0');
        }
        if($request->hasFile('img')){
            if($request->id>0){
                unlinkImg($rec->icon,'uploads/industry_img');
            }
            $filename=$this->core->fileUpload($request->file('img'),'uploads/industry_img',1);
            $request->merge(['icon'=>$filename]);
        }
        if($request->hasFile('img_hover')){
            if($request->id>0){
                unlinkImg($rec->icon_hover,'uploads/industry_img');
            }
            $filename=$this->core->fileUpload($request->file('img_hover'),'uploads/industry_img',1);
            $request->merge(['icon_hover'=>$filename]);
        }
        $res = Industry::updateOrCreate(['id'=>$request->id],$request->except(['_token','img','img_hover']));
        if($res){
            return redirect()->back()->with(['success' => $success]);
        }
        return redirect()->back()->with(['error' => $error]);
    }
    public function deleteIndustry(Request $request){
        if(Industry::whereId($request->id)->update(['status'=>2])){
            return redirect()->back()->with(['success' => config('constants.FLASH_REC_DELETE_1')]);
        }
        return redirect()->back()->with(['error' => config('constants.FLASH_REC_DELETE_0')]);
    }
}
