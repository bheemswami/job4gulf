<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\FunctionalArea;
class FunctionalAreaController extends Controller
{
   private $core;
    public $data=[];
    public function __construct()
    {
        $this->middleware('usersession');
        $this->core=app(\App\Http\Controllers\CoreController::class);
    }
    public function index()
    {
        $this->data['functional_area']=FunctionalArea::where('status', '!=' ,2)->get();
        return view('admin::functional_area.all_functional_area',$this->data);
    }
    public function addFunctionalArea(){
        return view('admin::functional_area.add_edit_functional_area');
    }
    public function updateFunctionalArea(Request $request){
        $this->data['functional_area_data']=FunctionalArea::whereId($request->id)->first();
        return view('admin::functional_area.add_edit_functional_area',$this->data);
    }
    public function saveFunctionalArea(Request $request) {
        $res = FunctionalArea::updateOrCreate(['id'=>$request->id],$request->except(['_token']));
        if($request->id>0){
            $success = config('constants.FLASH_REC_UPDATE_1');
            $error = config('constants.FLASH_REC_UPDATE_0');
        } else {
            $success = config('constants.FLASH_REC_ADD_1');
            $error = config('constants.FLASH_REC_ADD_0');
        }
        
        if($res){
            return redirect()->back()->with(['success' => $success]);
        }
        return redirect()->back()->with(['error' => $error]);
    }
    public function deleteFunctionalArea(Request $request){
        if(FunctionalArea::whereId($request->id)->update(['status'=>2])){
            return redirect()->back()->with(['success' => config('constants.FLASH_REC_DELETE_1')]);
        }
        return redirect()->back()->with(['error' => config('constants.FLASH_REC_DELETE_0')]);
    }
}
