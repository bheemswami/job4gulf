<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\City,App\State,App\Country;
class CityController extends Controller
{
   private $core;
    public $data=[];
    public function __construct()
    {
        $this->middleware('usersession');
        $this->core=app(\App\Http\Controllers\CoreController::class);
    }
    public function getState($id)
    {
        $state=State::where('status',1)->where('country_id',$id)->select('id','name')->get();
        return response()->json($state);
    }
    public function index()
    {
        $this->data['cities']=City::with('country')->where('status', '!=' ,2)->get();
        //dd($this->data);
        return view('admin::city.all_city',$this->data);
    }
    public function addCity(){
        return view('admin::city.add_edit_city');
    }
    public function updateCity(Request $request){
        $city=City::whereId($request->id)->first();
        $this->data['city_data']=$city;
        //$this->data['state_id']=$city->state_id;
        //$state=State::whereId($city->state_id)->first();
        //$country=Country::whereId($city->country_id)->first();
        //dd($state);
        $this->data['country_id']=$city->country_id;
        //dd($this->data);
        
        return view('admin::city.add_edit_city',$this->data);
    }
    public function saveCity(Request $request) {
       if($request->hasFile('img')){
            if($request->id>0){
                $rec = City::find($request->id);
                unlinkImg($rec->icon,'uploads/city_img');
            }
            $filename=$this->core->fileUpload($request->file('img'),'uploads/city_img',1);
            $request->merge(['icon'=>$filename]);
        }
        $res = City::updateOrCreate(['id'=>$request->id],$request->except(['_token','img']));
        if($request->id>0){
            $success = config('constants.FLASH_REC_UPDATE_1');
            $error = config('constants.FLASH_REC_UPDATE_0');
        } else {
            $success = config('constants.FLASH_REC_ADD_1');
            $error = config('constants.FLASH_REC_ADD_0');
        }
        
        if($res){
            return redirect()->back()->with(['success' => $success]);
        }
        return redirect()->back()->with(['error' => $error]);
    }
    public function deleteCity(Request $request){
        if(City::whereId($request->id)->update(['status'=>2])){
            return redirect()->back()->with(['success' => config('constants.FLASH_REC_DELETE_1')]);
        }
        return redirect()->back()->with(['error' => config('constants.FLASH_REC_DELETE_0')]);
    }
}
