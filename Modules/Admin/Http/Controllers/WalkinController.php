<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Walkin,App\WalkinInterviewDetail,App\WalkinPosition;
use App\Designation;
use App\Industry;
use App\AppliedWalkin;
use App\Employer;

class WalkinController extends Controller
{
   private $core;
    public $data=[];
    public function __construct()
    {
        $this->middleware('usersession');
        $this->core=app(\App\Http\Controllers\CoreController::class);
    }
    public function index()
    {
        $this->data['walkins']=Walkin::where('status', '!=' ,2)->with('industry')->orderBy('id','DESC')->get();
        return view('admin::walkin.all_walkins',$this->data);
    }
    public function addWalkin(){
        $employerList=Employer::where('status',1)->orderBy('comp_name','ASC')->get();
        foreach($employerList as $k=>$val){
            if($val->comp_type==1){
                $employers['employers'][$val->id]=$val->comp_name;
            } else {
                $employers['consultants'][$val->id]=$val->comp_name;
            }
        }
        $this->data['employers']=$employers;
        $this->data['cities']=$this->core->getCityList('pluck');
        $this->data['industries']=$this->core->getIndustryList('pluck');
        $this->data['designations']=Designation::where('status',1)->orderBy('name','ASC')->pluck('name','id');
        return view('admin::walkin.add_walkin',$this->data);
    }
    public function editWalkin($id){
        $this->data['walkin_data']=Walkin::whereId($id)->with('walkin_interview','walkin_position')->first();
        $this->data['cities']=$this->core->getCityList('pluck');
        $this->data['industries']=$this->core->getIndustryList('pluck');
        $this->data['designations']=Designation::where('status',1)->orderBy('name','ASC')->pluck('name','id');
        return view('admin::walkin.edit_walkin',$this->data); 
    }
    public function updateWalkin(Request $request,$id){
        //dd($request->all());
        if($request->hasFile('logo')){
            $filename=$this->core->fileUpload($request->file('logo'),'uploads/walkin_comp_logo');
            $request->merge(['comp_logo'=>$filename]);
        }
        foreach($request->interview_detail as $k=>$val){
            $interview_data[]=['walkin_id'=>$id,'city_id'=>$val['location'],'date_from'=>dateConvert($val['date_from'],'Y-m-d H:i:s'),'date_to'=>dateConvert($val['date_to'],'Y-m-d H:i:s'),'address'=>$val['address']];
        }       
        WalkinInterviewDetail::where('walkin_id',$id)->delete();
        WalkinInterviewDetail::insert($interview_data);  
        if($request->designation_ids){
            $position_data=[];
            foreach($request->designation_ids as $k=>$val){
                $position_data[]=['walkin_id'=>$id,'designation_id'=>$val];
            }
            WalkinPosition::where('walkin_id',$id)->delete();
            WalkinPosition::insert($position_data);
        }

        if(Walkin::where('id',$id)->update($request->except(['_token','designation_ids','interview_detail','logo'])))
            return redirect()->back()->with(['success' => config('constants.FLASH_REC_UPDATE    _1')]);
        else
            return redirect()->back()->with(['error' => config('constants.FLASH_REC_UPDATE  _0')]);
        
        //dd($request->all());
        //$this->data['walkin_data']=Walkin::whereId($request->id)->first();
       // return view('admin::walkin.add_edit_walkin',$this->data);
    }
    public function saveWalkin(Request $request) {
        if($request->emp_id==0){
            if($request->hasFile('logo')){
                $filename=$this->core->fileUpload($request->file('logo'),'uploads/employer_logo');
                $comp_logo=$filename;
            }
            $walkinsData=['title'=>$request->title,'comp_name'=>$request->com_name,'comp_logo'=>$comp_logo,'comp_industry'=>$request->comp_industry,'country_id'=>$request->country_id,'about_us'=>$request->about_us,'job_description'=>$request->job_description];

        } else {
            $employerData=Employer::where('id',$request->emp_id)->first();
            $comp_logo=$employerData->comp_logo;
            $walkinsData=['title'=>$request->title,'comp_name'=>$employerData->comp_name,'comp_logo'=>$comp_logo,'comp_industry'=>$employerData->comp_industry,'country_id'=>$employerData->comp_country,'about_us'=>$employerData->comp_about,'job_description'=>$request->job_description,'employer_type'=>$employerData->comp_type];
        }
          
        $lastWalkinId = Walkin::insertGetId($walkinsData);
        
        if($lastWalkinId > 0){
            if($request->interview_detail){
                $interview_data=[];
                foreach($request->interview_detail as $k=>$val){
                    $interview_data[]=['walkin_id'=>$lastWalkinId,'city_id'=>$val['location'],'date_from'=>dateConvert($val['date_from'],'Y-m-d H:i:s'),'date_to'=>dateConvert($val['date_to'],'Y-m-d H:i:s'),'address'=>$val['address']];
                }
                WalkinInterviewDetail::insert($interview_data);
            }
            if($request->designation_ids){
                $position_data=[];
                foreach($request->designation_ids as $k=>$val){
                    $position_data[]=['walkin_id'=>$lastWalkinId,'designation_id'=>$val];
                }
                WalkinPosition::insert($position_data);
            }
            return redirect()->back()->with(['success' => config('constants.FLASH_REC_ADD_1')]);
        } else {            
            return redirect()->back()->with(['error' => config('constants.FLASH_REC_ADD_0')]);
        }
    }
    public function deleteWalkin(Request $request){
        if(Walkin::whereId($request->id)->update(['status'=>2])){
            return redirect()->back()->with(['success' => config('constants.FLASH_REC_DELETE_1')]);
        }
        return redirect()->back()->with(['error' => config('constants.FLASH_REC_DELETE_0')]);
    }
    public function viewWalkin($id){
        $this->data['walkin_data']=Walkin::whereId($id)->with('industry','country','walkin_interview','walkin_position')->first();
        //dd($this->data);
        $this->data['applied_walkins']=AppliedWalkin::where('walkin_id',$id)->with('walkin','user')->get();
        return view('admin::walkin.view_walkin',$this->data);
    }
    public function allAppliedWalkin(){
        $this->data['walkins']=AppliedWalkin::with('walkin','user')->get();
        return view('admin::walkin.all_applied_walkins',$this->data);

    }
    public function deleteAppliedWalkin($id){
        if(AppliedWalkin::where('id',$id)->delete())
            return redirect()->back()->with(['success' => config('constants.FLASH_REC_DELETE_1')]);
        else
            return redirect()->back()->with(['error' => config('constants.FLASH_REC_DELETE_0')]);
    }
}
