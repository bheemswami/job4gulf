<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Course;
class CourseController extends Controller
{
   private $core;
    public $data=[];
    public function __construct()
    {
        $this->middleware('usersession');
        $this->core=app(\App\Http\Controllers\CoreController::class);
    }
    public function index()
    {
        $this->data['courses']=Course::with('degree')->where('status', '!=' ,2)->get();
        return view('admin::course.all_course',$this->data);
    }
    public function addCourse(){
        return view('admin::course.add_edit_course');
    }
    public function updateCourse(Request $request){
        $this->data['course_data']=Course::whereId($request->id)->first();
        return view('admin::course.add_edit_course',$this->data);
    }
    public function saveCourse(Request $request) {
       if($request->hasFile('img')){
            $filename=$this->core->fileUpload($request->file('img'),'uploads/course_img');
            $request->merge(['icon'=>$filename]);
        }
        $res = Course::updateOrCreate(['id'=>$request->id],$request->except(['_token','img']));
        if($request->id>0){
            $success = config('constants.FLASH_REC_UPDATE_1');
            $error = config('constants.FLASH_REC_UPDATE_0');
        } else {
            $success = config('constants.FLASH_REC_ADD_1');
            $error = config('constants.FLASH_REC_ADD_0');
        }
        
        if($res){
            return redirect()->back()->with(['success' => $success]);
        }
        return redirect()->back()->with(['error' => $error]);
    }
    public function deleteCourse(Request $request){
        if(Course::whereId($request->id)->update(['status'=>2])){
            return redirect()->back()->with(['success' => config('constants.FLASH_REC_DELETE_1')]);
        }
        return redirect()->back()->with(['error' => config('constants.FLASH_REC_DELETE_0')]);
    }
}
