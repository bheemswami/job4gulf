<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Company;
class CompanyController extends Controller
{
   private $core;
    public $data=[];
    public function __construct()
    {
        $this->middleware('usersession');
        $this->core=app(\App\Http\Controllers\CoreController::class);
    }
    public function index()
    {
        $this->data['companies']=Company::where('status', '!=' ,2)->get();
        return view('admin::company.all_company',$this->data);
    }
    public function addCompany(){
        return view('admin::company.add_edit_company');
    }
    public function updateCompany(Request $request){
        $this->data['company_data']=Company::whereId($request->id)->first();
        return view('admin::company.add_edit_company',$this->data);
    }
    public function saveCompany(Request $request) {
        $res = Company::updateOrCreate(['id'=>$request->id],$request->except(['_token']));
        if($request->id>0){
            $success = config('constants.FLASH_REC_UPDATE_1');
            $error = config('constants.FLASH_REC_UPDATE_0');
        } else {
            $success = config('constants.FLASH_REC_ADD_1');
            $error = config('constants.FLASH_REC_ADD_0');
        }
        
        if($res){
            return redirect()->back()->with(['success' => $success]);
        }
        return redirect()->back()->with(['error' => $error]);
    }
    public function deleteCompany(Request $request){
        if(Company::whereId($request->id)->update(['status'=>2])){
            return redirect()->back()->with(['success' => config('constants.FLASH_REC_DELETE_1')]);
        }
        return redirect()->back()->with(['error' => config('constants.FLASH_REC_DELETE_0')]);
    }
}
