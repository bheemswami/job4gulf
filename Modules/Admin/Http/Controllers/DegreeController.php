<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Degree;
class DegreeController extends Controller
{
   private $core;
    public $data=[];
    public function __construct()
    {
        $this->middleware('usersession');
        $this->core=app(\App\Http\Controllers\CoreController::class);
    }
    public function index()
    {
        $this->data['degries']=Degree::where('status', '!=' ,2)->get();
        return view('admin::degree.all_degree',$this->data);
    }
    public function addDegree(){
        return view('admin::degree.add_edit_degree');
    }
    public function updateDegree(Request $request){
        $this->data['degree_data']=Degree::whereId($request->id)->first();
        return view('admin::degree.add_edit_degree',$this->data);
    }
    public function saveDegree(Request $request) {
        $res = Degree::updateOrCreate(['id'=>$request->id],$request->except(['_token']));
        if($request->id>0){
            $success = config('constants.FLASH_REC_UPDATE_1');
            $error = config('constants.FLASH_REC_UPDATE_0');
        } else {
            $success = config('constants.FLASH_REC_ADD_1');
            $error = config('constants.FLASH_REC_ADD_0');
        }
        
        if($res){
            return redirect()->back()->with(['success' => $success]);
        }
        return redirect()->back()->with(['error' => $error]);
    }
    public function deleteDegree(Request $request){
        if(Degree::whereId($request->id)->update(['status'=>2])){
            return redirect()->back()->with(['success' => config('constants.FLASH_REC_DELETE_1')]);
        }
        return redirect()->back()->with(['error' => config('constants.FLASH_REC_DELETE_0')]);
    }
}
