<?php

Route::group(['middleware' => 'web', 'prefix' => 'administrator', 'namespace' => 'Modules\Admin\Http\Controllers'], function()
{
    Route::get('/', 'LoginController@index');
   // Route::get('/login', 'LoginController@index')->name('login');
    Route::get('/login-process', 'LoginController@index')->name('login-process');
    Route::post('/login-process', 'LoginController@loginProcess')->name('login-process');
    Route::get('/logoutme', 'LoginController@logYouOut')->name('logMeOut');
    
    //Dashboard
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

    //Admin Controller
    Route::get('employers', 'AdminController@registeredEmployer')->name('employers');
    Route::get('view-employer/{id}', 'AdminController@viewEmployer')->name('view-employer');
    Route::get('consultants', 'AdminController@registeredConsultant')->name('consultants');
    Route::post('update-employer-status', 'AdminController@updateEmployerStatus')->name('update-employer-status');

    Route::get('apply-job-plan/{emp_id}', 'AdminController@applyJobPlanEmployer')->name('apply-job-plan');
    Route::post('save-job-plan', 'AdminController@saveJobPlanEmployer')->name('save-job-plan');

    Route::get('manage-top-member/{emp_id}', 'AdminController@manageTopEmployer')->name('manage-top-member');
    Route::post('make-top-member', 'AdminController@makeTopEmployer')->name('make-top-member');

    Route::post('update-emp-walkins', 'AdminController@updateEmployerWalkins')->name('update-emp-walkins');
    
    Route::get('all-transactions', 'AdminController@transactionHistory')->name('all-transactions');

    //Candidate
    Route::get('candidates', 'AdminController@registeredCandidates')->name('candidates');
    Route::get('view-candidate/{id}', 'AdminController@viewCandidate')->name('view-candidate');
    Route::post('update-candidate-status', 'AdminController@updateCandidateStatus')->name('update-candidate-status');


   //Country
    Route::get('all-countries', 'CountryController@index')->name('all-countries');
    Route::get('/add-country', 'CountryController@addCountry')->name('add-country');
    Route::post('/save-country', 'CountryController@saveCountry')->name('save-country');
    Route::get('/update-country/{id}', 'CountryController@updateCountry')->name('update-country');
    Route::get('/delete-country/{id}', 'CountryController@deleteCountry')->name('delete-country');

    //LatestNews
    Route::get('all-news', 'LatestNewsController@index')->name('all-news');
    Route::get('/add-news', 'LatestNewsController@addNews')->name('add-news');
    Route::post('/save-news', 'LatestNewsController@saveNews')->name('save-news');
    Route::get('/update-news/{id}', 'LatestNewsController@updateNews')->name('update-news');
    Route::get('/delete-news/{id}', 'LatestNewsController@deleteNews')->name('delete-news');

    //City
    Route::get('all-cities', 'CityController@index')->name('all-cities');
    Route::get('/add-city', 'CityController@addCity')->name('add-city');
    Route::post('/save-city', 'CityController@saveCity')->name('save-city');
    Route::get('/update-city/{id}', 'CityController@updateCity')->name('update-city');
    Route::get('/delete-city/{id}', 'CityController@deleteCity')->name('delete-city');
    Route::get('/get-state/{id}', 'CityController@getState')->name('get-state');

    //Degree
    Route::get('all-degries', 'DegreeController@index')->name('all-degries');
    Route::get('/add-degree', 'DegreeController@addDegree')->name('add-degree');
    Route::post('/save-degree', 'DegreeController@saveDegree')->name('save-degree');
    Route::get('/update-degree/{id}', 'DegreeController@updateDegree')->name('update-degree');
    Route::get('/delete-degree/{id}', 'DegreeController@deleteDegree')->name('delete-degree');

    //Course
    Route::get('all-courses', 'CourseController@index')->name('all-courses');
    Route::get('/add-course', 'CourseController@addCourse')->name('add-course');
    Route::post('/save-course', 'CourseController@saveCourse')->name('save-course');
    Route::get('/update-course/{id}', 'CourseController@updateCourse')->name('update-course');
    Route::get('/delete-course/{id}', 'CourseController@deleteCourse')->name('delete-course');


    //Designation
    Route::get('all-designations', 'DesignationController@index')->name('all-designations');
    Route::get('/add-designation', 'DesignationController@addDesignation')->name('add-designation');
    Route::post('/save-designation', 'DesignationController@saveDesignation')->name('save-designation');
    Route::get('/update-designation/{id}', 'DesignationController@updateDesignation')->name('update-designation');
    Route::get('/delete-designation/{id}', 'DesignationController@deleteDesignation')->name('delete-designation');
    
    //Industry
    Route::get('all-industries', 'IndustryController@index')->name('all-industries');
    Route::get('/add-industry', 'IndustryController@addIndustry')->name('add-industry');
    Route::post('/save-industry', 'IndustryController@saveIndustry')->name('save-industry');
    Route::get('/update-industry/{id}', 'IndustryController@updateIndustry')->name('update-industry');
    Route::get('/delete-industry/{id}', 'IndustryController@deleteIndustry')->name('delete-industry');

    //FunctionalArea
    Route::get('all-functional-area', 'FunctionalAreaController@index')->name('all-functional-area');
    Route::get('/add-functional-area', 'FunctionalAreaController@addFunctionalArea')->name('add-functional-area');
    Route::post('/save-functional-area', 'FunctionalAreaController@saveFunctionalArea')->name('save-functional-area');
    Route::get('/update-functional-area/{id}', 'FunctionalAreaController@updateFunctionalArea')->name('update-functional-area');
    Route::get('/delete-functional-area/{id}', 'FunctionalAreaController@deleteFunctionalArea')->name('delete-functional-area');
    
    //Institute
    Route::get('all-institutes', 'InstituteController@index')->name('all-institutes');
    Route::get('/add-institute', 'InstituteController@addInstitute')->name('add-institute');
    Route::post('/save-institute', 'InstituteController@saveInstitute')->name('save-institute');
    Route::get('/update-institute/{id}', 'InstituteController@updateInstitute')->name('update-institute');
    Route::get('/delete-institute/{id}', 'InstituteController@deleteInstitute')->name('delete-institute');

    //Company
    Route::get('all-companies', 'CompanyController@index')->name('all-companies');
    Route::get('/add-company', 'CompanyController@addCompany')->name('add-company');
    Route::post('/save-company', 'CompanyController@saveCompany')->name('save-company');
    Route::get('/update-company/{id}', 'CompanyController@updateCompany')->name('update-company');
    Route::get('/delete-company/{id}', 'CompanyController@deleteCompany')->name('delete-company');

    //Specialization
    Route::get('all-specializations', 'SpecializationController@index')->name('all-specializations');
    Route::get('/add-specialization', 'SpecializationController@addSpecialization')->name('add-specialization');
    Route::post('/save-specialization', 'SpecializationController@saveSpecialization')->name('save-specialization');
    Route::get('/update-specialization/{id}', 'SpecializationController@updateSpecialization')->name('update-specialization');
    Route::get('/delete-specialization/{id}', 'SpecializationController@deleteSpecialization')->name('delete-specialization');

    //Profile
    Route::get('/profile', 'ProfileController@index')->name('profile');
    Route::get('/update-profile', 'ProfileController@saveProfile')->name('save-profile');

    //walkins
    Route::get('all-walkins', 'WalkinController@index')->name('all-walkins'); 
    Route::get('all-applied-walkins', 'WalkinController@allAppliedWalkin')->name('all-applied-walkins');
    Route::get('/add-walkin', 'WalkinController@addWalkin')->name('add-walkin');
    Route::post('/save-walkin', 'WalkinController@saveWalkin')->name('save-walkin');
    Route::post('/update-walkin/{id}', 'WalkinController@updateWalkin')->name('update-walkin');
    Route::get('/delete-walkin/{id}', 'WalkinController@deleteWalkin')->name('delete-walkin');
    Route::get('/view-walkin/{id}','WalkinController@viewWalkin')->name('view-walkin');
    Route::get('/edit-walkin/{id}', 'WalkinController@editWalkin')->name('edit-walkin');
    Route::get('/delete-applied-walkin/{id}', 'WalkinController@deleteAppliedWalkin')->name('delete-applied-walkin');

    //posted jobs
    Route::get('all-posted-jobs', 'AdminController@postedJobs')->name('all-posted-jobs');
    Route::get('view-job-detail', 'AdminController@viewJobDetail')->name('view-job-detail');
    Route::post('is-approve-job', 'AdminController@isApproveJob')->name('is-approve-job');

    //services
    Route::get('all-services', 'AdminController@serviceIndex')->name('all-services');
    Route::post('/save-service', 'AdminController@saveService')->name('save-service');
    Route::get('/update-service/{id}', 'AdminController@updateService')->name('update-service');

    //job plans
    Route::get('all-jobplans', 'AdminController@jobPlansIndex')->name('all-jobplans');
    Route::get('add-jobplan', 'AdminController@addJobPlan')->name('add-jobplan');
    Route::post('create-jobplan', 'AdminController@createJobPlan')->name('create-jobplan');
    Route::get('/update-jobplan/{id}', 'AdminController@updateJobPlan')->name('update-jobplan');
    Route::post('/save-jobplan', 'AdminController@saveJobPlan')->name('save-jobplan');
    Route::get('/delete-jobplan/{id}', 'AdminController@deleteJobPlan')->name('delete-jobplan');

    //advertise job plans features
    Route::get('advertise-features', 'AdminController@advertisePlanFeatures')->name('advertise-features');
    Route::post('/save-advertise-feature', 'AdminController@saveAdvertiseFeature')->name('save-advertise-feature');
    Route::get('/delete-advertise-feature/{id}', 'AdminController@deleteAdvertiseFeature')->name('delete-advertise-feature');


    //Enquiry
    Route::get('all-enquiry', 'AdminController@allEnquiry')->name('all-enquiry');
    Route::get('delete-enquiry/{id}', 'AdminController@deleteEnquiry')->name('delete-enquiry');

    //settings
    Route::get('settings', 'AdminController@settingsForm')->name('settings');
    Route::post('/save-settings', 'AdminController@saveSettings')->name('save-settings');
    Route::get('page-banners', 'AdminController@pageBannerForm')->name('page-banners');
    Route::post('/save-page-banner', 'AdminController@savePageBanner')->name('save-page-banner');

    
});
