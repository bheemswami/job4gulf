@extends('admin::layouts.master')
@section('content')
  @php 
      $flag=0;
      $heading='Add';
      if(isset($city_data) && !empty($city_data)){
          $flag=1;
          $heading='Update';
      }
  @endphp
      <div class="row form-parentBlock">      
          <div class="block-title">
            <h4>{{ $heading }} City</h4>
          </div>
          @if($flag==1)
                  {{ Form::model($city_data,array('url'=>route('save-city'),'class'=>'city-form','files' => true))}}
                      {{Form::hidden('id',null)}}
              @else
                  {{ Form::open(array('url'=>route('save-city'),'class'=>'city-form','files' => true))}}
              @endif
          <div class="form-parentBlock-inner"> 
              
              {{ csrf_field() }}
              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 form-group">
                <label for="">Country</label>
                {{Form::select('country_id',['Gulf Country'=>getGulfCountries(),'Other Country'=>getOtherCountries()],@$country_id,['id'=>'country','class'=>"form-control",'placeholder'=>'--Select--'])}}
              </div>
              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 form-group">
                <label for="">Name</label>
                {{Form::text('name',null,['class'=>"form-control",'placeholder'=>'Enter Country Name'])}}
              </div>
              
              
              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 form-group">
                <label for="">Status</label>
                {{Form::select('status',config('constants.status'),1,['class'=>"form-control",'placeholder'=>'--Select--'])}}
              </div>
              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 form-group">
                <label for="">Icon</label>
                <div class="box">
                  {{Form::file('img',['class'=>"form-control",'id'=>'file-1'])}}
                </div>
                  @if($flag==1)
                    <img class="icon-medium" src="{{checkFile($city_data->icon,'/uploads/city_img/','small-logo.png')}}"/>
                  @endif
              </div>
       </div>
     </div>
        <div class="form-action">   
            <button type="submit" class="btn btn-primary submit-form">Submit</button>
          </div>
      {{Form::close()}}
     
@stop