@extends('admin::layouts.master')
@section('content')
  @php 
      $flag=0;
      $heading='Add';
      if(isset($country_data) && !empty($country_data)){
          $flag=1;
          $heading='Update';
      }
  @endphp
      <div class="row form-parentBlock">      
          <div class="block-title">
            <h4>{{ $heading }} Country</h4>
          </div>
          @if($flag==1)
                  {{ Form::model($country_data,array('url'=>route('save-country'),'class'=>'country-form','files' => true))}}
                      {{Form::hidden('id',null)}}
              @else
                  {{ Form::open(array('url'=>route('save-country'),'class'=>'country-form','files' => true))}}
              @endif
          <div class="form-parentBlock-inner"> 
              
              {{ csrf_field() }}
              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 form-group">
                <label for="">Name</label>
                {{Form::text('name',null,['class'=>"form-control",'placeholder'=>'Enter Country Name'])}}
              </div>
               <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 form-group">
                <label for="">Short Name</label>
                {{Form::text('sortname',null,['class'=>"form-control",'placeholder'=>'Enter Country Short Name'])}}
              </div>
              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 form-group">
                <label for="">Phone Code</label>
                {{Form::text('phonecode',null,['class'=>"form-control",'placeholder'=>'Enter Country Phonecode'])}}
              </div>
        
              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 form-group">
                <label for="">Icon</label>
                <div class="box">
                  {{Form::file('img',['class'=>"form-control",'id'=>'file-1'])}}
                </div>
                
                  @if($flag==1)
                    <img class="icon-medium" src="{{$country_data->icon}}"/>
                  @endif
              </div>
              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 form-group">
                <label for="">Status</label>
                {{Form::select('status',config('constants.status'),1,['class'=>"form-control",'placeholder'=>'--Select--'])}}
              </div>
              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 form-group">
                <label for="">Is Gulf</label>
                @if($country_data)
                @if($country_data->is_gulf==1)
                {{Form::checkbox('is_gulf',1,true)}}
                @else
                {{Form::checkbox('is_gulf',1,false)}}
                @endif
                @else
                {{Form::checkbox('is_gulf',1,false)}}
                @endif
              </div>
              
       </div>
     </div>
        <div class="form-action">   
            <button type="submit" class="btn btn-primary submit-form">Submit</button>
          </div>
      {{Form::close()}}
@stop