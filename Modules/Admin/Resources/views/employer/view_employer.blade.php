@extends('admin::layouts.master')
@section('content')
      <div class="row form-parentBlock">      
          <div class="block-title">
            <h4>Employer Details</h4>
          </div>
          <div class="form-parentBlock-inner"> 
            <table class="table table-bordered" cellspacing="0" width="100%">
              <tr>
                <th>Name :</th>
                <td>{{ ($employer_data->user!=null) ? $employer_data->user->name : '' }}</td>                
              </tr>
              <tr>
                <th>Email :</th>
                <td>
                  {{ ($employer_data->user!=null) ? $employer_data->user->email : '' }}<br/>
                  ({!!($employer_data->is_email==1) ? '<span class="status_green">Verified</span>' : '<span class="status_red">Unverified</span>' !!}</span>)
                </td>                
              </tr>
              <tr>               
                <th>Account Status :</th>
                <td>{!!($employer_data->status==1) 
                  ? '<button class="btn btn-success btn-sm">Approved</button>' 
                  : '<button class="btn btn-warning btn-sm">Pending</button>'!!}
                </td>
              </tr>            
            </table>
          </div>
     </div>

     <div class="row form-parentBlock">      
          <div class="block-title">
            <h4>Contact Information</h4>
          </div>
          <div class="form-parentBlock-inner"> 
            <table class="table table-bordered" cellspacing="0" width="100%">
              <tr>
                <th>Contact Person :</th>
                <td>{{ $employer_data->cp_title.' '.$employer_data->cp_name }}</td>                
              </tr>
              <tr>
                <th>Designation :</th>
                <td>{{ ($employer_data->designations!=null) ? $employer_data->designations->name : '' }}</td>                
              </tr>
              <tr>
                <th>Phone</th>
                <td>+{{ $employer_data->dial_code }}-{{ $employer_data->cp_mobile }}</td>
              </tr>           
            </table>
          </div>
     </div>
     <div class="row form-parentBlock">      
          <div class="block-title">
            <h4>Company Information</h4>
          </div>
          <div class="form-parentBlock-inner"> 
            <table class="table table-bordered" cellspacing="0" width="100%">
              <tr>
                <th>Name :</th>
                <td>{{ $employer_data->comp_name }}</td>                
              </tr>
              <tr>
                <th>Address :</th>
                <td>{{ $employer_data->comp_address }}, {{ ($employer_data->city!=null) ? $employer_data->city->name : '' }}, {{ ($employer_data->country!=null) ? $employer_data->country->name : '' }}</td>                
              </tr>
              <tr>
                <th>Website :</th>
                <td>{{ $employer_data->comp_website }}</td>                
              </tr>          
              <tr>
                <th>Logo:</th>
                <td><img class="comapny_logo" src="{{checkFile($employer_data->comp_logo,'/uploads/comp_logo/','no_logo.png')}}"/></td>
              </tr>               
              <tr>
                <th>Industry:</th>
                <td>{{ ($employer_data->industry!=null) ? $employer_data->industry->name : '' }}</td>
              </tr>  
              <!-- <tr>
                <th>Key Skills:</th>
                <td>{{ $employer_data->skills }}</td>
              </tr> 
                -->
              <tr>
                <th>About:</th>
                <td>{{ $employer_data->comp_about }}</td>
              </tr>   
            </table>
          </div>
     </div>
@stop
