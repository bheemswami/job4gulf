@extends('admin::layouts.master')
@section('content')
<div class="row form-parentBlock">      
        <div class="col-sm-2">
             <button name="jobs" type="button" value="all" class="btn btn-sm btn-info jobs_btn">All Employers</button>
        </div>
        <div class="col-sm-2">
             <button name="jobs" type="button" value="1" class="btn btn-sm btn-success jobs_btn">Top Employers</button>
        </div>
        
    </div>
    <div class="row form-parentBlock all">      
        <div class="block-title">
            <h4>All Employers</h4>
        </div>
         @include('admin::employer.data_list',['list'=>'all'])
    </div>
    <div class="row form-parentBlock top hide-elem">      
        <div class="block-title">
            <h4>Top Employers</h4>
        </div>
         @include('admin::employer.data_list',['list'=>'top'])
    </div>
<!-- Start Walkins Modal -->
    <div id="updateWalkinsNumber" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add/Update Walkins</h4>
                </div>
                {{ Form::open(array('url'=>route('update-emp-walkins'),'class'=>''))}}  
                {{ Form::hidden('emp_id',null,['id'=>'emp_id']) }}
                    <div class="modal-body">                 
                        <div class="">
                          <label for="">No. of Walkins</label>
                          {{Form::number('no_of_walkins',null,['class'=>"form-control",'min'=>'0','id'=>'update_walkin_num'])}}
                        </div>
                        <div class="">
                          <label for="">Walkins End Date</label>
                          {{Form::text('walkins_end_date',null,['class'=>"form-control datepicker",'id'=>'update_walkin_date','readonly'=>true])}}
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success">Submit</button>
                    </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
<!-- End Walkins Modal -->

<script>
$('.jobs_btn').click(function(){
        $('.top,.all').hide();
      jobType = $(this).val();
      if(jobType=='all')
      { $('.all').show(); }
      else
      { $('.top').show(); }
    });
$('.update_walkin_num_btn').click(function(){
    $('#emp_id').val($(this).data('empid'));
    $('#update_walkin_num').val($(this).data('walkins'));
    $('#update_walkin_date').val($(this).data('walkinsdate'));
});
$('#updateWalkinsNumber').on('hidden.bs.modal', function () {
    $(this).find('form').trigger('reset');
});
$( ".datepicker" ).datepicker({
      changeMonth: true,
      changeYear: true,
      yearRange: "0:+5",
      showOn: "both",
      buttonImage: "{{url('public/images/icons/calendar.png')}}",
      buttonImageOnly: true,
      buttonText: "Select date",
      dateFormat: "dd-mm-yy",
      minDate:'0'
      
    });
</script>
@stop
