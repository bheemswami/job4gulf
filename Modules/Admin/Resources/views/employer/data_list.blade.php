
         {{ Form::open(array('url'=>route('update-employer-status'),'class'=>'form-horizontal','files' => true))}}
        <div class="form-parentBlock-inner"> 
            <table class="table table-bordered data_table" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>S.no.</th>                        
                        <th class="nosort"><input type="checkbox" name="id" id="select_all_checkbox"></th>                        
                        <th>Name </th>
                        <th>Email </th>
                        <th>Company Name </th>
                        <th>Company Address </th>
                        <th>Total Walkins </th>
                        {{-- <th>Company Website </th> --}}
                        <th>Status</th>
                       <th class="nosort">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @php $i=1; @endphp
                    @foreach($records as $k=>$val)
                    @if( ($list=='top' && $val->account_validity>date('Y-m-d')) || $list=='all')
                        <tr>
                            <td>{{$i++}}. </td>
                            <td><input type="checkbox" name="ids[]" value="{{$val->id}}" class="data_checkbox"></td>
                            <td>{{ ($val->user!=null) ? $val->user->name : '' }}</td>
                            <td>{{ ($val->user!=null) ? $val->user->email : '' }}</td>
                            <td>
                                <a href="{{$val->comp_website}}" class="comp_name" target="_blank">
                                    @if($val->account_validity>date('Y-m-d')) 
                                        <img src="{{url('public/images/icons/premium.png')}}">
                                    @endif
                                    {{ $val->comp_name }}
                                </a>
                            </td>
                            <td>{{ $val->comp_address }},{{ $val->comp_city }},{{ $val->comp_country }}</td>
                            <td>{{ $val->no_of_walkins }}<br/><span class="text-danger">End Date :</span><br/> {{ ($val->walkins_end_date!='') ? dateConvert($val->walkins_end_date,'d-m-Y') : 'NA' }}</td>
                            {{-- <td>{{ $val->comp_website }}</td> --}}
                            <td> 
                                @if($val->status==0)
                                    <span class="label label-warning"> Pending</span>
                                @elseif($val->status==1)
                                    <span class="label label-success"> Approved</span> 
                                @else
                                    <span class="label label-danger"> Un-Approved</span>
                                @endif
                            </td>
                            <td>
                                <a href="{{route('view-employer',[$val->id])}}" class="btn btn-xs btn-default" target="_blank"><i class="fa fa-pencil" aria-hidden="true"></i> View</a>
                                <a href="{{route('apply-job-plan',[$val->id])}}" class="btn btn-xs btn-warning"><i class="fa fa-info-circle" aria-hidden="true"></i> Job Plan</a>
                                <a href="{{route('manage-top-member',[$val->id])}}" class="btn btn-xs btn-info"><i class="fa fa-info-circle" aria-hidden="true"></i> Manage Employer</a>
                                <button type="button" class="btn btn-xs btn-info update_walkin_num_btn" data-toggle="modal" data-target="#updateWalkinsNumber" data-empid="{{ $val->id }}" data-walkins="{{ $val->no_of_walkins }}" data-walkinsdate="{{ ($val->walkins_end_date!='') ? dateConvert($val->walkins_end_date,'d-m-Y') : '' }}"><i class="fa fa-info-circle" aria-hidden="true"></i> Manage Walkins</button>
                            </td>
                        </tr>
                        @endif
                    @endforeach
                </tbody>
            </table>
            <div class="col-lg-12">
                <button name="status" type="submit" value="1" class="btn btn-sm btn-success">Approve Selected</button>
                <button name="status" type="submit" value="2" class="btn btn-sm btn-danger">Un-Approve Selected</button>
            </div>
        </div>
        {{ Form::close() }}