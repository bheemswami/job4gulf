@extends('admin::layouts.master')
@section('content')
      <div class="row form-parentBlock">      
          <div class="block-title">
            <h4>Candidate Details</h4>
          </div>
          <div class="form-parentBlock-inner"> 
            <table class="table table-bordered" cellspacing="0" width="100%">
              <tr>
                <td rowspan="5" class="text-center">
                  <img width="150px" class="" src="{{checkFile($candidate_data->avatar,'uploads/user_img/','user_img.png')}}"><br/>
                   @if($candidate_data->status==1)
                        <span class="label label-success"> Approved</span> 
                    @else
                        <span class="label label-default"> Inactive</span>
                    @endif
                </td>
              </tr>
              <tr>
                <th>Name :</th>
                <td>{{  $candidate_data->name }}</td>

                <th>E-mail :</th>
                <td>{{ $candidate_data->email }} {!! ($candidate_data->email_verified==0) ? '<span class="unverified">Not-verified</span>' : '<span class="verifylink">Verified</span>' !!}</td>
              </tr>
              <tr>
                <th>Gender :</th>
                <td>{{ $candidate_data->gender }}</td>

                <th>Mobile :</th> 
                <td>{{ $candidate_data->mobile }} {!! ($candidate_data->mobile_verified==0) ? '<span class="unverified">Not-verified</span>' : '<span class="verifylink">Verified</span>' !!}</td>
              </tr>
              <tr>
                <th>DOB :</th>
                <td>{{ dateConvert($candidate_data->dob,'d-m-Y') }}</td>

                <th>Country :</th>
                <td>{{ ($candidate_data->country!=null) ? $candidate_data->country->name : '-' }}</td>

              </tr>
              <tr>
                <th>City :</th>
                <td>{{ ($candidate_data->city!=null) ? $candidate_data->city->name : '-' }}</td>

                <th></th>
                <td></td>
              </tr>         
            </table>
          </div>
     </div>
    @if($candidate_data->additional_info!=null)
       <div class="row form-parentBlock">      
            <div class="block-title">
              <h4>Additional Information</h4>
            </div>
            <div class="form-parentBlock-inner"> 
              <table class="table table-bordered" cellspacing="0" width="100%">
                <tr>
                  <th>Experience Level :</th>
                  <td>{{  ($candidate_data->additional_info->exp_level==0) ? 'Fresher' : 'Experienced' }}</td>

                  <th>Experience :</th>
                  <td>
                    @if($candidate_data->additional_info->exp_level==1)
                      {{ ($candidate_data->additional_info->exp_year>0) ? $candidate_data->additional_info->exp_year : 0 }} Year 
                      {{ ($candidate_data->additional_info->exp_year>0) ? $candidate_data->additional_info->exp_year : 0 }} Month 
                    @else 
                      0 Year 0 Month
                    @endif
                  </td>
                </tr>
                <tr>
                  <th>Current Company :</th>
                  <td>{{ $candidate_data->additional_info->current_company }}</td>

                  <th>Current Position :</th> 
                  <td>{{ $candidate_data->additional_info->current_position }}</td>
                </tr>
                <tr>
                  <th>Industry :</th>
                  <td>{{ ($candidate_data->additional_info->industry!=null) ? $candidate_data->additional_info->industry->name : '' }}</td>

                  <th>Functional Area :</th>
                  <td>{{ ($candidate_data->additional_info->functional_area!=null) ? @$candidate_data->additional_info->functional_area->name : '' }}</td>

                </tr>
                <tr>
                  <th>Key Skills :</th>
                  <td colspan="3">{{ $candidate_data->additional_info->key_skills }}</td>
                </tr>         
              </table>
            </div>
       </div>
    @endif

    @if($candidate_data->education!=null)
       <div class="row form-parentBlock">      
            <div class="block-title">
              <h4>Educational Information</h4>
            </div>
            <div class="form-parentBlock-inner"> 
              <table class="table table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                      <th>Education</th>
                      <th>Course</th>
                      <th>Specialization</th>
                      <th>Year</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <th>Basic</th>
                      <td>{{ @$candidate_data->education->basic_course->name }}</td>
                      <td>{{ @$candidate_data->education->basic_specialization->name }}</td>
                      <td>{{ @$candidate_data->education->basic_comp_year }}</td>
                    </tr>
                    <tr>
                      <th>Master</th>
                      <td>{{ @$candidate_data->education->master_course->name }}</td>
                      <td>{{ @$candidate_data->education->master_specialization->name }}</td>
                      <td>{{ @$candidate_data->education->master_comp_year }}</td>
                    </tr>
                    <tr>
                      <th>Institute / University :</th>
                      <td colspan="3">{{ @$candidate_data->education->institute }}</td>
                    </tr>
                  </tbody>
              </table>
            </div>
       </div>
    @endif
  
@stop
