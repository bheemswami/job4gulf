@extends('admin::layout.master')
@section('content')
{{ Form::open(array('url'=>route('save-service'),'class'=>'service-form','files' => true))}}
{{ csrf_field() }}
        <div class="row form-parentBlock">      
            <div class="block-title">
              <h4>Avatar</h4>
            </div>
            <div class="form-parentBlock-inner">
            </div>
        </div>
      <div class="row form-parentBlock">      
          <div class="block-title">
            <h4>My Profile</h4>
          </div>
          <div class="form-parentBlock-inner">
          <input type="hidden" value="{{Session::get('auth')->id}}" name="id"/>
          <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
            <label for="">Name</label>
            {{Form::text('name',Session::get('auth')->name,['class'=>"form-control"])}}
          </div>
          <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
            <label for="">Email</label>
            {{Form::email('email',Session::get('auth')->email,['class'=>"form-control",'disabled'=>'true'])}}
          </div>
          <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 form-group">
            <label for="">Phone</label>
            {{Form::text('phone',Session::get('auth')->phone,['class'=>"form-control"])}}
          </div>
          <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 form-group">
            <label for="">Password</label>
            <input type="password" class="form-control" name="password">
          </div>
          <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 form-group">
            <label for="">Confirm Password</label>
            <input type="password" class="form-control" name="cpassword">
          </div>
      </div>
    </div>
    <div class="form-action">   
      <button type="submit" class="btn btn-primary submit-form">Update</button>
    </div>
      {{Form::close()}}
@stop