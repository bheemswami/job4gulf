@extends('admin::layouts.master')
@section('content')

      <div class="row form-parentBlock">      
          <div class="block-title">
            <h4>Walkin Detail</h4>
          </div>
          <div class="form-parentBlock-inner"> 
              <table class="table table-bordered" cellspacing="0" width="100%">
              <tbody><tr>
                <th>Title :</th>
                <td>{{$walkin_data->title}}</td>                
              </tr>
              <tr>
                <th>Position :</th>
                <td>
                  @foreach($walkin_data->walkin_position as $position)
                      <li>{{$position->designation->name or ''}}</li>
                  @endforeach
                </td>                
              </tr>
              <tr>               
                <th>Job Description :</th>
                <td>
                {!!$walkin_data->job_description!!}
                </td>
              </tr>            
            </tbody></table>
             
          </div>
     </div>

     <div class="row form-parentBlock">      
          <div class="block-title">
            <h4>Company Info</h4>
          </div>
          <div class="form-parentBlock-inner">  
          <table class="table table-bordered" cellspacing="0" width="100%">
              <tbody><tr>
                <th>Company Name :</th>
                <td>{{$walkin_data->comp_name}}</td>                
              </tr>
              <tr>
                <th>Company Industry :</th>
                <td>
                  {{$walkin_data->industry->name or ''}}
                </td>                
              </tr>
              <tr>               
                <th>Country :</th>
                <td>
                {{ $walkin_data->country->name or ''}}
                </td>
              </tr>  
              <tr>               
                <th>Company Logo :</th>
                <td><img class="comapny_logo" src="{{checkFile($walkin_data->comp_logo,'/uploads/employer_logo/','no_logo.png')}}"/></td>
                  <!-- {{ $walkin_data->comp_logo }} -->
                </td>
              </tr>            
              <tr>               
                <th>About Us :</th>
                <td>
                {!!$walkin_data->about_us!!}
                </td>
              </tr>  
            </tbody></table>               
          </div>
     </div>

     <div class="row form-parentBlock">      
          <div class="block-title">
            <h4>Interview Details</h4>
          </div>
          <div class="form-parentBlock-inner">
          <table class="table table-bordered" cellspacing="0" width="100%">
              <tbody><tr>
                <th>DateTime (From) :</th>
                <th>DateTime (To) :</th>            
                <th>Location :</th>           
                <th>Address :</th>
              </tr> 
              @foreach($walkin_data->walkin_interview as $data)              
                  <tr>
                    <td>{{$data->date_from or ''}}</td>
                    <td>{{$data->date_to or ''}}</td>
                    <td>{{$data->city->name or ''}}</td>
                    <td>{{$data->address or ''}}</td>

                  </tr>
              @endforeach
            </tbody></table>
     </div>
  </div>
  <div class="row form-parentBlock">      
          <div class="block-title">
            <h4>Applied Walkins Details</h4>
          </div>
          <div class="form-parentBlock-inner">
           <table id="datatbl" class="table table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>S.no.</th>                        
                        <th>Walkin </th>
                        <th>Apply By</th>
                        <th>Company</th>
                        <th>Company Email</th>
                        <th>Vacancy</th>
                        <th>Vacancy Ref No</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Email Subject</th>
                        <th>Messages</th>
                        <!-- <th>Action</th> -->
                    </tr>
                </thead>
                <tbody>
                    @foreach($applied_walkins as $k=>$val)
                        <tr>
                            <td>{{$k+1}}. </td>
                            <td>{{$val->walkin->title or ''}} </td>
                            <td>{{$val->user->name}} </td>
                            <td>{{$val->company}} </td>
                            <td>{{$val->company_email}} </td>
                            <td>{{$val->vacancy}} </td>
                            <td>{{$val->vacancy_ref_no}} </td>
                            <td>{{$val->name}} </td>
                            <td>{{$val->email}} </td>
                            <td>{{$val->email_subject}} </td>
                            <td>{{$val->messages}} </td><!-- 
                            <td>
                                <button class="btn btn-danger btn-sm btn-icon icon-left delete_btn" data-url="{{route('delete-applied-walkin',[$val->id])}}" title="Delete"><i class="fa fa-times"></i>Delete</button>
                            </td> -->
                        </tr>
                    @endforeach
                </tbody>
            </table>
          </div>
        </div>
@stop