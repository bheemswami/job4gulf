@extends('admin::layouts.master')
@section('content')
    <div class="row form-parentBlock">      
        <div class="block-title">
            <h4>All Applied Walkins</h4>
        </div>
        <div class="form-parentBlock-inner"> 
            <table id="datatbl" class="table table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>S.no.</th>                        
                        <th>Walkin </th>
                        <th>Apply By</th>
                        <th>Company</th>
                        <th>Company Email</th>
                        <th>Vacancy</th>
                        <th>Vacancy Ref No</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Email Subject</th>
                        <th>Messages</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($walkins as $k=>$val)
                        <tr>
                            <td>{{$k+1}}. </td>
                            <td>{{$val->walkin->title or ''}} </td>
                            <td>{{$val->user->name}} </td>
                            <td>{{$val->company}} </td>
                            <td>{{$val->company_email}} </td>
                            <td>{{$val->vacancy}} </td>
                            <td>{{$val->vacancy_ref_no}} </td>
                            <td>{{$val->name}} </td>
                            <td>{{$val->email}} </td>
                            <td>{{$val->email_subject}} </td>
                            <td>{{$val->messages}} </td>
                            <td>
                                <button class="btn btn-danger btn-sm btn-icon icon-left delete_btn" data-url="{{route('delete-applied-walkin',[$val->id])}}" title="Delete"><i class="fa fa-times"></i>Delete</button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop
