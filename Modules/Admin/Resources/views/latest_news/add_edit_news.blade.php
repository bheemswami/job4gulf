@extends('admin::layouts.master')
@section('content')
  @php 
      $flag=0;
      $heading='Add';
      if(isset($news_data) && !empty($news_data)){
          $flag=1;
          $heading='Update';
      }
  @endphp
      <div class="row form-parentBlock">      
          <div class="block-title">
            <h4>{{ $heading }} News</h4>
          </div>
          @if($flag==1)
                  {{ Form::model($news_data,array('url'=>route('save-news'),'class'=>'news-form','files' => true))}}
                      {{Form::hidden('id',null)}}
              @else
                  {{ Form::open(array('url'=>route('save-news'),'class'=>'news-form','files' => true))}}
              @endif
          <div class="form-parentBlock-inner"> 
              
              {{ csrf_field() }}
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                <label for="">News</label>
                {{Form::textarea('title',null,['class'=>"form-control",'placeholder'=>'Enter News'])}}
              </div>
       </div>
     </div>
        <div class="form-action">   
            <button type="submit" class="btn btn-primary submit-form">Submit</button>
          </div>
      {{Form::close()}}
@stop