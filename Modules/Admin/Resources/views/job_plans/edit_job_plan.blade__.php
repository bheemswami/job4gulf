@extends('admin::layouts.master')
@section('content')

    {{ Form::model($plan_data,array('url'=>route('save-jobplan'),'class'=>'plan-form','files' => true))}}  
    {{ Form::hidden('id',null)}}           
    {{ csrf_field() }}
      <div class="row form-parentBlock">      
          <div class="block-title">
            <h4>Plan Detail</h4>
          </div>
          <div class="form-parentBlock-inner">              
              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 form-group">
                <label for="">Plan Name</label>
                {{Form::text('name',null,['class'=>"form-control",'placeholder'=>'Enter Name'])}}
              </div>

              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 form-group">
                <label for="">Plan Price</label>
                {{Form::text('price',null,['class'=>"form-control",'placeholder'=>'Enter Price'])}}
              </div> 
             
              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 form-group">
                <label for="">Status</label>
                {{Form::select('status',config('constants.status'),null,['class'=>"form-control"])}}
              </div>

             
          </div>
     </div>

   
     <div class="row form-parentBlock">      
          <div class="block-title">
            <h4>Key Features</h4>
          </div>
          <div class="form-parentBlock-inner">
            <table class="table table-bordered features hide-elema" cellspacing="0" width="100%">
              <tr>
                <th>S.no.</th>  
                <th></th>     
                <th>Features</th>
                <th>Benefit(Unit)</th>
              </tr> 
              @php
                $ids=$units=[];
                if($plan_data->features!=null){
                  foreach($plan_data->features as $val){
                    $ids[]=$val->feature_id;
                    $units[$val->feature_id]=$val->units;
                  }
                }
                $arr = [1=>'One Email-id',5=>'Multiple Email-id'];
              @endphp
                @foreach(config('constants.jobplan_features') as $k=>$val)             
                  <tr>
                    <td>{{$k+1}}. </td>
                    <td>
                      @if(in_array($val['id'],$ids))
                        <span class="icon yes fa fa-check" style="color: green"></span>
                      @else
                        <span class="icon no" style="color: red">×</span>
                      @endif
                    </td>
                    <td>{!! $val['name'] !!}</td>
                    <td>
                      @if($val['allow_unit']==1)
                          @if($val['id']==7)
                            {{$arr[$units[$val['id']]]}}
                          @else
                            {{@$units[$val['id']]}}
                          @endif
                      @endif
                    </td>
                  </tr>
                @endforeach
            </table>
          </div>
     </div>
        <div class="form-action">   
            <button type="submit" class="btn btn-primary submit-form">Submit</button>
          </div>
      {{Form::close()}}
@stop