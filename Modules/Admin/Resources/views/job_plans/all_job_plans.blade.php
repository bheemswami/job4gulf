@extends('admin::layouts.master')
@section('content')
    <div class="row form-parentBlock">      
        <div class="block-title">
            <h4>All Job Plans</h4>
        </div>
        <div class="form-parentBlock-inner"> 
            <table id="datatbl" class="table table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>S.no.</th>                        
                        <th>Name</th>
                        <th>Price</th>
                        <th>Basic Jobs</th>
                        <th>Premium Jobs</th>
                        <th>Features</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($plan_list as $k=>$val)
                        <tr>
                            <td>{{$k+1}}. </td>
                            <td> {{$val->name}}</td>
                            <td> {{$val->price}}</td>
                            <td> {{$val->basic_jobs}}</td>
                            <td> {{$val->premium_jobs}}</td>
                            <td>
                                @if($val->features!=null)
                                 <table class="table table-bordered" cellspacing="0" width="100%">
                                    @foreach($val->features as $key=>$value)
                                       <tr><td><i class="fa fa-caret-right"></i>&nbsp;&nbsp;{{config('constants.jobplan_features')[$value->feature_id]['name']}}</td></tr>
                                    @endforeach
                                </table>
                                @endif
                            </td>
                            
                            <td> 
                                @if($val->status==1)
                                    <span class="label label-success"> Active</span> 
                                @else
                                    <span class="label label-default"> Inactive</span>
                                @endif
                            </td>
                            <td>
                                <a class="btn btn-default btn-sm btn-icon icon-left" href="{{route('update-jobplan',[$val->id])}}" title="Edit"><i class="fa fa-pencil"></i>Edit</a>
                                @if($val->is_deletable==1)
                                    <a class="btn btn-danger btn-sm btn-icon icon-left" href="{{route('delete-jobplan',[$val->id])}}"  title="Delete" onclick="return confirm('Are you sure?')"><i class="fa fa-times"></i>Delete</a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@stop
