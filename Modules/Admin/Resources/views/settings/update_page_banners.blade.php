@extends('admin::layouts.master')
@section('content')
    @foreach($banners as $k=>$val)
     <div class="row form-parentBlock">      
        <div class="block-title down-up" data-toggle="collapse" data-target="#banner{{$k}}">
          <h4><span class="fa-icon"><i class="fa fa-toggle-up"></i></span>&nbsp;&nbsp;&nbsp;{{$val->page_name}}</h4>
        </div>
        <div class="collapse" id="banner{{$k}}">
        <div class="form-parentBlock-inner">
          {{ Form::model($val,array('url'=>route('save-page-banner'),'class'=>'page-banner-form','files' => true))}}
          {{Form::hidden('id',null)}}
          {{ csrf_field() }}
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 form-group">
                <label for="">Title</label>
                {{Form::text('title',null,['class'=>"form-control",'placeholder'=>'Enter Title'])}}
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 form-group">
                <label for="">Description (Line 1)</label>
                {{Form::text('description',null,['class'=>"form-control",'placeholder'=>'Enter Description'])}}
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 form-group">
                <label for="">Description (Line 2)</label>
                {{Form::text('description_2',null,['class'=>"form-control",'placeholder'=>'Enter Description'])}}
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 form-group">
                <label for="">Banner Image</label>
                {{Form::file('banner_img',['class'=>"form-control",'id'=>'file-1'])}}
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                <img class="img-responsive" src="{{checkFile($val->image,'/uploads/banner_img/','default_banner.png')}}"/>    
            </div>
      
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">   
              <button type="submit" class="btn btn-primary submit-form">Submit</button>
            </div>
          {{Form::close()}}
        </div>
      </div>
     </div>
    @endforeach
  <script>
$(document).ready(function(){
  $(".block-title").on("click", function(){
    if($(this).hasClass('down-arrow')){
      $(this).removeClass('down-arrow').addClass('up-arrow');
      $(this).children().children('.fa-icon').html('<i class="fa fa-toggle-up"></i>');
    } else{
      $(this).removeClass('up-arrow').addClass('down-arrow');
      $(this).children().children('.fa-icon').html('<i class="fa fa-toggle-down"></i>');
    }
    
  });
  
});
</script>      
@stop