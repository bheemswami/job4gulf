<table class="table table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                      <th>Plan Name</th>
                      <th>Basic Jobs (Remaining/Total)</th>
                      <th>Premium Jobs (Remaining/Total)</th>
                      <th>Plan Validity(Days)</th>
                      <th>Start Date</th>
                      <th>End Date</th>
                      <th>Status</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse($purchase_plans as $k=>$plan_info)

                    <tr>
                      <td>{{($plan_info->job_plan!=null) ? $plan_info->job_plan->name : ''}}</td>
                      <td>{{$plan_info->remaining_basic_jobs}} / {{$plan_info->total_basic_jobs}}</td>
                      <td>{{$plan_info->remaining_premium_jobs}} / {{$plan_info->total_premium_jobs}}</td>
                      <td>{{$plan_info->plan_validity_days}}</td>
                      <td>{{dateConvert($plan_info->plan_start_date,'d M Y')}}</td>
                      <td>{{dateConvert($plan_info->plan_end_date,'d M Y')}}</td>
                      <td>{!! getPlanStatusLabel($plan_info->plan_start_date,$plan_info->plan_end_date,$plan_info->status,'is_admin') !!}</td>
                    </tr>
                    @empty
                    <tr>
                      <td colspan="7" class="bg-danger">No purchased plans</td>
                    </tr>
                    @endforelse
                    </tbody>
                  </table>