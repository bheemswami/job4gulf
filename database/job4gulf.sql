-- phpMyAdmin SQL Dump
-- version 4.7.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 28, 2018 at 05:04 AM
-- Server version: 5.7.18
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `job4gulf`
--

-- --------------------------------------------------------

--
-- Table structure for table `applied_jobs`
--

CREATE TABLE `applied_jobs` (
  `id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `apply_by` int(11) NOT NULL COMMENT 'user id',
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `applied_jobs`
--

INSERT INTO `applied_jobs` (`id`, `job_id`, `apply_by`, `status`, `created_at`) VALUES
(1, 12, 1, 1, '2017-11-14 06:10:03'),
(2, 11, 6, 1, '2017-11-14 06:14:51'),
(3, 1, 6, 1, '2017-11-15 05:34:36'),
(4, 9, 6, 1, '2017-11-16 08:26:34'),
(5, 11, 1, 1, '2017-11-16 08:30:55'),
(6, 10, 6, 1, '2017-11-16 08:31:14'),
(7, 2, 6, 1, '2017-11-16 08:31:25'),
(8, 12, 8, 1, '2017-11-16 08:32:45'),
(9, 2, 8, 1, '2017-11-16 08:33:34');

-- --------------------------------------------------------

--
-- Table structure for table `applied_walkins`
--

CREATE TABLE `applied_walkins` (
  `id` int(11) NOT NULL,
  `walkin_id` int(11) NOT NULL,
  `apply_by` int(11) NOT NULL COMMENT 'user id',
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `applied_walkins`
--

INSERT INTO `applied_walkins` (`id`, `walkin_id`, `apply_by`, `status`, `created_at`) VALUES
(1, 9, 1, 1, '2018-01-24 09:42:10'),
(2, 9, 1, 1, '2018-01-24 09:49:27'),
(3, 9, 1, 1, '2018-01-24 09:50:33'),
(4, 9, 1, 1, '2018-01-24 09:50:52'),
(5, 9, 1, 1, '2018-01-24 09:50:57'),
(6, 9, 1, 1, '2018-01-24 09:54:29'),
(7, 9, 1, 1, '2018-01-24 09:54:39'),
(8, 9, 1, 1, '2018-01-24 09:56:27'),
(9, 9, 1, 1, '2018-01-24 10:06:44');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `country_id` int(11) NOT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `filter_by` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `updated_at` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `name`, `country_id`, `icon`, `filter_by`, `status`, `updated_at`, `created_at`) VALUES
(1, 'Dubai', 1, 'bb.png', 0, 1, '2018-02-23 10:00:36', '2017-12-09 01:05:42'),
(2, 'Abu Dhabi', 1, NULL, 0, 1, '2017-12-09 06:35:56', '2017-12-09 01:05:56'),
(3, 'Sharjah', 1, NULL, 0, 1, '2017-12-09 06:36:06', '2017-12-09 01:06:06'),
(4, 'Al Ain', 1, NULL, 0, 1, '2017-12-09 06:36:18', '2017-12-09 01:06:18'),
(5, 'Ajman', 1, NULL, 0, 1, '2017-12-09 06:36:31', '2017-12-09 01:06:31'),
(6, 'Riyadh', 2, NULL, 0, 1, '2017-12-09 06:37:24', '2017-12-09 01:07:24'),
(7, 'Jeddah', 1, NULL, 0, 1, '2017-12-09 06:37:35', '2017-12-09 01:07:35'),
(8, 'Dammam', 2, NULL, 0, 1, '2017-12-09 06:37:51', '2017-12-09 01:07:51'),
(9, 'Doha', 3, NULL, 0, 1, '2017-12-09 06:38:42', '2017-12-09 01:08:42'),
(10, 'Al Rayyan', 3, NULL, 0, 1, '2017-12-09 06:39:04', '2017-12-09 01:09:04'),
(11, 'Dukhan', 3, NULL, 0, 1, '2017-12-09 06:39:20', '2017-12-09 01:09:20'),
(12, 'Al Jasrah', 3, NULL, 0, 1, '2017-12-09 06:39:42', '2017-12-09 01:09:42');

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `name`, `status`, `updated_at`, `created_at`) VALUES
(1, 'Walmart', 1, '2017-12-09 07:23:14', '2017-12-09 01:53:14'),
(2, 'State Grid', 1, '2017-12-09 07:23:22', '2017-12-09 01:53:22'),
(3, 'Sinopec Group', 1, '2017-12-09 07:23:30', '2017-12-09 01:53:30'),
(4, 'China National Petroleum', 1, '2017-12-09 07:23:38', '2017-12-09 01:53:38'),
(5, 'Sinopec Group 2', 1, '2017-12-09 07:23:30', '2017-12-09 01:53:30'),
(6, 'Sinopec Group 3', 1, '2017-12-09 07:23:30', '2017-12-09 01:53:30'),
(7, 'Sinopec Group 4', 1, '2017-12-09 07:23:30', '2017-12-09 01:53:30');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `sortname` varchar(10) NOT NULL,
  `name` varchar(150) NOT NULL,
  `phonecode` int(11) NOT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `filter_by` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `updated_at` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `sortname`, `name`, `phonecode`, `icon`, `filter_by`, `status`, `updated_at`, `created_at`) VALUES
(1, 'UA', 'UAE', 971, '297ca2662eef15c8ea1bb673796dd17a_1512800926.png', 0, 1, '2017-12-09 06:28:46', '2017-12-09 00:58:46'),
(2, 'SA', 'Saudi Arabia', 972, 'sa.png', 0, 1, '2018-02-23 09:42:03', '2017-12-09 01:01:32'),
(3, 'Q', 'Qatar', 973, 'qa.png', 0, 1, '2018-02-23 09:42:35', '2017-12-09 01:02:23'),
(4, 'K', 'Kuwait', 988, '1ce4c64118814c5872acf1c5aedfba9f_1513920424.png', 0, 1, '2017-12-22 05:27:04', '2017-12-21 23:57:04'),
(5, 'O', 'Oman', 910, '96daf702fd1dbe67c113350dea669b98_1513920485.png', 0, 1, '2017-12-22 05:28:05', '2017-12-21 23:58:05'),
(6, 'B', 'Bahrain', 989, '79f298823ae2a67554468d45f4796061_1513920513.png', 0, 1, '2017-12-22 05:28:33', '2017-12-21 23:58:33'),
(7, 'E', 'Egypt', 990, '009dbf97b2d84c17b5d8897368a80cf7_1513920536.png', 0, 1, '2017-12-22 05:28:56', '2017-12-21 23:58:56'),
(8, 'JO', 'Jordan', 991, '4eebf7dc95e7bb892e56d98f1bae2983_1513920570.png', 0, 1, '2017-12-22 05:29:31', '2017-12-21 23:59:31');

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` int(11) NOT NULL,
  `degree_id` int(11) DEFAULT NULL,
  `education_type` int(11) DEFAULT NULL COMMENT '1=Basic 2=Master',
  `name` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `degree_id`, `education_type`, `name`, `status`, `updated_at`, `created_at`) VALUES
(1, NULL, 1, 'Bachelor of Architecture', 1, '2017-12-29 08:20:58', '2017-12-29 02:50:58'),
(2, NULL, 1, 'Bachelor of Arts', 1, '2017-12-29 08:21:06', '2017-12-29 02:51:06'),
(3, NULL, 1, 'Bachelor of Business Administration', 1, '2017-12-29 08:21:13', '2017-12-29 02:51:13'),
(4, NULL, 1, 'Bachelor of Commerce', 1, '2017-12-29 08:21:21', '2017-12-29 02:51:21'),
(5, NULL, 1, 'Bachelor of Dental Surgery', 1, '2017-12-29 08:21:42', '2017-12-29 02:51:42'),
(6, NULL, 1, 'Bachelor of Education', 1, '2017-12-29 08:21:49', '2017-12-29 02:51:49'),
(7, NULL, 1, 'Bachelor of Hotel Management', 1, '2017-12-29 08:21:56', '2017-12-29 02:51:56'),
(8, NULL, 1, 'Bachelor of Laws (LLB)', 1, '2017-12-29 08:22:03', '2017-12-29 02:52:03'),
(9, NULL, 1, 'Bachelor of Pharmacy', 1, '2017-12-29 08:22:09', '2017-12-29 02:52:09'),
(10, NULL, 1, 'Bachelor of Science', 1, '2017-12-29 08:22:17', '2017-12-29 02:52:17'),
(11, NULL, 1, 'Bachelor of Technology/Engineering', 1, '2017-12-29 08:22:25', '2017-12-29 02:52:25'),
(12, NULL, 1, 'Bachelor of Veterinary Science', 1, '2017-12-29 08:22:32', '2017-12-29 02:52:32'),
(13, NULL, 1, 'Bachelors in Computer Application', 1, '2017-12-29 08:22:39', '2017-12-29 02:52:39'),
(14, NULL, 1, 'MBBS', 1, '2017-12-29 08:22:45', '2017-12-29 02:52:45'),
(15, NULL, 1, 'Diploma', 1, '2017-12-29 08:22:52', '2017-12-29 02:52:52'),
(16, NULL, 1, 'Intermediate School', 1, '2017-12-29 08:23:00', '2017-12-29 02:53:00'),
(17, NULL, 1, 'Secondary School', 1, '2017-12-29 08:23:10', '2017-12-29 02:53:10'),
(18, NULL, 2, 'Chartered Accountant', 1, '2017-12-29 08:24:32', '2017-12-29 02:54:32'),
(19, NULL, 2, 'CA Inter', 1, '2017-12-29 08:24:43', '2017-12-29 02:54:43'),
(20, NULL, 2, 'Chartered Financial Analyst', 1, '2017-12-29 08:24:50', '2017-12-29 02:54:50'),
(21, NULL, 2, 'Company Secretary', 1, '2017-12-29 08:24:58', '2017-12-29 02:54:58'),
(22, NULL, 2, 'Doctor of Medicine (MD)', 1, '2017-12-29 08:25:05', '2017-12-29 02:55:05'),
(23, NULL, 2, 'Doctor of Surgery (MS)', 1, '2017-12-29 08:25:13', '2017-12-29 02:55:13'),
(24, NULL, 2, 'Inst. of Cost &amp; Works Accountants', 1, '2017-12-29 08:25:20', '2017-12-29 02:55:20'),
(25, NULL, 2, 'ICWA Inter', 1, '2017-12-29 08:25:46', '2017-12-29 02:55:46'),
(26, NULL, 2, 'Master of Architecture', 1, '2017-12-29 08:25:52', '2017-12-29 02:55:52'),
(27, NULL, 2, 'Master of Arts', 1, '2017-12-29 08:25:59', '2017-12-29 02:55:59'),
(28, NULL, 2, 'Master of Commerce', 1, '2017-12-29 08:26:06', '2017-12-29 02:56:06'),
(29, NULL, 2, 'Master of Education', 1, '2017-12-29 08:26:14', '2017-12-29 02:56:14'),
(30, NULL, 2, 'Master of Laws (LLM)', 1, '2017-12-29 08:26:22', '2017-12-29 02:56:22'),
(31, NULL, 2, 'Master of Pharmacy', 1, '2017-12-29 08:26:28', '2017-12-29 02:56:28'),
(32, NULL, 2, 'Master of Science', 1, '2017-12-29 08:26:35', '2017-12-29 02:56:35'),
(33, NULL, 2, 'Master of Technology/Engineering', 1, '2017-12-29 08:26:42', '2017-12-29 02:56:42'),
(34, NULL, 2, 'Master of Veterinary Science', 1, '2017-12-29 08:26:50', '2017-12-29 02:56:50'),
(35, NULL, 2, 'Masters in Computer Application', 1, '2017-12-29 08:26:57', '2017-12-29 02:56:57'),
(36, NULL, 2, 'MBA/PG Diploma in Business Mgmt', 1, '2017-12-29 08:27:04', '2017-12-29 02:57:04');

-- --------------------------------------------------------

--
-- Table structure for table `degrees`
--

CREATE TABLE `degrees` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `degrees`
--

INSERT INTO `degrees` (`id`, `name`, `status`, `updated_at`, `created_at`) VALUES
(1, 'Doctorate', 1, '2017-12-09 06:48:53', '2017-12-09 01:18:53'),
(2, 'Masters', 1, '2017-12-09 06:49:00', '2017-12-09 01:19:00'),
(3, 'Basic(Bachelors/Diploma/School)', 1, '2017-12-09 06:50:30', '2017-12-09 01:20:30');

-- --------------------------------------------------------

--
-- Table structure for table `designations`
--

CREATE TABLE `designations` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `designations`
--

INSERT INTO `designations` (`id`, `name`, `status`, `updated_at`, `created_at`) VALUES
(1, 'Accountant', 1, '2017-12-09 07:17:30', '2017-12-09 01:47:30'),
(2, 'Administartor', 1, '2017-12-09 07:17:46', '2017-12-09 01:47:46'),
(3, 'Branch Manager', 1, '2017-12-09 07:18:10', '2017-12-09 01:48:10'),
(4, 'Business Analyst', 1, '2017-12-09 07:18:34', '2017-12-09 01:48:34'),
(5, 'Sales Executive', 1, '2017-12-09 07:18:54', '2017-12-09 01:48:54'),
(6, 'Supervisor', 1, '2017-12-09 07:19:01', '2017-12-09 01:49:01');

-- --------------------------------------------------------

--
-- Table structure for table `employers`
--

CREATE TABLE `employers` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `contact_person` varchar(100) DEFAULT NULL,
  `designation` varchar(100) DEFAULT NULL,
  `country_code` varchar(10) DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `comp_name` varchar(255) NOT NULL,
  `comp_address` text,
  `comp_website` varchar(255) DEFAULT NULL,
  `comp_country` varchar(5) DEFAULT NULL,
  `comp_city` varchar(25) DEFAULT NULL,
  `comp_type` int(11) DEFAULT NULL COMMENT '1=Consultants 2=Company',
  `comp_industry` varchar(100) DEFAULT NULL,
  `comp_profile` text,
  `comp_logo` varchar(255) DEFAULT NULL,
  `skills` varchar(500) DEFAULT NULL,
  `about` text,
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '1=Approved 0=Pending',
  `is_email` int(11) NOT NULL DEFAULT '0' COMMENT '1=Email Verified',
  `is_deleted` int(11) NOT NULL DEFAULT '0' COMMENT '1=Deleted',
  `updated_at` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employers`
--

INSERT INTO `employers` (`id`, `user_id`, `contact_person`, `designation`, `country_code`, `phone`, `comp_name`, `comp_address`, `comp_website`, `comp_country`, `comp_city`, `comp_type`, `comp_industry`, `comp_profile`, `comp_logo`, `skills`, `about`, `status`, `is_email`, `is_deleted`, `updated_at`, `created_at`) VALUES
(1, 7, 'bheem', 'SW DEV', '0', '9001456808', 'Micromax Informatics', 'RRB Bikaner', 'Commodity alert', '0', '0', 2, '0', 'ertyu', '', 'Struts, Sql, .Net, Asp, .Net, Mysql, Html, Dreamweaver, Flash', 'Headquartered in Walldorf, Germany, SAP is the world\'s largest business software company with more than 51,500 employees at sales and development locations in more than 50 countries worldwide.\r\n\r\nOur global development approach focuses on distributing development across the world in strategically important markets. A global network of SAP Labs spanning Bulgaria, Canada, China, Germany, Hungary, India, Israel, and the United States, enables SAP to operate locally, yet organize globally.\r\n\r\nAs the global technology research unit of SAP, SAP Research significantly contributes to SAP\'s product portfolio and extends SAP\'s leading position by identifying and shaping emerging IT trends through applied research and corporate venturing. SAP Research has highly skilled teams in 11 research centers worldwide.', 1, 0, 0, NULL, '2017-12-12 12:14:58'),
(2, 2, 'bheem', 'SW DEV', '1', '9001456808', 'Presales Digital Business Architect', 'RRB Bikaner', 'Commodity alert', '1', '1', 1, '5', 'ertyu', 'c65d62eccba91b692bd9278e12a6e535_1513081135.png', 'Struts, Sql, .Net, Asp, .Net, Mysql, Html, Dreamweaver, Flash', 'Headquartered in Walldorf, Germany, SAP is the world\'s largest business software company with more than 51,500 employees at sales and development locations in more than 50 countries worldwide.\r\n\r\nOur global development approach focuses on distributing development across the world in strategically important markets. A global network of SAP Labs spanning Bulgaria, Canada, China, Germany, Hungary, India, Israel, and the United States, enables SAP to operate locally, yet organize globally.\r\n\r\nAs the global technology research unit of SAP, SAP Research significantly contributes to SAP\'s product portfolio and extends SAP\'s leading position by identifying and shaping emerging IT trends through applied research and corporate venturing. SAP Research has highly skilled teams in 11 research centers worldwide.', 1, 0, 0, '2018-02-17 08:35:38', '2017-12-12 12:18:55'),
(3, 23, 'w', '3', '2', '9001456808', 'Sinopec Group', 'Bikaner\r\nrani bazar', 'Commodity alert', '3', '10', 1, '5', 'kljknk', 'c71644086dd4f76b63a1b8c468b2a375_1514198041.png', 'Struts, Sql, .Net, Asp, .Net, Mysql, Html, Dreamweaver, Flash', 'Headquartered in Walldorf, Germany, SAP is the world\'s largest business software company with more than 51,500 employees at sales and development locations in more than 50 countries worldwide.\r\n\r\nOur global development approach focuses on distributing development across the world in strategically important markets. A global network of SAP Labs spanning Bulgaria, Canada, China, Germany, Hungary, India, Israel, and the United States, enables SAP to operate locally, yet organize globally.\r\n\r\nAs the global technology research unit of SAP, SAP Research significantly contributes to SAP\'s product portfolio and extends SAP\'s leading position by identifying and shaping emerging IT trends through applied research and corporate venturing. SAP Research has highly skilled teams in 11 research centers worldwide.', 0, 0, 0, '2018-02-17 08:33:23', '2017-12-25 10:34:01'),
(4, 1, 'Yogesh swami', '2', '1', '9001456808', 'Indian Oil Corporation', 'Bikaner\r\nrani bazar', 'Commodity alert', '3', '6', 2, '5', 'qqqq', 'fb5c81ed3a220004b71069645f112867_1514360621.png', 'Struts, Sql, .Net, Asp, .Net, Mysql, Html, Dreamweaver, Flash', 'Headquartered in Walldorf, Germany, SAP is the world\'s largest business software company with more than 51,500 employees at sales and development locations in more than 50 countries worldwide.\r\n\r\nOur global development approach focuses on distributing development across the world in strategically important markets. A global network of SAP Labs spanning Bulgaria, Canada, China, Germany, Hungary, India, Israel, and the United States, enables SAP to operate locally, yet organize globally.\r\n\r\nAs the global technology research unit of SAP, SAP Research significantly contributes to SAP\'s product portfolio and extends SAP\'s leading position by identifying and shaping emerging IT trends through applied research and corporate venturing. SAP Research has highly skilled teams in 11 research centers worldwide.', 1, 0, 0, NULL, '2017-12-27 07:43:41'),
(5, 39, 'Yogesh swami', '2', '1', '9001456808', '	Reliance Industries', 'Bikaner\r\nrani bazar', 'Commodity alert', '3', NULL, 1, '5', 'qqqq', 'fb5c81ed3a220004b71069645f112867_1514363880.png', 'Struts, Sql, .Net, Asp, .Net, Mysql, Html, Dreamweaver, Flash', 'Headquartered in Walldorf, Germany, SAP is the world\'s largest business software company with more than 51,500 employees at sales and development locations in more than 50 countries worldwide.\r\n\r\nOur global development approach focuses on distributing development across the world in strategically important markets. A global network of SAP Labs spanning Bulgaria, Canada, China, Germany, Hungary, India, Israel, and the United States, enables SAP to operate locally, yet organize globally.\r\n\r\nAs the global technology research unit of SAP, SAP Research significantly contributes to SAP\'s product portfolio and extends SAP\'s leading position by identifying and shaping emerging IT trends through applied research and corporate venturing. SAP Research has highly skilled teams in 11 research centers worldwide.', 0, 0, 0, '2018-02-17 08:33:13', '2017-12-27 08:38:00'),
(6, 3, 'bheem', '2', '1', '9001456808', 'State Grid', 'RRB Bikaner', 'aaaa', '2', '6', 1, '3', 'JG ghgkjyy ut yutyuthgu ytyut', NULL, 'Struts, Sql, .Net, Asp, .Net, Mysql, Html, Dreamweaver, Flash', 'Headquartered in Walldorf, Germany, SAP is the world\'s largest business software company with more than 51,500 employees at sales and development locations in more than 50 countries worldwide.\r\n\r\nOur global development approach focuses on distributing development across the world in strategically important markets. A global network of SAP Labs spanning Bulgaria, Canada, China, Germany, Hungary, India, Israel, and the United States, enables SAP to operate locally, yet organize globally.\r\n\r\nAs the global technology research unit of SAP, SAP Research significantly contributes to SAP\'s product portfolio and extends SAP\'s leading position by identifying and shaping emerging IT trends through applied research and corporate venturing. SAP Research has highly skilled teams in 11 research centers worldwide.', 0, 0, 0, '2018-02-17 08:33:23', '2018-01-09 05:04:36'),
(7, 5, 'bheem', '3', '1', '9001456808', 'State Bank of India', 'RRB Bikaner', 'aaaa', '1', '3', 1, '4', 'JG ghgkjyy ut yutyuthgu ytyut', NULL, 'Struts, Sql, .Net, Asp, .Net, Mysql, Html, Dreamweaver, Flash', 'Headquartered in Walldorf, Germany, SAP is the world\'s largest business software company with more than 51,500 employees at sales and development locations in more than 50 countries worldwide.\r\n\r\nOur global development approach focuses on distributing development across the world in strategically important markets. A global network of SAP Labs spanning Bulgaria, Canada, China, Germany, Hungary, India, Israel, and the United States, enables SAP to operate locally, yet organize globally.\r\n\r\nAs the global technology research unit of SAP, SAP Research significantly contributes to SAP\'s product portfolio and extends SAP\'s leading position by identifying and shaping emerging IT trends through applied research and corporate venturing. SAP Research has highly skilled teams in 11 research centers worldwide.', 1, 0, 0, NULL, '2018-01-09 05:07:30'),
(8, 5, 'bheem', '3', '1', '9001456808', 'Amrutanjan Healthcare', 'RRB Bikaner', 'aaaa', '1', '3', 1, '4', 'JG ghgkjyy ut yutyuthgu ytyut', NULL, 'Struts, Sql, .Net, Asp, .Net, Mysql, Html, Dreamweaver, Flash', 'Headquartered in Walldorf, Germany, SAP is the world\'s largest business software company with more than 51,500 employees at sales and development locations in more than 50 countries worldwide.\r\n\r\nOur global development approach focuses on distributing development across the world in strategically important markets. A global network of SAP Labs spanning Bulgaria, Canada, China, Germany, Hungary, India, Israel, and the United States, enables SAP to operate locally, yet organize globally.\r\n\r\nAs the global technology research unit of SAP, SAP Research significantly contributes to SAP\'s product portfolio and extends SAP\'s leading position by identifying and shaping emerging IT trends through applied research and corporate venturing. SAP Research has highly skilled teams in 11 research centers worldwide.', 1, 0, 0, NULL, '2018-01-09 05:07:30'),
(9, 5, 'bheem', '3', '1', '9001456808', 'Ballarpur Industries Limited', 'RRB Bikaner', 'aaaa', '1', '3', 1, '4', 'JG ghgkjyy ut yutyuthgu ytyut', NULL, 'Struts, Sql, .Net, Asp, .Net, Mysql, Html, Dreamweaver, Flash', 'Headquartered in Walldorf, Germany, SAP is the world\'s largest business software company with more than 51,500 employees at sales and development locations in more than 50 countries worldwide.\r\n\r\nOur global development approach focuses on distributing development across the world in strategically important markets. A global network of SAP Labs spanning Bulgaria, Canada, China, Germany, Hungary, India, Israel, and the United States, enables SAP to operate locally, yet organize globally.\r\n\r\nAs the global technology research unit of SAP, SAP Research significantly contributes to SAP\'s product portfolio and extends SAP\'s leading position by identifying and shaping emerging IT trends through applied research and corporate venturing. SAP Research has highly skilled teams in 11 research centers worldwide.', 1, 0, 0, NULL, '2018-01-09 05:07:30'),
(10, 39, 'Yogesh swami', '2', '1', '9001456808', 'Bharat Aluminium Company', 'Bikaner\r\nrani bazar', 'Commodity alert', '3', NULL, 1, '5', 'qqqq', 'fb5c81ed3a220004b71069645f112867_1514363880.png', 'Struts, Sql, .Net, Asp, .Net, Mysql, Html, Dreamweaver, Flash', 'Headquartered in Walldorf, Germany, SAP is the world\'s largest business software company with more than 51,500 employees at sales and development locations in more than 50 countries worldwide.\r\n\r\nOur global development approach focuses on distributing development across the world in strategically important markets. A global network of SAP Labs spanning Bulgaria, Canada, China, Germany, Hungary, India, Israel, and the United States, enables SAP to operate locally, yet organize globally.\r\n\r\nAs the global technology research unit of SAP, SAP Research significantly contributes to SAP\'s product portfolio and extends SAP\'s leading position by identifying and shaping emerging IT trends through applied research and corporate venturing. SAP Research has highly skilled teams in 11 research centers worldwide.', 1, 0, 0, NULL, '2017-12-27 08:38:00'),
(11, 39, 'Yogesh swami', '2', '1', '9001456808', 'Damodar Valley Corporation', 'Bikaner\r\nrani bazar', 'Commodity alert', '3', NULL, 1, '5', 'qqqq', 'fb5c81ed3a220004b71069645f112867_1514363880.png', 'Struts, Sql, .Net, Asp, .Net, Mysql, Html, Dreamweaver, Flash', 'Headquartered in Walldorf, Germany, SAP is the world\'s largest business software company with more than 51,500 employees at sales and development locations in more than 50 countries worldwide.\r\n\r\nOur global development approach focuses on distributing development across the world in strategically important markets. A global network of SAP Labs spanning Bulgaria, Canada, China, Germany, Hungary, India, Israel, and the United States, enables SAP to operate locally, yet organize globally.\r\n\r\nAs the global technology research unit of SAP, SAP Research significantly contributes to SAP\'s product portfolio and extends SAP\'s leading position by identifying and shaping emerging IT trends through applied research and corporate venturing. SAP Research has highly skilled teams in 11 research centers worldwide.', 1, 0, 0, NULL, '2017-12-27 08:38:00'),
(12, 5, 'bheem', '3', '1', '9001456808', 'Emcure Pharmaceuticals', 'RRB Bikaner', 'aaaa', '1', '3', 1, '4', 'JG ghgkjyy ut yutyuthgu ytyut', NULL, 'Struts, Sql, .Net, Asp, .Net, Mysql, Html, Dreamweaver, Flash', 'Headquartered in Walldorf, Germany, SAP is the world\'s largest business software company with more than 51,500 employees at sales and development locations in more than 50 countries worldwide.\r\n\r\nOur global development approach focuses on distributing development across the world in strategically important markets. A global network of SAP Labs spanning Bulgaria, Canada, China, Germany, Hungary, India, Israel, and the United States, enables SAP to operate locally, yet organize globally.\r\n\r\nAs the global technology research unit of SAP, SAP Research significantly contributes to SAP\'s product portfolio and extends SAP\'s leading position by identifying and shaping emerging IT trends through applied research and corporate venturing. SAP Research has highly skilled teams in 11 research centers worldwide.', 1, 0, 0, NULL, '2018-01-09 05:07:30'),
(13, 5, 'bheem', '3', '1', '9001456808', 'Bharat Electronics Limited', 'RRB Bikaner', 'aaaa', '1', '3', 1, '4', 'JG ghgkjyy ut yutyuthgu ytyut', NULL, 'Struts, Sql, .Net, Asp, .Net, Mysql, Html, Dreamweaver, Flash', 'Headquartered in Walldorf, Germany, SAP is the world\'s largest business software company with more than 51,500 employees at sales and development locations in more than 50 countries worldwide.\r\n\r\nOur global development approach focuses on distributing development across the world in strategically important markets. A global network of SAP Labs spanning Bulgaria, Canada, China, Germany, Hungary, India, Israel, and the United States, enables SAP to operate locally, yet organize globally.\r\n\r\nAs the global technology research unit of SAP, SAP Research significantly contributes to SAP\'s product portfolio and extends SAP\'s leading position by identifying and shaping emerging IT trends through applied research and corporate venturing. SAP Research has highly skilled teams in 11 research centers worldwide.', 1, 0, 0, NULL, '2018-01-09 05:07:30'),
(14, 5, 'bheem', '3', '1', '9001456808', 'Financial Technologies Group', 'RRB Bikaner', 'aaaa', '1', '3', 1, '4', 'JG ghgkjyy ut yutyuthgu ytyut', NULL, 'Struts, Sql, .Net, Asp, .Net, Mysql, Html, Dreamweaver, Flash', 'Headquartered in Walldorf, Germany, SAP is the world\'s largest business software company with more than 51,500 employees at sales and development locations in more than 50 countries worldwide.\r\n\r\nOur global development approach focuses on distributing development across the world in strategically important markets. A global network of SAP Labs spanning Bulgaria, Canada, China, Germany, Hungary, India, Israel, and the United States, enables SAP to operate locally, yet organize globally.\r\n\r\nAs the global technology research unit of SAP, SAP Research significantly contributes to SAP\'s product portfolio and extends SAP\'s leading position by identifying and shaping emerging IT trends through applied research and corporate venturing. SAP Research has highly skilled teams in 11 research centers worldwide.', 1, 0, 0, NULL, '2018-01-09 05:07:30'),
(15, 5, 'bheem', '3', '1', '9001456808', 'China National Petroleum', 'RRB Bikaner', 'aaaa', '1', '3', 1, '4', 'JG ghgkjyy ut yutyuthgu ytyut', NULL, 'Struts, Sql, .Net, Asp, .Net, Mysql, Html, Dreamweaver, Flash', 'Headquartered in Walldorf, Germany, SAP is the world\'s largest business software company with more than 51,500 employees at sales and development locations in more than 50 countries worldwide.\r\n\r\nOur global development approach focuses on distributing development across the world in strategically important markets. A global network of SAP Labs spanning Bulgaria, Canada, China, Germany, Hungary, India, Israel, and the United States, enables SAP to operate locally, yet organize globally.\r\n\r\nAs the global technology research unit of SAP, SAP Research significantly contributes to SAP\'s product portfolio and extends SAP\'s leading position by identifying and shaping emerging IT trends through applied research and corporate venturing. SAP Research has highly skilled teams in 11 research centers worldwide.', 1, 0, 0, NULL, '2018-01-09 05:07:30'),
(16, 5, 'bheem', '3', '1', '9001456808', 'Glenmark Pharmaceuticals', 'RRB Bikaner', 'aaaa', '1', '3', 1, '4', 'JG ghgkjyy ut yutyuthgu ytyut', NULL, 'Struts, Sql, .Net, Asp, .Net, Mysql, Html, Dreamweaver, Flash', 'Headquartered in Walldorf, Germany, SAP is the world\'s largest business software company with more than 51,500 employees at sales and development locations in more than 50 countries worldwide.\r\n\r\nOur global development approach focuses on distributing development across the world in strategically important markets. A global network of SAP Labs spanning Bulgaria, Canada, China, Germany, Hungary, India, Israel, and the United States, enables SAP to operate locally, yet organize globally.\r\n\r\nAs the global technology research unit of SAP, SAP Research significantly contributes to SAP\'s product portfolio and extends SAP\'s leading position by identifying and shaping emerging IT trends through applied research and corporate venturing. SAP Research has highly skilled teams in 11 research centers worldwide.', 1, 0, 0, NULL, '2018-01-09 05:07:30'),
(17, 5, 'bheem', '3', '1', '9001456808', 'Gujarat Mineral Development Corporation', 'RRB Bikaner', 'aaaa', '1', '3', 1, '4', 'JG ghgkjyy ut yutyuthgu ytyut', NULL, 'Struts, Sql, .Net, Asp, .Net, Mysql, Html, Dreamweaver, Flash', 'Headquartered in Walldorf, Germany, SAP is the world\'s largest business software company with more than 51,500 employees at sales and development locations in more than 50 countries worldwide.\r\n\r\nOur global development approach focuses on distributing development across the world in strategically important markets. A global network of SAP Labs spanning Bulgaria, Canada, China, Germany, Hungary, India, Israel, and the United States, enables SAP to operate locally, yet organize globally.\r\n\r\nAs the global technology research unit of SAP, SAP Research significantly contributes to SAP\'s product portfolio and extends SAP\'s leading position by identifying and shaping emerging IT trends through applied research and corporate venturing. SAP Research has highly skilled teams in 11 research centers worldwide.', 1, 0, 0, NULL, '2018-01-09 05:07:30'),
(18, 5, 'bheem', '3', '1', '9001456808', 'Indian Telephone Industries Limited', 'RRB Bikaner', 'aaaa', '1', '3', 1, '4', 'JG ghgkjyy ut yutyuthgu ytyut', NULL, 'Struts, Sql, .Net, Asp, .Net, Mysql, Html, Dreamweaver, Flash', 'Headquartered in Walldorf, Germany, SAP is the world\'s largest business software company with more than 51,500 employees at sales and development locations in more than 50 countries worldwide.\r\n\r\nOur global development approach focuses on distributing development across the world in strategically important markets. A global network of SAP Labs spanning Bulgaria, Canada, China, Germany, Hungary, India, Israel, and the United States, enables SAP to operate locally, yet organize globally.\r\n\r\nAs the global technology research unit of SAP, SAP Research significantly contributes to SAP\'s product portfolio and extends SAP\'s leading position by identifying and shaping emerging IT trends through applied research and corporate venturing. SAP Research has highly skilled teams in 11 research centers worldwide.', 1, 0, 0, NULL, '2018-01-09 05:07:30'),
(19, 5, 'bheem', '3', '1', '9001456808', 'Hindustan Construction Company', 'RRB Bikaner', 'aaaa', '1', '3', 1, '4', 'JG ghgkjyy ut yutyuthgu ytyut', NULL, 'Struts, Sql, .Net, Asp, .Net, Mysql, Html, Dreamweaver, Flash', 'Headquartered in Walldorf, Germany, SAP is the world\'s largest business software company with more than 51,500 employees at sales and development locations in more than 50 countries worldwide.\r\n\r\nOur global development approach focuses on distributing development across the world in strategically important markets. A global network of SAP Labs spanning Bulgaria, Canada, China, Germany, Hungary, India, Israel, and the United States, enables SAP to operate locally, yet organize globally.\r\n\r\nAs the global technology research unit of SAP, SAP Research significantly contributes to SAP\'s product portfolio and extends SAP\'s leading position by identifying and shaping emerging IT trends through applied research and corporate venturing. SAP Research has highly skilled teams in 11 research centers worldwide.', 1, 0, 0, NULL, '2018-01-09 05:07:30'),
(20, 5, 'bheem', '3', '1', '9001456808', 'China National Petroleum', 'RRB Bikaner', 'aaaa', '1', '3', 1, '4', 'JG ghgkjyy ut yutyuthgu ytyut', NULL, 'Struts, Sql, .Net, Asp, .Net, Mysql, Html, Dreamweaver, Flash', 'Headquartered in Walldorf, Germany, SAP is the world\'s largest business software company with more than 51,500 employees at sales and development locations in more than 50 countries worldwide.\r\n\r\nOur global development approach focuses on distributing development across the world in strategically important markets. A global network of SAP Labs spanning Bulgaria, Canada, China, Germany, Hungary, India, Israel, and the United States, enables SAP to operate locally, yet organize globally.\r\n\r\nAs the global technology research unit of SAP, SAP Research significantly contributes to SAP\'s product portfolio and extends SAP\'s leading position by identifying and shaping emerging IT trends through applied research and corporate venturing. SAP Research has highly skilled teams in 11 research centers worldwide.', 1, 0, 0, NULL, '2018-01-09 05:07:30'),
(21, 6, 'bheem', '2', '3', '9001456808', 'Walmart', 'RRB Bikaner', 'Commodity alert', '2', '8', 1, '2', 'JKHJKHKJHJKHKHKJHKJ KJHJKHK jhkjh jhjkhjk hjk h', NULL, 'Java,J2Ee,Spring ,Hibernate,Struts,Sql,Asp.Net,.Net,Vb.Net', 'hjh hjkh jkhjk hjk jk jhj jh hjk jhj jh jhj  jkh j jh jhjkh uiyui uyg uy abcdefghijklmnopqrstuvwxyz', 1, 0, 0, '2018-02-23 08:19:39', '2018-02-17 06:31:26');

-- --------------------------------------------------------

--
-- Table structure for table `enquiries`
--

CREATE TABLE `enquiries` (
  `id` int(11) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `email` varchar(80) NOT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `country_code` int(10) DEFAULT NULL,
  `query` text,
  `forward_query` int(11) NOT NULL DEFAULT '0' COMMENT '1=forward query more corporate',
  `are_you` varchar(80) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `enquiries`
--

INSERT INTO `enquiries` (`id`, `name`, `email`, `mobile`, `location`, `country_code`, `query`, `forward_query`, `are_you`, `updated_at`, `created_at`) VALUES
(1, 'bheem', 'bheemswami808@gmail.com', '9001456808', 'bikaner', 1, 'Personal Details:\r\nbheem\r\n\r\nEmail: role3@gmail.com\r\n\r\nSkype ID: Not Available\r\n\r\nAddress:\r\nRRB Bikaner\r\n\r\nContact Numbers:\r\nPhone: 9001456808', 1, '0', NULL, '2018-01-25 07:25:32'),
(2, 'bheem', 'bheemswami808@gmail.com', '9001456808', 'bikaner', 1, 'Please Share Your Details To Connect The Placement Conultant\r\nAre you :  Jobseeker  Employer/Corporate\r\nYour Email', 0, '0', NULL, '2018-01-25 08:48:13'),
(3, 'bheem', 'bheemswami808@gmail.com', '9001456808', 'bikaner', 1, 'Please Share Your Details To Connect The Placement Conultant\r\nAre you :  Jobseeker  Employer/Corporate\r\nYour Email', 0, '0', NULL, '2018-01-25 09:28:09'),
(4, 'bheem', 'bheemswami808@gmail.com', '9001456808', 'Bikaner', 1, 'Please Share Your Details To Connect The Placement Conultant\r\nAre you :  Jobseeker  Employer/Corporate\r\nYour Email', 1, '0', NULL, '2018-01-25 09:30:26'),
(5, 'bheem', 'bheemswami808@gmail.com', '9001456808', 'bikaner', 910, 'Remaining : 1000Characters\r\nInterested to forward my query to more corporate', 0, 'Jobseeker', NULL, '2018-01-25 09:48:32'),
(6, 'bheem', 'bheemswami808@gmail.com', '9001456808', 'Bikaner', 972, 'Remaining : 1000Characters\r\nInterested to forward my query to more corporate\r\nRemaining : 1000Characters\r\nInterested to forward my query to more corporate\r\nRemaining : 1000Characters\r\nInterested to forward my query to more corporate\r\nRemaining : 1000Characters\r\nInterested to forward my query to more corporate\r\n\r\nRemaining : 1000Characters\r\nInterested to forward my query to more corporate\r\nRemaining : 1000Characters\r\nInterested to forward my query to more corporateRemaining : 1000Characters\r\nInterested to forward my query to more corporateRemaining : 1000Characters\r\nInterested to forward my query to more corporateRemaining : 1000Characters\r\nInterested to forward my query to more corporate', 1, 'Employer/Corporate', NULL, '2018-01-25 09:50:00');

-- --------------------------------------------------------

--
-- Table structure for table `functional_areas`
--

CREATE TABLE `functional_areas` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `functional_areas`
--

INSERT INTO `functional_areas` (`id`, `name`, `status`, `updated_at`, `created_at`) VALUES
(1, 'Freshers/Trainee', 1, '2017-12-09 07:21:23', '2017-12-09 01:51:23'),
(2, 'Accounting/Auditing/Tax/Financial Services', 1, '2017-12-09 07:21:31', '2017-12-09 01:51:31'),
(3, 'IT- Software', 1, '2017-12-09 07:21:39', '2017-12-09 01:51:39'),
(4, 'Packaging /Delivery/ Logistics Operations', 1, '2017-12-09 07:21:47', '2017-12-09 01:51:47'),
(5, 'Photography/TV / films / Radio12', 1, '2017-12-09 07:21:55', '2017-12-09 01:51:55');

-- --------------------------------------------------------

--
-- Table structure for table `industries`
--

CREATE TABLE `industries` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `industries`
--

INSERT INTO `industries` (`id`, `name`, `icon`, `status`, `updated_at`, `created_at`) VALUES
(1, 'Fresh Graduate', NULL, 1, '2017-12-09 07:20:19', '2017-12-09 01:50:19'),
(2, 'Accounting ', NULL, 1, '2017-12-09 07:20:27', '2017-12-09 01:50:27'),
(3, 'Advertising', NULL, 1, '2017-12-09 07:20:35', '2017-12-09 01:50:35'),
(4, 'Agriculture / Dairy / Fishing', NULL, 1, '2017-12-09 07:20:43', '2017-12-09 01:50:43'),
(5, 'Architecture / Interior Design', NULL, 1, '2017-12-09 07:20:52', '2017-12-09 01:50:52'),
(6, 'Oil and gas', NULL, 1, '2017-12-09 07:21:03', '2017-12-09 01:51:03'),
(7, 'General merchandisers', NULL, 1, '2017-12-09 07:22:46', '2017-12-09 01:52:46'),
(8, 'Utilities', NULL, 1, '2017-12-09 07:22:54', '2017-12-09 01:52:54'),
(9, 'Petroleum refining', NULL, 1, '2017-12-09 07:23:03', '2017-12-09 01:53:03'),
(10, 'Event Management', NULL, 1, '2017-12-09 07:23:03', '2017-12-09 01:53:03');

-- --------------------------------------------------------

--
-- Table structure for table `institutes`
--

CREATE TABLE `institutes` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `institutes`
--

INSERT INTO `institutes` (`id`, `name`, `status`, `updated_at`, `created_at`) VALUES
(1, 'Abu Dhabi Polytechnic', 1, '2017-12-09 07:25:50', '2017-12-09 01:55:50'),
(2, 'Abu Dhabi Vocational Education & Training Institute (ADVETI)', 1, '2017-12-09 07:26:03', '2017-12-09 01:56:03'),
(3, 'Emirates College of Technology', 1, '2017-12-09 07:26:15', '2017-12-09 01:56:15'),
(4, 'Masdar Institute of Science and Technology', 1, '2017-12-09 07:26:32', '2017-12-09 01:56:32'),
(5, 'National Defense College of the United Arab Emirates', 1, '2017-12-09 07:26:41', '2017-12-09 01:56:41'),
(6, 'New York Institute of Technology', 1, '2017-12-09 07:26:49', '2017-12-09 01:56:49'),
(7, 'Palpa University', 1, '2017-12-09 07:26:57', '2017-12-09 01:56:57'),
(8, 'Paris-Sorbonne University Abu Dhabi', 1, '2017-12-09 07:27:04', '2017-12-09 01:57:04'),
(9, 'Syscoms College, Abu Dhabi', 1, '2017-12-09 07:27:17', '2017-12-09 01:57:17'),
(10, 'Syscoms Institute', 1, '2017-12-09 07:27:26', '2017-12-09 01:57:26'),
(11, 'United Arab Emirates University', 1, '2017-12-09 07:27:40', '2017-12-09 01:57:40'),
(12, 'University of Strathclyde Business School - Abu Dhabi Campus', 1, '2017-12-09 07:27:49', '2017-12-09 01:57:49');

-- --------------------------------------------------------

--
-- Table structure for table `job_plans`
--

CREATE TABLE `job_plans` (
  `id` int(11) NOT NULL,
  `name` varchar(250) DEFAULT NULL,
  `price` float DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '1=Enable',
  `updated_at` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_plans`
--

INSERT INTO `job_plans` (`id`, `name`, `price`, `status`, `updated_at`, `created_at`) VALUES
(1, 'FREE JOB', 0, 1, '2018-02-17 16:11:57', '2018-02-17 09:12:14'),
(2, 'PREMIUM JOB', 29, 1, '2018-02-23 08:05:11', '2018-02-17 09:12:14'),
(3, 'PREMIUM+ JOB', 18, 1, '2018-02-17 15:43:36', '2018-02-17 09:12:14');

-- --------------------------------------------------------

--
-- Table structure for table `job_plan_features`
--

CREATE TABLE `job_plan_features` (
  `id` int(11) NOT NULL,
  `job_plan_id` int(11) NOT NULL,
  `feature_id` int(11) NOT NULL,
  `units` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_plan_features`
--

INSERT INTO `job_plan_features` (`id`, `job_plan_id`, `feature_id`, `units`, `updated_at`, `created_at`) VALUES
(51, 3, 1, 12, NULL, '2018-02-17 15:43:36'),
(52, 3, 2, NULL, NULL, '2018-02-17 15:43:36'),
(53, 3, 3, NULL, NULL, '2018-02-17 15:43:36'),
(54, 3, 4, NULL, NULL, '2018-02-17 15:43:36'),
(55, 3, 5, NULL, NULL, '2018-02-17 15:43:36'),
(56, 3, 6, 250, NULL, '2018-02-17 15:43:36'),
(57, 3, 7, NULL, NULL, '2018-02-17 15:43:36'),
(58, 3, 8, NULL, NULL, '2018-02-17 15:43:36'),
(59, 1, 1, 12, NULL, '2018-02-17 16:11:57'),
(60, 1, 3, NULL, NULL, '2018-02-17 16:11:57'),
(61, 1, 5, NULL, NULL, '2018-02-17 16:11:57'),
(68, 2, 2, NULL, NULL, '2018-02-23 08:05:11'),
(69, 2, 3, NULL, NULL, '2018-02-23 08:05:11'),
(70, 2, 4, NULL, NULL, '2018-02-23 08:05:11'),
(71, 2, 5, NULL, NULL, '2018-02-23 08:05:11'),
(72, 2, 6, 200, NULL, '2018-02-23 08:05:11');

-- --------------------------------------------------------

--
-- Table structure for table `logins`
--

CREATE TABLE `logins` (
  `id` int(11) NOT NULL,
  `name` varchar(80) NOT NULL,
  `email` varchar(80) NOT NULL,
  `password` varchar(80) DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `address` text,
  `avatar` text,
  `user_type` enum('Admin','Manager') NOT NULL DEFAULT 'Manager',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `updated_at` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logins`
--

INSERT INTO `logins` (`id`, `name`, `email`, `password`, `phone`, `address`, `avatar`, `user_type`, `status`, `updated_at`, `created_at`) VALUES
(1, 'Bheem Swami', 'bheemswami808@gmail.com', '$2y$10$dSHRgkDR2eLr1pn8bu8tzOrexraWXhiaff51QL6dlowcAPueGPxkC', '9950000111', NULL, NULL, 'Admin', 1, NULL, '2017-11-20 00:09:31'),
(3, 'bheem12', 'bheemswami809@gmail.com', '$2y$10$w6.vS5Fiy9kqvtCQ1/ff5u0V.QvQXns3l0AbTyWP4ajYRYERyp4nS', '9001456808', 'RRB Bikaner', 'sample_logo.png', 'Manager', 1, '2017-11-25 17:50:32', '2017-11-25 11:06:13');

-- --------------------------------------------------------

--
-- Table structure for table `nationalities`
--

CREATE TABLE `nationalities` (
  `id` int(11) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '1=Active',
  `updated_at` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nationalities`
--

INSERT INTO `nationalities` (`id`, `name`, `status`, `updated_at`, `created_at`) VALUES
(1, 'Afghan', 1, NULL, '2017-12-09 07:45:22'),
(2, 'Albanian', 1, NULL, '2017-12-09 07:45:22'),
(3, 'Algerian', 1, NULL, '2017-12-09 07:45:22'),
(4, 'American', 1, NULL, '2017-12-09 07:45:22'),
(5, 'Andorran', 1, NULL, '2017-12-09 07:45:22'),
(6, 'Angolan', 1, NULL, '2017-12-09 07:45:22'),
(7, 'Antiguans', 1, NULL, '2017-12-09 07:45:22'),
(8, 'Argentinean', 1, NULL, '2017-12-09 07:45:22'),
(9, 'Armenian', 1, NULL, '2017-12-09 07:45:22'),
(10, 'Australian', 1, NULL, '2017-12-09 07:45:22'),
(11, 'Austrian', 1, NULL, '2017-12-09 07:45:22'),
(12, 'Azerbaijani', 1, NULL, '2017-12-09 07:45:22'),
(13, 'Bahamian', 1, NULL, '2017-12-09 07:45:22'),
(14, 'Bahraini', 1, NULL, '2017-12-09 07:45:22'),
(15, 'Bangladeshi', 1, NULL, '2017-12-09 07:45:22'),
(16, 'Barbadian', 1, NULL, '2017-12-09 07:45:22'),
(17, 'Barbudans', 1, NULL, '2017-12-09 07:45:22'),
(18, 'Batswana', 1, NULL, '2017-12-09 07:45:22'),
(19, 'Belarusian', 1, NULL, '2017-12-09 07:45:22'),
(20, 'Belgian', 1, NULL, '2017-12-09 07:45:22'),
(21, 'Belizean', 1, NULL, '2017-12-09 07:45:22'),
(22, 'Beninese', 1, NULL, '2017-12-09 07:45:22'),
(23, 'Bhutanese', 1, NULL, '2017-12-09 07:45:22'),
(24, 'Bolivian', 1, NULL, '2017-12-09 07:45:22'),
(25, 'Bosnian', 1, NULL, '2017-12-09 07:45:22'),
(26, 'Brazilian', 1, NULL, '2017-12-09 07:45:22'),
(27, 'British', 1, NULL, '2017-12-09 07:45:22'),
(28, 'Bruneian', 1, NULL, '2017-12-09 07:45:22'),
(29, 'Bulgarian', 1, NULL, '2017-12-09 07:45:22'),
(30, 'Burkinabe', 1, NULL, '2017-12-09 07:45:22'),
(31, 'Burmese', 1, NULL, '2017-12-09 07:45:22'),
(32, 'Burundian', 1, NULL, '2017-12-09 07:45:22'),
(33, 'Cambodian', 1, NULL, '2017-12-09 07:45:22'),
(34, 'Cameroonian', 1, NULL, '2017-12-09 07:45:22'),
(35, 'Canadian', 1, NULL, '2017-12-09 07:45:22'),
(36, 'Cape Verdean', 1, NULL, '2017-12-09 07:45:22'),
(37, 'Central African', 1, NULL, '2017-12-09 07:45:22'),
(38, 'Chadian', 1, NULL, '2017-12-09 07:45:22'),
(39, 'Chilean', 1, NULL, '2017-12-09 07:45:22'),
(40, 'Chinese', 1, NULL, '2017-12-09 07:45:22'),
(41, 'Colombian', 1, NULL, '2017-12-09 07:45:22'),
(42, 'Comoran', 1, NULL, '2017-12-09 07:45:22'),
(43, 'Congolese', 1, NULL, '2017-12-09 07:45:22'),
(44, 'Costa Rican', 1, NULL, '2017-12-09 07:45:22'),
(45, 'Croatian', 1, NULL, '2017-12-09 07:45:22'),
(46, 'Cuban', 1, NULL, '2017-12-09 07:45:22'),
(47, 'Cypriot', 1, NULL, '2017-12-09 07:45:22'),
(48, 'Czech', 1, NULL, '2017-12-09 07:45:22'),
(49, 'Danish', 1, NULL, '2017-12-09 07:45:22'),
(50, 'Djibouti', 1, NULL, '2017-12-09 07:45:22'),
(51, 'Dominican', 1, NULL, '2017-12-09 07:45:22'),
(52, 'Dutch', 1, NULL, '2017-12-09 07:45:22'),
(53, 'East Timorese', 1, NULL, '2017-12-09 07:45:22'),
(54, 'Ecuadorean', 1, NULL, '2017-12-09 07:45:22'),
(55, 'Egyptian', 1, NULL, '2017-12-09 07:45:22'),
(56, 'Emirian', 1, NULL, '2017-12-09 07:45:22'),
(57, 'Equatorial Guinean', 1, NULL, '2017-12-09 07:45:22'),
(58, 'Eritrean', 1, NULL, '2017-12-09 07:45:22'),
(59, 'Estonian', 1, NULL, '2017-12-09 07:45:22'),
(60, 'Ethiopian', 1, NULL, '2017-12-09 07:45:22'),
(61, 'Fijian', 1, NULL, '2017-12-09 07:45:22'),
(62, 'Filipino', 1, NULL, '2017-12-09 07:45:22'),
(63, 'Finnish', 1, NULL, '2017-12-09 07:45:22'),
(64, 'French', 1, NULL, '2017-12-09 07:45:22'),
(65, 'Gabonese', 1, NULL, '2017-12-09 07:45:22'),
(66, 'Gambian', 1, NULL, '2017-12-09 07:45:22'),
(67, 'Georgian', 1, NULL, '2017-12-09 07:45:22'),
(68, 'German', 1, NULL, '2017-12-09 07:45:22'),
(69, 'Ghanaian', 1, NULL, '2017-12-09 07:45:22'),
(70, 'Greek', 1, NULL, '2017-12-09 07:45:22'),
(71, 'Grenadian', 1, NULL, '2017-12-09 07:45:22'),
(72, 'Guatemalan', 1, NULL, '2017-12-09 07:45:22'),
(73, 'Guinea-Bissauan', 1, NULL, '2017-12-09 07:45:22'),
(74, 'Guinean', 1, NULL, '2017-12-09 07:45:22'),
(75, 'Guyanese', 1, NULL, '2017-12-09 07:45:22'),
(76, 'Haitian', 1, NULL, '2017-12-09 07:45:22'),
(77, 'Herzegovinian', 1, NULL, '2017-12-09 07:45:22'),
(78, 'Honduran', 1, NULL, '2017-12-09 07:45:22'),
(79, 'Hungarian', 1, NULL, '2017-12-09 07:45:22'),
(80, 'I-Kiribati', 1, NULL, '2017-12-09 07:45:22'),
(81, 'Icelander', 1, NULL, '2017-12-09 07:45:22'),
(82, 'Indian', 1, NULL, '2017-12-09 07:45:22'),
(83, 'Indonesian', 1, NULL, '2017-12-09 07:45:22'),
(84, 'Iranian', 1, NULL, '2017-12-09 07:45:22'),
(85, 'Iraqi', 1, NULL, '2017-12-09 07:45:22'),
(86, 'Irish', 1, NULL, '2017-12-09 07:45:22'),
(87, 'Israeli', 1, NULL, '2017-12-09 07:45:22'),
(88, 'Italian', 1, NULL, '2017-12-09 07:45:22'),
(89, 'Ivorian', 1, NULL, '2017-12-09 07:45:22'),
(90, 'Jamaican', 1, NULL, '2017-12-09 07:45:22'),
(91, 'Japanese', 1, NULL, '2017-12-09 07:45:22'),
(92, 'Jordanian', 1, NULL, '2017-12-09 07:45:22'),
(93, 'Kazakhstani', 1, NULL, '2017-12-09 07:45:22'),
(94, 'Kenyan', 1, NULL, '2017-12-09 07:45:22'),
(95, 'Kittian and Nevisian', 1, NULL, '2017-12-09 07:45:22'),
(96, 'Kuwaiti', 1, NULL, '2017-12-09 07:45:22'),
(97, 'Kyrgyz', 1, NULL, '2017-12-09 07:45:22'),
(98, 'Laotian', 1, NULL, '2017-12-09 07:45:22'),
(99, 'Latvian', 1, NULL, '2017-12-09 07:45:22'),
(100, 'Lebanese', 1, NULL, '2017-12-09 07:45:22'),
(101, 'Liberian', 1, NULL, '2017-12-09 07:45:22'),
(102, 'Libyan', 1, NULL, '2017-12-09 07:45:22'),
(103, 'Liechtensteiner', 1, NULL, '2017-12-09 07:45:22'),
(104, 'Lithuanian', 1, NULL, '2017-12-09 07:45:22'),
(105, 'Luxembourger', 1, NULL, '2017-12-09 07:45:22'),
(106, 'Macedonian', 1, NULL, '2017-12-09 07:45:22'),
(107, 'Malagasy', 1, NULL, '2017-12-09 07:45:22'),
(108, 'Malawian', 1, NULL, '2017-12-09 07:45:22'),
(109, 'Malaysian', 1, NULL, '2017-12-09 07:45:22'),
(110, 'Maldivan', 1, NULL, '2017-12-09 07:45:22'),
(111, 'Malian', 1, NULL, '2017-12-09 07:45:22'),
(112, 'Maltese', 1, NULL, '2017-12-09 07:45:22'),
(113, 'Marshallese', 1, NULL, '2017-12-09 07:45:22'),
(114, 'Mauritanian', 1, NULL, '2017-12-09 07:45:22'),
(115, 'Mauritian', 1, NULL, '2017-12-09 07:45:22'),
(116, 'Mexican', 1, NULL, '2017-12-09 07:45:22'),
(117, 'Micronesian', 1, NULL, '2017-12-09 07:45:22'),
(118, 'Moldovan', 1, NULL, '2017-12-09 07:45:22'),
(119, 'Monacan', 1, NULL, '2017-12-09 07:45:22'),
(120, 'Mongolian', 1, NULL, '2017-12-09 07:45:22'),
(121, 'Moroccan', 1, NULL, '2017-12-09 07:45:22'),
(122, 'Mosotho', 1, NULL, '2017-12-09 07:45:22'),
(123, 'Motswana', 1, NULL, '2017-12-09 07:45:22'),
(124, 'Mozambican', 1, NULL, '2017-12-09 07:45:22'),
(125, 'Namibian', 1, NULL, '2017-12-09 07:45:22'),
(126, 'Nauruan', 1, NULL, '2017-12-09 07:45:22'),
(127, 'Nepalese', 1, NULL, '2017-12-09 07:45:22'),
(128, 'New Zealander', 1, NULL, '2017-12-09 07:45:22'),
(129, 'Nicaraguan', 1, NULL, '2017-12-09 07:45:22'),
(130, 'Nigerian', 1, NULL, '2017-12-09 07:45:22'),
(131, 'Nigerien', 1, NULL, '2017-12-09 07:45:22'),
(132, 'North Korean', 1, NULL, '2017-12-09 07:45:22'),
(133, 'Northern Irish', 1, NULL, '2017-12-09 07:45:22'),
(134, 'Norwegian', 1, NULL, '2017-12-09 07:45:22'),
(135, 'Omani', 1, NULL, '2017-12-09 07:45:22'),
(136, 'Pakistani', 1, NULL, '2017-12-09 07:45:22'),
(137, 'Palauan', 1, NULL, '2017-12-09 07:45:22'),
(138, 'Panamanian', 1, NULL, '2017-12-09 07:45:22'),
(139, 'Papua New Guinean', 1, NULL, '2017-12-09 07:45:22'),
(140, 'Paraguayan', 1, NULL, '2017-12-09 07:45:22'),
(141, 'Peruvian', 1, NULL, '2017-12-09 07:45:22'),
(142, 'Polish', 1, NULL, '2017-12-09 07:45:22'),
(143, 'Portuguese', 1, NULL, '2017-12-09 07:45:22'),
(144, 'Qatari', 1, NULL, '2017-12-09 07:45:22'),
(145, 'Romanian', 1, NULL, '2017-12-09 07:45:22'),
(146, 'Russian', 1, NULL, '2017-12-09 07:45:22'),
(147, 'Rwandan', 1, NULL, '2017-12-09 07:45:22'),
(148, 'Saint Lucian', 1, NULL, '2017-12-09 07:45:22'),
(149, 'Salvadoran', 1, NULL, '2017-12-09 07:45:22'),
(150, 'Samoan', 1, NULL, '2017-12-09 07:45:22'),
(151, 'San Marinese', 1, NULL, '2017-12-09 07:45:22'),
(152, 'Sao Tomean', 1, NULL, '2017-12-09 07:45:22'),
(153, 'Saudi', 1, NULL, '2017-12-09 07:45:22'),
(154, 'Scottish', 1, NULL, '2017-12-09 07:45:22'),
(155, 'Senegalese', 1, NULL, '2017-12-09 07:45:22'),
(156, 'Serbian', 1, NULL, '2017-12-09 07:45:22'),
(157, 'Seychellois', 1, NULL, '2017-12-09 07:45:22'),
(158, 'Sierra Leonean', 1, NULL, '2017-12-09 07:45:22'),
(159, 'Singaporean', 1, NULL, '2017-12-09 07:45:22'),
(160, 'Slovakian', 1, NULL, '2017-12-09 07:45:22'),
(161, 'Slovenian', 1, NULL, '2017-12-09 07:45:22'),
(162, 'Solomon Islander', 1, NULL, '2017-12-09 07:45:22'),
(163, 'Somali', 1, NULL, '2017-12-09 07:45:22'),
(164, 'South African', 1, NULL, '2017-12-09 07:45:22'),
(165, 'South Korean', 1, NULL, '2017-12-09 07:45:22'),
(166, 'Spanish', 1, NULL, '2017-12-09 07:45:22'),
(167, 'Sri Lankan', 1, NULL, '2017-12-09 07:45:22'),
(168, 'Sudanese', 1, NULL, '2017-12-09 07:45:22'),
(169, 'Surinamer', 1, NULL, '2017-12-09 07:45:22'),
(170, 'Swazi', 1, NULL, '2017-12-09 07:45:22'),
(171, 'Swedish', 1, NULL, '2017-12-09 07:45:22'),
(172, 'Swiss', 1, NULL, '2017-12-09 07:45:22'),
(173, 'Syrian', 1, NULL, '2017-12-09 07:45:22'),
(174, 'Taiwanese', 1, NULL, '2017-12-09 07:45:22'),
(175, 'Tajik', 1, NULL, '2017-12-09 07:45:22'),
(176, 'Tanzanian', 1, NULL, '2017-12-09 07:45:22'),
(177, 'Thai', 1, NULL, '2017-12-09 07:45:22'),
(178, 'Togolese', 1, NULL, '2017-12-09 07:45:22'),
(179, 'Tongan', 1, NULL, '2017-12-09 07:45:22'),
(180, 'Trinidadian/Tobagonian', 1, NULL, '2017-12-09 07:45:22'),
(181, 'Tunisian', 1, NULL, '2017-12-09 07:45:22'),
(182, 'Turkish', 1, NULL, '2017-12-09 07:45:22'),
(183, 'Tuvaluan', 1, NULL, '2017-12-09 07:45:22'),
(184, 'Ugandan', 1, NULL, '2017-12-09 07:45:22'),
(185, 'Ukrainian', 1, NULL, '2017-12-09 07:45:22'),
(186, 'Uruguayan', 1, NULL, '2017-12-09 07:45:22'),
(187, 'Uzbekistani', 1, NULL, '2017-12-09 07:45:22'),
(188, 'Venezuelan', 1, NULL, '2017-12-09 07:45:22'),
(189, 'Vietnamese', 1, NULL, '2017-12-09 07:45:22'),
(190, 'Welsh', 1, NULL, '2017-12-09 07:45:22'),
(191, 'Yemenite', 1, NULL, '2017-12-09 07:45:22'),
(192, 'Zambian', 1, NULL, '2017-12-09 07:45:22'),
(193, 'Zimbabwean', 1, NULL, '2017-12-09 07:45:22');

-- --------------------------------------------------------

--
-- Table structure for table `post_jobs`
--

CREATE TABLE `post_jobs` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `job_title` varchar(250) NOT NULL,
  `job_description` longtext,
  `total_vacancy` int(11) NOT NULL DEFAULT '0',
  `job_in_country` varchar(255) DEFAULT NULL,
  `key_skill` text NOT NULL,
  `industry_id` int(11) DEFAULT NULL,
  `functional_area_id` int(11) DEFAULT NULL,
  `min_experience` varchar(100) NOT NULL,
  `max_experience` varchar(100) NOT NULL,
  `salary_min` varchar(150) NOT NULL,
  `salary_max` varchar(150) NOT NULL,
  `nationality_id` int(11) DEFAULT NULL,
  `current_location` varchar(150) NOT NULL COMMENT 'current cuntry for candidate',
  `currency` enum('US$') NOT NULL DEFAULT 'US$',
  `basic_course_id` int(11) DEFAULT NULL,
  `basic_specialization_id` int(11) DEFAULT NULL,
  `master_course_id` int(11) DEFAULT NULL,
  `master_specialization_id` int(11) DEFAULT NULL,
  `gender` enum('Female','Male') DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `expiry_date` date DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `post_jobs`
--

INSERT INTO `post_jobs` (`id`, `user_id`, `job_title`, `job_description`, `total_vacancy`, `job_in_country`, `key_skill`, `industry_id`, `functional_area_id`, `min_experience`, `max_experience`, `salary_min`, `salary_max`, `nationality_id`, `current_location`, `currency`, `basic_course_id`, `basic_specialization_id`, `master_course_id`, `master_specialization_id`, `gender`, `status`, `expiry_date`, `updated_at`, `created_at`) VALUES
(1, 2, 'Guest Service Officer - Arabic Speaking', '<p>Assistant to CEO – International bank in DIFC, Dubai<br>\r\n<br>\r\nOn behalf of our client -an international bank, Swisslinx Middle East is seeking an experienced Assistant to join their office in DIFC, Dubai.<br>\r\n<br>\r\nIn this role, you will be responsible for assisting the CEO and the relationship managers with all administrational tasks and duties. You will be presentable and professional, and have an outstanding attention to detail and strong administrative skills.</p>', 23456, '3', 'Struts,Sql,.Net,Asp.Net,Mysql,Html,Dreamweaver,Flash,J2Ee,Spring', 3, 3, '16', '20', '2000', '4000', 144, 'UAE', 'US$', 1, 11, 18, 14, 'Male', 1, NULL, '2018-02-27 06:46:06', '2017-12-27 06:44:18'),
(2, 2, '	Marketing and Social Media Executive ', '<p>An exciting opportunity is currently available within one of the world\'s leading law firms. <br>\r\n<br>\r\nYou would be joining them as a Legal PA supporting a Partner plus team in the Banking division. <br>\r\n<br>\r\nThis is a challenging and rewarding role that will see you providing support on a wide range of duties including:<br>\r\n<br>\r\n* Extensive diary and travel management<br>\r\n* Assisting with business development duties<br>\r\n* Direct client management and client care<br>\r\n* Typing and amending documents<br>\r\n* Drafting correspondence<br>\r\n* Email/Inbox management<br>\r\n* Processing expense claims<br>\r\n* Maintaining internal client database</p>', 23456, '2', 'Java,J2Ee,Spring ,Hibernate,Struts,Sql,.Net,Asp.Net', 1, 2, '8', '23', '2000', '4000', 1, '3', 'US$', NULL, NULL, NULL, NULL, 'Male', 1, NULL, NULL, '2017-12-27 06:50:51'),
(3, 2, 'Web Support Executive ', '<p>Global client seeking an Native Arabic speaker based in Dubai with strong experience within digital marketing activities - product development, process optimization, campaign management, whilst leading establishment and management of all aspects of successful commercial partnerships with agencies and brands across the GCC.<br>\r\n<br>\r\nYou will develop and sustain strong partner relationships by providing relevant data insights and superior consulting services to clients. Also responsible for identifying partnership opportunities that can exist outside of the standard paid media environments and effectively maximize communication across owned and earned media platforms as well.</p>', 8768, '1', 'Asp.Net,J2Ee,Spring ,Hibernate', 3, 2, '11', '18', '4000', '7000', 82, 'UAE', 'US$', NULL, NULL, NULL, NULL, 'Male', 1, NULL, NULL, '2018-01-06 00:19:23'),
(4, 2, '	Workshop Manager ', '<p>An exciting opportunity is currently available within one of the world\'s leading law firms. <br>\r\n<br>\r\nYou would be joining them as a Legal PA supporting a Partner plus team in the Banking division. <br>\r\n<br>\r\nThis is a challenging and rewarding role that will see you providing support on a wide range of duties including:<br>\r\n<br>\r\n* Extensive diary and travel management<br>\r\n* Assisting with business development duties<br>\r\n* Direct client management and client care<br>\r\n* Typing and amending documents<br>\r\n* Drafting correspondence<br>\r\n* Email/Inbox management<br>\r\n* Processing expense claims<br>\r\n* Maintaining internal client database</p>', 8768, '1', 'Asp.Net,J2Ee,Spring ,Hibernate', 3, 2, '11', '18', '4000', '7000', 82, 'UAE', 'US$', NULL, NULL, NULL, NULL, 'Male', 1, NULL, NULL, '2018-01-06 00:19:23'),
(9, 2, 'Assistant to CEO - International Bank, DIFC ', '<p>Assistant to CEO – International bank in DIFC, Dubai<br>\r\n<br>\r\nOn behalf of our client -an international bank, Swisslinx Middle East is seeking an experienced Assistant to join their office in DIFC, Dubai.<br>\r\n<br>\r\nIn this role, you will be responsible for assisting the CEO and the relationship managers with all administrational tasks and duties. You will be presentable and professional, and have an outstanding attention to detail and strong administrative skills.</p>', 23456, '2', 'Java,J2Ee,Spring ,Hibernate,Struts,Sql,.Net,Asp.Net', 1, 2, '8', '23', '2000', '4000', 1, '3', 'US$', NULL, NULL, NULL, NULL, 'Male', 1, NULL, NULL, '2017-12-27 06:50:51'),
(10, 2, 'e-Commerce Partnership Marketing Manager ', '<p>Global client seeking an Native Arabic speaker based in Dubai with strong experience within digital marketing activities - product development, process optimization, campaign management, whilst leading establishment and management of all aspects of successful commercial partnerships with agencies and brands across the GCC.<br>\r\n<br>\r\nYou will develop and sustain strong partner relationships by providing relevant data insights and superior consulting services to clients. Also responsible for identifying partnership opportunities that can exist outside of the standard paid media environments and effectively maximize communication across owned and earned media platforms as well.</p>', 769, '1', 'Flash,Construction,Indesign,Dreamweaver,Mysql,Jquery', 5, 5, '6', '18', '500', '3000', 2, 'UAE', 'US$', NULL, NULL, NULL, NULL, 'Male', 1, NULL, NULL, '2018-01-16 07:44:40'),
(11, 2, 'Legal Secretary - International Law Firm ', '<p>An exciting opportunity is currently available within one of the world\'s leading law firms. <br>\r\n<br>\r\nYou would be joining them as a Legal PA supporting a Partner plus team in the Banking division. <br>\r\n<br>\r\nThis is a challenging and rewarding role that will see you providing support on a wide range of duties including:<br>\r\n<br>\r\n* Extensive diary and travel management<br>\r\n* Assisting with business development duties<br>\r\n* Direct client management and client care<br>\r\n* Typing and amending documents<br>\r\n* Drafting correspondence<br>\r\n* Email/Inbox management<br>\r\n* Processing expense claims<br>\r\n* Maintaining internal client database</p>', 769, '1', 'Flash,Construction,Indesign,Dreamweaver,Mysql,Jquery', 5, 5, '6', '18', '500', '3000', 2, 'UAE', 'US$', NULL, NULL, NULL, NULL, 'Male', 1, NULL, NULL, '2018-01-16 08:21:54'),
(12, 2, 'PHP', 'HJHJH JHJ jh jh jhjk', 77, '3', 'Asp.Net,J2Ee,Spring ,Hibernate,Struts,Sql', 2, 2, '12', '27', '500', '2000', 6, 'Qatar', 'US$', 1, 11, 18, 14, 'Male', 1, '2018-02-20', NULL, '2018-02-19 09:13:19');

-- --------------------------------------------------------

--
-- Table structure for table `post_jobs_cities`
--

CREATE TABLE `post_jobs_cities` (
  `id` int(11) NOT NULL,
  `post_job_id` int(11) NOT NULL DEFAULT '0',
  `city_id` int(11) NOT NULL DEFAULT '0',
  `updated_at` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `post_jobs_cities`
--

INSERT INTO `post_jobs_cities` (`id`, `post_job_id`, `city_id`, `updated_at`, `created_at`) VALUES
(3, 2, 6, '0000-00-00 00:00:00', '2018-01-16 01:46:45'),
(4, 2, 8, '0000-00-00 00:00:00', '2018-01-16 01:46:55'),
(5, 9, 6, '0000-00-00 00:00:00', '2018-01-16 01:48:29'),
(6, 9, 8, '0000-00-00 00:00:00', '2018-01-16 01:48:29'),
(7, 4, 1, '0000-00-00 00:00:00', '2018-01-16 01:48:54'),
(8, 4, 2, '0000-00-00 00:00:00', '2018-01-16 01:48:54'),
(9, 4, 4, '0000-00-00 00:00:00', '2018-01-16 01:48:54'),
(10, 3, 1, '0000-00-00 00:00:00', '2018-01-16 01:48:54'),
(11, 3, 2, '0000-00-00 00:00:00', '2018-01-16 01:48:54'),
(12, 3, 4, '0000-00-00 00:00:00', '2018-01-16 01:48:54'),
(22, 10, 11, NULL, '2018-02-19 09:13:19'),
(23, 10, 12, NULL, '2018-02-19 09:13:19'),
(24, 1, 9, NULL, '2018-02-27 06:46:06'),
(25, 1, 10, NULL, '2018-02-27 06:46:06'),
(26, 1, 11, NULL, '2018-02-27 06:46:06');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `description` text,
  `show_btn` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=None 1=Enquire Btn  2=Job Plans Btn',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=Enable',
  `updated_at` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `title`, `icon`, `image`, `description`, `show_btn`, `status`, `updated_at`, `created_at`) VALUES
(1, 'Advertise a Job', '8699c27af0bfc6a14afc0414aea04acb_1518777196.png', '768cd605bc77a80d4cdc7b3fd552f803_1518777196.png', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type', 2, 1, '2018-02-23 08:03:31', '2018-02-16 06:58:37'),
(2, 'Top Employer Zone', '894ddc014d7d997224556f4dba917d21_1518777251.png', '768cd605bc77a80d4cdc7b3fd552f803_1518777251.png', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type.', 1, 1, '2018-02-16 10:34:11', '2018-02-16 06:58:37'),
(3, 'Best Consultant Zone', 'cd8a28786b571bc881ecd108c1ec40ee_1518777306.png', '768cd605bc77a80d4cdc7b3fd552f803_1518777306.png', 'Best Consultants Zone is a powerful recruitment/ branding solution by TimesJobs. These services ensure high visibility to candidates, with traffic directly from the home page. With this service you can ensure increased and instantaneous responses to your job listings.', 1, 1, '2018-02-16 10:35:06', '2018-02-16 06:58:37'),
(4, 'Walk-in Zone', 'b75c7306438fe72d1f39573da794c809_1518777346.png', '768cd605bc77a80d4cdc7b3fd552f803_1518777346.png', 'Walk-in Zone is a powerful recruitment/ branding solution by TimesJobs. With this service you can ensure that you get highly talented candidates flocking to you for interviews without you having to make a single call.', 1, 1, '2018-02-16 10:35:46', '2018-02-16 06:58:37');

-- --------------------------------------------------------

--
-- Table structure for table `service_features`
--

CREATE TABLE `service_features` (
  `id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `service_features`
--

INSERT INTO `service_features` (`id`, `service_id`, `title`, `updated_at`, `created_at`) VALUES
(24, 2, 'Speedy Hiring', NULL, '2018-02-16 10:34:11'),
(25, 2, 'Quality Hiring', NULL, '2018-02-16 10:34:11'),
(26, 2, 'High Quality of Response', NULL, '2018-02-16 10:34:11'),
(27, 2, 'Higher brand recall value.', NULL, '2018-02-16 10:34:11'),
(28, 3, 'Prominent display of your company logo in Best Consultants Zone.', NULL, '2018-02-16 10:35:07'),
(29, 3, 'Unlimited Basic Job Postings ensuring increased response to your listings.', NULL, '2018-02-16 10:35:07'),
(30, 3, 'High quality of response.', NULL, '2018-02-16 10:35:07'),
(31, 3, 'High quality of response and higher brand recall value.', NULL, '2018-02-16 10:35:07'),
(32, 4, 'Quality Hiring.', NULL, '2018-02-16 10:35:46'),
(33, 4, 'Speedy Hiring.', NULL, '2018-02-16 10:35:46'),
(38, 1, 'Quality Hiring', NULL, '2018-02-23 08:03:31'),
(39, 1, 'Speedy Hiring', NULL, '2018-02-23 08:03:31'),
(40, 1, 'High Quality of Response', NULL, '2018-02-23 08:03:31');

-- --------------------------------------------------------

--
-- Table structure for table `skill_keywords`
--

CREATE TABLE `skill_keywords` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `skill_keywords`
--

INSERT INTO `skill_keywords` (`id`, `name`, `status`, `updated_at`, `created_at`) VALUES
(6, 'Java', 1, NULL, '2017-12-19 09:51:56'),
(7, 'J2Ee', 1, NULL, '2017-12-19 09:51:56'),
(8, 'Spring ', 1, NULL, '2017-12-19 09:51:56'),
(9, 'Hibernate', 1, NULL, '2017-12-19 09:51:56'),
(10, 'Struts', 1, NULL, '2017-12-19 09:51:56'),
(11, 'Sql', 1, NULL, '2017-12-19 09:51:56'),
(12, '.Net', 1, NULL, '2017-12-19 09:51:56'),
(13, 'Asp.Net', 1, NULL, '2017-12-19 09:51:56'),
(14, 'Vb.Net', 1, NULL, '2017-12-19 09:51:56'),
(15, 'Mvc', 1, NULL, '2017-12-19 09:51:56'),
(16, 'Jquery', 1, NULL, '2017-12-19 09:51:56'),
(17, 'Css', 1, NULL, '2017-12-19 09:51:56'),
(18, 'Php', 1, NULL, '2017-12-19 09:51:56'),
(19, 'Mysql', 1, NULL, '2017-12-19 09:51:56'),
(20, 'Html', 1, NULL, '2017-12-19 09:51:56'),
(21, 'Dreamweaver', 1, NULL, '2017-12-19 09:51:56'),
(22, 'Flash', 1, NULL, '2017-12-19 09:51:56'),
(23, 'Illustrator', 1, NULL, '2017-12-19 09:51:56'),
(24, 'Indesign', 1, NULL, '2017-12-19 09:51:56'),
(25, 'Design', 1, NULL, '2017-12-19 09:51:56'),
(26, 'Construction', 1, NULL, '2017-12-19 09:51:56'),
(27, 'Project', 1, NULL, '2017-12-19 09:51:56'),
(28, 'Planning', 1, NULL, '2017-12-19 09:51:56');

-- --------------------------------------------------------

--
-- Table structure for table `specializations`
--

CREATE TABLE `specializations` (
  `id` int(11) NOT NULL,
  `course_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `specializations`
--

INSERT INTO `specializations` (`id`, `course_id`, `name`, `status`, `updated_at`, `created_at`) VALUES
(1, 11, 'Agriculture', 1, '2017-12-30 06:12:01', '2017-12-30 00:42:01'),
(2, 11, 'Automobile', 1, '2017-12-30 06:13:40', '2017-12-30 00:43:40'),
(3, 11, 'Aviation', 1, '2017-12-30 06:14:43', '2017-12-30 00:44:43'),
(4, 11, 'Bio-Chemistry', 1, '2017-12-30 06:14:51', '2017-12-30 00:44:51'),
(5, 11, 'Bio-Technology', 1, '2017-12-30 06:14:58', '2017-12-30 00:44:58'),
(6, 11, 'Biomedical', 1, '2017-12-30 06:15:04', '2017-12-30 00:45:04'),
(7, 11, 'Ceramics', 1, '2017-12-30 06:16:05', '2017-12-30 00:46:05'),
(8, 11, 'Chemical', 1, '2017-12-30 06:21:29', '2017-12-30 00:51:29'),
(9, 11, 'Civil', 1, '2017-12-30 06:21:35', '2017-12-30 00:51:35'),
(10, 11, 'Electrical', 1, '2017-12-30 06:21:49', '2017-12-30 00:51:49'),
(11, 1, 'Architecture', 1, '2017-12-30 06:22:51', '2017-12-30 00:52:51'),
(12, 2, 'Fine Art', 1, '2017-12-30 06:23:18', '2017-12-30 00:53:18'),
(13, 2, 'History', 1, '2017-12-30 06:23:31', '2017-12-30 00:53:31'),
(14, 18, 'Charted Accountant', 1, '2017-12-30 07:19:19', '2017-12-30 01:49:19');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `role` int(11) NOT NULL DEFAULT '2' COMMENT '2=Candidate 3=Employer (Role field only use type identify)',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dob` date DEFAULT NULL,
  `gender` enum('Male','Female') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `country_code` int(11) DEFAULT NULL,
  `nationality` int(11) DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_verified` int(11) NOT NULL DEFAULT '0',
  `email_verified` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role`, `name`, `email`, `password`, `dob`, `gender`, `mobile`, `city_id`, `country_id`, `country_code`, `nationality`, `avatar`, `mobile_verified`, `email_verified`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 2, 'Bheem', 'role2@gmail.com', '$2y$10$9HWjgVsIu8LSyDn9WVSYfOHqm1fR8lA/Bts2T9CSTGSQ90aigdkQC', '1991-12-10', NULL, '9001456808', NULL, NULL, NULL, NULL, 'banner-img.jpg', 0, 1, 1, 'ej1DdLrg5W6ww2SAI8d8KFa10PbyjgJJf8pGxUgrvIEjEIGQRahEdpqYcfMv', '2017-12-06 01:43:35', '2017-12-26 07:03:13'),
(2, 3, 'Bheem', 'role3@gmail.com', '$2y$10$9HWjgVsIu8LSyDn9WVSYfOHqm1fR8lA/Bts2T9CSTGSQ90aigdkQC', '1991-12-10', NULL, '9001456808', NULL, NULL, NULL, NULL, 'banner-img.jpg', 0, 1, 1, 'zE865zqlTFbOtiHAkUeA37lZAotUnPNb2YybtCfvQejWRpdCQw2owc1ojCE9', '2017-12-06 01:43:35', '2017-12-26 07:03:13'),
(3, 3, 'bheemswami808', 'bheemswami145@gmail.com', '$2y$10$CZuyVmyaUlZAmDP4Z05D6.gFn/4CUF0CzlPikiPn1yJh2VRPVghT.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 1, NULL, '2018-01-08 23:34:36', '2018-01-08 23:34:36'),
(5, 3, 'bheemqqqswami808', 'bheemqqqswami808@gmail.com', '$2y$10$vlIaSCy0ppe7nVEoGBxcCeti5fsGY12yLY81ATg9PbcErAuQOm5RG', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 1, NULL, '2018-01-08 23:37:30', '2018-01-08 23:37:30'),
(6, 3, 'bheemswami80890', 'bheemswami80890@gmail.com', '$2y$10$79Z6b.0ILV5D8oEuXC.TrOxze4mR.fmdT5BVuQl5OJH9JX2QMVkxy', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 1, NULL, '2018-02-17 01:01:25', '2018-02-17 01:01:25'),
(7, 2, 'bheem', 'role5@gmail.com', '$2y$10$rXsE1wrD3lRJqgsbhnBIIOgo/Q/cp9koVBD6oHQwTIpJPSGKjceKa', '1986-02-03', 'Male', '9001456808', 10, 3, 973, 3, '5ff4b8af9479658dfc9c99a8bab4b6e5_1519119396.jpeg', 0, 0, 1, 'GeBYsvFllxU0LEVR0ftdiw40EYCCbCEoaK3rzdSLZkg2vYdfWo2BFIbmjPVh', '2018-02-20 04:06:37', '2018-02-20 04:06:37'),
(8, 2, 'Bheem', 'bheemswami808@gmail.com', '$2y$10$mazfc6GoQD8Zg6NvzNqnw.SKJA9McpoHmZ/l2UKx9gOnT8BjjbMv2', '1996-02-10', 'Male', '8989734873', 10, 3, 973, 54, NULL, 0, 0, 1, 'ihzS5jjWQ3u2Vdc1FJla6bHiv73fxmvpt2Pa0UcR2V1bLgmDWMrYCqYTdA5q', '2018-02-21 01:05:31', '2018-02-21 03:10:53');

-- --------------------------------------------------------

--
-- Table structure for table `user_additional_info`
--

CREATE TABLE `user_additional_info` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `exp_level` tinyint(4) DEFAULT NULL COMMENT 'experience level [0=Fresher]',
  `exp_year` int(11) DEFAULT NULL,
  `exp_month` int(11) DEFAULT NULL,
  `current_company` varchar(255) DEFAULT NULL,
  `current_position` varchar(100) DEFAULT NULL,
  `current_industry` int(11) DEFAULT NULL,
  `functional_area` int(11) DEFAULT NULL,
  `key_skills` text NOT NULL,
  `salary` float DEFAULT NULL,
  `salary_in` varchar(25) DEFAULT NULL,
  `interested_in` varchar(80) DEFAULT NULL,
  `resume_headline` varchar(255) DEFAULT NULL,
  `resume` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `updated_at` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_additional_info`
--

INSERT INTO `user_additional_info` (`id`, `user_id`, `exp_level`, `exp_year`, `exp_month`, `current_company`, `current_position`, `current_industry`, `functional_area`, `key_skills`, `salary`, `salary_in`, `interested_in`, `resume_headline`, `resume`, `status`, `updated_at`, `created_at`) VALUES
(1, 7, 1, 2, 1, NULL, NULL, 2, 1, 'Java,J2Ee,Spring ,Hibernate,Struts,Sql,.Net', 7878, '3', '0', 'RESUME H', NULL, 1, NULL, '2018-02-20 09:36:37'),
(2, 8, 1, 5, 3, '3', 'Supervisor', 4, 5, 'Java,J2Ee,Spring ,Hibernate,Struts,Sql,Vb.Net,Mvc', 20000, '2', '', 'RESUME PHP', NULL, 1, '2018-02-21 07:23:42', '2018-02-21 06:35:31');

-- --------------------------------------------------------

--
-- Table structure for table `user_educations`
--

CREATE TABLE `user_educations` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `basic_course_id` int(11) DEFAULT NULL,
  `basic_specialization_id` int(11) DEFAULT NULL,
  `basic_comp_year` int(11) DEFAULT NULL,
  `master_course_id` int(11) DEFAULT NULL,
  `master_specialization_id` int(11) DEFAULT NULL,
  `master_comp_year` int(11) DEFAULT NULL,
  `institute` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `updated_at` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_educations`
--

INSERT INTO `user_educations` (`id`, `user_id`, `basic_course_id`, `basic_specialization_id`, `basic_comp_year`, `master_course_id`, `master_specialization_id`, `master_comp_year`, `institute`, `status`, `updated_at`, `created_at`) VALUES
(1, 7, 1, 11, 1977, 18, 14, 1975, 'Emirates College of Technology', 1, NULL, '2018-02-20 09:36:37'),
(2, 8, 1, 11, 1975, 18, 14, 1974, 'Abu Dhabi Vocational Education & Training Institute (ADVETI)', 1, '2018-02-21 08:40:53', '2018-02-21 06:35:31');

-- --------------------------------------------------------

--
-- Table structure for table `walkins`
--

CREATE TABLE `walkins` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `comp_name` varchar(255) NOT NULL,
  `comp_logo` varchar(255) DEFAULT NULL,
  `comp_industry` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `about_us` text,
  `job_description` text,
  `employer_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=company 2=consultant',
  `status` int(11) NOT NULL DEFAULT '1',
  `updated_at` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `walkins`
--

INSERT INTO `walkins` (`id`, `title`, `comp_name`, `comp_logo`, `comp_industry`, `country_id`, `about_us`, `job_description`, `employer_type`, `status`, `updated_at`, `created_at`) VALUES
(1, 'Wanted For Dubai - Engineer', 'JK Jaipur News Pvt. Ltd', 'c2f1211710357a423b83930ada6f232e_1517029666.png', 2, 7, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '<h5>Hireing for internation process</h5>\r\n                               <ul>\r\n                                 <li>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages</li>\r\n                                 <li>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages</li>\r\n                                 <li>It was popularised in the 1960s with the release of Letraset sheets </li>\r\n                                 <li>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages</li>\r\n                                 <li>It was popularised in the 1960s with the release of Letraset sheets </li>\r\n                                 \r\n                               </ul>\r\n                               \r\n                               <h5>Travel process</h5>\r\n                               <ul>\r\n                                 <li>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages</li>\r\n                                 <li>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages</li>\r\n                                 <li>It was popularised in the 1960s with the release of Letraset sheets </li>\r\n                                 <li>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages</li>\r\n                                 <li>It was popularised in the 1960s with the release of Letraset sheets </li>\r\n                               </ul>', 1, 1, NULL, '2018-01-27 05:07:46'),
(2, 'Wanted for Doha - Accountant', 'Reliance Industries', 'c2f1211710357a423b83930ada6f232e_1517029666.png', 1, 3, 'Hireing For Internation Process\r\nIt was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages\r\nIt was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages\r\nIt was popularised in the 1960s with the release of Letraset sheets\r\nIt was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages\r\nIt was popularised in the 1960s with the release of Letraset sheets\r\nTravel Process\r\nIt was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages\r\nIt was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages\r\nIt was popularised in the 1960s with the release of Letraset sheets\r\nIt was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages\r\nIt was popularised in the 1960s with the release of Letraset sheets', '<h5>Hireing for internation process</h5>\r\n                               <ul>\r\n                                 <li>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages</li>\r\n                                 <li>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages</li>\r\n                                 <li>It was popularised in the 1960s with the release of Letraset sheets </li>\r\n                                 <li>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages</li>\r\n                                 <li>It was popularised in the 1960s with the release of Letraset sheets </li>\r\n                                 \r\n                               </ul>\r\n                               \r\n                               <h5>Travel process</h5>\r\n                               <ul>\r\n                                 <li>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages</li>\r\n                                 <li>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages</li>\r\n                                 <li>It was popularised in the 1960s with the release of Letraset sheets </li>\r\n                                 <li>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages</li>\r\n                                 <li>It was popularised in the 1960s with the release of Letraset sheets </li>\r\n                               </ul>', 1, 1, NULL, '2018-01-27 05:11:01'),
(3, 'Wanted for Doha - Advertising', 'State Bank of India', 'c2f1211710357a423b83930ada6f232e_1517029666.png', 1, 3, 'Hireing For Internation Process\r\nIt was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages\r\nIt was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages\r\nIt was popularised in the 1960s with the release of Letraset sheets\r\nIt was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages\r\nIt was popularised in the 1960s with the release of Letraset sheets\r\nTravel Process\r\nIt was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages\r\nIt was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages\r\nIt was popularised in the 1960s with the release of Letraset sheets\r\nIt was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages\r\nIt was popularised in the 1960s with the release of Letraset sheets', 'Hireing For Internation Process\r\nIt was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages\r\nIt was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages\r\nIt was popularised in the 1960s with the release of Letraset sheets\r\nIt was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages\r\nIt was popularised in the 1960s with the release of Letraset sheets\r\nTravel Process\r\nIt was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages\r\nIt was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages\r\nIt was popularised in the 1960s with the release of Letraset sheets\r\nIt was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages\r\nIt was popularised in the 1960s with the release of Letraset sheets<h5>Hireing for internation process</h5>\r\n                               <ul>\r\n                                 <li>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages</li>\r\n                                 <li>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages</li>\r\n                                 <li>It was popularised in the 1960s with the release of Letraset sheets </li>\r\n                                 <li>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages</li>\r\n                                 <li>It was popularised in the 1960s with the release of Letraset sheets </li>\r\n                                 \r\n                               </ul>\r\n                               \r\n                               <h5>Travel process</h5>\r\n                               <ul>\r\n                                 <li>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages</li>\r\n                                 <li>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages</li>\r\n                                 <li>It was popularised in the 1960s with the release of Letraset sheets </li>\r\n                                 <li>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages</li>\r\n                                 <li>It was popularised in the 1960s with the release of Letraset sheets </li>\r\n                               </ul>', 1, 1, NULL, '2018-01-27 05:11:01'),
(4, 'Wanted for Doha - Advertising', 'Tata Motors', 'c2f1211710357a423b83930ada6f232e_1517029666.png', 8, 2, 'Hireing For Internation Process\r\nIt was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages\r\nIt was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages\r\nIt was popularised in the 1960s with the release of Letraset sheets\r\nIt was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages\r\nIt was popularised in the 1960s with the release of Letraset sheets\r\nTravel Process\r\nIt was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages\r\nIt was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages\r\nIt was popularised in the 1960s with the release of Letraset sheets\r\nIt was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages\r\nIt was popularised in the 1960s with the release of Letraset sheets', '<h5>Hireing for internation process</h5>\r\n                               <ul>\r\n                                 <li>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages</li>\r\n                                 <li>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages</li>\r\n                                 <li>It was popularised in the 1960s with the release of Letraset sheets </li>\r\n                                 <li>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages</li>\r\n                                 <li>It was popularised in the 1960s with the release of Letraset sheets </li>\r\n                                 \r\n                               </ul>\r\n                               \r\n                               <h5>Travel process</h5>\r\n                               <ul>\r\n                                 <li>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages</li>\r\n                                 <li>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages</li>\r\n                                 <li>It was popularised in the 1960s with the release of Letraset sheets </li>\r\n                                 <li>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages</li>\r\n                                 <li>It was popularised in the 1960s with the release of Letraset sheets </li>\r\n                               </ul>', 1, 1, NULL, '2018-01-27 05:11:01'),
(5, 'Wanted for Doha - Advertising', 'Indian Oil Corporation', 'c2f1211710357a423b83930ada6f232e_1517029666.png', 7, 5, 'Hireing For Internation Process\r\nIt was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages\r\nIt was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages\r\nIt was popularised in the 1960s with the release of Letraset sheets\r\nIt was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages\r\nIt was popularised in the 1960s with the release of Letraset sheets\r\nTravel Process\r\nIt was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages\r\nIt was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages\r\nIt was popularised in the 1960s with the release of Letraset sheets\r\nIt was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages\r\nIt was popularised in the 1960s with the release of Letraset sheets', '<h5>Hireing for internation process</h5>\r\n                               <ul>\r\n                                 <li>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages</li>\r\n                                 <li>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages</li>\r\n                                 <li>It was popularised in the 1960s with the release of Letraset sheets </li>\r\n                                 <li>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages</li>\r\n                                 <li>It was popularised in the 1960s with the release of Letraset sheets </li>\r\n                                 \r\n                               </ul>\r\n                               \r\n                               <h5>Travel process</h5>\r\n                               <ul>\r\n                                 <li>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages</li>\r\n                                 <li>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages</li>\r\n                                 <li>It was popularised in the 1960s with the release of Letraset sheets </li>\r\n                                 <li>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages</li>\r\n                                 <li>It was popularised in the 1960s with the release of Letraset sheets </li>\r\n                               </ul>', 1, 1, NULL, '2018-01-27 05:11:01'),
(6, 'Wanted for Doha - Advertising', 'JK Jaipur News Pvt. Ltd 4', 'c2f1211710357a423b83930ada6f232e_1517029666.png', 3, 8, 'Hireing For Internation Process\r\nIt was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages\r\nIt was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages\r\nIt was popularised in the 1960s with the release of Letraset sheets\r\nIt was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages\r\nIt was popularised in the 1960s with the release of Letraset sheets\r\nTravel Process\r\nIt was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages\r\nIt was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages\r\nIt was popularised in the 1960s with the release of Letraset sheets\r\nIt was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages\r\nIt was popularised in the 1960s with the release of Letraset sheets', '<h5>Hireing for internation process</h5>\r\n                               <ul>\r\n                                 <li>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages</li>\r\n                                 <li>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages</li>\r\n                                 <li>It was popularised in the 1960s with the release of Letraset sheets </li>\r\n                                 <li>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages</li>\r\n                                 <li>It was popularised in the 1960s with the release of Letraset sheets </li>\r\n                                 \r\n                               </ul>\r\n                               \r\n                               <h5>Travel process</h5>\r\n                               <ul>\r\n                                 <li>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages</li>\r\n                                 <li>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages</li>\r\n                                 <li>It was popularised in the 1960s with the release of Letraset sheets </li>\r\n                                 <li>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages</li>\r\n                                 <li>It was popularised in the 1960s with the release of Letraset sheets </li>\r\n                               </ul>', 1, 1, NULL, '2018-01-27 05:11:01'),
(7, 'Jobs for KNPC Kuwait - Urgent Hiring', 'Recruiters Zone India pvt Ltd.', '313d29de76ad061b4bf8ca308500724f_1517215813.jpg', 6, 4, 'We are one of the leading oil and gas consultancy in Gulf, dealing in almost all the sector.\r\n\r\nWe have big clientele all over gulf', '<pre>\r\n1) Mechanical Engineer\r\n- B.E Mechanical with 5 Years experience\r\n2) Software Engineer\r\n- B.E with 10 years experience\r\n\r\nInterest candidate please contact Mr. Ajay on : +91-9571952222  and send your CVs on eng.ajaysharma@gmail.com</pre>', 1, 1, NULL, '2018-01-29 08:50:13');

-- --------------------------------------------------------

--
-- Table structure for table `walkin_interview_details`
--

CREATE TABLE `walkin_interview_details` (
  `id` int(11) NOT NULL,
  `walkin_id` int(11) NOT NULL,
  `address` text,
  `city_id` int(11) NOT NULL,
  `date_form` datetime DEFAULT NULL,
  `date_to` datetime DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `updated_at` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `walkin_interview_details`
--

INSERT INTO `walkin_interview_details` (`id`, `walkin_id`, `address`, `city_id`, `date_form`, `date_to`, `status`, `updated_at`, `created_at`) VALUES
(1, 1, 'C-147, C-scheme, New Modern', 5, '2018-01-25 10:37:00', '2018-01-28 10:37:00', 1, NULL, '2018-01-27 05:07:46'),
(2, 1, 'C-147, C-scheme, New Modern', 10, '2018-01-26 10:37:00', '2018-01-27 10:37:00', 1, NULL, '2018-01-27 05:07:46'),
(3, 2, 'C-2147, C-scheme, New Modern', 9, '2018-01-22 10:40:00', '2018-01-30 10:40:00', 1, NULL, '2018-01-27 05:11:01'),
(4, 2, 'A-147, C-scheme, New Modern', 1, '2018-01-24 10:40:00', '2018-01-28 10:40:00', 1, NULL, '2018-01-27 05:11:01'),
(5, 3, 'A-147, C-scheme, New Modern', 7, '2018-01-24 10:40:00', '2018-01-28 10:40:00', 1, NULL, '2018-01-27 05:11:01'),
(6, 3, 'AA-147, C-scheme, New Modern', 6, '2018-01-24 10:40:00', '2018-01-28 10:40:00', 1, NULL, '2018-01-27 05:11:01'),
(7, 4, 'AA-167, C-scheme, New Modern', 8, '2018-01-24 10:40:00', '2018-01-28 10:40:00', 1, NULL, '2018-01-27 05:11:01'),
(8, 4, 'AA-167, C-scheme, New Modern', 7, '2018-01-24 10:40:00', '2018-01-28 10:40:00', 1, NULL, '2018-01-27 05:11:01'),
(9, 5, 'A5-167, C-scheme, New Modern', 7, '2018-01-24 10:40:00', '2018-01-28 10:40:00', 1, NULL, '2018-01-27 05:11:01'),
(10, 6, 'B1-167, C-scheme, New Modern', 6, '2018-01-24 10:40:00', '2018-01-28 10:40:00', 1, NULL, '2018-01-27 05:11:01'),
(11, 6, 'B2-167, C-scheme, New Modern', 3, '2018-01-24 10:40:00', '2018-01-28 10:40:00', 1, NULL, '2018-01-27 05:11:01'),
(12, 6, 'B22-167, C-scheme, New Modern', 5, '2018-01-24 10:40:00', '2018-01-28 10:40:00', 1, NULL, '2018-01-27 05:11:01'),
(13, 7, 'DBS Business Center\r\nPrescot road\r\nMumbai', 9, '2018-02-10 09:00:00', '2018-02-11 17:00:00', 1, NULL, '2018-01-29 08:50:13');

-- --------------------------------------------------------

--
-- Table structure for table `walkin_positions`
--

CREATE TABLE `walkin_positions` (
  `id` int(11) NOT NULL,
  `walkin_id` int(11) NOT NULL,
  `designation_id` int(11) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `walkin_positions`
--

INSERT INTO `walkin_positions` (`id`, `walkin_id`, `designation_id`, `updated_at`, `created_at`) VALUES
(1, 1, 1, NULL, '2018-01-27 05:07:46'),
(2, 1, 2, NULL, '2018-01-27 05:07:46'),
(3, 1, 3, NULL, '2018-01-27 05:07:46'),
(4, 1, 4, NULL, '2018-01-27 05:07:46'),
(5, 1, 5, NULL, '2018-01-27 05:07:46'),
(6, 1, 6, NULL, '2018-01-27 05:07:46'),
(7, 2, 1, NULL, '2018-01-27 05:11:01'),
(8, 2, 2, NULL, '2018-01-27 05:11:01'),
(9, 2, 3, NULL, '2018-01-27 05:11:01'),
(10, 2, 4, NULL, '2018-01-27 05:11:01'),
(11, 3, 1, NULL, '2018-01-27 05:07:46'),
(12, 3, 2, NULL, '2018-01-27 05:07:46'),
(13, 3, 3, NULL, '2018-01-27 05:07:46'),
(14, 3, 4, NULL, '2018-01-27 05:07:46'),
(15, 3, 5, NULL, '2018-01-27 05:07:46'),
(16, 3, 6, NULL, '2018-01-27 05:07:46'),
(17, 4, 1, NULL, '2018-01-27 05:07:46'),
(18, 4, 2, NULL, '2018-01-27 05:07:46'),
(19, 4, 3, NULL, '2018-01-27 05:07:46'),
(20, 4, 4, NULL, '2018-01-27 05:07:46'),
(21, 4, 5, NULL, '2018-01-27 05:07:46'),
(22, 4, 6, NULL, '2018-01-27 05:07:46'),
(23, 5, 6, NULL, '2018-01-27 05:07:46'),
(24, 5, 5, NULL, '2018-01-27 05:07:46'),
(25, 5, 4, NULL, '2018-01-27 05:07:46'),
(26, 5, 3, NULL, '2018-01-27 05:07:46'),
(27, 5, 2, NULL, '2018-01-27 05:07:46'),
(28, 5, 1, NULL, '2018-01-27 05:07:46'),
(29, 6, 3, NULL, '2018-01-27 05:07:46'),
(30, 6, 2, NULL, '2018-01-27 05:07:46'),
(31, 6, 1, NULL, '2018-01-27 05:07:46'),
(32, 6, 4, NULL, '2018-01-27 05:07:46'),
(33, 6, 6, NULL, '2018-01-27 05:07:46'),
(34, 6, 5, NULL, '2018-01-27 05:07:46'),
(35, 7, 1, NULL, '2018-01-29 08:50:13'),
(36, 7, 3, NULL, '2018-01-29 08:50:13'),
(37, 7, 4, NULL, '2018-01-29 08:50:13');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `applied_jobs`
--
ALTER TABLE `applied_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `applied_walkins`
--
ALTER TABLE `applied_walkins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `degrees`
--
ALTER TABLE `degrees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `designations`
--
ALTER TABLE `designations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employers`
--
ALTER TABLE `employers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `enquiries`
--
ALTER TABLE `enquiries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `functional_areas`
--
ALTER TABLE `functional_areas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `industries`
--
ALTER TABLE `industries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `institutes`
--
ALTER TABLE `institutes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_plans`
--
ALTER TABLE `job_plans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_plan_features`
--
ALTER TABLE `job_plan_features`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logins`
--
ALTER TABLE `logins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `nationalities`
--
ALTER TABLE `nationalities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_jobs`
--
ALTER TABLE `post_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_jobs_cities`
--
ALTER TABLE `post_jobs_cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_features`
--
ALTER TABLE `service_features`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `skill_keywords`
--
ALTER TABLE `skill_keywords`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `specializations`
--
ALTER TABLE `specializations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_additional_info`
--
ALTER TABLE `user_additional_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_educations`
--
ALTER TABLE `user_educations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `walkins`
--
ALTER TABLE `walkins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `walkin_interview_details`
--
ALTER TABLE `walkin_interview_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `walkin_positions`
--
ALTER TABLE `walkin_positions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `applied_jobs`
--
ALTER TABLE `applied_jobs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `applied_walkins`
--
ALTER TABLE `applied_walkins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `degrees`
--
ALTER TABLE `degrees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `designations`
--
ALTER TABLE `designations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `employers`
--
ALTER TABLE `employers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `enquiries`
--
ALTER TABLE `enquiries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `functional_areas`
--
ALTER TABLE `functional_areas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `industries`
--
ALTER TABLE `industries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `institutes`
--
ALTER TABLE `institutes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `job_plans`
--
ALTER TABLE `job_plans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `job_plan_features`
--
ALTER TABLE `job_plan_features`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `logins`
--
ALTER TABLE `logins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `nationalities`
--
ALTER TABLE `nationalities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=194;

--
-- AUTO_INCREMENT for table `post_jobs`
--
ALTER TABLE `post_jobs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `post_jobs_cities`
--
ALTER TABLE `post_jobs_cities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `service_features`
--
ALTER TABLE `service_features`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `skill_keywords`
--
ALTER TABLE `skill_keywords`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `specializations`
--
ALTER TABLE `specializations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `user_additional_info`
--
ALTER TABLE `user_additional_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_educations`
--
ALTER TABLE `user_educations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `walkins`
--
ALTER TABLE `walkins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `walkin_interview_details`
--
ALTER TABLE `walkin_interview_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `walkin_positions`
--
ALTER TABLE `walkin_positions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
