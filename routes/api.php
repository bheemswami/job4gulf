<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('filter-employer', 'AjaxController@getFilterdEmployer');
Route::get('nationality-suggestion', 'AjaxController@getNationalityList');
Route::get('state-suggestion', 'AjaxController@getStateList');
Route::get('get-gulf-city-list', 'AjaxController@getGulfCityList');

Route::get('city-suggestion', 'AjaxController@getCityList');
Route::post('state-by-country', 'AjaxController@getStateListByCountry');
Route::post('specialization-by-course', 'AjaxController@getSpecializationList');
Route::get('specialization-suggestion', 'AjaxController@getSpecializationList');
Route::post('specialization-list', 'AjaxController@getSpecializationList');

Route::get('company-suggestion', 'AjaxController@getCoumpanyList');
Route::get('institute-suggestion', 'AjaxController@getInstituteList');
Route::get('func-area-suggestion', 'AjaxController@getFunctionalAreaList');
Route::get('industry-suggestion', 'AjaxController@getIndustryList');
Route::get('designation-suggestion', 'AjaxController@getDesignationList');
Route::get('course-suggestion', 'AjaxController@getCourseList');
Route::get('skill-keyword-suggestion', 'AjaxController@getSkillKeywordList');

Route::post('check-email-exist', 'AjaxController@checkEmailExist');

//use in admin section
Route::post('get-plan-details-by-id', 'AjaxController@getPlanDetailsById');
Route::post('generate-payment-url', 'PaypalController@generatePaymentUrl');
Route::post('send-unpaid-invoice', 'PaypalController@sendUnpaidInvoice');
